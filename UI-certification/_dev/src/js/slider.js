/* global jQuery */
(function ($) {

    var $slider = $('[slider]'),
        $btn_left = $('[slider-left]', $slider),
        $btn_right = $('[slider-right]', $slider),
        $band = $('[slider-band]', $slider),
        enabled = true,
        band_width = 0,
        moving = false;

    function move(direction) {
        console.log(direction);
    }

    function enable() {
        var ww = window.innerWidth;
        var bw = $band.outerWidth();

        if(band_width <= ww) {
            enabled = false;
            $band.addClass('centered');
        } else {
            enabled = true;
            $band.removeClass('centered');
        }
        $band.width(band_width);

        console.log('enabled', enabled);
        if(enabled) {
            $btn_left.show();
            $btn_right.show();
        } else {
            $btn_left.hide();
            $btn_right.hide();
        }
    }
    function init() {
        var $items = $('li', $band),
            $first = $items.first(),
            $last = $items.last();

            $('li', $band).each(function(i,e){
                band_width += $(e).outerWidth();
            });
        enable();
        var $items = $('li', $band);
            //$first = $items.first(),
            //$last = $items.last();

        console.log('items', $items);
        console.log('first', $first);



        console.log('count', band_width);

    }



    $btn_right.on('click', function(e){
        e.preventDefault();
        if(moving) return;

        moving = true;
        var $item = $('li', $band).first();
        var offset = $item.outerWidth();
        $band.append($item.clone());
        $band.animate({left: '-='+offset}, 1000, 'linear', function(){
            $band.css({left: 0});
            $item.remove();

            moving = false;
        })
    });

    $btn_left.on('click', function(e){
        e.preventDefault();
        if(moving) return;

        moving = true;
        var $item = $('li', $band).last();
        var offset = $item.outerWidth();
        $band.prepend($item.clone());
        $band.css({left: -offset});
        $band.animate({left: 0}, 1000, 'linear', function(){
            $item.remove();
            moving = false;
        })
    });


    $band.imagesLoaded(function(){
        init();
        $(window).on('resize', function(e){
            enable();
        });
    })


}(jQuery));
