/* global jQuery */
(function($) {
    $('[main-nav-trigger]').on('click', function(e){
        e.preventDefault();
        $('body').toggleClass('main-nav-expanded');
    })
})(jQuery);
