var gulp = require('gulp'),
	gutil = require('gulp-util'),
	livereload = require('gulp-livereload'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	cssnano = require('cssnano');

var path = {
	bower: 'bower_components/',
	public: '../public/',
	assets: '../public/assets/'
};

gulp.task('js', function () {
	return gulp.src([
			path.bower + 'jquery/dist/jquery.js',
			path.bower + 'ev-emitter/ev-emitter.js',
			path.bower + 'imagesloaded/imagesloaded.js',
			'src/js/**/*.js'
		])
		.pipe(concat('site.js'))
		//.pipe(uglify())
		.pipe(gulp.dest(path.assets))
		.pipe(livereload());
});

gulp.task('html', function () {
	return gulp.src(path.public + '**/*.html')
		.pipe(livereload());
});

gulp.task('sass', function () {

	var plugins = [
		autoprefixer({browsers: ['last 2 versions'], grid: false}),
		cssnano()
	];

	return gulp.src('src/site.scss')
		.pipe(sass({
			includePaths: [
				path.bower + 'normalize-scss/sass/',
                 path.bower + 'font-awesome/scss'
             ]
		}).on('error', sass.logError))
		.pipe(postcss(plugins))
		.pipe(gulp.dest(path.assets))
		.pipe(livereload());

});

gulp.task('copy', function(){
	return gulp.src(path.bower + 'font-awesome/fonts/**.*') 
		.pipe(gulp.dest(path.assets +'fonts')); 
});


gulp.task('watch', function() {
	livereload.listen();
	gulp.watch(['src/site.scss', 'src/scss/**/*.scss'], ['sass']);
	gulp.watch('src/js/**/*.js', ['js']);
	gulp.watch('../public/**/*.html', ['html']);
});
