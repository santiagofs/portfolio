<style>

</style>

<div class="wrap">
    <h1>Toptal Social Settings</h1>
    <form method="post" action="options.php">
    <?php
        error_reporting(E_ALL);
		ini_set( 'display_errors', 1 );
		ini_set( 'error_log', WP_CONTENT_DIR . '/debug.log' );

    // This prints out all hidden setting fields
    settings_fields( 'toptal_social_option_group' );
    //do_settings_sections( 'toptal-social-admin' );
    ?>
    <table class="form-table">
        <tr>
            <th scope="row">Display On:</th>
            <td>
            <?php
                $arr = get_post_types( ['public'   => true], 'objects', 'and' );
                $post_types = [];
                foreach($arr as $key => $item) {
                    if($key == 'attachment') continue;
                    //$post_types[] = isset($item->labels) && isset($item->labels->name) ? $item->labels->name : $key;
                    $post_types[] = $key;
                    //var_dump($item);
                }
                $display_on = explode(',', (isset($options['display_on']) ? $options['display_on'] : '' ));
            ?>
                <ul class="display-on">
            <?php
                foreach($post_types as $post_type):
                    $checked = in_array($post_type, $display_on) ? 'checked="checked"' : '';
            ?>

                    <li><label><input type="checkbox" name="toptal_social[display_on][]" value="<?php echo $post_type ?>" <?php echo $checked ?> /><?php echo $post_type ?></label></li>
            <?php
                endforeach;
            ?>
                </ul>
            </td>
        </tr>
        <tr>
            <th scope="row">Where to display:</th>
            <td>
            <?php
                $positions = ['top' => 'Below the post title', 'left' => 'Floating on the left area', 'bottom'=>'After the post content', 'image'=>'Inside the featured image'];
                $display_at = explode(',', (isset($options['display_at']) ? $options['display_at'] : '' ));

            ?>
                <ul class="display-on">
            <?php
                foreach($positions as $key=>$position):
                    $checked = in_array($key, $display_at) ? 'checked="checked"' : '';
            ?>

                    <li><label><input type="checkbox" name="toptal_social[display_at][]" value="<?php echo $key ?>" <?php echo $checked ?> /><?php echo $position ?></label></li>
            <?php
                endforeach;
            ?>
                </ul>
            </td>
        </tr>
        <tr>
            <th scope="row">Select networks & colors:</th>
            <td class="select-networks">
                <ul class="" id="select-networks">
            <?php
                $selected_networks = isset($options['networks']) ? explode(',',$options['networks']) : [];
                $sort =isset($options['sort']) ? explode(',',$options['sort']) : [];
                count($sort) || $sort = array_keys($networks);

                foreach($sort as $key):
                    $network = $networks[$key];
                    $checked = in_array($key, $selected_networks) ? 'checked="checked"' : '';
                    $label = $network['label'];
                    $color = $network['color'];
                    $current_color = (isset($options['colors']) && isset($options['colors'][$key])) ? $options['colors'][$key] : $network['color'];
                    $back = $network['back'];
                    $current_back = (isset($options['backs']) && isset($options['backs'][$key])) ? $options['backs'][$key] : $network['back'];

                    $faclass = $network['class'];
            ?>
                    <li>
                        <input type="hidden" name="toptal_social[sort][]" value="<?php echo $key ?>" />
                        <label><input type="checkbox" name="toptal_social[networks][]" value="<?php echo $key ?>" <?php echo $checked ?> /><?php echo $label ?></label>
                        #<input type="text" name="toptal_social[colors][<?php echo $key ?>]" color-input="<?php echo $key ?>" value="<?php echo $current_color ?>" />
                        <!-- #<input type="text" name="toptal_social[backs][<?php echo $key ?>]" back-input="<?php echo $key ?>" value="<?php echo $current_back ?>" /> -->
                        <div class="color-display fa <?php echo $faclass ?>" style="background-color:#FFFFFF; color:#<?php echo $current_color ?>" color-display="<?php echo $key ?>"></div>
                        <a color-reset="<?php echo $key ?>" color-original="<?php echo $color ?>"  class="color-reset">use default</a>
                        <div class="grip"></div>
                    </li>
            <?php
                endforeach;
            ?>
                </ul>
                <p class="italic">You can drag & drop from the right grip to change the networks order</p>
            </td>
        </tr>
        <tr>
            <th scope="row">Size:</th>
            <td class="radios">
                <?php
                    $size = isset($options['size']) ? $options['size'] : 'medium';
                ?>
                <label><input type="radio" name="toptal_social[size]" value="small" <?php echo $size=='small' ? 'checked="checked"' : '' ?> />Small</label>
                <label><input type="radio" name="toptal_social[size]" value="medium" <?php echo $size=='medium' ? 'checked="checked"' : '' ?> />Medium</label>
                <label><input type="radio" name="toptal_social[size]" value="large" <?php echo $size=='large' ? 'checked="checked"' : '' ?> />Large</label>
            </td>
        </tr>
    </table>
    <?php
    submit_button();
    ?>
    </form>
</div>
<script type="text/javascript">


</script>
<pre>

Support for the following social networks is required: Facebook, Twitter, Google+, Pinterest, LinkedIn, Whatsapp (for mobile browsers only).

The plugin options page must include the following configurable items:
1) <strike>The choice to display on posts / pages / other registered custom post types</strike>
2) <strike>Options to activate / deactivate the buttons for different social networks</strike>
3) <strike>Three different button sizes to choose from (small / medium / large)</strike>
4) <strike>The choice to display the icons in their original colors (default) or all in a selected color</strike>
5) <strike>An option to determine in which order the icons will appear (e.g: FB - TW - G+ - etc)</strike>
6) <strike>Options to place the social share bar (one or more of these can be selected)</strike>
a) below the post title
b) floating on the left area
c) after the post content
d) inside the featured image
</pre>
