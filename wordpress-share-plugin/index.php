<?php
/*
Plugin Name: Toptal Social plugin
Description: Social share
Author: SFS
Version: 1.0
*/

/*
The goal is to create a plugin that will automatically display selected social network(s) sharing buttons in posts and/or on pages.

Support for the following social networks is required: Facebook, Twitter, Google+, Pinterest, LinkedIn, Whatsapp (for mobile browsers only).

The plugin options page must include the following configurable items:
1) The choice to display on posts / pages / other registered custom post types
2) Options to activate / deactivate the buttons for different social networks
3) Three different button sizes to choose from (small / medium / large)
4) The choice to display the icons in their original colors (default) or all in a selected color
5) An option to determine in which order the icons will appear (e.g: FB - TW - G+ - etc)
6) Options to place the social share bar (one or more of these can be selected)
a) below the post title
b) floating on the left area
c) after the post content
d) inside the featured image

The plugin should also enable a shortcode to include the sharing bar inside a post content

*/

include('classes/settings.php');
include('classes/social.php');
include('classes/view.php');


function toptal_social_init() {
	register_post_type( 'custom', [
		'public'	=> true,
		'labels'	=> [
			'name'	=> 'Custom post'
		]
	]);
}
// register a custom post type for test pourposes
add_action( 'init', 'toptal_social_init' );


function toptal_social_enqueue_admin_scripts() {
	$scripts_dir = plugin_dir_url( __FILE__ ).'/scripts/';
	wp_enqueue_media();
	wp_enqueue_style( 'dragula.min.css', $scripts_dir . 'dragula.min.css');
	wp_enqueue_script( 'dragula.min.js', $scripts_dir . 'dragula.min.js', array(), null, true);
	wp_enqueue_style('font-awesome.min.css', $scripts_dir . 'font-awesome.min.css');
	wp_enqueue_style('toptal-social-admin.css', $scripts_dir . 'toptal-social-admin.css');
	wp_enqueue_script( 'toptal-social-admin.js', $scripts_dir . 'toptal-social-admin.js', array(), null, true);

}
add_action("admin_enqueue_scripts", "toptal_social_enqueue_admin_scripts");


function toptal_social_enqueue_scripts() {
	$scripts_dir = plugin_dir_url( __FILE__ ).'/scripts/';
	wp_enqueue_style('font-awesome.min.css', $scripts_dir . 'font-awesome.min.css');
	wp_enqueue_style('toptal-social.css', $scripts_dir . 'toptal-social.css');
	wp_enqueue_script( 'toptal-social.js', $scripts_dir . 'toptal-social.js', array(), null, true);
}

// function toptal_social_the_title($title, $id) {
// 	$sharer = \Toptal\SocialSharer::get('top');
// 	return $title.$sharer;
// }
function toptal_social_the_content($content) {

	if(!is_single()) {
		$left = \Toptal\SocialSharer::get('left');
		return $left.$content;
	}

	$top = '';
	if(!has_post_thumbnail()) {
		$top = \Toptal\SocialSharer::get('top');
	}
	$left = \Toptal\SocialSharer::get('left');
	$bottom = \Toptal\SocialSharer::get('bottom');

	return $top.$left.$content.$bottom;
}

function toptal_social_post_thumbnail_html($html, $post_id = null, $post_thumbnail_id = null, $size = null, $attr = null) {
	if(!is_single()) return $html;
	$top = '';
	if(has_post_thumbnail()) {
		$top = \Toptal\SocialSharer::get('top');
	}
	$image = \Toptal\SocialSharer::get('image');
	return $top.$html.$image;
}


if( is_admin() ) {
	new \Toptal\SocialSettings;
} else {
	add_action( 'wp_enqueue_scripts', 'toptal_social_enqueue_scripts' );
	add_filter( 'the_content', 'toptal_social_the_content' );
	add_filter( 'post_thumbnail_html', 'toptal_social_post_thumbnail_html' );
	//add_filter( 'the_title', 'toptal_social_the_title', 10, 2 );

}

function toptal_social_shortcode( $atts ) {

	return \Toptal\SocialSharer::get('shortcode');
}
add_shortcode( 'toptal_social', 'toptal_social_shortcode' );
