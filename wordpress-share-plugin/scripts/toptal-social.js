var $ = jQuery;

function openShare(link) {

    window.open(link,'Share!', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

$('[share-facebook]').on('click', function(e){
    e.preventDefault();
    var link = $(this).attr('share-facebook');
    openShare('https://www.facebook.com/sharer/sharer.php?u='+link);
})
$('[share-facebook]').removeClass('hidden');

$('[share-twitter]').on('click', function(e){
    e.preventDefault();

    var link = $(this).attr('share-twitter');
    openShare('https://twitter.com/intent/tweet?text='+link);
})
$('[share-twitter]').removeClass('hidden');

$('[share-google]').on('click', function(e){
    e.preventDefault();

    var link = $(this).attr('share-google');
    openShare("https://plus.google.com/share?url='"+link+"'");
})
$('[share-google]').removeClass('hidden');

$('[share-pinterest]').on('click', function(e){
    e.preventDefault();
})
$('[share-pinterest]').removeClass('hidden');

$('[share-linkedin]').on('click', function(e){
    e.preventDefault();

    var link = $(this).attr('share-linkedin');
    openShare('https://www.linkedin.com/cws/share?url='+link);

})
$('[share-linkedin]').removeClass('hidden');


if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $('[share-whatsapp]').on('click', function(e){
        e.preventDefault();
        var link = $(this).attr('share-twitter');
        var whatsapp_url = "whatsapp://send?text="+encodeURIComponent(link);;
        window.location.href= whatsapp_url;
        // <a href="whatsapp://send?text=Hello%20World!">Hello, world!</a>
    })
    $('[share-whatsapp]').removeClass('hidden');

}else{
    $('[share-whatsapp]').hide();
}
