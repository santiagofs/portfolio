var $ = jQuery;

$('[color-reset]').on('click', function(e){
    e.preventDefault();
    var key = $(this).attr('color-reset');
    var original = $(this).attr('color-original');
    var $input = $('[color-input="'+key+'"]');
    var $display = $('[color-display="'+key+'"]');
    $input.val(original);
    $display.css({'color': '#'+original});
});
$('[color-input]').on('blur', function(e) {
    var key = $(this).attr('color-input');
    var $display = $('[color-display="'+key+'"]');
    var color = $(this).val();
    $display.css({'color': '#'+color});
});

dragula([document.getElementById('select-networks')]);
