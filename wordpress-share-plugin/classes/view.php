<?php
namespace Toptal;

class View {

	protected static function get_current_plugin_folder ()  {

		$trace = debug_backtrace();
		$file = $trace[1]['file'];
		$folders = explode('/', str_replace(ABSPATH.'wp-content/plugins/', '', $file) );
		$folder = $folders[0];
		return $folder;
	}

	protected static function get_template_path($plugin, $view) {
		return ABSPATH.'wp-content/plugins/'.$plugin.'/views/'.$view.'.php';
	}

	protected static function load_file($filepath, $args = array()) {

		if(!file_exists($filepath)) return false;

		extract($args);

		ob_start(); // turn on output buffering
		include($filepath);
		$ret = ob_get_contents(); // get the contents of the output buffer
		ob_end_clean();

		return $ret;
	}

	// loads templates from plugins folder
	public static function make($template, $args = array(), $echo = true) {

		if(strpos($template, '::')) {
			list($folder, $template) = explode('::', $template);
		} else {
			$folder = static::get_current_plugin_folder ();
		}

		$filepath = static::get_template_path($folder, $template);

		$view = static::load_file($filepath, $args);

		if($echo) echo $view;

		return $view;
	}

	// loads templates from the current theme
	public static function get($template, $args = array(), $echo = true) {

		$path = get_template_directory().'/'.$template.'.php';

		$view = static::load_file($filepath, $args);

		if($echo) echo $view;

		return $view;
	}

}
