<?php
namespace Toptal;

class SocialSharer {

    static $options = null;
    static $initialized = false;
    static $built = false;

    static $permalink = null;
    static $post_type = null;
    static $networks = null;

    static $display_on = [];
    static $display_at = [];
    static $selected_networks = [];
    static $sort = [];
    static $size = 'small';
    static $colors = [];

    static $links = [];

    static $filtered_titles = [];


    public static function initialize () {
        if(static::$initialized) return;
        static::$initialized = true;
        $options = get_option( 'toptal_social' );

        static::$networks = SocialSettings::$networks;

        static::$display_on = isset($options['display_on']) ? explode(',',  $options['display_on'] ) : ['top', 'left', 'bottom', 'image'];
        static::$display_at = isset($options['display_at']) ? explode(',',  $options['display_at'] ) : array_keys(get_post_types( ['public'   => true], 'objects', 'and' ));
        static::$selected_networks = isset($options['networks']) ? explode(',',  $options['networks'] ) : array_keys(static::$networks);
        static::$sort = isset($options['sort']) ? explode(',',  $options['sort'] ) : array_keys(static::$networks);
        static::$size = isset($options['size']) ? $options['size'] : 'small';
        static::$colors = isset($options['colors']) ? $options['colors'] : 'small';

        static::$permalink = get_permalink();
        static::$post_type = get_post_type();


    }

    private static function build_link($network_key) {
        $network = static::$networks[$network_key];
        $color = isset(static::$colors[$network_key]) ? static::$colors[$network_key] : $network['color'];
        $permalink = static::$permalink;
        $class = $network['class'];
        $label = $network['label'];

        static::$links[] = View::make('link', get_defined_vars(), false);
    }
    private static function sharer($position) {
        if(!static::$built) {
            foreach(static::$sort as $network) {
                static::build_link($network);
            }
            static::$built = true;
        }
        //var_dump(static::$links); die();
        return View::make('sharer', [
            'size' => static::$size,
            'links' => static::$links,
            'position' => $position,
        ], false );


    }


    public static function get($position) { // position can also be shortcode
        static::initialize();

        if($position != 'shortcode') {
            if(!in_array(static::$post_type, static::$display_on)) return ''; // not in the current post type
            if(!in_array($position, static::$display_at)) return ''; // not in the current position
        }

        //var_dump('passss');
        return static::sharer($position);

    }
}
// array(6) { ["display_on"]=> string(16) "post,page,custom" ["display_at"]=> string(21) "top,left,bottom,image" ["networks"]=> string(41) "whatsapp,facebook,twitter,google,linkedin" ["size"]=> string(5) "large" ["colors"]=> array(6) { ["whatsapp"]=> string(6) "4BB74B" ["facebook"]=> string(6) "3B5A9A" ["pinterest"]=> string(6) "CD2129" ["twitter"]=> string(6) "29A9E1" ["google"]=> string(6) "DF4B38" ["linkedin"]=> string(6) "117BB8" } ["sort"]=> string(51) "whatsapp,facebook,pinterest,twitter,google,linkedin" }
