<?php
namespace Toptal;

class SocialSettings {

    private $options;
    public static $networks = [
        'facebook' => ['label'=>'Facebook', 'class'=>'fa-facebook', 'color'=>'3B5A9A', 'back'=>'FFFFFF'],
        'twitter' => ['label'=>'Twitter', 'class'=>'fa-twitter', 'color' => '29A9E1', 'back'=>'FFFFFF'],
        'google' => ['label'=>'Google+', 'class'=>'fa-google-plus', 'color' => 'DF4B38', 'back'=>'FFFFFF'],
        'pinterest' => ['label'=>'Pinterest', 'class'=>'fa-pinterest', 'color' => 'CD2129', 'back'=>'FFFFFF'],
        'linkedin' => ['label'=>'LinkedIn', 'class'=>'fa-linkedin', 'color' => '117BB8', 'back'=>'FFFFFF'],
        'whatsapp' => ['label'=>'Whatsapp', 'class'=>'fa-whatsapp', 'color' => '4BB74B', 'back'=>'FFFFFF'],
    ];


    public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    public function add_plugin_page(){
        add_options_page(
            'Toptal Social Settings',
            'Toptal Social',
            'manage_options',
            'toptal-social-admin',
            array( $this, 'create_admin_page' )
        );
    }

    public function create_admin_page() {
        View::make('settings', [
            'options' => get_option( 'toptal_social' ),
            'networks' => static::$networks,
        ] );
    }

    public function page_init() {
        register_setting(
        'toptal_social_option_group', // Option group
        'toptal_social', // Option name
        array( $this, 'sanitize' ) // Sanitize
        );
    }

    public function sanitize( $input )
    {

        $new_input = array();
        if( isset( $input['display_on'] ) )
            $new_input['display_on'] = implode(',', $input['display_on']);

        if( isset( $input['display_at'] ) )
            $new_input['display_at'] = implode(',', $input['display_at']);

        if( isset( $input['networks'] ) )
            $new_input['networks'] = implode(',', $input['networks']);

        if( isset( $input['size'] ) )
            $new_input['size'] = sanitize_text_field( $input['size'] );

        if( isset( $input['colors'] ) )
            $new_input['colors'] = $input['colors'];

        if( isset( $input['backs'] ) )
            $new_input['backs'] = $input['backs'];

        if( isset( $input['sort'] ) )
            $new_input['sort'] =implode(',', $input['sort']);
        // var_dump($new_input);
        // die();
        return $new_input;

    }

}
