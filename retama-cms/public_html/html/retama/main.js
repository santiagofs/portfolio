$(function(){

	$('[toggle-class]').on('click', function(e) {
		e.preventDefault();

		var $this = $(this);
		var targetname = $this.attr('toggle-target');
		var classname = $this.attr('toggle-class');
		var $target = $(targetname);
		$target.toggleClass(classname);

		var data = {};
		data['target']=targetname;
		data['key']=classname;
		data['value'] = $target.hasClass(classname) ? 1 : 0 ;

		$.post('/set-cookie',data);

	});

});;$(function(){

	var $lis = $('.with-children', '#site-sidebar');

	$('> a', $lis).on('click', function(e) {
		e.preventDefault();

		var $this = $(this);
		var $to_collapse = $lis.filter('.expanded');
		var $to_expand = $this.closest('li');

		if($to_expand.hasClass('expanded')) {
			collapse($to_expand);
		} else {
			if($to_collapse.length) collapse($to_collapse);
			expand( $to_expand );
		}
	});

	function expand($item) {


		$('ul', $item).slideDown('fast', function(){
			$item.addClass('expanded');
		})
	}
	function collapse( $item ) {

		$('ul', $item).slideUp('fast', function(){
			$item.removeClass('expanded');
		});
	}
})
/*

(function( $ ) {

	'use strict';

	var $items = $( '.nav-main li.nav-parent' );

	function expand( li ) {
		li.children( 'ul.nav-children' ).slideDown( 'fast', function() {
			li.addClass( 'nav-expanded' );
			$(this).css( 'display', '' );
			ensureVisible( li );
		});
	}

	function collapse( li ) {
		li.children('ul.nav-children' ).slideUp( 'fast', function() {
			$(this).css( 'display', '' );
			li.removeClass( 'nav-expanded' );
		});
	}

	function ensureVisible( li ) {
		var scroller = li.offsetParent();
		if ( !scroller.get(0) ) {
			return false;
		}

		var top = li.position().top;
		if ( top < 0 ) {
			scroller.animate({
				scrollTop: scroller.scrollTop() + top
			}, 'fast');
		}
	}

	$items.find('> a').on('click', function() {
		var prev = $( this ).closest('ul.nav').find('> li.nav-expanded' ),
			next = $( this ).closest('li');

		if ( prev.get( 0 ) !== next.get( 0 ) ) {
			collapse( prev );
			expand( next );
		} else {
			collapse( prev );
		}
	});

}).apply( this, [ jQuery ]);
*/;$(function(){

	$('[toggle-checkbox]').on('click', function(e) {
		e.preventDefault();

		var $this = $(this);
		var targetname = $this.attr('toggle-checkbox');
		var $target = $(targetname);

		$this.toggleClass('checked');
		$target.val( $this.hasClass('checked') ? '1' : 0 );

	});

});;$(function(){

	$('.records-list .check-all').on('click', function(e) {
		var checked = 	$(this).prop('checked') ? true : false;
		$('.records-list .check-item').prop('checked',checked);
	});

	$('.records-list .check-item').on('click', function(e) {

		var checked = true;
		$('.records-list .check-item').each(function(i,e){
			var this_checked = 	$(e).prop('checked') ? true : false;
			checked = checked && this_checked;
		});

		$('.records-list .check-all').prop('checked',checked);
	});


	function deleted_selected() {

	}

	function delete_confirm(callback)
	{
		$('#retama-modal [modal-title]').html(messages.confirm_action);
		$('#retama-modal [modal-body]').html(messages.confirm_deletion);

		$('#retama-modal [modal-button-ok]').html(messages.confirm);
		$('#retama-modal [modal-button-ok]').off();
		$('#retama-modal [modal-button-ok]').on('click', callback);

		$('#retama-modal').modal({'backdrop':'static'});
	}

	$('[delete-selected]').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		delete_confirm(function(){
			var checkboxs = $('td.select-item .check-item:checked');
			var ids = new Array();
			checkboxs.each(function(i,e)
			{
				ids.push($(e).val());
			})
			if(!ids.length) {
				$('#retama-modal').modal('hide');
				return false;
			}

			var post = {
				type: "POST",
				url: $this.attr('href'),
				data: {'id':ids},
				success: function()
				{
					top.location.reload();
				},
				/* dataType: dataType, */
				beforeSend: function(request) {
			        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
			    }
			}
			$.ajax(post);
		})
	});

	$('[delete-item]').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		delete_confirm(function(){
			top.location.href = $this.attr('href');
		})

	});


});

;(function(window, document, $, undefined) {
	"use strict";
	var defaults = {
		max_file_size: 1024 * 1024 * 30,
		auto: true,
		/* extensions: '*' */
	};

	var Uploader = function (input_name, upload_url, options)
	{
		this.options = $.extend({}, defaults, options);

		this.upload_uri = upload_url;
		this.extensions = options.extensions

		this.file_input = document.getElementById(input_name+'_uploader');
		this.upload_button = document.getElementById(input_name+'_uploader_select');
		this.upload_list = document.getElementById(input_name+'_uploader_queue');
		this.upload_callback_name = input_name+'_uploader_callback';

		this.file_queue_control = new Array();
		this.file_queue = new Array();
		this.file_queue_unique_ndx = 0;

		this.init();


	};

	Uploader.prototype.init = function()
	{
		var that = this;
		this.upload_button.addEventListener("click", function (e) {

			if (that.file_input) { that.file_input.click();}
			e.preventDefault(); // prevent navigation to "#"
		}, false);

		this.file_input.addEventListener('change',function(e){
			that.files_selected(this.files);
		})
		if(window[this.upload_callback_name] != undefined) {
			this.upload_callback = window[this.upload_callback_name];
		}
	}
	Uploader.prototype.upload_callback = function(file) {}

	Uploader.prototype.set_callback = function(callback) {
		this.upload_callback = callback;
	}

	Uploader.prototype.on_files_selected = function(files) {}

	Uploader.prototype.files_selected = function(files)
	{
		this.on_files_selected(files);
		if(files.length)
		{
			for(var ndx = 0; ndx < files.length; ndx ++)
			{
				var file = files.item(ndx);

				if(this.extensions && this.extensions.length) {
					var farray = file.name.split('.');
					var ext = farray[farray.length-1].toLowerCase();

					var valid_file_flag = false;
					for(var i=0; i < this.extensions.length; i++) {
						if(this.extensions[i] == ext) valid_file_flag = true;
					}

					if(!valid_file_flag) {
						alert('El archivo ' + file.name + ' no es válido');
						return false;
					}
				}



				if(file.size > this.options.max_file_size) continue;
				if(!this.file_queue_control[file.name])
				{
					file.unique_id = 'uid' + (++this.file_queue_unique_ndx);
					this.file_queue_control[file.name] = 1;
					this.file_queue.push(file);
					this.append_li(file);
				}

			}

			if(this.options.auto)
			{
				this.process_queue();
			}
		}
	}

	Uploader.prototype.append_li = function(file)
	{
		var li = document.createElement('li');
		var name = document.createElement('label');
		var progress_bar = document.createElement('div');
		var progress_bar_progress = document.createElement('div');

		var text=document.createTextNode(file.name)
		name.appendChild(text);

		progress_bar.className = 'progress-bar';
		progress_bar.appendChild(progress_bar_progress);

		li.setAttribute('id', 'li-'+file.unique_id);
		li.appendChild(name);
		li.appendChild(progress_bar);
		this.upload_list.appendChild(li);
	}

	Uploader.prototype.process_queue = function()
    {
    	if(this.file_queue.length)
    	{
	    	var file = this.file_queue.shift();
	    	this.send_file(file);
    	}
    }

	Uploader.prototype.on_file_sent = function (file) {}

	Uploader.prototype.send_file = function (file)
	{
		var self = this;
		var selector = '#li-'+file.name;
		var li = document.getElementById('li-'+file.unique_id);
		var progress_bar_progress = document.querySelector('#li-'+file.unique_id +' .progress-bar div');
		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		xhr.open("POST", this.upload_uri, true);

		xhr.upload.addEventListener("progress", function(e) {

			if (e.lengthComputable) {
				var percentage = Math.round((e.loaded * 100) / e.total);
				progress_bar_progress.style.width = percentage + '%';

			}
		}, false);

		xhr.upload.addEventListener("load", function(e){


		}, false);

		xhr.addEventListener('readystatechange', function(e) {

			if( this.readyState === 4 ) {
				if(this.status === 200) {

					var callback = window[self.upload_callback_name];
					if( typeof(callback) == "function" ) callback(file);

					if(self.on_file_sent) self.on_file_sent(file);
				  	self.process_queue();


				} else {
					var jsonResponse = JSON.parse(this.responseText);
					alert(jsonResponse.message);
				}

				$(li).hide('slow');
			}
		});

		fd.append('media_upload', file);
        xhr.send(fd);
	}

	window.Uploader = Uploader;

	$(function(){
		$('[uploader-name]').each(function(i,e){
			var $uploader = $(e);
			var name = $uploader.attr('uploader-name');
			var url = $uploader.attr('uploader-url');
			var second = $uploader.attr('second-button');
			var extensions = $uploader.attr('extensions');
			if(extensions) extensions = extensions.split('|');

			$uploader.data('uploader', new Uploader(name, url, {'extensions': extensions}));

		})
	});

})(window, window.document, window.jQuery);



;$(function(){
	$('[create-structure]').on('click', function(e) {
		e.preventDefault();

		$('#retama-modal [modal-title]').html(messages.create_structure_title);
		$('#retama-modal [modal-body]').html($('#structure-create-holder').html());

		$('#retama-modal [modal-button-ok]').hide();
		$('#retama-modal [modal-button-ok]').html(messages.create);
		$('#retama-modal [modal-button-ok]').off();
		$('#retama-modal [modal-button-ok]').on('click', function(){

		});

		$('#retama-modal').modal({'backdrop':'static'});
		var $this = $(this);
		var parent_id = $this.attr('node-id');
		if(parent_id) {
			var $links = $('#retama-modal [modal-body] a');
			$links.each(function(i,el){
				var $e = $(el);
				$e.attr('href', $e.attr('href')+'?parent_id='+parent_id);
			})
		}

	});
});;
$(function(){
	$('[remove-file]').on('click', function(e) {
		e.preventDefault();
		var name = $(this).attr('remove-file');
		console.log('test');
		$('#'+name+'_status').val('delete');
		$('#'+name+'_display').html('');
		$('#'+name+'_validation').val('');
	});
});;$(function(){
	/* sliders */

	var $original = undefined;

	function save_slide(form) {

		if(!$slider_form.valid()) return false;

		var img_bg = $('.thumb', $slide).css('background-image');
		//console.log(bg);
        //img_bg = bg.replace('url(','').replace(')','');


		var slide_data = {
			file: $file.val(),
			file_status: $file_status.val(),
			text: $text.val(),
			button: $button.val(),
			link: $link.val(),
		}
		console.log(slide_data);

		var $slide = $original ? $original : $slide_sample.clone();

		var $input = $('[name^="slide"]', $slide);
		if(!$input.val()) {
			slide_data.file_status = 'new';
		} else {
			var current = JSON.parse($input.val());
			if(!current || (current.file != slide_data.file)) {
				slide_data.file_status = 'new';
			}
		}
		
		
		

		$input.val(JSON.stringify(slide_data));
		$('.thumb', $slide).css('background-image', 'url(' + (slide_data.file_status == 'old' ? '/media/' + img_bg : '/media/temp/' + slide_data.file)  + ')');
		$('.text', $slide).html(slide_data.text);
		$('.button', $slide).html(slide_data.button);
		$('.link', $slide).html(slide_data.link);

		if(!$original) {
			$list.append($slide);
		}

		$('#slider-modal').modal('hide');
	}

	$('#slider-modal [modal-button-ok]').html('Ok');
	$('#slider-modal [modal-button-ok]').on('click', function(){
		save_slide();
	});
	var $slider_form = $('#form-slide');
	var $slider_form_validator = $slider_form.validate({
		ignore: ":hidden:not(#slide_form_image_file)",
		rules: {
			'slide_form_image': 'required'
		},
		submitHandler: save_slide
	});

	var $file = $('#slide_form_image_file');
	var $file_display = $('#slide_form_image_display');
	var $file_status = $('#slide_form_image_status');
	var $text = $('#id-slide_form_text')
	var $button = $('#id-slide_form_button')
	var $link = $('#id-slide_form_link')

	var $slide_sample = $('#slide-sample');
	$slide_sample.detach();
	var $list = $('#slides-list');


	function show_slide_modal(title) {
		$('#slider-modal [modal-title]').html(title);
		$('#slider-modal').modal({'backdrop':'static'});
	}

	$('[add-slide]').on('click', function(e) {
		e.preventDefault();

		$original = undefined;

		var that = this;
		$slider_form_validator.resetForm();

		$file.val('');
		$file_display.html('');
		$file_status.val('new');
		$text.val('')
		$button.val('')
		$link.val('')

		show_slide_modal('Add Slide');

	});

	$(document).on('click', '[edit-slide]', function(e) {
		e.preventDefault();

		$original = $(this).parents('.slide');

		var data = JSON.parse($('[name^="slide"]', $original).val());
		if(data) {
			$file.val(data.file);
			$file_display.html(data.file);
			$file_status.val(data.file_status);
			$text.val(data.text)
			$button.val(data.button)
			$link.val(data.link)
		}


		

		show_slide_modal('Edit Slide');

	});
});;(function(window, document, undefined) {

	var defaults = {
		max_file_size: 1024 * 1024 * 30,
		auto: true,
		extensions: '*'

	};

	//XHRUploader.prototype.on_invalid_file = function(file, motive, other) {}
	//XHRUploader.prototype.on_file_set = function(file) {}
	//XHRUploader.prototype.on_file_progress = function(file, percentage) {}
	//XHRUploader.prototype.on_file_sent = function(file, status, other) {}


	function extend(){
	    for(var i=1; i<arguments.length; i++)
	        for(var key in arguments[i])
	            if(arguments[i].hasOwnProperty(key))
	                arguments[0][key] = arguments[i][key];
	    return arguments[0];
	}

	var XHRUploader = function (options)
	{
		this.options = extend({}, defaults, options);
		this.queue = {
			files: new Array(),
			control: new Array(),
			unique_index: 0
		}

	};

	XHRUploader.prototype.set_files = function(files) {
		var that = this;

		if(files.length)
		{
			for(var ndx = 0; ndx < files.length; ndx++)
			{
				console.log(ndx);
				var file = files.item(ndx);

				/* check is a valid file by extension */
				if(this.options.extensions != '*' && this.options.extensions.length) {

					var name_array = file.name.split('.');
					var ext = name_array[name_array.length-1].toLowerCase();

					var valid_file_flag = false;
					for(var i=0; i < this.options.extensions.length; i++) {
						if(this.options.extensions[i] == ext) valid_file_flag = true;
					}

					if(!valid_file_flag) {
						this.on_invalid_file(file, 'Extension', {extensions: this.options.extensions, extension: ext});
						continue;
					}
				}

				if(file.size > this.options.max_file_size) {
					this.on_invalid_file(file, 'File size', {max_file_size: this.options.max_file_size});
					continue;
				}

				if(!this.queue.control[file.name])
				{
					file.unique_id = 'uid' + (++this.queue.unique_index);
					this.queue.control[file.name] = 1;
					this.queue.files.push(file);

					var on_file_set = that.options.on_file_set;
					if(typeof(on_file_set) == 'function') on_file_set(file);
				}

			}

			if(this.options.auto)
			{
				this.process_queue();
			}
		}
	}

	XHRUploader.prototype.process_queue = function() {
    	if(this.queue.files.length)
    	{
	    	var file = this.queue.files.shift();
	    	this.send_file(file);
    	}
    }

	XHRUploader.prototype.send_file = function(file) {
    	var that = this;
		var xhr = new XMLHttpRequest();
		var fd = new FormData();

		xhr.open("POST", this.options.upload_url, true);

		xhr.upload.addEventListener("progress", function(e) {
			if (e.lengthComputable) {
				var percentage = Math.round((e.loaded * 100) / e.total);

				var on_file_progress = that.options.on_file_progress;
				if(typeof(on_file_progress) == 'function') on_file_progress(file, percentage)
			}
		}, false);

		xhr.upload.addEventListener("load", function(e){

		}, false);

		xhr.addEventListener('readystatechange', function(e) {

			if( this.readyState === 4 ) {
				if(this.status === 200) {

					var on_file_uploaded = that.options.on_file_uploaded;
					if(typeof(on_file_uploaded) == 'function') on_file_uploaded(file, 'OK');

					that.on_file_sent(file, 'OK')
					that.process_queue();
				} else {
					var jsonResponse = JSON.parse(this.responseText);
					var on_file_uploaded = that.options.on_file_uploaded;
					if(typeof(on_file_uploaded) == 'function') on_file_uploaded(file, 'ERROR', jsonResponse)
				}
			}
		});

		fd.append('media_upload', file);
        xhr.send(fd);
        var on_start_upload = that.options.on_start_upload;
		if(typeof(on_start_upload) == 'function') on_start_upload(file);
    }


	XHRUploader.prototype.on_invalid_file = function(file, motive, other) {}
	XHRUploader.prototype.on_file_set = function(file) {}
	XHRUploader.prototype.on_file_progress = function(file, percentage) {}
	XHRUploader.prototype.on_file_sent = function(file, status, other) {}

	window.XHRUploader = XHRUploader;

})(window, window.document);


;(function(window, document, $, undefined) {
	"use strict";
	var defaults = {

	};

	var modal_template = Hogan.compile([
		'<div id="modal-embed">',
			'<input type="hidden" name="video_id" id="modal_video_id">',
			'<input type="hidden" name="video_source" id="modal_video_source">',
			'<input type="text" class="form-control" placeholder="Video URL" name="modal_embed_url" id="modal_embed_url" value="" />',
			'<div id="modal_embed_target" style="height:314px; margin-top: 20px;"></div>',
		'</div>'
	].join(''));

	var vimeo_embed = Hogan.compile('<iframe src="http://player.vimeo.com/video/{{video_id}}?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=f7d64f" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
	var youtube_embed = Hogan.compile('<iframe width="100%" height="100%" src="http://www.youtube.com/embed/{{video_id}}" frameborder="0" allowfullscreen></iframe>');



	function update_modal_embed()
	{

	}


	function select_embed(callback)
	{



	}

	var that = null;

	var Embed = function ()
	{


	}
	Embed.prototype.update_modal_embed = function()
	{
		var $holder = $('#modal_embed_target');
		$holder.addClass('loading');
		var video_url = $('#modal_embed_url').val();
		if(!video_url) return false;

		var source = '';
		var video_id = '';

		if(video_url.indexOf('vimeo')  >= 0) {
			source = 'vimeo';
			var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
			var match = video_url.match(regExp);
			video_id = match[2];
			//console.log(video_id);
		} else {

			if(video_url.indexOf('youtube') >= 0) {
				source = 'youtube'
				var test_id = video_url.split('v=')[1];
				var ampersandPosition = test_id.indexOf('&');
				if(ampersandPosition != -1) {
					video_id = test_id.substring(0, ampersandPosition);
				} else {
					video_id = test_id;
				}

			} else {
				return false
			}
		}



		$holder.removeClass('loading');

		if(!video_id) {
			$holder.text('Error parsing video url');
			return false;
		}

		$('#modal_video_source').val(source);
		$('#modal_video_id').val(video_id);

		this.source = source;
		this.video_id = video_id;
		this.video_url = video_url;



		$holder.html(this.get_iframe());
	}

	Embed.prototype.get_iframe = function()
	{
		var data = {video_id: this.video_id}

		if(this.source == 'vimeo')
		{
			var iframe = vimeo_embed.render(data);
		}
		else
		{
			var iframe = youtube_embed.render(data);
		}

		return iframe;
	}

	Embed.prototype.get_thumb_url = function(callback)
	{
		if(this.source=='vimeo')
		{
			$.ajax({
	            url: 'http://vimeo.com/api/v2/video/' + this.video_id + '.json',
	            dataType: 'jsonp',
	            success: function(data) {
		            callback(data[0].thumbnail_large);
	            }
	        });
		} else {
			 callback('http://i2.ytimg.com/vi/' + this.video_id + '/hqdefault.jpg');
		}
	}

	Embed.prototype.open_modal_embed = function()
	{
		var that = this;
		$('#retama-modal [modal-title]').html('Select Video Embed');
		$('#retama-modal [modal-body]').html(modal_template.render());

		$('#retama-modal [modal-button-ok]').html('Ok');
		$('#retama-modal [modal-button-ok]').off();
		$('#retama-modal [modal-button-ok]').on('click', function(){
			that.ok_modal_embed()
		});

		$('#retama-modal').modal({'backdrop':'static'});

		$('#modal_embed_url').on('change', function(e) {
			e.preventDefault();
			that.update_modal_embed()
		});
	}

	Embed.prototype.ok_modal_embed = function()
	{
		var url = $('#modal_embed_url').val();
		var source = $('#modal_video_source').val();
		var video_id = $('#modal_video_id').val();
		this.url = url;
		this.source = source;
		this.video_id = video_id;

		if(url && source && video_id) {
			this.on_embed_set(url, source, video_id);
			$('#retama-modal').modal('hide');
		} else {
			console.log('test');
		}
	}

	Embed.prototype.on_embed_set = function(url, source, id){
		console.log('on_embed_set');
	}

	window.Embed = Embed;


	$(function(){

		$('[trigger-video-embed]').each(function(i,e){
			var $this = $(this);
			var name = $this.attr('trigger-video-embed');

			$this.data('embed', new Embed(name, $this));

			var that = this;
			$this.on('click', function(e) {
				e.preventDefault();
				$(this).data('embed').open_modal_embed();
			});
		})
	})


})(window, window.document, window.jQuery);






;(function(window, document, $, undefined) {
	"use strict";
	var defaults = {

	};

	var image_template = Hogan.compile([
		'<li draggable="true">',
			'<input type="hidden" json="true" name="{{input_name}}[jsondata][]" value="{{json_value}}">',
			'<input type="hidden" name="{{input_name}}[id][]" value="{{id}}">',
			'<a href="#" data-toggle="media-remove" class="delete"><i class="fa fa-ban"></i></a>',
			'<i data-action="media-move" class="move fa fa-arrows"></i>',
			'<label>Filename: <span>{{name}}</span></label>',
			'<div class="thumb-control">',
				'<img class="thumb" src="{{ image_source }}">',
			'</div>',
			'<div class="right">',
				'<input type="text" name="{{input_name}}[title][]" placeholder="{{title_placeholder}}" value="{{title}}" class="form-control">',
				'<textarea name="{{input_name}}[description][]" placeholder="{{description_placeholder}}" class="form-control">{{description}}</textarea>',
			'</div>',
		'</li>'
	].join(''));

	var embed_template = Hogan.compile([
		'<li draggable="true">',
			'<input type="hidden" json="true"  name="{{input_name}}[jsondata][]" value="{{json_value}}">',
			'<input type="hidden" name="{{input_name}}[id][]" value="{{id}}">',
			'<a href="#" data-toggle="media-remove" class="delete"><i class="fa fa-ban"></i></a>',
			'<i data-action="media-move" class="move fa fa-arrows"></i>',
			'<label>Source: <span>{{source}} : {{key}}</span></label>',
			'<div class="thumb-control">',
				'<img class="thumb" src="">',
			'</div>',
			'<div class="right">',
				'<input type="text" name="{{input_name}}[title][]" placeholder="title" value="{{title}}" class="form-control">',
				'<textarea name="{{input_name}}[description][]" placeholder="description" class="form-control">{{description}}</textarea>',
			'</div>',
		'</li>'
	].join(''));

	var that = null;

	var MediaGallery = function ($holder, input_name, options)
	{
		this.options = $.extend({}, defaults, options);
		this.input_name = input_name;
		this.list = $holder;
		this.$placeholder = $('<li draggable="true" id="gallery-placeholder"></div>');
		this.current_drag = undefined;
		that = this;

		this.texts = {
			title_placeholder: this.options.title_placeholder ? this.options.title_placeholder : 'title',
			description_placeholder: this.options.description_placeholder ? this.options.description_placeholder : 'description'
		}


		this.current_target = false;
		$(document).on('mousedown', '[draggable]', function(e) {
			that.current_target = e.originalEvent.target;
		})

		$(document).on('dragstart', '[draggable]', function(e) {
			var $this = $(this);

			var handle = $('[data-action="media-move"]', $this)[0];
			if(handle.contains(that.current_target))
			{
				that.$current_drag = $this;
				//e.originalEvent.dataTransfer.setData('data','test');
				e.originalEvent.dataTransfer.setData('text/plain', 'handle');
			} else {
				e.preventDefault();
			}


		})

		$(this.list).on('drop', '#gallery-placeholder', function(e){
			e.preventDefault();
			e.stopPropagation();
			var $target = $(e.originalEvent.target);
			if($target.attr('id') == 'gallery-placeholder')
			{
				that.$current_drag.insertAfter(that.$placeholder);
			}

		})

		$(this.list).on('dragenter','li:not(#gallery-placeholder)', function(e) {
			e.preventDefault();
			e.stopPropagation();
			if(e.originalEvent.target === that.$current_drag[0]) return;

			var $target = $(e.originalEvent.target)
			var cx = e.originalEvent.clientX
			var ox = $target.offset().left;
			var d = (cx - ox) - ($target.width() / 2);

			if(d > 0) {
				that.$placeholder.insertBefore($target);
			} else {
				that.$placeholder.insertAfter($target);
			}
		})
		$(document).on('dragleave',this.list, function(e) {
			e.preventDefault();
			e.stopPropagation();

			//console.log('leave');
		})

		$(document).on('dragend',this.list, function(e) {
			  e.preventDefault();
			  e.stopPropagation();
			  that.$placeholder.detach();
		})

		$(document).on('dragover',this.list,function(e){
			e.preventDefault();
			e.stopPropagation();
		})

	}

	MediaGallery.prototype.uploader_callback = function(file)
	{
		var data = {status:'new', url:'/media/temp/'+file.name, id:0, name: file.name}
		var farray = file.name.split('.');
		var ext = farray[farray.length-1].toLowerCase();
		console.log(file);
		switch(ext)
		{
			case 'gif':
			case 'png':
			case 'jpg':
			case 'jpeg':
				data.type = 'image';
				break;
			case 'flv':
			case 'f4v':
			case 'mp4':
				data.type = 'video';
				break;
			case 'swf':
				data.type = 'flash';
				break;
			default:
				data.type = 'document';
		}

		that.create_media(data);
	}

	MediaGallery.prototype.embed_callback = function(embed)
	{
		var that = this;
		var data = {status:'new', key: embed.video_id, embed_url: embed.url, source: embed.source, embed_thumb: '', 'type': 'embed'};
		embed.get_thumb_url(function(url){
			data.embed_thumb = url;
			that.create_embed(data);
		});
	}

	MediaGallery.prototype.create_media = function(data)
	{
		var template_data = data;
		template_data.input_name = this.input_name;
		template_data.json_value = JSON.stringify(data);
		template_data.image_source = data.url+'?width=160&height=120';

		template_data.title_placeholder = this.texts.title_placeholder;
		template_data.description_placeholder = this.texts.description_placeholder;

		var $li = $(image_template.render(template_data));
		$li.addClass(data.type);
		this.list.append($li);

		$('.delete',$li).on('click', function(e) {
			e.preventDefault();
			that.remove($(this));
		});
	}

	MediaGallery.prototype.create_embed = function(data)
	{

		var template_data = data;
		template_data.input_name = this.input_name;
		template_data.json_value = JSON.stringify(data);


		var $li = $(embed_template.render(template_data));
		$('.thumb', $li).attr('src', data.embed_thumb);

		$li.addClass(data.type);
		this.list.append($li);

		$('.delete',$li).on('click', function(e) {
			e.preventDefault();
			that.remove($(this));
		});
	}

	MediaGallery.prototype.remove = function($e)
	{
		var $li = $e.parents('li');
		var npt_name = this.input_name + '[jsondata][]';
		var $npt = $('input[name="'+npt_name+'"]', $li);
		var data = JSON.parse($npt.val());
		data.status = 'delete';
		$npt.val(JSON.stringify(data));
		$li.hide('fast');
	}

	MediaGallery.prototype.from_json = function(json)
	{
		var that = this;
		$.each(json, function(i,e){
			if(e.status == undefined) e.status = 'old';
			if(e.path == undefined) e.path = e.url;
			if(e.name == undefined) e.name = e.original_name;

			if(e.type == 'embed')
			{
				console.log(e);
				that.create_embed(e);
			} else {

				that.create_media(e);
			}

		})
	}

	window.MediaGallery = MediaGallery;


	$(function(){

		$('[media-gallery]').each(function(i,e){
			var $gallery = $(e);
			var name = $gallery.attr('media-gallery');
			var with_embeds = $gallery.attr('with-embeds');

			var $uploader = $('[uploader-name="'+name+'"]');

			var options = {
				title_placeholder: $gallery.attr('title-placeholder'),
				description_placeholder: $gallery.attr('description-placeholder')
			}
			var media_gallery = new MediaGallery($gallery, name, options);

			var json = $gallery.attr('gallery-items');
			media_gallery.from_json(eval(json));
			//media_gallery.init();

			var uploader = $uploader.data('uploader');
			uploader.on_file_sent = function(file){
				media_gallery.uploader_callback(file);
			}

			var $embed = $('[trigger-video-embed="'+name+'"]');
			if($embed.length) {

				var embed = $embed.data('embed');
				embed.on_embed_set = function(url, source, id){
					media_gallery.embed_callback(embed);
				}
			}


			$gallery.data('media_gallery', media_gallery);
		})

	})


})(window, window.document, window.jQuery);






;(function(window, document, $, undefined) {
	"use strict";
	var defaults = {

	};

	var select_template = Hogan.compile([
		'<div class="gmappoint-modal-body">',
			'<input id="address_or_place" type="text" class="form-control" placeholder="Address or Place" value="{{address}}"/>',
			'<div class="fields-control">',
				'<input id="place_locality" type="text" class="form-control" placeholder="Locality" value="{{place_locality}}" readonly />',
				'<input id="place_district" type="text" class="form-control" placeholder="District" value="{{place_district}}" readonly />',
				'<input id="place_state" type="text" class="form-control" placeholder="State" value="{{place_state}}" readonly />',
				'<input id="place_country" type="text" class="form-control" placeholder="Country" value="{{place_country}}" readonly />',
				'<input id="place_zip" type="text" class="form-control" placeholder="ZIP" value="{{place_zip}}" readonly />',
				'<input id="place_lat" type="text" class="form-control" placeholder="Latitude" value="{{place_lat}}"  />',
				'<input id="place_lng" type="text" class="form-control" placeholder="Longitude" value="{{place_lng}}"  />',
				'<input id="place_zoom" type="hidden" class="form-control" value="{{place_zoom}}" />',
			'</div>',
			'<div class="map-control">',
				'<div id="modal-gmap" style="width:273px; height:274px"></div>',
			'</div>',
			'<div class="legend">You can drag and drop the marker to the correct location</div>',
		'</div>'
	].join(''));


	var GmapPoint = function ($trigger,options)
	{
		var that = this;
		$trigger.on('click', function(e) {
			e.preventDefault();
			that.open_modal();
		});

		this.name = $trigger.attr('gmap-trigger');
		this.$display = $('#'+ this.name +'-gmap-point-display');
		this.$input = $('#'+ this.name +'-gmap-point');

	}

	GmapPoint.prototype.init = function()
	{

	}

	GmapPoint.prototype.open_modal = function()
	{
		var that = this;

		$('#retama-modal [modal-title]').html('Select Map Point');
		$('#retama-modal [modal-body]').html(select_template.render());

		$('#retama-modal [modal-button-ok]').html('Ok');
		$('#retama-modal [modal-button-ok]').off();
		$('#retama-modal [modal-button-ok]').on('click', function(){
			that.ok_modal()
		});



		this.data = this.$input.val();
		var mapOptions = {
		   	zoom: 10,
	        scrollwheel: false,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
	        id: '#modal-gmap'

	    };

// {map: {id: '#map'}, marker: {draggable: true, visible: true}, zoomForLocation: 18, reverseGeocoding: true}

	    if(this.data != '') {
			var data = JSON.parse(this.data);
			$('#place_lat').val(data.lat);
			$('#place_lng').val(data.lng);
			$('#address_or_place').val(data.fulladdress);
			$('#place_locality').val(data.locality);
			$('#place_district').val(data.district);
			$('#place_state').val(data.state);
			$('#place_country').val(data.country);
			$('#place_zip').val(data.postal_code);
			mapOptions.center= new google.maps.LatLng(data.lat, data.lng);
			console.log(data.lat); console.log(data.lng);
			mapOptions.zoom = 18;

		}

		var addressPicker = new AddressPicker({map: mapOptions, marker: {draggable: true, visible: true}, zoomForLocation: 18, reverseGeocoding: true});

		// instantiate the typeahead UI
		$('#address_or_place').typeahead(null, {
			displayKey: 'description',
			source: addressPicker.ttAdapter()
		});
		// Bind some event to update map on autocomplete selection
		$('#address_or_place').bind("typeahead:selected", addressPicker.updateMap);
		$('#address_or_place').bind("typeahead:cursorchanged", addressPicker.updateMap);
		addressPicker.bindDefaultTypeaheadEvent($('#address_or_place'))

		$(addressPicker).on('addresspicker:selected', function (event, result) {

			$('#place_lat').val(result.latitude);
			$('#place_lng').val(result.longitude);
			$('#place_locality').val(result.nameForType('locality'));
			$('#place_district').val(result.nameForType('administrative_area_level_2'));
			$('#place_state').val(result.nameForType('administrative_area_level_1'));
			$('#place_country').val(result.nameForType('country'));
			$('#place_zip').val(result.nameForType('postal_code'));

			if (result.isReverseGeocoding()) {
				$('#address_or_place').val(result.address())
			}
		});
		console.log(addressPicker);

		$('#retama-modal').on('shown.bs.modal', function () {
			var currentCenter = addressPicker.map.getCenter();  // Get current center before resizing
			google.maps.event.trigger(addressPicker.map, "resize");
			addressPicker.map.setCenter(currentCenter); // Re-set previous center

		});

		$('#retama-modal').modal({'backdrop':'static'});


	}



	GmapPoint.prototype.ok_modal = function()
	{
		var that = this;

		if( (!$('#place_lat').val()) || (!$('#place_lng').val()) )
		{
			return false;
		}

		var data = {
			lat: $('#place_lat').val(),
			lng: $('#place_lng').val(),
			fulladdress: $('#address_or_place').val(),
			locality: $('#place_locality').val(),
			district: $('#place_district').val(),
			state: $('#place_state').val(),
			country: $('#place_country').val(),
			zip_code: $('#place_zip').val(),
			zoom: $('#place_zoom').val()
		}

		this.$display.text(data.fulladdress);
		this.$input.val(JSON.stringify(data));
		$('#retama-modal').modal('hide');

		$('#retama-modal [modal-body]').html('');

	}

	window.GmapPoint = GmapPoint;

	$(function(){

		$('[gmap-trigger]').each(function(i,e){

			var $select = $(e);
			var point = new GmapPoint($select);

			$select.data('gmap_point', point);
		})

	})
})(window, window.document, window.jQuery);;(function(window, document, $, undefined) {
	"use strict";

	var DraggableTree = function ($ul)
	{
		this.$current_drag = undefined;
		this.$placeholder = $('<li id="placeholder" dropable="true"	 style="height:20px; border: 1px solid #CCC; width:200px;"></li>');
		this.url = $ul.attr('update-url');
		var that = this;


		$(document).on('dragstart', '[draggable]', function(e) {

			var $this = $(this);
			that.$current_drag = $(e.originalEvent.target);
			var node_id = $this.attr('node-id');
			e.originalEvent.dataTransfer.setData('node-id',node_id);
			e.originalEvent.dataTransfer.effectAllowed = 'all';
		})



		$(document).on('drop', '[dropable]', function(e){
			e.preventDefault();
			e.stopPropagation();

			var $target = $(e.originalEvent.target);
			var $this = $(this);
			var $parent = null;

			if(that.$current_drag.has($this).length) return false;

			if($this.attr('id') == 'placeholder') {
				$parent = $this.closest('[node-id]');
				that.$current_drag.insertAfter($target);
			} else {
				if($this.attr('node-id'))
				{
					$parent = $this;
					$('> ul', $this).prepend(that.$current_drag);
				} else {
					$parent = null;
				}
			}

			if($parent)
			{
				that.$placeholder.detach();
				var $previous = that.$current_drag.prev();
				var previous_id = $previous.length ? $previous.attr('node-id') : undefined;
				var parent_id = $parent.attr('node-id') ? $parent.attr('node-id') : 1;
				var node_id = that.$current_drag.attr('node-id');
				that.update_position(node_id, parent_id, previous_id);
			}

		})

		$(document).on('dragenter','[dropable]', function(e) {
			 e.preventDefault();
			 e.stopPropagation();
			 var $this = $(this);
			 var $target = $(e.originalEvent.target);
			 if(that.$current_drag.has($this).length || (that.$current_drag.get(0) === $this.get(0))) return;

			 that.$placeholder.removeClass('onover');
			 $this.addClass('onover');

			 if($this.attr('node-id') != that.$current_drag.attr('node-id'))
			 {
			 	that.$placeholder.insertAfter($this);
			 }
		})
		$(document).on('dragleave','[dropable]', function(e) {
			e.preventDefault();
			e.stopPropagation();
			var $this = $(this);
			$this.removeClass('onover');
		})

		$(document).on('dragend','[dropable]', function(e) {
			  e.preventDefault();
			  e.stopPropagation();
			  that.$placeholder.detach();
			  that.$placeholder.removeClass('onover');
		})

		$(document).on('dragover','[dropable]',function(e){
			e.preventDefault();
			e.stopPropagation();
		})

	};

	DraggableTree.prototype.update_position = function(node_id, parent_id, previous_id)
	{
		var post = {
			type: "POST",
			url: this.url,
			data:{
				'node_id': node_id,
				'parent_id': parent_id,
				'previous_id': previous_id
			},
			success: function(rsp)
			{
				console.log(rsp);
			},
			/* dataType: dataType, */
			beforeSend: function(request) {
		        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
		    }
		}
		$.ajax(post);

	}

	window.DraggableTree = DraggableTree;
	$(function(){

		$('[draggable-tree]').each(function(i,e){
			var dt = new DraggableTree($(e));
		})
	});


})(window, window.document, window.jQuery);


;(function(window, document, $, undefined) {
function url_title(url)
{
	url_val = url.replace(/ /gi,'-');
	url_val = url_val.replace(/(á|Á)/gi,'a');
	url_val = url_val.replace(/(é|É)/gi,'e');
	url_val = url_val.replace(/(í|Í)/gi,'i');
	url_val = url_val.replace(/(ó|Ó)/gi,'o');
	url_val = url_val.replace(/(ú|Ú|ü|Ü)/gi,'u');
	url_val = url_val.toLowerCase();

	return url_val;
}

$(function(){

	function do_url_title(evt) {
		var $target = $( $(this).attr('slugify-to') );
		$( $(this).attr('slugify-to') ).val( url_title($(this).val()) );
	}
	$('[slugify-to]').bind('keyup',do_url_title)
	$('[slugify-to]').bind('blur',do_url_title)

})

})(window, window.document, window.jQuery);;(function(window, document, $, undefined) {

	$(function(){

		function do_copy_to(evt) {
			var $target = $( $(this).attr('copy-to') );
			$( $(this).attr('copy-to') ).val( $(this).val() );
		}
		$('[copy-to]').bind('keyup',do_copy_to);
		$('[copy-to]').bind('blur',do_copy_to);

	})

})(window, window.document, window.jQuery);