<?php
Route::group(array('prefix' => 'rt-admin/content'), function()
{
	Route::controller('obras','\Site\Controllers\Obras');
	Route::controller('desarrollos','\Site\Controllers\Desarrollos');
	Route::controller('categorias','\Site\Controllers\DesarrolloCategories');
	Route::controller('puntos','\Site\Controllers\Puntos');
});

// Route::get('obras/{id}', function($id){
//
//     $obra = \Site\Models\Obra::where('id', '=', $id)
//         ->with('media')
//         ->first();
//
//
// 	return \View::make('site::obra', ['obra'=>$obra]);
// });

// Route::get('desarrollos/{id}', function($id){
//
//     $desarrollo = \Site\Models\Desarrollo::where('id', '=', $id)
//         ->with('media')
// 		->with('gmap')
//         ->first();
//
// 	return \View::make('site::desarrollo', ['desarrollo'=>$desarrollo]);
// });
?>
