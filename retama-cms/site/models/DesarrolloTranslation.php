<?php
namespace Site\Models;

class DesarrolloTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['nombre','bajada', 'descripcion', 'caracteristicas'];
	protected $fillable = ['nombre','bajada', 'descripcion', 'caracteristicas'];

	protected $table = 'desarrollos_translation';

}
