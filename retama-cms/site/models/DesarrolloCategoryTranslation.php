<?php
namespace Site\Models;

class DesarrolloCategoryTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['nombre'];
	protected $fillable = ['nombre'];

	protected $table = 'desarrollos_categories_translation';

}
