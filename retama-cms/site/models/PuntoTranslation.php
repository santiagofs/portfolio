<?php
namespace Site\Models;

class PuntoTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['title','type','description'];
	protected $fillable = ['title','type','description'];

	protected $table = 'puntos_translation';

}
