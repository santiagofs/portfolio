<?php
namespace Site\Models;

class Punto extends \BaseModel {

	use \Modules\International\Models\TranslatableTrait;
	//use \Modules\International\Models\GmapableTrait;

	//use \Modules\Media\Models\MediableTrait;

	protected  $table = 'puntos';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

	public static function sorted() {
		$points = static::orderBy('province','ASC')
	        ->orderBy('category')
			->translate()
			->get();

		//var_dump($points); die();

		$sorted = [];
		foreach($points as $point) {
			if(!isset($sorted[$point->province])) {
				$sorted[$point->province] = [];
			}
			if(!isset($sorted[$point->province][$point->category])) {
				$sorted[$point->province][$point->category] = [];
			}
			$sorted[$point->province][$point->category][] = (object)['title'=>$point->title, 'type'=>$point->type, 'description'=>$point->description];
		}

		return $sorted;
	}


	// public function save(array $options = array())
    // {
	//     parent::save($options);
	//     $this->save_file_from_post('main_image');
	//     $this->save_gallery_from_post('gallery');
	// 	$point = \Input::get('point-gmap-point',null);
	// 	if($point)
	// 	{
	// 		$point = (array) json_decode($point);
	// 		$this->save_gmap($point);
	// 	}
    // }

}
