<?php
namespace Site\Models;

class ObraTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['nombre','bajada', 'descripcion', 'caracteristicas', 'caracteristicas2'];
	protected $fillable = ['nombre','bajada', 'descripcion', 'caracteristicas', 'caracteristicas2'];

	protected $table = 'obras_translation';

}
