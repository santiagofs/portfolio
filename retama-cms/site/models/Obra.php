<?php
namespace Site\Models;

class Obra extends \BaseModel {

	use \Modules\International\Models\TranslatableTrait;
	use \Modules\International\Models\GmapableTrait;

	use \Modules\Media\Models\MediableTrait;

	use \Modules\Content\Models\SitemapableTrait;

	protected  $table = 'obras';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

	public function save(array $options = array())
    {
	    parent::save($options);

		$this->save_sitemap('obra', 'nombre');
	    $this->save_gallery_from_post('gallery');

		$point = \Input::get('point-gmap-point',null);
		if($point)
		{
			$point = (array) json_decode($point);
			$this->save_gmap($point);
		}
    }

	public function C1() {
		$c = $this->caracteristicas;
		$c = explode(PHP_EOL, $c);
		$ret = array();
		foreach($c as $i) {
			$e = explode(':', $i);
			$o = array(
				'label'=> isset($e[0]) ? $e[0] : '',
				'value'=> isset($e[1]) ? $e[1] : ''
			);

			$ret[] = (object)$o;
		}
		return $ret;
	}

	public function C2() {
		$c = $this->caracteristicas2;
		$c = explode(PHP_EOL, $c);
		return $c;
	}

}
