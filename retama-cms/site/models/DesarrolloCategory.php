<?php
namespace Site\Models;

class DesarrolloCategory extends \BaseModel {

	use \Modules\International\Models\TranslatableTrait;

	use \Modules\Media\Models\MediableTrait;

	use \Modules\Content\Models\SitemapableTrait;

	protected  $table = 'desarrollos_categories';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

	public function save(array $options = array())
    {
	    parent::save($options);

		$this->save_sitemap('desarrollos', 'nombre');
		$this->save_gallery_from_post('gallery');
    }

}
