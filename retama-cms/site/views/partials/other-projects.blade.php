<?php
$projectSliders = '{
        "arrows": false,
        "infinite": false,
        "slidesToShow": 5,
    "responsive": [
      {
        "breakpoint": 991,
        "settings": {
            "infinite": true,
            "slidesToShow": 2,
          "centerMode": true,
          "centerPadding": "calc(50% - 260px)"
        }
      },
      {
        "breakpoint": 767,
        "settings": {
            "infinite": true,
            "slidesToShow": 1,
          "centerMode": true,
          "centerPadding": "calc(100% - 260px) 0 0"
        }
      }
    ]
}';
?>
<div class="projects-row" data-slick='<?php echo $projectSliders; ?>'>
    @foreach($otras as $otra)
    <?php
        $media = $otra->media_assorted();
        $img =  $media->thumb();
    ?>
    <a href="{{ URL::to($otra->sitemap->path) }}" class="projects-row__item">
        <div class="projects-row__item-img" style="background-image: url({{ $img->url() }})"></div>
        <div class="projects-row__item-info">
            <h5 class="projects-row__item-title">{{ $otra->nombre }}</h5>
            <p class="projects-row__item-description">{{ $otra->bajada }}</p>
        </div>
    </a>
    @endforeach
</div>
