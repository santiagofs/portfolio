<hr class="divider-gradient">
<footer class="main-footer">

	<div class="container">
    <div class="row">

      <div class="col-xs-12 col-md-3">
        <a href="#" class="main-footer__brand">
          <img src="{{asset('html/site/img/logo-podesta.png')}}" title="Podesta Desarrollo y Construcciones" alt="Podesta Desarrollo y Construcciones">
        </a>
      </div>

      <div class="col-xs-12 col-md-4">
        <div class="footer-contact">
					<h5 class="heading-6">{{ \Lang::get('site::footer.OFICINA_CENTRAL') }}</h5>
					<p><strong>D:</strong> Av. Sucre 1658, San Isidro, Buenos Aires</p>
					<p><strong>T:</strong> +54 11 4719-6100</p>
					<p><strong>M:</strong> <a href="mailto:podesta@pedropodesta.com.ar">podesta@pedropodesta.com.ar</a></p>
        </div>
				<div class="footer-contact">
					<h5 class="heading-6">{{ \Lang::get('site::footer.DEPÓSITO') }}</h5>
					<p><strong>D:</strong> Haití 4834, Tortuguitas, Buenos Aires</p>
					<p><strong>T:</strong> +54 11 4719-6100</p>
				</div>
      </div>

	    <div class="col-xs-12 col-md-2 hidden-sm-down">
	        <nav class="footer-nav">
	            <h5 class="heading-6">{{ \Lang::get('site::footer.MENU') }}</h5>
	            @include('site::partials/menu', array(
		            'slug'=>'main-menu',
		            'view'=>'site::partials/footer-menu-item',
		            'cssClass'=>''
	            ))
	        </nav>
	    </div>

	    <div class="col-xs-12 col-md-3">
			  @include('site::partials/footer-recent')
	    </div>

    </div>
	</div>
	<div class="copyright">
		<div class="container">
			{{-- <p class="pull-md-left">&copy; {{ \Lang::get('site::footer.Todos_los_derechos_reservados') }}.</p> --}}
			<p class="">Power by <a href="http://www.greentomato.com.ar/" target="_blank"><strong>GreenTomato</strong></a></p>
		</div>
	</div>
</footer>
