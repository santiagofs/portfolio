<?php
    $parsed = \Navigation::parse_item($item);
    extract($parsed);
?>
<li><a href="{{ $url }}" class="footer-nav__link {{ $is_current }}" target="{{ $item->target }}"><span class="menu-text">> {{ $item->name }}</span></a></li>
