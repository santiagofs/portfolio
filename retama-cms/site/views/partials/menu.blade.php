<?php
	isset($slug) || $slug = 'main-menu';
	$menu = \Navigation::translate()
		->with('sitemap')
		->where('slug','=',$slug)
		->where('parent_id','=',1)
		->first();

	if(!$menu) return null;

	$menu->branch();

	isset($view) || $view = 'site::partials/main-menu-item';

	isset($cssClass) || $cssClass = "nav navbar-nav hidden-sm-down";

?>
<ul class="{{ $cssClass }}">
	@foreach($menu->childs as $item)
	@include($view, array('item'=>$item, 'view'=>$view))
	@endforeach

</ul>
