<?php
    $obras = \Site\Models\Obra::orderBy('created_at','DESC')
        ->with('media')
        ->with('sitemap')
        ->take(8)
        ->get();
?>

<h5 class="heading-6">{{ \Lang::get('site::footer.OBRAS_RECIENTES') }}</h5>
<div class="footer-obras">
    @foreach($obras as $obra)
    <?php
        $media = $obra->media_assorted();
        $img =  $media->thumb();
    ?>
    <a href="{{ URL::to($obra->sitemap->path)}}" class="footer-obras__item" style="background-image: url({{ $img->url(70,70) }})"></a>
    @endforeach
</div>
