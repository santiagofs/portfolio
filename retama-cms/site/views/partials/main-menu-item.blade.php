<?php
	if(!$item->sitemap) {
		$is_home_link = false;
		$prefix = '';
		$is_current = false;
		$url = $item->url;
	} else {
		if(isset($parent)) {
			$is_home_link = false;
			$path_translated = \URL::translate($item->sitemap->path);

			$home_translated = \URL::translate('/');
			$prefix = \Language::url_prefix();

			$is_current = ( Request::is($path_translated .'*') || ($is_home_link && Request::is($home_translated))) ? 'active' : '';
			$url = $is_home_link ? URL::to('/') : URL::to($parent->sitemap->path).'#'.$item->sitemap->slug;

		} else {
			$is_home_link = $item->sitemap->is_homepage;
			$path_translated = \URL::translate($item->sitemap->path);

			$home_translated = \URL::translate('/');
			$prefix = \Language::url_prefix();

			$is_current = ( Request::is($path_translated .'*') || ($is_home_link && Request::is($home_translated))) ? 'active' : '';
			$url = $is_home_link ? URL::to('/') : URL::to($item->sitemap->path);

		}


	}
?>
<li class="nav-item {{ $item->has_children() ? 'with-children' : '' }} ">
	<a href="{{ $url }}" class="nav-link {{ $is_current }}" target="{{ $item->target }}"><span class="menu-text">{{ $item->name }}</span></a>
@if($item->has_children())
	<ul>
	@foreach($item->childs as $child)
		@include( 'site::partials/main-menu-item' , array('item'=>$child, 'parent'=>$item))
	@endforeach
	</ul>
@endif
</li>
