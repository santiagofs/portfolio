<?php
    $parsed = \Navigation::parse_item($item);
    extract($parsed);
?>
<li><a href="{{ $url }}" class="menu-mobile-link {{ $is_current }}" target="{{ $item->target }}">{{ $item->name }}</a></li>
