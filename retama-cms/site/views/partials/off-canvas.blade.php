<div class="menu-mobile hidden-md-up">
    <nav class="menu-mobile-nav">
        @include('site::partials/menu', array('slug'=>'off-canvas-menu', 'view'=>'site::partials/menu-mobile-item', 'cssClass' => ''))

         {{ \Language::language_selection('site::partials/mobile-language-selection') }}
    </nav>
</div>
