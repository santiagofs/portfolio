<nav class="navbar">
  <div class="navbar-sticky">
    <div class="container">

      <a class="navbar-brand" href="{{ URL::to('/') }}"><img src="{{asset('html/site/img/logo-podesta.png')}}" title="Podesta Desarrollo y Construcciones" alt="Podesta Desarrollo y Construcciones"></a>

      {{ \Language::language_selection('site::partials.language-selection') }}

      <!-- <div class="navbar-lang pull-xs-right hidden-sm-down">
        <a href="#" class="navbar-lang__item">
          <img src="{{asset('html/site/img/en.svg')}}" alt="" /> EN
        </a>
        <a href="#" class="navbar-lang__item active">
          <img src="{{asset('html/site/img/es.svg')}}" alt="" /> ES
        </a>
      </div> -->

      @include('site::partials/menu', array('slug'=>'main-menu', 'view'=>'site::partials/main-menu-item', 'cssClass' => 'nav navbar-nav pull-xs-right hidden-sm-down'))

    </div>
  </div>
</nav>

<button id="menu-mobile-trigger" class="hidden-md-up">
  <span></span><span></span>
</button>
