<div class="menu-mobile-lang">

    <a href="{{ URL::to($current_language->href,false) }}" class="menu-mobile-lang__item active">
		<img src="{{asset('html/site/img/'.$current_language->laravel_prefix.'.svg')}}" alt="" />
	</a>

	@foreach($arr_languages as $label => $language)
	<a href="{{ URL::to($language->href,false) }}" class="menu-mobile-lang__item">
		<img src="{{asset('html/site/img/'.$language->language->laravel_prefix.'.svg')}}" alt="" />
	</a>
	@endforeach

<!--
    <a href="#" class="menu-mobile-lang__item">
        <img src="{{asset('html/site/img/es.svg')}}" alt="" />
    </a>
    <a href="#" class="menu-mobile-lang__item">
        <img src="{{asset('html/site/img/en.svg')}}" alt="" />
    </a> -->
</div>
