<div class="navbar-lang pull-xs-right hidden-sm-down">

	<a href="{{ URL::to($current_language->href,false) }}" class="navbar-lang__item active">
		<img src="{{asset('html/site/img/'.$current_language->laravel_prefix.'.svg')}}" alt="" /> {{strtoupper($current_language->laravel_prefix)}}
	</a>
	
	@foreach($arr_languages as $label => $language)
	<a href="{{ URL::to($language->href,false) }}" class="navbar-lang__item">
		<img src="{{asset('html/site/img/'.$language->language->laravel_prefix.'.svg')}}" alt="" /> {{strtoupper($language->language->laravel_prefix)}}
	</a>
	@endforeach

</div>
