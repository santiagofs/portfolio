<div class="project-gallery-box">
    <div class="project-gallery">
        <?php $i=0 ?>
        @foreach($gallery as $img)
        <?php $big = ($i%3 === 0) || (count($gallery) - 1 === $i && $i%3 === 1) ? 'big' : ''; ?>
        <div class="project-gallery__item {{$big}}" style="background-image: url({{ $img->url() }})" data-gallery="{{ $img->id }}"></div>
        <?php $i++ ?>
        @endforeach
    </div>
</div>

<!-- GALLERY LIGHTBOX -->

<?php $gallerySlider = '{
  "arrows": true,
  "infinite": true,
  "slidesToShow": 1,
  "slidesToScroll": 1,
  "prevArrow" : "<button class=\"mdi mdi-chevron-left left\"></button>",
  "nextArrow" : "<button class=\"mdi mdi-chevron-right right\"></button>"
}'; ?>

<div class="lightbox" id="gallery-lightbox">
  <button class="lightbox__close mdi mdi-close"></button>
  <div class="lightbox__overlay">
    <div class="lightbox__content">
      <div class="project-gallery__slider" data-slick='{{$gallerySlider}}'>
        @foreach($gallery as $img)
          <div class="project-gallery__slider-item" style="background-image: url()" data-gallery-target="{{ $img->id }}">
            <img src="{{ $img->url() }}" />
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
