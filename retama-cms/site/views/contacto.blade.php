@extends('site::layout')


@section('page-content')
<main class="main-content">

  <section  class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <div class="contact-location" style="background-image: url({{ asset('html/site/img/office-bg.jpg') }})">
              <div class="contact-location__content">
                <h2 class="heading-3">{{ \Lang::get('site::contacto.Oficina_central')}}</h2>
                <p class="contact-location__data">
                  Av. Sucre 1658, San Isidro <br>
                  +54 11 4719-6100 <br>
                  <a href="mailto:podesta@pedropodesta.com.ar">podesta@pedropodesta.com.ar</a>
                </p>

                <a href="https://www.google.com/maps?ll=-34.483895,-58.558985&z=14&t=m&hl=es&gl=US&mapclient=embed&q=Av.+Sucre+1658+B1642AFR+San+Isidro+Buenos+Aires+Argentina" target="_blank" class="button-fill-white">GOOGLE MAPS</a>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-md-6">
            <div class="contact-location" style="background-image: url({{ asset('html/site/img/warehouse-bg.jpg') }})">
              <div class="contact-location__content">
                <h2 class="heading-3">{{ \Lang::get('site::contacto.Depósito')}}</h2>
                <p class="contact-location__data">
                  Haití 4834, Tortuguitas <br>
                  +54 11 4719-6100 <br>
                  <a href="mailto:deposito@pedropodesta.com.ar">deposito@pedropodesta.com.ar</a>
                </p>

                <a href="https://www.google.com/maps?ll=-34.453219,-58.705635&z=15&t=m&hl=es-ES&gl=AR&mapclient=embed&q=Hait%C3%AD+4834+Area+de+Promoci%C3%B3n+El+Tri%C3%A1ngulo+Buenos+Aires" target="_blank" class="button-fill-white">GOOGLE MAPS</a>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>

  <div class="join">
    <div class="join__content">
      <h2 class="join__title">{{ \Lang::get('site::contacto.trabaja_con_nosotros')}}</h2>
      <a class="button-fill-main button-send" href="mailto:rrhh@pedropodesta.com.ar">{{ \Lang::get('site::contacto.ENVIAR CV')}}</a>
    </div>
  </div>

</main>
@stop
