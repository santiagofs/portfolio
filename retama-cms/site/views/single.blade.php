@extends('site::page')
<?php
	$body_class = 'page single';
	
	$media_assorted = $content->media_assorted();
	$main_image = $media_assorted->one('main_image')->url();
	
?>
@section('page-content')
{{--
@include('components::hero-unit', array(
	'class' => 'full',
	'image'=> $main_image,
	'hero_text'=>$category->preview,
))
 --}}
<section class="section-layout">
	<div class="section-width">
		<article class="cms-article">
			<header class="section-header">
				<h1>{{$content->title}}</h1>
			</header>
		
			<div class="section-body" >
				
				<div class="section-width">
		
					{{$content->body}}
						
				</div>
			</div>
		</article>
	</div>
</section>


@stop



