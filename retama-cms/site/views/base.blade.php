<!DOCTYPE html>
<html lang="en" class="{{ \Browser::device() }}">
<head>

	<meta name="csrf-token" content="<?= csrf_token() ?>">

	@section('common-heads')
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	@show

	@yield('seo')

	@yield('head-scripts')

	<?php
		$now = '?v='.time();
	?>
	<!-- Fav Icon -->
	<link rel="shortcut icon" href="{{ asset('html/site/img/favicon.ico') }}{{$now}}">
	<link rel="apple-touch-icon" href="{{ asset('html/site/img/favicon.png') }}">
	<link rel="apple-touch-icon" sizes="128x128" href="{{ asset('html/site/img/favicon-128.png') }}{{$now}}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('html/site/img/favicon-144.png') }}{{$now}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('html/site/img/favicon-152.png') }}{{$now}}">

  <meta name="description" content="PEDRO PODESTA S.A es una de las compañías líderes de su sector, con una trayectoria que incluye emprendimientos como los edificios corporativos de BMW, Johnson & Johnson Medical, Glencore, Air Liquide y Edison OFFICIA, en Martínez, sede de empresas de primer nivel, tales como Toyota Argentina, CCU y A T&T, entre otras.">
  <meta property="og:locale" content="en_US">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Pedro Podestá S.A.">
  <meta property="og:description" content="PEDRO PODESTA S.A es una de las compañías líderes de su sector, con una trayectoria que incluye emprendimientos como los edificios corporativos de BMW, Johnson & Johnson Medical, Glencore, Air Liquide y Edison OFFICIA, en Martínez, sede de empresas de primer nivel, tales como Toyota Argentina, CCU y A T&T, entre otras.">
  <meta property="og:image" content="{{ asset('html/site/img/fb-share.jpg') }}">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">

</head>

<body id="{{ $body_id or '' }}" class="{{ $body_class or '' }}">

	@yield('fb-root')

	<!-- offcanvas menu -->
	@yield('site-off-canvas')
	<!-- end offcanvas menu -->

	<!-- site header -->
	@yield('site-header')
	<!-- end site header -->

	<!-- site body -->
	@yield('site-body')
	<!-- end site content -->

	@yield('site-footer')


	@yield('fixed-elements')

	@yield('footer-scripts')

</body>
</html>
