@extends('site::layout')
<?php
	$media_assorted = $content->media_assorted();
	$gallery =  $media_assorted->sluged('gallery');
	$fullSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
        "dots": true
    }';
?>
@section('page-content')

<main class="main-content">

    <section class="container">
        <header class="project-header">
          <h1 class="heading-2 project-header__title">{{ $content->nombre }}</h1>
          <p class="project-header__type">{{ $content->bajada }}</p>
        </header>

        <div class="breadcrumbs">
          <a class="breadcrumbs__item" href="{{ URL::to('/') }}">Home</a> &raquo; <a class="breadcrumbs__item" href="{{ URL::to('obras') }}">Obras</a> &raquo; <span class="breadcrumbs__item active">{{ $content->nombre }}</span>
        </div>
    </section>

    <section class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                @include('site::partials.gallery', array('gallery'=>$gallery))
            </div>

            <div class="col-xs-12 col-md-4 project-details__trail">
                <div class="project-details" data-scroll>
                    <h3 class="project-details__title">DETALLES DEL PROYECTO</h3>
                    @foreach($content->C1() as $c)
                    <div class="project-details__item">
                        <h4 class="project-details__item-name">{{ $c->label }}</h4>
                        <div class="project-details__item-data">{{ $c->value }}</div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="project-description">
                    <h3 class="project-description__title">DESCRIPCIÓN</h3>
                    <div class="project-description__text">{{ $content->descripcion }}</div>
                    <div class="project-description__items">
                        @foreach($content->C2() as $c)
                        <div class="project-description__item">{{ $c }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
      </section>

  <!-- OTROS DESARROLLOS -->

  <?php
  $otras = \Site\Models\Obra::orderBy('created_at','DESC')
	  ->whereNotIn('id', array($content->id))
	  ->with('media')
	  ->translate()
	  ->take(8)
	  ->get();
  ?>
  <section class="section">
    <header class="section-header">
      <h3 class="heading-3">OTRAS OBRAS</h3>
    </header>
	@include('site::partials.other-projects', array('otras'=>$otras, 'base'=>'obras'))

  </section>
  <!-- END OTROS DESARROLLOS -->

</main>

@stop


@section('footer-scripts')
@parent
<script type="text/javascript">
	var galleryLightboxOptions = {
		container: $('#gallery-lightbox')[0],
		trigger: $('[data-gallery]'),
		beforeOpen: function(e){
			var imgId = $(e.target).data('gallery');
			var slideId = $('[data-gallery-target="'+imgId+'"]').data('slick-index');
			$('.project-gallery__slider').slick('slickGoTo', slideId, true);
		}
	}
	var galleryLightbox = new Lightbox(galleryLightboxOptions);
</script>

<script type="text/javascript">
	$(document).on('scroll', function(){
		var $trail = $('.project-details__trail');
		var $details = $('.project-details');
		$details.width($trail.width() - 20)
		if (($trail.offset().top + $trail.outerHeight()) <= ($(document).scrollTop() + $details.outerHeight() + 100)) {
				$details.attr('data-scroll', 'scrolled'); // SEE ON ARTICLES CSS
		} else if ($trail.offset().top <= ($(document).scrollTop() + 100)) {
				$details.attr('data-scroll', 'scrolling');
		} else {
				$details.attr('data-scroll', '');
		}
	});
</script>
@stop
