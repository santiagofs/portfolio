@extends('site::layout')
<?php

	$media_assorted = $content->media_assorted();
	$gallery =  $media_assorted->sluged('gallery');
	$thumb = $media_assorted->thumb();
	$fullSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
				"autoplay": true,
				"fade": true,
        "dots": true
    }';

?>
@section('page-content')

<main class="main-content">
	<header>
		<div class="featured-slider" data-slick='<?php echo $fullSlider; ?>'>
			<div class="featured-slider__item" style="background-image: url({{$thumb->url()}})"></div>
		</div>
	</header>

	<section class="section">
		<div class="container">
			<header class="section-header">
				<h3 class="heading-3">{{ $content->title }}</h3>
				<p class="section-header__subtitle">{{ $content->preview }}</p>
			</header>

			<div  class="about-history">
			{{ $content->body }}
			</div>
		</div>
	</section>


	<section class="full-image-section" style="background-image: url({{asset('html/site/img/records-bg.jpg')}})">
		<div class="full-image-section-overlay">
			<header class="section-header section-header_white">
				<h3 class="heading-3">{{ \Lang::get('site::content.MAPA_DE_ANTECEDENTES') }}</h3>
			</header>
			<div class="sp30"></div>
			<a href="{{ URL::to('mapa') }}" class="button-fill-white">{{ \Lang::get('site::content.VER_MAPA') }}</a>
		</div>
	</section>
{{--
	<hr class="divider-gradient">

	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-lg-10 col-lg-push-1 col-xl-8 col-xl-push-2">
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<div class="about-contact">
								<i class="mdi mdi-map-marker"></i>
								<h5 class="about-contact__title">{{ \Lang::get('site::content.DIRECCIÓN') }}</h5>
								<p class="about-contact__data">
								Av. Sucre 1658, San Isidro <br>
								Buenos Aires
								</p>
							</div>
						</div>
						<div class="col-xs-12 col-md-4">
							<div class="about-contact">
								<i class="mdi mdi-phone"></i>
								<h5 class="about-contact__title">{{ \Lang::get('site::content.TELÉFONO') }}</h5>
								<p class="about-contact__data">
								4719-6100
								</p>
							</div>
						</div>
						<div class="col-xs-12 col-md-4">
							<div class="about-contact">
								<i class="mdi mdi-email"></i>
								<h5 class="about-contact__title">{{ \Lang::get('site::content.EMAIL') }}</h5>
								<p class="about-contact__data">
								<a href="mailto:podesta@pedropodesta.com.ar">podesta@pedropodesta.com.ar</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}

  <hr class="divider-gradient">

  <!-- OTROS DESARROLLOS -->
  <?php $projectSliders = '{
  		"arrows": false,
  		"infinite": false,
  		"slidesToShow": 5,
      "responsive": [
        {
          "breakpoint": 991,
          "settings": {
        		"infinite": true,
        		"slidesToShow": 2,
            "centerMode": true,
            "centerPadding": "calc(50% - 260px)"
          }
        },
        {
          "breakpoint": 767,
          "settings": {
        		"infinite": true,
        		"slidesToShow": 1,
            "centerMode": true,
            "centerPadding": "calc(100% - 260px) 0 0"
          }
        }
      ]
  }'; ?>

	<?php
		$desarrollos = \Site\Models\Desarrollo::orderBy('created_at','ASC')
		  ->with('media')
		  ->with('sitemap')
		  ->translate()
		  ->take(8)
		  ->get();
	?>
	<section class="section">
		<header class="section-header">
			<h3 class="heading-3">{{ \Lang::get('site::footer.ÚLTIMOS_PROYECTOS') }}</h3>
		</header>

		<div class="projects-row" data-slick='<?php echo $projectSliders; ?>'>
			@foreach($desarrollos as $desarrollo)
	        <?php
	            $media = $desarrollo->media_assorted();
	            $img =  $media->thumb();
	        ?>
			<a href="#" class="projects-row__item">
				<div class="projects-row__item-img" style="background-image: url({{ $img->url(340,340) }})"></div>
				<div class="projects-row__item-info">
					<h5 class="projects-row__item-title">{{ $desarrollo->nombre }}</h5>
					<p class="projects-row__item-description">{{ $desarrollo->bajada }}</p>
				</div>
			</a>
			@endforeach
		</div>
	</section>
  <!-- END OTROS DESARROLLOS -->
</main>


@stop
