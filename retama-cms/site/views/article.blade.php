@extends('site::page')
<?php
	$body_class = 'page article';
?>

@section('main-content')
<article class="main-content" id="main-content">
	$content->body
</article>
@stop