@extends('retama::edit.edit')

@section('form-header')

@stop

<?php
	$view = 'international::retama.modules.field';
?>
@section('form')

{{ Form::model($translation, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			Translate Categoría: {{ $original->nombre }}
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}

		@include($view, array(
			'name' => 'nombre',
			'label' => 'Nombre',
			'original' => $original,
			'input' => Form::text('nombre', null, array('class'=>'form-control required'))
		))

	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop
