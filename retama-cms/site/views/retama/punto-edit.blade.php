@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => URL::full(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<ul class="nav nav-pills">
			<li role="presentation" class="active"><a href="#tab-main" data-toggle="tab">Principal</a></li>
		</ul>
		<h2>
			@if($model->id)
			Editar Punto:
			@else
			Nuevo Punto
			@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}
		{{ Form::hidden('sitemap[parent_id]') }}

		<div class="tab-content">
			<div id="tab-main" class="tab-pane active">


	    	{{ Form::one('text', 'title', 'Título', null, array('class'=>'form-control')) }}

			{{ Form::one('select', 'category', 'Categoria', null, array('class'=>'form-control','list'=>[
				'rural' => 'Rural',
				'officia' => 'Officia',
				'podesta' => 'Podesta',
			]))}}

			{{ Form::one('select', 'province', 'Provincia', null, array('class'=>'form-control','list'=>[
				'caba' => 'Ciudad de Buenos Aires',
				'gba' => 'Gran Buenos Aires',
				'buenos_aires' => 'Buenos Aires',
				'catamarca' => 'Catamarca',
				'chaco' => 'Chaco',
				'chubut' => 'Chubut',
				'cordoba' => 'Córdoba',
				'corrientes' => 'Corrientes',
				'entre_rios' => 'Entre Ríos',
				'formosa' => 'Formosa',
				'jujuy' => 'Jujuy',
				'la_pampa' => 'La Pampa',
				'la_rioja' => 'La Rioja',
				'mendoza' => 'Mendoza',
				'misiones' => 'Misiones',
				'neuquen' => 'Neuquen',
				'rio_negro' => 'Río Negro',
				'salta' => 'Salta',
				'san_juan' => 'San Juan',
				'san_luis' => 'San Luis',
				'santa_cruz' => 'Santa Cruz',
				'santa_fe' => 'Santa Fe',
				'santiago_del_estero' => 'Santiago del Estero',
				'tierra_del_fuego' => 'Tierra del Fuego',
				'tucuman' => 'Tucuman',
			]))}}

    		{{ Form::one('text', 'type', 'Tipo', null, array('class'=>'form-control')) }}

            {{ Form::one('tinymce', 'description', 'Descripción', null, array('class'=>'form-control')) }}

            </div>

        </div>

	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop

@section('footer-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAa6dalIlsnnZdnEk3ZYiFuW3sv6D0-3ls&libraries=places"
  type="text/javascript"></script>
@parent


@stop
