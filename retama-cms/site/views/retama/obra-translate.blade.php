@extends('retama::edit.edit')

@section('form-header')

@stop

<?php
	$view = 'international::retama.modules.field';
?>
@section('form')

{{ Form::model($translation, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			Translate Obra: {{ $original->nombre }}
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}

		@include($view, array(
			'name' => 'nombre',
			'label' => 'Nombre',
			'original' => $original,
			'input' => Form::text('nombre', null, array('class'=>'form-control required'))
		))

		@include($view, array(
			'name' => 'bajada',
			'label' => 'Bajada',
			'original' => $original,
			'input' => Form::textarea('bajada', null, array('class'=>'form-control'))
		))

		@include($view, array(
			'name' => 'descripcion',
			'label' => 'Descripción',
			'original' => $original,
			'input' => Form::textarea('descripcion', null, array('class'=>'form-control', 'tinymce'=>true, 'id'=>'id-descripcion'))
		))

        @include($view, array(
			'name' => 'caracteristicas',
			'label' => 'Caracterísitcas',
			'original' => $original,
			'input' => Form::textarea('caracteristicas', null, array('class'=>'form-control'))
		))

		@include($view, array(
			'name' => 'detalles',
			'label' => 'Detalles',
			'original' => $original,
			'input' => Form::textarea('caracteristicas2', null, array('class'=>'form-control'))
		))




	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop
