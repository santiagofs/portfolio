@extends('retama::edit.edit')

@section('form-header')

@stop

<?php
	$view = 'international::retama.modules.field';
?>
@section('form')

{{ Form::model($translation, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			Translate Punto: {{ $original->title }}
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}

		@include($view, array(
			'name' => 'title',
			'label' => 'Título',
			'original' => $original,
			'input' => Form::text('title', null, array('class'=>'form-control required'))
		))

		@include($view, array(
			'name' => 'type',
			'label' => 'Tipo',
			'original' => $original,
			'input' => Form::text('type', null, array('class'=>'form-control'))
		))

		@include($view, array(
			'name' => 'description',
			'label' => 'Descripción',
			'original' => $original,
			'input' => Form::text('description', null, array('class'=>'form-control'))
		))

	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop
