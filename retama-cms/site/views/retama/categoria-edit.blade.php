@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => URL::full(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<ul class="nav nav-pills">
			<li role="presentation" class="active"><a href="#tab-main" data-toggle="tab">Principal</a></li>
			<li role="presentation"><a href="#tab-gallery" data-toggle="tab">Imágenes</a></li>
			<li role="presentation"><a href="#tab-seo" data-toggle="tab">SEO</a></li>
		</ul>
		<h2>
			@if($model->id)
			Editar Categoría:
			@else
			Nueva Categoría
			@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}
		{{ Form::hidden('sitemap[parent_id]') }}

		<div class="tab-content">
			<div id="tab-main" class="tab-pane active">

    		<?php
    			$attr = array('class'=>'form-control required');
				if(!$model->id) {
					$attr['slugify-to'] = '#sitemap-slug';
					$attr['copy-to'] = '#sitemap-title';
				}
				$attr['copy-to'] = '#sitemap-name';
    		?>

    		{{ Form::one('text', 'nombre', 'Nombre', null, $attr) }}

            </div>

            <div id="tab-gallery" class="tab-pane">
            @include('media::gallery', array('name'=>'gallery', 'with_embeds'=>false, 'gallery_items'=>$model->media_assorted()->sluged('gallery')->toJson(JSON_HEX_APOS | JSON_HEX_QUOT)))
            </div>

			<div id="tab-seo" class="tab-pane">
			@include('content::sitemap-fields')
			</div>

        </div>



	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop

@section('footer-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAa6dalIlsnnZdnEk3ZYiFuW3sv6D0-3ls&libraries=places"
  type="text/javascript"></script>
@parent


@stop
