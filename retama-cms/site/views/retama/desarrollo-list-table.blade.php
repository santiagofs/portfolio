<div class="records-list-holder rounded-box">
	<table cellpadding="0" cellspacing="0" class="records-list">
		<thead>
			<tr>
				<td colspan="{{ $list->column_count()+2 }}">
					<div class="clear-block">
						@include('lists::pagination')
						<div style="float:right; margin-right:20px; line-height:34px;"> Mostrando {{$list->records()->getFrom()}} a {{$list->records()->getTo()}} de {{$list->records()->getTotal()}}</div>
						@include('lists::actions')
					</div>
				</td>
			</tr>
			<tr>
				@if($list->sortable)
				<th class="sort-grip"></th>
				@endif
				<th class="select-item" style="cursor: pointer;"><input type="checkbox" name="check_all" class="check-all"></th>

				<th></th> {{-- image --}}
				{{ $list->headers() }}
				<th>Categoría</th>
				<th class="actions"></th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<td colspan="{{ $list->column_count() +2 }}">
					<div class="clear-block">
						@include('lists::pagination')
						<div style="float:right; margin-right:20px; line-height:34px;"> Mostrando {{$list->records()->getFrom()}} a {{$list->records()->getTo()}} de {{$list->records()->getTotal()}}</div>
						@include('lists::actions')
					</div>
				</td>
			</tr>
		</tfoot>

		<tbody>
			@foreach($list->records() as $record)

			<tr>
				@if($list->sortable)
				<td class="sort-grip"><a class="fa fa-arrows-v"></a></td>
				@endif
				<td class="select-item"><input type="checkbox" class="check-item" name="id[]" value="{{ $record->id }}" /></td>
				<td>
				<?php
					$image = $record->media_assorted()->one('main_image');
				?>
					<a href="{{ $list->get_edit_url($record->id) }}" title="{{ \Lang::get('retama::retama.create') }}">{{ $image->img(80,60)}}</a>
				</td>
				@foreach($list->field_names as $name)
				<td class="{{ $name }}">{{ $list->field_value($name, $record) }}</td>
				@endforeach
				<td>
					{{ $categories[$record->category_id] }}
				</td>
				<td class="actions">
				@if($list->creatable)
				<a href="{{ $list->get_edit_url($record->id) }}" class="fa fa-edit" title="{{ \Lang::get('retama::retama.create') }}"></a>
				@endif

				@if($list->deletable)
				<a href="{{ $list->get_delete_url($record->id) }}" class="fa fa-times" delete-item title="{{ \Lang::get('retama::retama.delete_item') }}"></a>
				@endif

			</tr>
			@endforeach
		</tbody>
	</table>
</div>


@section('footer-scripts')
@parent
<script>
var a=1
</script>
@stop
