<?php
	if(\App::environment() != 'production'):
		error_reporting(E_ALL);
	endif;

	//$file_prefix = \Retama::file_prefix();
	//$body_class = 'overlap-header';
	$file_prefix = '-min';
?>
@extends('site::base')

@section('seo')
@if(isset($nav))
<?php
	$seo = \Seo::forge($nav);
?>
	<title>{{ $seo->get_title('','', 'ignore') }}</title>
	<meta name="description" content="{{ $seo->description }}">
    <meta name="keywords" content="{{ $seo->keywords }}">
@endif
@stop

@section('head-scripts')

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- <link rel="stylesheet" href="{{ asset('/html/site/roboto.local.css') }}" /> -->
	<link rel="stylesheet" href="{{ asset('/html/site/slick.css') }}" />
	<link rel="stylesheet" href="{{ asset('/html/site/app.css') }}" />
	<link rel="stylesheet" href="{{ asset('/html/site/materialdesignicons.min.css') }}" />


<!--
	<style>
	@include('site::atf-min')
	</style>
-->

@stop

@section('site-off-canvas')
	@include('site::partials.off-canvas')
@stop

@section('site-header')
	@include('site::partials.header')
@stop

@section('site-body')
	@yield('page-content')
@stop

@section('site-footer')
	@include('site::partials.footer')
@stop

@section('site-layout')
	@parent



	{{-- @include('retama::partials.modal') --}}
@stop

@section('footer-scripts')

<script src="{{ asset('/html/site/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/html/site/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/html/site/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/html/site/slick.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/html/site/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	/*-------------*/
	/* MENU MOBILE */
	/*-------------*/
	var $html = $('html');

	$('#menu-mobile-trigger').on('click', function(e){
			$html.toggleClass('menu-mobile-on');
	});

	$('.navbar-lang').on('click', function(e){
			e.stopPropagation();
			$(this).toggleClass('active');
	});

	$(document).on('click', function(e){
		$('.navbar-lang').removeClass('active');
	});

	/*------------------*/
	/* FORMS VALIDATION */
	/*------------------*/
	$.extend($.validator.messages, {
			required: "Este campo es obligatorio.",
			email: "Ingresa un email válido.",
			equalTo: "Please enter the same value again.",
	});

	$('.form').validate({
		errorClass: 'form-error-label',
		errorPlacement: function(error, element) {
			$(element).parent().after(error);
		},
		highlight: function(element) {
			$(element).parent().addClass("error");
		},
		unhighlight: function(element) {
			$(element).parent().removeClass("error");
		}
	});

  /*---------------*/
  /* SLICK SLIDERS */
  /*---------------*/
  $(window).load(function() {
    $('[data-slick]').each(function(){
      var slick_options = !!$(this).data('slick') ? $(this).data('slick') : {};
      $(this).slick(slick_options);
    });
  });

	/*----------*/
	/* lIGHTBOX */
	/*----------*/
	function Lightbox (options) {
    this.init(options);
  }

  Lightbox.prototype.open = function(e) {
		this.beforeOpen.call(this, e);
    document.documentElement.classList.add('lightbox-on');
    this.container.classList.add('lightbox_show');
  };

  Lightbox.prototype.close = function(e) {
		this.beforeClose.call(this, e);
    document.documentElement.classList.remove('lightbox-on');
    this.container.classList.remove('lightbox_show');
  };

  Lightbox.prototype.init = function(options) {
    for (var property in options) { this[property] = options[property]; }

    this.trigger = this.trigger instanceof Element ? [this.trigger] : this.trigger;

    this.closeBtn = this.container.getElementsByClassName('lightbox__close');
    this.overlay = this.container.getElementsByClassName('lightbox__overlay')[0];

    var that = this;

		for(var i = 0; i < that.trigger.length; i++){
			if (that.trigger[i] instanceof Element) {
				that.trigger[i].addEventListener('click', function(e){
					that.open.call(that, e);
				});
			}
		}

		for(var i = 0; i < that.closeBtn.length; i++){
			if (that.closeBtn[i] instanceof Element) {
				that.closeBtn[i].addEventListener('click', function(e){
					that.close.call(that, e);
				});
			}
		}


    that.overlay.addEventListener('click', function(e){
      if(e.target === this) {
        that.close();
      }
    });
  };

  Lightbox.prototype.beforeOpen = function() {};
  Lightbox.prototype.beforeClose = function() {};

	$('[data-lightbox]').each(function(e){
    var trigger = this,
        lightboxName = $(this).data('lightbox') + '-lightbox',
        element = document.getElementById(lightboxName),
        myLightbox = new Lightbox({ container: element, trigger: trigger });
	});
</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-83683936-1', 'auto');
	ga('send', 'pageview');

</script>

	<?php
		if(\App::environment() != 'production'):
	?>

	<?php
		endif;
	?>
@stop
