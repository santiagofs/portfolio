@extends('site::layout')

@section('page-content')
<main class="main-content map" id="map">

</main>

<div class="map-offcanvas">
  <div class="map-offcanvas__content">
    <header class="map-offcanvas__header" style="display: none;">
      <h5 class="map-offcanvas__header-location"></h5>
      <h6 class="map-offcanvas__header-category"></h6>
    </header>
    <div class="map-markers">
    </div>
  </div>

  <button class="map-offcanvas__toggle" style="display: none;">
    <i class="mdi mdi-format-list-bulleted"></i>
  </button>

</div>

@stop

@section('footer-scripts')
@parent
<script>
  var map;
  function initialize() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: new google.maps.LatLng(-36.06395559466041, -63.687744140625),
      mapTypeId: 'roadmap',
      styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"weight":"2"},{"color":"#004d9b"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#004d9b"},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#ededed"}]},{"featureType":"landscape.natural.terrain","elementType":"labels.text.fill","stylers":[{"color":"#ff0000"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#92d0e9"},{"visibility":"on"}]}]
    });

    var provincias = {
      caba:               { name: 'CABA', lat: -34.6064588, lng: -58.4354786 },
      gba:                { name: 'GBA', lat: -34.670717, lng: -58.5785153 },
      buenos_aires:       { name: 'Buenos Aires', lat: -36.323977120112616, lng: -60.084228515625 },
      catamarca:          { name: 'Catamarca', lat: -27.244862521497268, lng: -66.9287109375 },
      chaco:              { name: 'Chaco', lat: -26.52956523826758, lng: -60.62255859375 },
      chubut:             { name: 'Chubut', lat: -43.937461690316646, lng: -68.466796875 },
      cordoba:            { name: 'Córdoba', lat: -32.06395559466041, lng: -63.687744140625 },
      corrientes:         { name: 'Corrientes', lat: -28.758028282691118, lng: -57.744140625 },
      entre_rios:         { name: 'Entre Ríos', lat: -32.15701248607009, lng: -59.26025390625 },
      formosa:            { name: 'Formosa', lat: -24.95618002005594, lng: -59.87548828125 },
      jujuy:              { name: 'Jujuy', lat: -22.88756221517448, lng: -65.98388671875 },
      la_pampa:           { name: 'La Pampa', lat: -37.151560502236656, lng: -65.0390625 },
      la_rioja:           { name: 'La Rioja', lat: -29.602118211647323, lng: -66.9287109375 },
      mendoza:            { name: 'Mendoza', lat: -34.66935854524542, lng: -68.48876953125 },
      misiones:           { name: 'Misiones', lat: -26.89267909590814, lng: -54.437255859375 },
      neuquen:            { name: 'Neuquén', lat: -38.43637960299999, lng: -69.642333984375 },
      rio_negro:          { name: 'Río Negro', lat: -40.02761443748664, lng: -67.060546875 },
      salta:              { name: 'Salta', lat: -24.76678452287443, lng: -63.863525390625 },
      san_juan:           { name: 'San Juan', lat: -31.0341083449035, lng: -68.84033203125 },
      san_luis:           { name: 'San Lius', lat: -33.84304531474468, lng: -65.9619140625 },
      santa_cruz:         { name: 'Santa Cruz', lat: -48.34894812401374, lng: -69.510498046875 },
      santa_fe:           { name: 'Santa Fe', lat: -31.156408414557, lng: -60.9521484375 },
      santiago_del_estero:{ name: 'Santiago del Estero', lat: -27.673798957817617, lng: -63.336181640625 },
      tierra_del_fuego:   { name: 'Tierra del Fuego', lat: -54.3357439553007, lng: -67.642822265625 },
      tucuman:            { name: 'Tucumán', lat: -26.980828590472097, lng: -65.357666015625 }
    }

    var proyectos = {{ json_encode(\Site\Models\Punto::sorted(), JSON_PRETTY_PRINT) }}

    var categories = {
      rural: {
        name: 'Agro'
      },
      officia: {
        name: 'Officia'
      },
      podesta: {
        name: 'Pedro Podestá'
      }
    };

    for(var provincia in proyectos) {
      var i = 1;
      for(var categoria in proyectos[provincia]) {
        var $category_list = $('<div class="map-markers__category" data-provincia="'+provincia+'" data-categoria="'+categoria+'"></div>');
        $('.map-markers').append($category_list);
        $category_list.hide();

        var offset = provincia === 'caba' || provincia === 'gba' ? 0.04 : 0.4;
        if (i === 1) {
          var position = new google.maps.LatLng(provincias[provincia].lat, provincias[provincia].lng - offset);
        } else if (i === 2) {
          var position = new google.maps.LatLng(provincias[provincia].lat, provincias[provincia].lng + offset);
        } else {
          var position = new google.maps.LatLng(provincias[provincia].lat - (offset*2), provincias[provincia].lng);
        }

        (function (provincia, categoria) {
          var marker = new google.maps.Marker({
            position: position,
            icon: '{{ asset("html/site/img/pin_'+categoria+'.svg") }}',
            map: map
          });

          marker.addListener('click', function(){
            $('.map-markers__category').hide();
            $('.map-markers__item-description').hide();
            $('.map-offcanvas__header-location').text(provincias[provincia].name);
            $('.map-offcanvas__header-category').text(categories[categoria].name);
            $('.map-offcanvas__header').show();
            $('.map-offcanvas__toggle').show();
            $('.map-markers__category[data-provincia="'+provincia+'"][data-categoria="'+categoria+'"]').show();

            $('html').addClass('map-offcanvas_active');
          });
        })(provincia, categoria);

        for(var j = 0; j < proyectos[provincia][categoria].length; j++) {
          var $list_item = $('<div class="map-markers__item"><h5 class="map-markers__item-title">'+proyectos[provincia][categoria][j].title+'</h5><div class="map-markers__item-type">'+proyectos[provincia][categoria][j].type+'</div><div class="map-markers__item-description" style="display: none">'+proyectos[provincia][categoria][j].description+'</div></div>');
          $category_list.append($list_item);
          $list_item.on('click', function(){
            $(this).find('.map-markers__item-description').slideToggle();
          });
        }

        i++;
      }
    }
  }

  $('.map-offcanvas__toggle').on('click', function(){
      $('html').toggleClass('map-offcanvas_active');
  });

  //if($(window).width() > 768) { $('html').toggleClass('map-offcanvas_active'); }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhQmDAhdD36OiHzJiuZHYdqZsvvySVlfc&callback=initialize"></script>
@stop
