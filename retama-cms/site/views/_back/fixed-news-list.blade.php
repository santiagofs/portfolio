@extends('site::page')
<?php
	$body_class = 'page team';

	$gallery = $content->media_assorted()->sluged('gallery');

	$news = \News::whereHas('sitemap', function($q) use($nav) {
		$q->where('parent_id', 33);
	});
	$news->translate();

	$news->with('media')
		->orderBy('publish_date','DESC')
		->where('publish','=',1);


	$records = $news->paginate(3);


?>
@section('main-content')
<section class="main-content">

	<div class="list news-list">
		@foreach($records as $item)
		@include('site::partials.news-list-item', array('item'=>$item, 'is_list'=>true))
		@endforeach
	</div>
	<div class="pagination-control padding-control">
		{{ $records->links() }}
	</div>

</section>
@stop