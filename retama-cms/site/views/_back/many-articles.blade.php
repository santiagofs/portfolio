<?php

	$body_class = 'page many-articles';
	$articles = array();
	
	foreach($nav->children as $child) {
		
		$child_class = $child->sitemapable_type;
		$child_content = new stdClass;
		if($child_class)
		{
			$child_content = $child_class::translate();
			if( $child_class::is_mediable() ) {
				$child_content = $child_content->WithMedia();
			}
			$child_content = $child_content->where('id','=', $child->sitemapable_id)->first();
		}
		$articles[] = $child_content;	
	}
?>
@extends('site::layout')

@section('page-content')
	<section class="section-layout cms-articles">
	<div class="section-width">
		
			<header class="section-header">
				<h1>{{$nav->name}}</h1>
			</header>
		
	</div>
	<div class="section-body" accordion>
		
		@foreach($articles as $article)
		<?php
			$media_assorted = $article->media_assorted();
			$main_image = $media_assorted->one('main_image')->url();
			$main_image || $main_image = '/html/site/img/dummy-hero.jpg';
			$gallery = $media_assorted->sluged('gallery');
			$gallery_srcs = array();
			$gallery_texts = array();
			
			foreach($gallery as $img) {
				$gallery_srcs[] = $img->url(940);
				$gallery_texts[] = $img->description;
			}
			$gallery_srcs = implode(',', $gallery_srcs);
			$gallery_texts = implode('|', $gallery_texts);
		?>
		
		<article class="cms-article" panel id="{{$article->sitemap->slug}}">
			
			<header class="article-header" toggle-panel style="background-image: url({{$main_image}})">
				<div class="article-header-overlay"></div>
				<div class="section-width">
					@if( count($gallery) && ($article->sitemap->layout != 'team') )
						<a href="#" open-modal="<?php echo($gallery_srcs); ?>" texts="{{$gallery_texts}}" class="btn"><i class="fa fa-camera"></i>Ver Galería</a>
					@endif
					<h2>{{$article->title}}</h2>
				</div>
			</header>
			<div class="section-width " collapse>
				<div collapse-content class="article-body">
				@if($article->sitemap->layout != 'article')
					@include('site::'.$article->sitemap->layout, array('content'=>$article))
				@else
					{{ $article->body }}
				@endif
					
				</div>
			</div>
		</article>
		
		@endforeach
		
	</div>
</section>
@stop




