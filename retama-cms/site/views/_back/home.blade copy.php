@extends('site::layout')
<?php
	$body_id = 'home';

	$media_assorted = $content->media_assorted();
	$main_image = $media_assorted->one('main_image')->url();

	$main_image || $main_image = '/html/site/img/dummy-hero.jpg';

?>
@section('page-content')

<!--
@include('components::hero-unit', array(
	'class' => 'full',
	'image'=>$main_image,
	'hero_text'=>$content->preview,
	'cta' => array(
		'content' => 'Reproducir Video',
		//'trigger-video' => true,
		'data-fancybox' => 'obrainsa',
		'url' => 'https://www.youtube.com/watch?v=xqFo59YveXo'
	)
))
-->



<?php
	$slides = \Widget::get('home-slider');
?>
@include('components::slider-hero', ['slides'=>$slides, 'show_arrows'=>true])



<section class="content page-content home-content">
	<div class="width-control">

		<div class="column-control">

			<div class="widget news-widget">
				<header class="widget-header">
					<div class="control slider-control"><a class="slider-previous fa fa-angle-left"></a><a class="slider-next fa fa-angle-right"></a></div>
					<h3>Novedades</h3>
				</header>

				<div class="widget-content">


					<div class="owl-carousel" id="news-widget-owl">
					<?php
						$news = \News::with('sitemap');
						$news->translate();
						$news->with('media')
							->orderBy('publish_date','DESC')
							->where('publish','=',1);

						$records = $news->paginate(3);
					?>
						@foreach($records as $item)
						@include('site::partials.news-list-item', array('item'=>$item, 'is_list'=>true))
						@endforeach

					</div>

				</div>
			</div>

			<div class="widget points-widget">
				@include('components::hero-unit', array(
					'image'=>'/html/site/img/mapa.svg',
					'cta' => array(
						'content' => 'Puntos de acción',
						'url' => URL::to('organizacion-obrainsa/nuestros-puntos-de-accion')
					)
				))
			</div>

		</div>

	</div>
</section>

@stop


@section('footer-scripts')
@parent
<script type="text/javascript">


	$("[data-fancybox]").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers : {
			media : {}
		}
	});


	$(document).ready(function() {
		$("#news-widget-owl").owlCarousel({
			items:1

		});

		var owl = $("#news-widget-owl").data('owlCarousel');
		$('.news-widget .slider-previous').on('click', function(e) {
			e.preventDefault();
			owl.prev();
		});
		$('.news-widget .slider-next').on('click', function(e) {
			e.preventDefault();
			owl.next();
		});

    });
</script>
@stop

