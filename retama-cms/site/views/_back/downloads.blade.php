@extends('site::page')
<?php
	$body_class = 'page downloads';

	$downloads = \Downloads::all();
?>
@section('main-content')
<article class="main-content">
	<ul class="download-list">
	@foreach($downloads as $item)
	<?php
		$item->media;
		$download = $item->media_assorted()->one('download');
		$thumb = $item->media_assorted()->one('main_image');
	?>
		<li>
			<a href="{{ $download->download() }}">

				<div class="thumb">{{ $thumb->img(90,110) }}</div>
				<div class="text-control">

					<h2>{{ $item->title }}</h2>
					<p>{{ $item->description }}</p>
					<span class="icon"><span class="text">Descargar PDF</span> <i class="fa fa-arrow-down"></i></span>
				</div>
			</a>
		</li>
	@endforeach
	</ul>
</article>
@stop