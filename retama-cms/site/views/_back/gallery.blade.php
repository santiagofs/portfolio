@extends('site::page')
<?php
	$body_id = 'gallery';
	$body_class = 'page article';
?>
@section('main-content')
<article class="main-content">
@include('components::mosaico', array('media' => $nav->sitemapable->media_assorted()->sluged('gallery'), 'width'=>222))
</article>
@stop

@section('footer-scripts')
@parent
<script type="text/javascript">


	$("[data-fancybox]").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers : {
			media : {}
		}
	});

</script>
@stop