<?php
	$body_class = 'page team';

	$gallery = $content->media_assorted()->sluged('gallery');

	$news = \News::whereHas('sitemap', function($q) use($nav) {
		$q->where('parent_id', $nav->id);
	});
	$news->translate();

	$news->with('media')
		->orderBy('publish_date','DESC')
		->where('publish','=',1);


	$records = $news->paginate(1000);
	


?>
@extends('site::layout')

@section('page-content')
<section class="section-layout cms-articles">
	<div class="section-width">
		<header class="section-header">
			<h1>Noticias OBRAINSA</h1>
		</header>	
	</div>
	<div class="section-body news-list" >
		
		<div class="section-width">
			<div class="">
				@foreach($records as $item)
				@include('site::partials.news-list-item', array('item'=>$item, 'is_list'=>true))
				@endforeach
		
			</div>
			<div class="news-list-actions"  style="display: none">
				<div class="pagination-control padding-control" style="display: none">
					{{ $records->links() }}
				</div>
				<a class="btn btn-primary">CARGAR MÁS NOTICIAS</a>
			</div>
		</div>
	</div>

</section>


@stop