<?php
	$body_class = 'page form';

	$parent_nav = $nav->parent;

	$menu = \Navigation::get_menu($parent_nav->slug, 'content::menu', 'site::partials.menu-node');
?>
@extends('site::layout')

@section('page-content')

<section class="section-layout">
	<div class="section-width">
		
			<header class="section-header">
				<h1>{{$nav->name}}</h1>
			</header>
		
	</div>
</section>
<style>

</style>
<section class="section-layout"  id="contacto-form">
	<div class="section-width">

		<div class="form-holder" id="form-holder">
		

			{{ \Form::open(array('url' => 'postula-send', 'id'=>'form-contact', 'class'=>'clearfix')) }}
			
			<div class="left-col">
				{{ Form::one('text', 'nombres', \Lang::get('site::postula.nombres'), null, array('class'=>'form-control required')) }}
	
				{{ Form::one('text', 'apellido', \Lang::get('site::postula.apellidos'), null, array('class'=>'form-control required')) }}
			<div class="clearfix dni-holder">
				{{ Form::one('select', 'dni_tipo', \Lang::get('site::postula.dni_tipo'), null, array('class'=>'form-control', 'list'=>array('DNI'=>'DNI', 'CE'=>'CE', 'Otro' => 'Otro')))}}
				{{ Form::one('text', 'dni', \Lang::get('site::postula.dni'), null, array('class'=>'form-control required number')) }}
			</div>
				{{ Form::one('email', 'email', \Lang::get('site::postula.email') , null, array('class'=>'form-control email required ')) }}
	
				{{ Form::one('text', 'grado_academico', \Lang::get('site::postula.grado_academico'), null, array('class'=>'form-control required')) }}
			</div>
			<div class="right-col">
				{{ Form::one('text', 'puesto_actual', \Lang::get('site::postula.puesto_actual'), null, array('class'=>'form-control required')) }}
				
				{{ Form::one('text', 'disponibilidad', \Lang::get('site::postula.disponibilidad'), null, array('class'=>'form-control required')) }}
				
				{{ Form::one('text', 'puesto_postula', \Lang::get('site::postula.puesto_postula'), null, array('class'=>'form-control required')) }}		
				
				{{ Form::one('text', 'expectativa_salarial', \Lang::get('site::postula.expectativa_salarial'), null, array('class'=>'form-control required')) }}	
				<div class="uploader-control">
					
					{{ Form::one('file', 'cv', \Lang::get('site::postula.adjunta_tu_cv'), null, array('class'=>'form-control', 'uploader'=>array('button_text'=>'Seleccionar'))) }}
	
				
				</div>
			</div>

			<div class="form-actions">
				{{ Form::submit('Enviar', array('id'=>'form-submit', 'class'=> 'btn btn-primary')) }}
			</div>


			{{ \Form::close(); }}
		</div>

		<div class="response-holder" id="message-holder"></div>
		
		<div class="linkedin-message"><a href="https://www.linkedin.com/company/obrainsa" target="_blank">Visita nuestro perfil de <span class="fa fa-linkedin-square"></span> para ver las últimas ofertas laborales.</a></div>
	</div>

</section>


@stop

@section('footer-scripts')
@parent

<script type="text/javascript">
	$.validator.messages.required = '{{  \Lang::get('site::errors.required') }}';
	$.validator.messages.email = '{{  \Lang::get('site::errors.email') }}';
	$.validator.messages.dateISO = '{{  \Lang::get('site::errors.dateISO') }}';
	$.validator.messages.number = '{{  \Lang::get('site::errors.number') }}';
	$.validator.messages.perma = '{{  \Lang::get('site::errors.perma') }}';
	$.validator.setDefaults({ ignore: ":hidden:not([summernote])" })
	//$.validator.setDefaults({ ignore: ':hidden.ignore' })

	$(function(){

		var counts = {};
		$('[duplicate-inputs]').on('click', function(e) {
			e.preventDefault();
			var href = $(this).attr('href');
			var $target = $(href);
			href = href.replace('#','');
			if( !counts[href] ) counts[href] = 0;
			counts[href] += 1;

			var $remove = $('<a href="" class="remove-fields" ><i class="fa fa-minus"></i> Quitar</a>')
			var $new = $('.items:first', $target).clone();
			$new.attr('id', '');
			$('input, textarea', $new).val('');

			$new.append($remove);

			$target.append($new);

			$remove.on('click', function(e) {
				e.preventDefault();
				$(this).parent().remove();
			});

			$('label', $new).each(function(i,e){
				var $this = $(this);
				$this.attr('for', $this.attr('for').replace('[0]', "["+counts[href]+"]"));
			});

			$('input, textarea', $new).each(function(i,e){
				var $this = $(this);
				$this.attr('id', '');
				$this.attr('name', $this.attr('name').replace('[0]', "["+counts[href]+"]"));

				if($this.hasClass('required')) {
					$this.rules("add", {
			            required: true
			        });
				}

				if($this.attr('datetimepicker'))
				{
					var format = $this.attr('format');
					var mirror =  $this.attr('mirror');
					$this.datetimepicker({
						format: format,
						minView:2,
						autoclose: true,
						linkField: mirror,
						linkFormat: "yyyy-mm-dd"
					});


				}
			});



		});

		$('[datetimepicker]').each(function(i,e){

			var $this = $(e);
			var format = $(e).attr('format');
			var mirror = $(e).attr('mirror');
			$this.datetimepicker({
				format: format,
				minView:2,
				autoclose: true,
				linkField: mirror,
				linkFormat: "yyyy-mm-dd"
			});

		})


		$('#form-contact').validate({
	        // other rules & options,
	        validClass: "success",
	        highlight: function (element, errorClass, validClass) {
	            $(element).parents('.form-group').removeClass(validClass).addClass(errorClass);
	        },
	        unhighlight: function (element, errorClass, validClass) {
		        var $element = $(element);
		        $element.next().html('');
	            $element.parents('.form-group').removeClass(errorClass).addClass(validClass);
	        },
	        invalidHandler: function(form, validator) {
	        	var $error = $(validator.errorList[0].element);
				var $tab = $error.closest('.tab-pane');
				if($tab.length) {
					var id = '#'+$tab.attr('id');
					$('a[href="'+id+'"]').trigger('click');
				}
			},
			submitHandler: function(form) {
				$('#form-holder').hide();
				$('#message-holder').html('{{ \Lang::get('site::forms.please_wait') }}').addClass('wait left');
				$('#form-contact').ajaxSubmit({
					success: function(rsp) {
						console.log(rsp);
						console.log('test');
						$('#message-holder').html('{{ \Lang::get('site::forms.mensaje_enviado') }}').addClass('success').removeClass('wait left');
					},
					error: function(rsp) {
						alert(rsp.responseJSON.message);
					}
				});
			}
	    });

	})

</script>
@stop