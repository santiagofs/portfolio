<?php
	$body_class = 'reclamos';
?>
@extends('site::layout')

@section('page-content')
<section class="section-layout cms-articles">
	<div class="section-width">
		<header class="section-header">
			<h1>Lista de <strong>Reclamos</strong></h1>
			 
		</header>	
	</div>
	<div class="section-body news-list" >
		
		<div class="section-width poly-content-block-body" style="display:block; padding: 0; margin-top: 0; margin-bottom: 0;  border: none">
			 <div class="search-input">
                                <label for="burscar-reclamo">Busca tu reclamo</label>
                                <input type="text" id="buscar-reclamo" placeholder="ID o Nº de Documento"/><!--
                             --><button type="submit" id="search-reset">
                                    <i class="fa fa-times"></i>
                                </button><!--
                             --><button type="submit" id="search-submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
            <div class="table-container" id="tabla-reclamos">
                <table>
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Reclamo</th>
                            <th>Resolución</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div class="results-shown">
                                Mostrando <span id="reclamos_from">1</span> - <span id="reclamos_to">3</span> de <span id="reclamos_total">3</span>
                            </div>

            <div class="paginator" id="reclamos-paginator">

            </div>
		</div>
		
	</div>

</section>


@stop

@section('footer-scripts')
	@parent;
	
	<style>
.pagination {
    border-radius: 4px;
    display: inline-block;
    margin: 20px 0;
    padding-left: 0;
}
.pagination > li {
    display: inline;
}
.pagination > li > a, .pagination > li > span {
    background-color: #ffffff;
/*     border: 1px solid #dddddd; */
    color: #CCC;
    float: left;
/*     line-height: 1.42857; */
    margin-left: 5px;
    padding: 3px 10px;
    position: relative;
    text-decoration: none;
    width: auto !important;
}
.pagination > li:first-child > a, .pagination > li:first-child > span {
    border-bottom-left-radius: 4px;
    border-top-left-radius: 4px;
    margin-left: 0;
}
.pagination > li:last-child > a, .pagination > li:last-child > span {
    border-bottom-right-radius: 4px;
    border-top-right-radius: 4px;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    background-color: #eeeeee;
    border-color: #dddddd;
    color: #CCC;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    background-color: #0B436E;
    border-color: #0B436E;
    color: #ffffff;
    cursor: default;
    z-index: 2;
}
.pagination > .disabled > span, .pagination > .disabled > span:hover, .pagination > .disabled > span:focus, .pagination > .disabled > a, .pagination > .disabled > a:hover, .pagination > .disabled > a:focus {
    background-color: #ffffff;
    border-color: #dddddd;
    color: #CCC;
    cursor: not-allowed;
}
.pagination-lg > li > a, .pagination-lg > li > span {
    font-size: 18px;
    padding: 10px 16px;
}
.pagination-lg > li:first-child > a, .pagination-lg > li:first-child > span {
    border-bottom-left-radius: 6px;
    border-top-left-radius: 6px;
}
.pagination-lg > li:last-child > a, .pagination-lg > li:last-child > span {
    border-bottom-right-radius: 6px;
    border-top-right-radius: 6px;
}
.pagination-sm > li > a, .pagination-sm > li > span {
    font-size: 12px;
    padding: 5px 10px;
}
.pagination-sm > li:first-child > a, .pagination-sm > li:first-child > span {
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
}
.pagination-sm > li:last-child > a, .pagination-sm > li:last-child > span {
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
}
.search-input input {
	border: 1px solid #CCC;
}
#submit-search {
	position: absolute;
	right: 5px; top: 0;
	font-size: 2rem;
}
</style>
    <script>


        (function(){

	        var $paginator = $('#reclamos-paginator');
			var $tabla = $('#tabla-reclamos tbody');
			var $search = $('#buscar-reclamo');
			var $submit_search = $('#search-submit');
			var $reset_search = $('#search-reset');

			var $from = $('#reclamos_from');
			var $to = $('#reclamos_to');
			var $total = $('#reclamos_total');

			$paginator.on('click', 'li a', function(e) {
				e.preventDefault();
				var $this = $(this);
				var url = $this.attr('href');
				console.log(url);
				get_records(url);
			});

	        function get_records(url, search) {
		        var list_url = url ? url : 'reclamos-list';
		        var data = {};
		        if(search) data.search = search;

		        $.get(list_url, data, function(rsp){
			        var reclamos = rsp.reclamos;
					console.log('test');
			        $paginator.html(rsp.links);
			        $tabla.html(rsp.list);
			        $from.text(rsp.from);
			        $to.text(rsp.to);
			        $total.text(rsp.total);

		        }, 'json');
	        }

			$reset_search.hide();
	        $submit_search.on('click', function(e) {
	        	e.preventDefault();
	        	var search  = $search.val();
	        	if(search) {
		        	get_records(undefined, search);
		        	console.log($reset_search)
		        	$reset_search.show();
		        }
	        });
	        $search.keyup(function(e){
				if(e.keyCode != 13) return;

	        	var search  = $search.val();
	        	if(search) {
		        	get_records(undefined, search);
		        	$reset_search.show();
		        }
			});
			$reset_search.on('click', function(e) {
				e.preventDefault();
				$search.val('');
				$reset_search.hide();
				get_records();

			});

	        get_records()
        })();

       </script>
@stop
