@extends('site::page')
<?php
	$body_class = 'page news-item';

	$category_nav = $nav->parent;
	$parent_nav = $category_nav->parent;
	$category = $category_nav->sitemapable;
	$category->media;

	$menu = \Navigation::get_menu($parent_nav->slug, 'content::menu', 'site::partials.menu-node');


	$media_assorted = $category->media_assorted();
	$main_image = $media_assorted->one('main_image')->url();

	$main_image || $main_image = '/html/site/img/dummy-hero.jpg';
	
	$images = $content->media_assorted()->sluged('gallery');
	$srcs = array();
	foreach($images as $img) {
		$srcs[] = $img->url(1024);
	}
	
	$pdf = $content->media_assorted()->one('pdf');
?>
@section('page-content')
{{--
@include('components::hero-unit', array(
	'class' => 'full',
	'image'=> $main_image,
	'hero_text'=>$category->preview,
))
 --}}
<section class="section-layout  header-image">
	<header class="section-header" style="background-image: url({{$main_image}})">
		&nbsp;
	</header>

	<div class="section-body" >
		
		<div class="section-width">

			<article class="news-article">
				<header class="article-header">
					<p class="date">{{$content->publish_date->formatLocalized(\Lang::get('retama::dateformats.middle-date'))}}</p>
					<h1>{{ $content->title }}</h1>
				</header>
				
				<div class="article-body">
					{{ $content->body }}
					
					@if(count($pdf) && $pdf->id)
					<div class="download-action" style="padding: 20px 0; font-size: 14px;">
					<a href="{{ $pdf->url() }}" target="_blank">Descargar PDF <span class="fa fa-arrow-down"></span> </a>
					</div>
					@endif
				
				</div>
				@if(count($srcs))
<!--
				<div class="images">
					<a href="#" open-modal="{{implode(',', $srcs)}}">
					<img src="<?php echo($srcs[0]); ?>">
					</a>
				</div>
-->
				@endif
				
				
				<footer class="article-footer">
					<a href="{{URL::to('noticias')}}" class="back-to-list"><i class="fa fa-long-arrow-left"></i> <span class="text">Volver a Noticias</span></a>
				</footer>
			</article>
				
		</div>
	</div>

</section>
<!--
<section class="section-layout full-size-gallery" id="news-gallery">
	<div class="ratio-control"></div>
	
	<div class="viewport gallery">
		<ul class="gallery-list list-unstyled" id="bottom-gallery">
			@foreach($images as $img)
			<li style="background-image: url({{ $img->url() }})">
				<div class="description">
					<div class="content-control">
						{{ nl2br($img->title) }}
					</div>
				</div>
			</li>
			@endforeach
		</ul>
		<a href="#" class="gallery-control left"><span class="text"><i class="fa fa-angle-left"></i></span></a>
		<a href="#" class="gallery-control right"><span class="text"><i class="fa fa-angle-right"></i></span></a>
	</div>
</section>
-->


@stop


@section('footer-scripts')
@parent
<script type="text/javascript">


	$("[data-fancybox]").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers : {
			media : {}
		}
	});


	$(document).ready(function() {
		var gallery_owl = $("#bottom-gallery").owlCarousel({
			items:1

		});
		
		$('.gallery-control.left').on('click', function(e) {
			e.preventDefault();
			gallery_owl.trigger('prev.owl.carousel');
		});
		$('.gallery-control.right').on('click', function(e) {
			e.preventDefault();
			gallery_owl.trigger('next.owl.carousel');
		});

    });
</script>
@stop

@stop
