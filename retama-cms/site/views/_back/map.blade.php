<?php
	$body_class = 'page map';
	$body_id = 'map';
	
	$puntos = \Site\Models\Punto::with('media')->with('gmap')->get();

	$points = [];
	foreach($puntos as $punto)
	{

		$gmap = (object)['lat'=> null, 'lng'=>null];
		if($punto->gmap)
		{
			$gmap->lat = $punto->gmap->lat;
			$gmap->lng = $punto->gmap->lng;
		}
		$body = htmlspecialchars_decode($punto->body);
		
		$gallery = array();
		$collection = $punto->media_assorted()->sluged('gallery');
		foreach($collection as $img) {
			$gallery[] = $img->url(1024);
		}
		
		$point = (object)[
			'uid' => $punto->id,
			'title'=> $punto->title,
			'image'=> $punto->media_assorted()->one('main_image')->url(600),
			'gallery' => $gallery,
			'years' => $punto->years,
			'client' => $punto->client,
			'location' => $punto->location,
			'preview' => Str::limit($body, 200),
			'body' => $body,
			'gmap' => $gmap
		];
		$points[] = $point;
	}

	$points = addslashes(json_encode($points));
	//$points = htmlspecialchars($points, ENT_QUOTES, 'UTF-8');
?>
@extends('site::layout')
@section('page-content')
<section class="section-layout">
	<div class="section-width">
		
			<header class="section-header">
				<h1>{{$nav->name}}</h1>
			</header>
		
	</div>

		<div class="map-control">
			<div class="padding-control"></div>
			<div id="map-info">
				<a href="#" close-info class="close-info"><i class="fa fa-long-arrow-right"></i></a>
				<div class="content"></div>
				
			</div>
			<div id="gmap"></div>
		</div>
	
		<div class="infobox-control">
			<div id="infobox"></div>
		</div>
		

</section>
@stop



@section('footer-scripts')
@parent
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/html/site/infobox.js"></script>
<!-- <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script> -->
<script type="text/javascript">

var points = JSON.parse('{{ $points }}');


function initialize() {

	var infobox_template =  Hogan.compile([
		'<h4>@{{title}}</h4>',
		//'@{{#years}}<div><label>{{ \Lang::get('site::map.years') }}</label>: @{{ years }}</div>@{{/years}}',
		//'@{{#client}}<div><label>{{ \Lang::get('site::map.client') }}</label>: @{{ client }}</div>@{{/client	}}',
		'@{{#location}}<div><label>{{ \Lang::get('site::map.location') }}</label>: @{{ location }}</div>@{{/location}}',
		'<div class="preview"></div>',
		'<div class="more">{{ \Lang::get('site::map.more') }}</div>',
		'@{{#gallery.length}}<div class="gallery" open-modal="#galery"><i class="fa fa-camera"></i> {{ \Lang::get('site::map.gallery') }}</div>@{{/gallery.length}}'
	].join(''));

	var info_template = Hogan.compile([
		'@{{#image}}<div class="image-control"><img src="@{{image}}" /></div>@{{/image}}',
		'<h2>@{{title}} @{{#years}}<span class="small">(@{{years}})</span>@{{/years}}</h2>',
		'<div class="body"></div>'
	].join(''));

    var loc, map, marker, infobox;
   	var marker_icon =  window.location.origin + '/html/site/img/marker.png';

    //loc = new google.maps.LatLng(bound.getCenter());

    map = new google.maps.Map(document.getElementById("gmap"), {
		zoom: 16,

		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
    });



	var bound = new google.maps.LatLngBounds();
	var markers = [];
	var indexes = {};
	for (i = 0; i < points.length; i++) {

		var point = points[i];
		bound.extend( new google.maps.LatLng(point.gmap.lat, point.gmap.lng) );

		var marker = new google.maps.Marker({
			map: map,
			position: point.gmap,
			visible: true,
			content: point,
			icon: marker_icon
		});


		var box = document.createElement("div");
		box.className = 'infobox';
		var infoOptions = {
			content: box,
			disableAutoPan: false,
			maxWidth: 150,
			pixelOffset: new google.maps.Size(-140, 0),
			zIndex: null,
			//closeBoxMargin: "12px 4px 2px 2px",
			closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
			infoBoxClearance: new google.maps.Size(1, 1)
		};
		var $box = $(box);
		$box.html(infobox_template.render(point));
		$('.preview', $box).html(point.preview);

		marker.infobox = new InfoBox(infoOptions);
		

		var full = document.createElement("div");
		var $full = $(full);
		$full.html(info_template.render(point));
		$('.body', $full).html(point.body);
		//marker.full = $full;
		marker.full = $box;
		indexes['#'+point.uid] = marker;

		markers.push(marker);

		var $more = $('.more', $box);
		$more.data('marker', marker);
		$('.more', $box).on('click', function(e) {
			e.preventDefault();
			
			var thismaker = $(this).data('marker');
			$('#map-info .content').html(thismaker.full);
			$('#map-info').addClass('open');
			thismaker.infobox.close();
		});
		
		var $gallery = $('.gallery', $box);
		var images = point.gallery.join(',');
		$gallery.attr('images',images);
		
		$gallery.on('click', function(e) {
			e.preventDefault();
			var imgs = $(this).attr('images');
			modal_open(imgs);
		});

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				
				var mq = window.matchMedia("(max-width: 768px)");
				console.log(mq);
				if(!mq.matches) {
					var $infobox = $('#infobox');
					for(h = 0; h < markers.length; h++)
					{
						markers[h].infobox.close();
					}
					var thismarker = markers[i];
				    thismarker.infobox.open(map, this);
				} else {
					var $mapinfo = $('#map-info');
					//console.log('test');
					$('.content', $mapinfo).html(marker.full);
					
					var $this_gallery = $('.gallery', $mapinfo);
					$this_gallery.on('click', function(e) {
						e.preventDefault();
						var imgs = $(this).attr('images');
						modal_open(imgs);
					});
					$mapinfo.addClass('open');
					
				}
				
				map.panTo(thismarker.getPosition());
			}
		})(marker, i));

		/*
google.maps.event.addListener(marker, 'click', function() {
			var $infobox = $('#infobox');
			var content = this.content;
			$('.more', $infobox).on('click', function(e) {
				e.preventDefault();
				$('#map-info .content').html(info_template.render(content));
				$('#map-info .content .body').html(content.body);
				$('#map-info').addClass('open');
				infoBox.close();
			});

            this.infoBox.open(map, this);
            map.panTo(this.getPosition());

		});
*/


	}
	map.fitBounds(bound);
	var listener = google.maps.event.addListener(map, "idle", function() {
		  if (map.getZoom() > 16) map.setZoom(16);
		  google.maps.event.removeListener(listener);
	});
	map.setCenter(bound.getCenter());
	
	var hash = window.location.hash;
	if(hash) {
		console.log(hash)
		var marker = indexes[hash];
		if(marker != undefined) {
			console.log(marker)
			google.maps.event.trigger(marker, 'click');
		}
	}

}
google.maps.event.addDomListener(window, 'load', initialize);

$('#map-info .close-info').on('click', function(e) {
	e.preventDefault();
	$('#map-info').removeClass('open');
});

</script>
@stop

