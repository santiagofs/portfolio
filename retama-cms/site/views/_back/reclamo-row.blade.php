<?php
		$reclamo = $item->media_assorted()->one('reclamo');
		$resolucion = $item->media_assorted()->one('resolucion');
		$link_reclamo = $reclamo ? $reclamo->download() : '#';

?>

		<tr>
            <td class="date">
                <span>{{$item->fecha->format('d-m-Y')}}</span>
            </td>
            <td>
                <a href="{{$link_reclamo}}">{{$item->consumidor}} <i class="fa fa-arrow-down">
            </i></a></td>
            <td>
            	@if($item->estado == 'En evaluación' || !$resolucion)
            	{{$item->estado}}
            	@else
					<a href="{{ $resolucion->download() }}">{{$item->titulo_res}} <i class="fa fa-arrow-down">
            	@endif

            </i></a></td>
        </tr>