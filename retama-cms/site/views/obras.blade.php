@extends('site::layout')
<?php

	$media_assorted = $content->media_assorted();
	$gallery =  $media_assorted->sluged('gallery');
	$fullSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
				"autoplay": true,
				"fade": true,
        "dots": true
    }';

    $obras = \Site\Models\Obra::orderBy('created_at','ASC')
        ->with('media')
		->with('sitemap')
		->translate()
        //->take(8)
        ->get();

?>
@section('page-content')

<main class="main-content">
	<header>
		<div class="featured-slider" data-slick='<?php echo $fullSlider; ?>'>
			<?php $slider = \Widget::get('obras-slider'); ?>
			@foreach($slider as  $key=> $slide)
			<div class="featured-slider__item" style="background-image: url({{$slide->img->url(1200)}})">
		        <div class="container">
		          <h3 class="featured-slider__item-title"><a href="{{ Url::to($slide->link) }}">{{$slide->button}}</a></h3>
		        </div>
			</div>
			@endforeach

			@foreach($gallery as $img)
			<!-- <div class="featured-slider__item" style="background-image: url({{$img->url()}})">
        <div class="container">
          <h3 class="featured-slider__item-title"><a href="#">Oficinas y Locales Comerciales</a></h3>
        </div>
			</div> -->
			@endforeach
		</div>
	</header>

    <section  class="section">
      <div class="container">
        <header class="section-header">
          <h3 class="heading-3">{{$content->title}}</h3>
        </header>

        <div class="projects-list">
            @foreach($obras as $obra)
            <?php
                $media = $obra->media_assorted();
                $img =  $media->thumb();
            ?>

          <a class="project-card" href="{{ URL::to($obra->sitemap->path) }}">
            <div class="project-card__img-placer">
              <div class="project-card__img" style="background-image: url({{$img->url(360, 360)}})"></div>
            </div>
            <div class="project-card__info">
              <div class="project-card__type">{{ $obra->bajada }}</div>
              <h4 class="project-card__title">{{ $obra->nombre }}</h4>
              <div class="project-card__description">{{ $obra->descripcion }}</div>
            </div>
          </a>
          @endforeach
        </div>
      </div>
    </section>

</main>


@stop
