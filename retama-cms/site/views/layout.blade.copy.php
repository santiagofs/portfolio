<?php
	if(\App::environment() != 'production'):
		error_reporting(E_ALL);
	endif;
	
	//$file_prefix = \Retama::file_prefix();
	//$body_class = 'overlap-header';
	$file_prefix = '-min';
?>
@extends('base.off-canvas', array('off_canvas_mode'=>'right'))

@section('seo')
@if(isset($nav))
<?php
	$seo = \Seo::forge($nav);
?>
	<title>{{ $seo->get_title() }}</title>
	<meta name="description" content="{{ $seo->description }}">
    <meta name="keywords" content="{{ $seo->keywords }}">
@endif
@stop

@section('head-scripts')
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,300|Roboto+Slab:400,700,300|Lato' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ asset('/html/site/site.css') }}" />
<!--     <link rel="stylesheet" href="/assets/site.css"> -->
<!--
	<style>
	@include('site::atf-min')
	</style>
-->

@stop

@section('site-off-canvas')
	@include('site::partials.off-canvas')
@stop

@section('site-header')
	@include('site::partials.header')
@stop

@section('site-body')
	@yield('page-content')
@stop

@section('site-footer')
	@include('site::partials.footer')
@stop

@section('site-layout')
	@parent



	{{-- @include('retama::partials.modal') --}}
@stop

@section('footer-scripts')

	<script type="text/javascript" src="{{ asset('/html/site/lib'.$file_prefix.'.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/html/site/main'.$file_prefix.'.js') }}"></script>

	<?php
		if(\App::environment() != 'production'):
	?>

	<?php
		endif;
	?>
@stop
