@extends('site::layout')
<?php

	$media_assorted = $content->media_assorted();
	$gallery =  $media_assorted->sluged('gallery');
	$fullSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
				"autoplay": true,
				"fade": true,
        "dots": true
    }';

    $clientes = \Widget::get_media('clientes');
?>
@section('page-content')

<main class="main-content">
	<header>
		<div class="featured-slider" data-slick='<?php echo $fullSlider; ?>'>
			@foreach($gallery as $img)
			<div class="featured-slider__item" style="background-image: url({{$img->url()}})"></div>
			@endforeach
		</div>
	</header>

  <section class="section">
    <div class="container">
      <header class="section-header">
        <h3 class="heading-3">{{ $content->title }}</h3>
        <p class="section-header__subtitle">{{ $content->preview }}</p>
      </header>

      <div class="clients">

			  <!-- SLIDER -->
			  <?php $clientSlider = '{
			  		"arrows": false,
			  		"infinite": true,
			  		"slidesToShow": 6,
			  		"slidesToScroll": 1,
			      "autoplay": true,
			      "autoplaySpeed": 0,
			      "speed": 3000,
			      "cssEase": "linear",
			      "responsive": [
			        {
			          "breakpoint": 991,
			          "settings": {
			        		"slidesToShow": 4,
			        		"slidesToScroll": 1
			          }
			        },
			        {
			          "breakpoint": 767,
			          "settings": {
			        		"slidesToShow": 3,
			        		"slidesToScroll": 1
			          }
			        },
			        {
			          "breakpoint": 543,
			          "settings": {
			        		"slidesToShow": 2,
			        		"slidesToScroll": 1
			          }
			        }
			      ]
			  }'; ?>

				<div class="clients-row" data-slick='<?php echo $clientSlider; ?>'>
					@foreach($clientes as $cliente)
					<div class="clients-row__item">
						<img class="clients-row__item-img" src="{{$cliente->url()}}" alt="" />
					</div>
					@endforeach
				</div>
			  <!-- END SLIDER -->

        <div class="row">
            @foreach($clientes as $cliente)
             <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2"><img src="{{$cliente->url()}}" alt="" /></div>
            @endforeach
        </div>
      </div>
    </div>
  </section>

</main>


@stop
