@extends('site::layout')
<?php

	$media_assorted = $content->media_assorted();
	$gallery =  $media_assorted->sluged('gallery');
	$fullSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
				"autoplay": true,
				"fade": true,
        "dots": true
    }';

	$class = get_class($content);
	$is_category = $class == 'Site\Models\DesarrolloCategory';

    $model = \Site\Models\Desarrollo::orderBy('sort','ASC')
        ->with('media')
		->with('sitemap')
		->translate();

	if($is_category) {
		$model->where('category_id', '=', $content->id);
		$page_caption = $content->nombre;
	} else {
		$model->where('category_id', '!=', 1);
		$page_caption = \Lang::get('site::content.NUESTROS_DESARROLLOS');
	}
        //->take(8)
    $desarrollos = $model->get();

?>
@section('page-content')

<main class="main-content">
	<header>
		<div class="featured-slider" data-slick='<?php echo $fullSlider; ?>'>
			<?php $slider = \Widget::get('desarrollos-slider'); ?>
			@foreach($slider as  $key=> $slide)
			<div class="featured-slider__item" style="background-image: url({{$slide->img->url()}})">
		        <div class="container">
		          <h3 class="featured-slider__item-title"><a href="{{ Url::to($slide->link) }}">{{$slide->button}}</a></h3>
		        </div>
			</div>
			@endforeach

			@foreach($gallery as $img)
			<!-- <div class="featured-slider__item" style="background-image: url({{$img->url()}})">
		        <div class="container">
		          <h3 class="featured-slider__item-title"><a href="#">Oficinas y Locales Comerciales</a></h3>
		        </div>
			</div> -->
			@endforeach
		</div>
	</header>

    <section  class="section">
      <div class="container">
        <header class="section-header">
          <h3 class="heading-3">{{ $page_caption }}</h3>
        </header>

			<div class="projects-list">
				@foreach($desarrollos as $desarrollo)
				<?php
					$media = $desarrollo->media_assorted();
					$img =  $media->thumb();
					$path = $desarrollo->sitemap->path;
				?>

				<a class="project-card" href="{{ URL::to( $path )}}">
					<div class="project-card__img-placer">
						<div class="project-card__img" style="background-image: url({{$img->url()}})"></div>
					</div>
					<div class="project-card__info">
						<div class="project-card__type">{{ $desarrollo->bajada }}</div>
						<h4 class="project-card__title">{{ $desarrollo->nombre }}</h4>
						<div class="project-card__description">{{ $desarrollo->descripcion }}</div>
					</div>
				</a>
				@endforeach

				@if(!$is_category)
				<?php
					$rurales = \Site\Models\DesarrolloCategory::with('media')
						->with('sitemap')
						->find(1)
						->translate()
						->first();

					$media = $rurales->media_assorted();
					$img =  $media->thumb();
					$path = $rurales->sitemap->path;
				?>
				{{-- <a class="project-card project-card_category" href="{{ URL::to( $path )}}" style="background-image: url({{$img->url()}})"> --}}
				<a class="project-card project-card_category" href="{{ URL::to( $path )}}" style="background-image: url({{asset('html/site/img/rurales.jpg')}})">
					<div class="project-card__info">
						<h4 class="project-card__title">{{ $rurales->nombre }}</h4>
						<span class="project-card__cta"><i class="mdi mdi-chevron-right"></i></span>
					</div>
				</a>
				@endif

			</div>
      </div>
    </section>

</main>


@stop
