@extends('site::layout')
<?php

	$media_assorted = $content->media_assorted();
	$gallery =  $media_assorted->sluged('gallery');
	$fullSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
				"autoplay": true,
				"fade": true,
        "dots": true
    }';

?>
@section('page-content')

<main class="main-content">
	<header>
		<div class="featured-slider" data-slick='<?php echo $fullSlider; ?>'>
			<div class="featured-slider__item" style="background-image: url({{asset('html/site/img/about-header.jpg')}})"></div>
		</div>
	</header>

  <section class="section">
    <div class="container">
      <header class="section-header">
        <h3 class="heading-3">{{ $content->title }}</h3>
        <p class="section-header__subtitle">{{ $content->preview }}</p>
      </header>

      <div  class="about-history">
        {{ $content->body }}
      </div>
    </div>
  </section>

  <section class="full-image-section" style="background-image: url({{asset('html/site/img/records-bg.jpg')}})">
    <div class="full-image-section-overlay">
      <header class="section-header section-header_white">
        <h3 class="heading-3">MAPA DE ANTECEDENTES</h3>
      </header>
      <div class="sp30"></div>
      <a href="map.php" class="button-fill-white">VER MAPA</a>
    </div>
  </section>

  <hr class="divider-gradient">

  <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-4">
          <div class="about-contact">
            <i class="mdi mdi-map-marker"></i>
            <h5 class="about-contact__title">DIRECCIÓN</h5>
            <p class="about-contact__data">
              Av. Sucre 1658, San Isidro <br>
              Buenos Aires
            </p>
          </div>
        </div>
        <div class="col-xs-12 col-md-4">
          <div class="about-contact">
            <i class="mdi mdi-phone"></i>
            <h5 class="about-contact__title">TELÉFONO</h5>
            <p class="about-contact__data">
              4719-6100
            </p>
          </div>
        </div>
        <div class="col-xs-12 col-md-4">
          <div class="about-contact">
            <i class="mdi mdi-email"></i>
            <h5 class="about-contact__title">EMAIL</h5>
            <p class="about-contact__data">
              <a href="mailto:podesta@pedropodesta.com.ar">podesta@pedropodesta.com.ar</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <hr class="divider-gradient">

  <!-- OTROS DESARROLLOS -->
  <?php $projectSliders = '{
  		"arrows": false,
  		"infinite": false,
  		"slidesToShow": 5,
      "responsive": [
        {
          "breakpoint": 991,
          "settings": {
        		"infinite": true,
        		"slidesToShow": 2,
            "centerMode": true,
            "centerPadding": "calc(50% - 260px)"
          }
        },
        {
          "breakpoint": 767,
          "settings": {
        		"infinite": true,
        		"slidesToShow": 1,
            "centerMode": true,
            "centerPadding": "calc(100% - 260px) 0 0"
          }
        }
      ]
  }'; ?>
  <section class="section">
    <header class="section-header">
      <h3 class="heading-3">ÚLTIMOS PROYECTOS</h3>
    </header>

    <div class="projects-row" data-slick='<?php echo $projectSliders; ?>'>
      <?php for($i = 0; $i < 5; $i++): ?>
      <a href="#" class="projects-row__item">
        <div class="projects-row__item-img" style="background-image: url(dummy/1.jpg)"></div>
        <div class="projects-row__item-info">
          <h5 class="projects-row__item-title">Coto Martín García</h5>
          <p class="projects-row__item-description">Hipermercado</p>
        </div>
      </a>
      <?php endfor; ?>
    </div>
  </section>
  <!-- END OTROS DESARROLLOS -->
</main>


@stop
