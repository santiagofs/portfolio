@extends('site::layout')
<?php

	$media_assorted = $content->media_assorted();
	$gallery =  $media_assorted->sluged('gallery');
	$category = $content->category;
	$category->sitemap;

	$mb = (object)array('path'=>'', 'name'=>'');
	if($category->id == 1) {
		$mb->path = URL::to($category->sitemap->path);
		$mb->name = $category->nombre;
	} else {
		$mb->path = URL::to('desarrollos');
		$mb->name = Lang::get('site::content.desarrollos');
	}

	$fullSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
        "dots": true
    }';

?>
@section('page-content')

<main class="main-content">
  <section class="container">

    <header class="project-header">
      <h1 class="heading-2 project-header__title">{{ $content->nombre }}</h1>
      <p class="project-header__type">{{ $content->bajada }}</p>
    </header>

    <div class="breadcrumbs">
      <a class="breadcrumbs__item" href="#">Home</a> &raquo; <a class="breadcrumbs__item" href="{{ $mb->path }}">{{$mb->name}}</a> &raquo; <span class="breadcrumbs__item active">{{ $content->nombre }}</span>
    </div>

    <div class="project-caption {{$mb->name == "Desarrollos Agro" ? 'project-caption_agro' : '' }}">{{ $content->descripcion }}</div>
  </section>

	<div class="bg-light">
		<section class="container">
			@include('site::partials.gallery', array('gallery'=>$gallery))
		</section>
	</div>

	<section class="container">
		<div class="project-features">

			@foreach($content->C1() as $c)
			<div class="project-features__item">
				<h5 class="heading-5">{{ $c->label }}</h5>
				<div class="project-features__item-data">{{ $c->value }}</div>
			</div>
			@endforeach
	</div>

	<div id="map" style="height: 300px"></div>

	</section>

	  <?php
		$otras = \Site\Models\Desarrollo::orderBy('created_at','DESC')
			->whereNotIn('id', array($content->id))
			->with('media')
			->take(8)
			->get();
		?>
	<section class="section">
		<header class="section-header">
		<h3 class="heading-3">{{ \Lang::get('site::content.otros_desarrollos' )}}</h3>
		</header>
		@include('site::partials.other-projects', array('otras'=>$otras, 'base'=>'desarrollos'))

	</section>
  <!-- END OTROS DESARROLLOS -->

</main>
@stop

@section('footer-scripts')
@parent

<script type="text/javascript">
	var galleryLightboxOptions = {
		container: $('#gallery-lightbox')[0],
		trigger: $('[data-gallery]'),
		beforeOpen: function(e){
			var imgId = $(e.target).data('gallery');
			var slideId = $('[data-gallery-target="'+imgId+'"]').data('slick-index');
			$('.project-gallery__slider').slick('slickGoTo', slideId, true);
		}
	}
	var galleryLightbox = new Lightbox(galleryLightboxOptions);
</script>

@if($content->gmap)
<script>
	var gmap = {{ $content->gmap->toJson() }}
	function initialize() {

		map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(gmap.lat, gmap.lng),
      mapTypeId: 'roadmap'
    });
<?php
	$ndx = $content->category->id;
	$markers = ['rural', 'podesta', 'officia'];
	$marker = isset($markers[$ndx-1]) ? $markers[$ndx-1] : $markers[0];
?>
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(gmap.lat, gmap.lng),
			icon: '{{ asset('html/site/img/pin_'.$marker.'.svg') }}',
			map: map
		});
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAa6dalIlsnnZdnEk3ZYiFuW3sv6D0-3ls&callback=initialize"></script>
@endif
@stop
