@extends('site::layout')
<?php
	$body_id = '';
?>
@section('page-content')

	<div id="column-layout" class="columns">
		@include('site::partials.site-aside')
		<div id="main-content">
				<h1 style="text-align:center;">Access denied. Please login</h1>
		</div>

	</div>

@stop
