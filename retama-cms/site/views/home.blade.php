@extends('site::layout')
<?php
	$body_id = 'home';

	$media_assorted = $content->media_assorted();
	$main_image = $media_assorted->one('gallery')->url();
	$body = preg_replace( '/^<[^>]+>|<\/[^>]+>$/', '', $content->body );
	$homeSlider = '{
        "arrows": false,
        "infinite": true,
        "slidesToShow": 1,
				"autoplay": true,
				"autoplaySpeed": 3000,
				"fade": true,
        "dots": false
    }';
?>


@section('page-content')
<main class="main-content">

	<!-- HERO HEADER -->
	<header class="hero-header hero-header_home">

		<div class="hero-header__slider" data-slick='<?php echo $homeSlider; ?>'>
			<?php $slider = \Widget::get('home-slider');
				//href="{{Url::to($slide->link)}}"
			?>
			@foreach($slider as  $key=> $slide)
			<a  class="hero-header__slider-item" style="background-image: url({{ $slide->img->url(1200) }})"></a>
			@endforeach
		</div>
		<div class="center">
			<div class="container">
				<h2 class="heading-1">{{ $body }}</h2>
			</div>
		</div>
		<button class="hero-header-cta"><span class="mdi mdi-chevron-down"></span></button>
	</header>
	<!-- END HERO HEADER -->

	<!-- OBRAS -->
	<?php $projectSliders = '{
		"arrows": false,
		"infinite": false,
		"slidesToShow": 5,
		"responsive": [{
			"breakpoint": 991,
			"settings": {
				"infinite": true,
				"slidesToShow": 2,
				"centerMode": true,
				"centerPadding": "calc(50% - 260px)"
			}
		},{
			"breakpoint": 767,
			"settings": {
			"infinite": true,
			"slidesToShow": 1,
			"centerMode": true,
			"centerPadding": "calc(100% - 260px) 0 0"
			}
		}]
	}';

	?>

	<section class="section">
		<header class="section-header">
			<h3 class="heading-3">{{ \Lang::get('site::home.Obras')}}</h3>
		</header>

		<div class="projects-row" data-slick='<?php echo $projectSliders; ?>'>
			<?php $obras = \Widget::get('home-obras'); ?>
			@foreach($obras as $obra)
			<?php
				$thumb = $obra->media_assorted()->thumb()->url(340,340);
				$path = $obra->sitemap->path;
			?>
			<a href="{{ URL::to( $path )}}" class="projects-row__item">
				<div class="projects-row__item-img" style="background-image: url({{ $thumb }})"></div>
				<div class="projects-row__item-info">
					<h5 class="projects-row__item-title">{{ $obra->nombre }}</h5>
					<p class="projects-row__item-description">{{ $obra->bajada }}</p>
				</div>
			</a>
			@endforeach
		</div>

		<div class="center-text">
			<a href="{{ URL::to('obras') }}" class="button-border-main">{{ \Lang::get('site::content.ver_más') }}</a>
		</div>
	</section>

	<hr class="divider-gradient">
	<!-- END OBRAS -->

	<!-- DESARROLLOS -->
	<section class="section bg-light">
		<header class="section-header">
			<h3 class="heading-3">{{ \Lang::get('site::home.Desarrollos') }}</h3>
		</header>

		<div class="projects-row" data-slick='<?php echo $projectSliders; ?>'>
			<?php $desarrollos = \Widget::get('home-desarrollos'); ?>
			@foreach($desarrollos as $desarrollo)
			<?php
				$thumb = $desarrollo->media_assorted()->thumb()->url(340,340);
				$path = $desarrollo->sitemap->path;
			?>
			<a href="{{ URL::to( $path )}}" class="projects-row__item">
				<div class="projects-row__item-img" style="background-image: url({{ $thumb }})"></div>
				<div class="projects-row__item-info">
					<h5 class="projects-row__item-title">{{ $desarrollo->nombre }}</h5>
					<p class="projects-row__item-description">{{ $desarrollo->bajada }}</p>
				</div>
			</a>
			@endforeach
		</div>

		<div class="center-text">
			<a href="{{ URL::to('desarrollos') }}" class="button-border-main">{{ \Lang::get('site::content.ver_más') }}</a>
		</div>
	</section>
  <!-- END DESARROLLOS -->

  <!-- HOME FEATURED PROJECT -->
  <section class="full-image-section video-lightbox-trigger full-image-section_home" style="background-image: url({{asset('html/site/img/home-video-section.jpg')}})">
    <div class="full-image-section-overlay">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-10 col-md-push-1 col-lg-8 col-lg-push-2">
            <h3 class="heading-2"><strong>{{ \Lang::get('site::home.video_caption') }}</strong></h3>
            <p class="full-image-section-description">{{ \Lang::get('site::home.video_description') }}<p>
            <div class="play-cta"><i class="mdi mdi-play"></i></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END HOME FEATURED PROJECT -->

  <!-- VIDEO MODAL -->
  <div class="lightbox" id="video-lightbox">
    <button class="lightbox__close mdi mdi-close"></button>
    <div class="lightbox__overlay">
      <div class="lightbox__content">
        <div class="video-box ratio-16-9">
          <iframe src="" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
  <!-- END VIDEO MODAL -->

  <!-- CLIENTS -->
  <?php $clientSlider = '{
  		"arrows": false,
  		"infinite": true,
  		"slidesToShow": 8,
  		"slidesToScroll": 1,
      "autoplay": true,
      "autoplaySpeed": 0,
      "speed": 3000,
      "cssEase": "linear",
      "responsive": [
        {
          "breakpoint": 991,
          "settings": {
        		"slidesToShow": 5,
        		"slidesToScroll": 1
          }
        },
        {
          "breakpoint": 767,
          "settings": {
        		"slidesToShow": 2,
        		"slidesToScroll": 1
          }
        }
      ]
  }'; ?>

	<section class="section">
		<header class="section-header">
			<h3 class="heading-3">{{ \Lang::get('site::home.clientes') }}</h3>
		</header>
		<?php
			$clientes = \Widget::get_media('clientes');
		?>
		<div class="clients-row" data-slick='<?php echo $clientSlider; ?>'>
			@foreach($clientes as $cliente)
			<div class="clients-row__item">
				<img class="clients-row__item-img" src="{{$cliente->url()}}" alt="" />
			</div>
			@endforeach
		</div>
	</section>
  <!-- END CLIENTS -->

</main>


@stop


@section('footer-scripts')
@parent
<script type="text/javascript">
  /*----------*/
  /* HERO CTA */
  /*----------*/
  $('.hero-header-cta').on('click', function(){
    $('html, body').animate({
      'scrollTop': $('.hero-header').outerHeight()
    }, 500);
  });

  /*----------------*/
  /* VIDEO LIGHTBOX */
  /*----------------*/
  var videoLightboxOptions = {
		container: document.getElementById('video-lightbox'),
		trigger: document.getElementsByClassName('video-lightbox-trigger'),
    beforeOpen: function(){
      $('#video-lightbox').find('iframe').attr('src', 'https://player.vimeo.com/video/145138752?autoplay=1');
    },
    beforeClose: function(){
      $('#video-lightbox').find('iframe').attr('src', 'https://player.vimeo.com/video/145138752');
    }
  }

  var videoLightbox = new Lightbox(videoLightboxOptions);
</script>
@stop
