<?php return array (
  'required' => 'Este campo es requerido',
  'email' => 'Ingresa un email válido',
  'dateISO' => 'Ingresa una fecha válida',
  'number' => 'Ingresa sólo números',
  'perma' => 'Ingresa una URL válida',
);