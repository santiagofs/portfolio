<?php
	return [
		'desarrollos' => [
			'table' => 'desarrollos',
			'caption' => 'desarrollos',
			'main' => 'nombre',
			'class' => '\Site\Models\Desarrollo',
			'view' =>	'site::retama.desarrollo-translate',
		],
		'obras' => [
			'table' => 'obras',
			'caption' => 'obras',
			'main' => 'nombre',
			'class' => '\Site\Models\Obra',
			'view' =>	'site::retama.obra-translate',
		],
		'categorias' => [
			'table' => 'desarrollos_categories',
			'caption' => 'categorias',
			'main' => 'nombre',
			'class' => '\Site\Models\DesarrolloCategory',
			'view' =>	'site::retama.category-translate',
		],
		'puntos' => [
			'table' => 'puntos',
			'caption' => 'Mapa',
			'main' => 'title',
			'class' => '\Site\Models\Punto',
			'view' =>	'site::retama.punto-translate',
		],
		// 'navigation' => [
		// 	'table' => 'navigation',
		// 	'original_count' => function() {
		// 		return  \DB::table('navigation')->where('parent_id','>',1)->count();
		// 	},
		// 	'caption' => 'Navigation',
		// 	'main' => 'name',
		// 	'class' => '\Modules\Content\Models\Navigation',
		// 	'get_module_query' => function($q) {
		// 		return $q->where('parent_id','>',1);
		// 	},
		// 	'view' =>	'international::retama.modules.navigation',
		// ]
	];
?>
