<?php return [
	'content' => [
		'modules' => [
			'obras' => [
				'label' => 'Obras',
				'icon' => 'fa fa-gear',
				'enabled' => '1',
				'url' => 'content/obras',
				'fields' => [],
			],
			'desarrollos' => [
				'label' => 'Desarrollos',
				'icon' => 'fa fa-gears',
				'enabled' => '1',
				'url' => 'content/desarrollos',
				'fields' => [],
			],
			'categories' => [
				'label' => 'Categorías',
				'icon' => 'fa fa-gears',
				'enabled' => '1',
				'url' => 'content/categorias',
				'fields' => [],
			],
			'puntos' => [
				'label' => 'Mapa',
				'icon' => 'fa fa-map-marker',
				'enabled' => '1',
				'url' => 'content/puntos',
				'fields' => [],
			],
		]
	]
];
