<?php return array (
  'content' =>
  array (
    'label' => 'Content',
    'icon' => 'fa fa-copy',
    'enabled' => '1',
    'url' => 'content',
    'modules' =>
    array (
      'structure' =>
      array (
        'label' => 'Site Structure',
        'icon' => 'fa fa-sitemap',
        'enabled' => '0',
        'url' => 'content/structure',
      ),
      'navigation' =>
      array (
        'label' => 'Navigation / Menus',
        'icon' => 'fa fa-bars',
        'enabled' => '0',
        'url' => 'content/navigation',
      ),
      'page' =>
      array (
        'label' => 'Pages',
        'icon' => 'fa fa-file',
        'enabled' => '1',
        'url' => 'content/pages',
        'fields' =>
        array (
          'title' =>
          array (
            'label' => 'Title',
            'enabled' => '1',
          ),
          'preview' =>
          array (
            'label' => 'Slogan',
            'enabled' => '1',
          ),
          'body' =>
          array (
            'label' => 'Cuerpo',
            'enabled' => '1',
          ),
          'extra' =>
          array (
            'label' => 'Descripción',
            'enabled' => '1',
          ),
        ),
      ),
    //   'news' =>
    //   array (
    //     'label' => 'News',
    //     'icon' => 'fa fa-bullhorn',
    //     'enabled' => '1',
    //     'url' => 'content/news',
    //     'fields' =>
    //     array (
    //     ),
    //   ),
    //   'blog' =>
    //   array (
    //     'label' => 'Blog',
    //     'icon' => 'fa fa-pencil',
    //     'enabled' => '0',
    //     'url' => 'content/blog',
    //     'fields' =>
    //     array (
    //     ),
    //   ),
      'widgets' =>
      array (
        'label' => 'Widgets',
        'icon' => 'fa fa-bell',
        'enabled' => '1',
        'url' => 'content/widgets',
        'fields' =>
        array (
        ),
      ),
    ),
  ),
  // 'media' =>
  // array (
  //   'label' => 'Media',
  //   'icon' => 'fa fa-picture-o',
  //   'enabled' => '1',
  //   'url' => 'media',
  //   'modules' =>
  //   array (
  //     'galleries' =>
  //     array (
  //       'label' => 'Galleries',
  //       'icon' => 'fa fa-file-image-o',
  //       'enabled' => '0',
  //       'url' => 'media/galleries',
  //     ),
  //     'downloads' =>
  //     array (
  //       'label' => 'Downloads',
  //       'icon' => 'fa fa-download',
  //       'enabled' => '1',
  //       'url' => 'media/downloads',
  //       'fields' =>
  //       array (
  //       ),
  //     ),
  //   ),
  // ),
  'international' =>
  array (
    'label' => 'International',
    'icon' => 'fa fa-globe',
    'enabled' => '1',
    'url' => 'international',
    'modules' =>
    array (
      'countries' =>
      array (
        'label' => 'Countries',
        'icon' => 'fa fa-flag',
        'enabled' => '0',
        'url' => 'international/countries',
      ),
      'languages' =>
      array (
        'label' => 'Languages',
        'icon' => 'fa fa-language',
        'enabled' => '0',
        'url' => 'international/languages',
      ),
      'content' =>
      array (
        'label' => 'Content translation',
        'icon' => 'fa fa-retweet',
        'enabled' => '1',
        'url' => 'international/content',
      ),
      'ui' =>
      array (
        'label' => 'UI translation',
        'icon' => 'fa fa-cube',
        'enabled' => '1',
        'url' => 'international/ui',
      ),
    ),
  ),
  'settings' =>
  array (
    'label' => 'Settings',
    'icon' => 'fa fa-cogs',
    'enabled' => '0',
    'url' => 'settings',
  ),
  'people' =>
  array (
    'label' => 'People',
    'icon' => 'fa fa-users',
    'enabled' => '1',
    'url' => 'people',
    'modules' =>
    array (
      'administrators' =>
      array (
        'label' => 'Administrators',
        'icon' => 'fa fa-users',
        'enabled' => '1',
        'url' => 'people/administrators',
      ),
      'users' =>
      array (
        'label' => 'Users',
        'icon' => 'fa fa-user',
        'enabled' => '0',
        'url' => 'people/users',
      ),
      'suscribers' =>
      array (
        'label' => 'Newsletter suscribers',
        'icon' => 'fa fa-envelope',
        'enabled' => '0',
        'url' => 'people/newsletter/suscribers',
      ),
    ),
  ),
  // 'messages' =>
  // array (
  //   'label' => 'Messages',
  //   'icon' => 'fa fa-envelope',
  //   'enabled' => '1',
  //   'url' => 'messages',
  //   'modules' =>
  //   array (
  //     'formtomail' =>
  //     array (
  //       'label' => 'Form To Mail',
  //       'icon' => 'fa fa-envelope',
  //       'enabled' => '1',
  //       'url' => 'messages/formtomail',
  //     ),
  //   ),
  // ),
);
