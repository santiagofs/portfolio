<?php
namespace Site;

class ServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function boot()
    {

	    $mydir = dirname(__FILE__);

	    $this->package('site', 'site', $mydir);

    }

    public function register()
    {
	    //var_dump('site service provider register');
	    $mydir = dirname(__FILE__);

	    $this->app['config']->package('site', 'site', $mydir);
        $routes = $mydir.'/routes.php';
        //if (file_exists($routes)) require $routes;
    }

    public static function get_routes()
    {
		$mydir = dirname(__FILE__);
		$routes = $mydir.'/routes.php';

        if (file_exists($routes)) {
	        return $routes;
	    }
	    else {
		    return false;
	    }
    }

    public static function get_path($file = null)
    {
	    $path = dirname(__FILE__);
	    if($file) $path .= '/'.$file;
	    return $path;
    }

    public static function get_file($path)
    {
	    $full_path = static::get_path().'/'.$path;
	    return file_exists($full_path) ? $full_path : false;
    }
}