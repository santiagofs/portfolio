<?php
namespace Site\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Puntos extends \Modules\Retama\RetamaController {
	protected $model_name = '\Site\Models\Punto';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		$list->sortable = true;

		$list->fields(
			array(
				'title'		=> 'Título',
				'province'  => 'Provincia',
				'category'  => 'Categoría',
			),
			'title'
		);
		$list->set_sort('sort');
		$list->per_page = 2000;

		$list->add_filter('text','title', 'Título');
		$list->add_filter('text','province', 'Provincia');
		$list->add_filter('text','category', 'Categoría');

		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Puntos del Mapa');
		$this->render('retama::lists.list', array(
			'section_title' => 'Puntos de Acción',
			'table_view' => 'site::retama.punto-list-table',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{

		//var_dump($errors); die();
		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Puntos de Acción');
		$this->render('site::retama.punto-edit', array(
			'section_title' => 'Puntos de Acción',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}
