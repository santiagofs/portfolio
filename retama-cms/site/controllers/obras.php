<?php
namespace Site\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Obras extends \Modules\Retama\RetamaController {
	protected $model_name = '\Site\Models\Obra';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		$list->sortable = true;

		$list->fields(
			array(
				'nombre'		=> 'Nombre',
			),
			'nombre'
		);
		$list->set_sort('sort');
		//$list->sort_field = 'order';

		// $list->add_filter('text','dni', 'Documento');
		// $list->add_filter('text','nombres', 'Nombres');
		// $list->add_filter('text','apellido', 'Apellido');
		//$filter = $list->add_filter('select','estado', 'Estado', '=', null, ['list' => ['' => 'Todos los estados', 'En evaluación'=>'En evaluación', 'Improcedente'=>'Improcedente', 'Resuelto'=>'Resuelto']]);

		//\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Obras');
		$this->render('retama::lists.list', array(
			'section_title' => 'Obras',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		$sitemap = $model->sitemap;
		$media = $model->media;


		//var_dump($errors); die();
		//\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Obras');
		$this->render('site::retama.obra-edit', array(
			'section_title' => 'Obras',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}
