<?php
namespace Site\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Desarrollos extends \Modules\Retama\RetamaController {
	protected $model_name = '\Site\Models\Desarrollo';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		$list->sortable = true;

		$list->fields(
			array(
				'nombre'		=> 'Nombre',
				//'category.name'=> 'Categoría',
			),
			'nombre'
		);
		$list->set_sort('sort');

		// $list->joins('desarrollos_categories as category', 'desarrollos.category_id', 'category.id');
		// $list->select('desarrollos.*', 'category.name as category_name');


		// $categories = ['skip filter' => 'Todas las categorías'];
		// $prefix = '';
		// foreach($flatten as $item) {
		// 	if($item->indent == '1') {
		// 		$prefix = $item->sitemapable->name;
		// 		continue;
		// 	}
		// 	$categories[$item->sitemapable->id] = $prefix .' - '.$item->sitemapable->name;
		// }

		// $list->add_filter('text','dni', 'Documento');
		// $list->add_filter('text','nombres', 'Nombres');
		// $list->add_filter('text','apellido', 'Apellido');
		//$filter = $list->add_filter('select','estado', 'Estado', '=', null, ['list' => ['' => 'Todos los estados', 'En evaluación'=>'En evaluación', 'Improcedente'=>'Improcedente', 'Resuelto'=>'Resuelto']]);

		//\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Desarrollos');
		$this->render('retama::lists.list', array(
			//'table_view' => 'site::retama.desarrollo-list-table',
			'section_title' => 'Desarrollos',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		$sitemap = $model->sitemap;
		$media = $model->media;
		$categories = \Site\Models\DesarrolloCategory::orderBy('nombre')->lists('nombre','id');


		//var_dump($errors); die();
		//\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Desarrollos');
		$this->render('site::retama.desarrollo-edit', array(
			'section_title' => 'Desarrollos',
			'model' => $model,
			'categories' => $categories
		));
	}

	protected function onPostEdit($model)
	{

	}

}
