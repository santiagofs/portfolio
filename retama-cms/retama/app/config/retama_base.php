<?php

return array(
	'enabled_modules' => array (
		'structure' => true,
		'content' => ['pages'=>true, 'news'=>false, 'blog'=>false],
		'media' => true,
		'international' => ['languages'=>true, 'content'=>true, 'ui'=>true],
		'settings' => ['social'=>false, 'google-analytics'=>true],
		'people' => ['administrators'=>true, 'users'=>false]
	),
);