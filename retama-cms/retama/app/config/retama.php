<?php

return array(
	'enabled_modules' => array (
		'structure' => true,
		'navigation' => true,
		'content' => ['pages'=>true, 'news'=>true, 'blog'=>false],
		'media' => true,
		'international' => ['languages'=>true, 'content'=>true, 'ui'=>true],
		'settings' => ['social'=>false, 'google-analytics'=>true],
		'people' => ['administrators'=>true, 'users'=>true]
	),
);