<?php

//
// NOTE Migration Created: 2015-02-14 19:46:10
// --------------------------------------------------

class CreateCmsDatabase {
//
// NOTE - Make changes to the database.
// --------------------------------------------------

public function up()
{

//
// NOTE -- blog
// --------------------------------------------------

Schema::create('blog', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('indent')->nullable();
 $table->unsignedInteger('parent_id');
 $table->unsignedInteger('tree_left');
 $table->unsignedInteger('tree_right');
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->string('title', 255)->nullable();
 $table->text('post')->nullable();
 $table->unsignedInteger('user_id')->nullable();
 $table->string('user_alias', 50)->nullable();
 $table->string('user_ip', 20)->nullable();
 $table->boolean('allow_comments')->nullable()->default("1");
 $table->unsignedInteger('blogable_id')->nullable();
 $table->string('blogable_type', 255)->nullable();
 });


//
// NOTE -- catalog
// --------------------------------------------------

Schema::create('catalog', function($table) {
 $table->increments('id')->unsigned();
 });


//
// NOTE -- configuration
// --------------------------------------------------

Schema::create('configuration', function($table) {
 $table->increments('id')->unsigned();
 $table->string('slug', 50)->nullable();
 $table->string('title', 50)->nullable();
 $table->text('config')->nullable();
 });


//
// NOTE -- galleries
// --------------------------------------------------

Schema::create('galleries', function($table) {
 $table->increments('id')->unsigned();
 $table->string('title', 255)->nullable();
 $table->string('kind', 20)->nullable();
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- galleries_translation
// --------------------------------------------------

Schema::create('galleries_translation', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('translatable_id')->nullable();
 $table->unsignedInteger('language_id')->nullable();
 $table->string('title', 255)->nullable();
 $table->timestamp('created_art')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->nullable()->default("0000-00-00 00:00:00");
 });


//
// NOTE -- geo
// --------------------------------------------------

Schema::create('geo', function($table) {
 $table->unsignedInteger('id');
 $table->string('iso', 2);
 $table->string('name', 80);
 $table->string('nicename', 80);
 $table->string('iso3', 3)->nullable();
 $table->('numcode')->nullable();
 $table->unsignedInteger('phonecode');
 $table->unsignedInteger('parent_id');
 $table->unsignedInteger('level');
 $table->unsignedInteger('tree_left');
 $table->unsignedInteger('tree_right');
 $table->boolean('is_enabled')->nullable();
 $table->dateTime('created_at')->default("0000-00-00 00:00:00");
 $table->dateTime('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- languages
// --------------------------------------------------

Schema::create('languages', function($table) {
 $table->increments('id');
 $table->string('name', 50);
 $table->string('display_name', 50);
 $table->string('short_display_name', 50);
 $table->string('i18n', 20);
 $table->string('laravel_prefix', 2);
 $table->boolean('is_default');
 $table->boolean('is_enabled');
 $table->boolean('is_active');
 $table->string('script', 5);
 $table->string('region', 3);
 $table->string('variant', 10);
 });


//
// NOTE -- logs
// --------------------------------------------------

Schema::create('logs', function($table) {
 $table->increments('id');
 $table->string('action', 255)->nullable();
 $table->unsignedInteger('logable_id')->nullable();
 $table->string('logable_type', 255)->nullable();
 $table->unsignedInteger('user_id')->nullable();
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- media
// --------------------------------------------------

Schema::create('media', function($table) {
 $table->increments('id')->unsigned();
 $table->string('title', 255);
 $table->text('description')->nullable();
 $table->string('type', 20);
 $table->string('mime', 20)->nullable();
 $table->string('original_name', 255)->nullable();
 $table->string('extension', 4)->nullable();
 $table->unsignedInteger('size')->nullable();
 $table->unsignedInteger('width')->nullable();
 $table->unsignedInteger('height')->nullable();
 $table->unsignedInteger('credits')->nullable();
 $table->string('embed_url', 255)->nullable();
 $table->string('key', 50)->nullable();
 $table->string('source', 50)->nullable();
 $table->string('embed_thumb', 255)->nullable();
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- media_translation
// --------------------------------------------------

Schema::create('media_translation', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('translatable_id');
 $table->unsignedInteger('language_id');
 $table->string('title', 255);
 $table->text('description');
 });


//
// NOTE -- mediables
// --------------------------------------------------

Schema::create('mediables', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('media_id');
 $table->unsignedInteger('mediable_id');
 $table->string('mediable_type', 255);
 $table->string('slug', 255);
 $table->unsignedInteger('sort');
 });


//
// NOTE -- navigation
// --------------------------------------------------

Schema::create('navigation', function($table) {
 $table->increments('id')->unsigned();
 $table->string('name', 255);
 $table->unsignedInteger('parent_id');
 $table->unsignedInteger('tree_left');
 $table->unsignedInteger('tree_right');
 $table->string('slug', 255);
 $table->string('path', 255);
 $table->unsignedInteger('sitemap_id');
 $table->string('url', 255);
 $table->string('target', 255);
 $table->dateTime('updated_at')->default("0000-00-00 00:00:00");
 $table->dateTime('created_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- navigation_translation
// --------------------------------------------------

Schema::create('navigation_translation', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('translatable_id');
 $table->unsignedInteger('language_id');
 $table->string('name', 255);
 });


//
// NOTE -- news
// --------------------------------------------------

Schema::create('news', function($table) {
 $table->increments('id')->unsigned();
 $table->string('title', 255);
 $table->string('subtitle', 255);
 $table->text('preview');
 $table->('body');
 $table->text('extra')->nullable();
 $table->dateTime('publish_date');
 $table->boolean('publish')->default("1");
 $table->unsignedInteger('user_id')->nullable();
 $table->dateTime('created_at');
 $table->dateTime('updated_at');
 });


//
// NOTE -- news_translation
// --------------------------------------------------

Schema::create('news_translation', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('translatable_id');
 $table->unsignedInteger('language_id');
 $table->string('title', 255);
 $table->string('subtitle', 255);
 $table->text('preview');
 $table->('body');
 $table->('extra');
 });


//
// NOTE -- newsletter_suscribers
// --------------------------------------------------

Schema::create('newsletter_suscribers', function($table) {
 $table->increments('id')->unsigned();
 $table->string('email', 255)->nullable();
 });


//
// NOTE -- pages
// --------------------------------------------------

Schema::create('pages', function($table) {
 $table->increments('id')->unsigned();
 $table->string('title', 255);
 $table->text('preview');
 $table->('body');
 $table->('extra');
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- pages_translation
// --------------------------------------------------

Schema::create('pages_translation', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('translatable_id');
 $table->unsignedInteger('language_id');
 $table->string('title', 255);
 $table->text('preview');
 $table->('body');
 $table->('extra');
 });


//
// NOTE -- settings
// --------------------------------------------------

Schema::create('settings', function($table) {
 $table->increments('id')->unsigned();
 $table->string('slug', 255);
 $table->text('value');
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- sitemap
// --------------------------------------------------

Schema::create('sitemap', function($table) {
 $table->increments('id')->unsigned();
 $table->string('name', 255)->nullable();
 $table->unsignedInteger('parent_id');
 $table->unsignedInteger('tree_left');
 $table->unsignedInteger('tree_right');
 $table->string('slug', 255);
 $table->string('path', 255)->unique();
 $table->boolean('is_homepage');
 $table->string('seotitle', 255);
 $table->string('seokeywords', 255);
 $table->string('seodescription', 255);
 $table->unsignedInteger('sitemapable_id');
 $table->string('sitemapable_type', 255);
 $table->string('is_root', 255);
 $table->string('layout', 50)->default("page");
 $table->unsignedInteger('requires_auth');
 $table->string('is_redirect', 255);
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- user_profiles
// --------------------------------------------------

Schema::create('user_profiles', function($table) {
 $table->increments('id')->unsigned();
 $table->unsignedInteger('user_id')->nullable();
 $table->unsignedInteger('born_geo_id')->nullable();
 $table->unsignedInteger('residence_geo_id')->nullable();
 $table->string('title', 255)->nullable();
 $table->string('affiliation', 255)->nullable();
 $table->text('links')->nullable();
 $table->text('areas')->nullable();
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- users
// --------------------------------------------------

Schema::create('users', function($table) {
 $table->increments('id')->unsigned();
 $table->string('email', 50);
 $table->string('password', 64);
 $table->string('first_name', 50);
 $table->string('last_name', 50);
 $table->boolean('is_super');
 $table->unsignedInteger('group')->default("1");
 $table->string('remember_token', 100)->nullable();
 $table->dateTime('last_login')->default("0000-00-00 00:00:00");
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- widgets
// --------------------------------------------------

Schema::create('widgets', function($table) {
 $table->increments('id')->unsigned();
 $table->string('name', 255);
 $table->string('slug', 40);
 $table->text('data');
 $table->string('type', 40);
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });


//
// NOTE -- widgets_definitions
// --------------------------------------------------

Schema::create('widgets_definitions', function($table) {
 $table->increments('id')->unsigned();
 $table->string('name', 40);
 $table->text('definition');
 $table->timestamp('created_at')->default("0000-00-00 00:00:00");
 $table->timestamp('updated_at')->default("0000-00-00 00:00:00");
 });



}

//
// NOTE - Revert the changes to the database.
// --------------------------------------------------

public function down()
{

Schema::drop('blog');
Schema::drop('catalog');
Schema::drop('configuration');
Schema::drop('galleries');
Schema::drop('galleries_translation');
Schema::drop('geo');
Schema::drop('languages');
Schema::drop('logs');
Schema::drop('media');
Schema::drop('media_translation');
Schema::drop('mediables');
Schema::drop('navigation');
Schema::drop('navigation_translation');
Schema::drop('news');
Schema::drop('news_translation');
Schema::drop('newsletter_suscribers');
Schema::drop('pages');
Schema::drop('pages_translation');
Schema::drop('settings');
Schema::drop('sitemap');
Schema::drop('user_profiles');
Schema::drop('users');
Schema::drop('widgets');
Schema::drop('widgets_definitions');

}
}