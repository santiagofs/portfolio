<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('initdb', function(){

	die('test');
	$initer = new \InitDB;
	$initer->create('castbook');

	return false;

});




Route::get('tests', function()
{


	$table =  DB::table('sitemap')->get();
	$output = '';
	
	foreach ($table as $row) {
		//var_dump('test');
        $output .=  implode(",",(array)$row).PHP_EOL;
    }
    //var_dump($output);
    $headers = array(
        'Content-Type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename="ExportFileName.csv"',
    );
 
    return Response::make(rtrim($output, "\n"), 200, $headers);
/*
	
	$last_videos = \Media::translate()->with(['mediable' => function($q){
				return $q->where('mediables.mediable_type','Modules\Media\Models\Gallerys');
			}])->where('type', 'embed')->orderBy('id', 'desc')->take(3)->get();

	$queries = \DB::getQueryLog();
	//$last_query = end($queries);
	dd($queries);
*/
});

Route::post('set-cookie', function(){

	$target = Input::get('target');
	$key = Input::get('key');
	$value = Input::get('value');

	if($target) $key = $target.'.'.$key;
	Session::put($key, $value);
	return Session::all();
});

$prefix = \Language::url_prefix();
\App::setLocale(\Language::current()->laravel_prefix);
setlocale(LC_ALL,\Language::current()->i18n);

\Language::translate();


Route::group(array('prefix' => $prefix), function()
{

	if( $site_routes = \Site\ServiceProvider::get_routes()) \File::requireOnce($site_routes);

	//\Site\ServiceProvider::include_routes();
	//require_once( public_path().'config/routes.php');
	//$site_routes = public_path().'/config/routes.php';
	//if(\File::isFile($site_routes))

	/* Authentication routes */
	Route::post('login', function(){

		$username = \Input::get('username');
		$password = \Input::get('password');
		$rememberme = \Input::get('rememberme');
		$redirect = \URL::to( \Input::get('redirect') );
		/* agregar url redirect en el form! */

		if(\Auth::attempt(array('email' => $username, 'password' => $password), $rememberme))
		{
			if(\Request::ajax())
			{
				return \Response::json(array('success'=>false, 'message'=>''));
			}
			else
			{
				return \Redirect::to($redirect);
			}
		}
		else
		{

			if(\Request::ajax())
			{
				return \Response::json(array('success'=>false, 'message'=>'Wrong username or password'), 400);
			}
			else
			{
				return \Redirect::back()
					->with('message','Wrong username or password')
					->with('username', $username)
					->with('rememberme', $rememberme);
			}
		}
    });

	Route::get('logout', function(){
		\Auth::logout();
		return \Redirect::to('/');
	});

	//\View::addNamespace('front', public_path().'/views');
	// HOME PAGE
	Route::get('/', function() {

		$nav = \Sitemap::where('is_homepage','=',1)->first();
		$content_class = $nav->sitemapable_type;

		$content = $content_class::translate()->WithMedia()->where('id','=', $nav->sitemapable_id)->first();

		$view = $nav->layout ? 'site::'.$nav->layout : 'retama::no-layout';

		return \View::make($view, array('nav'=>$nav, 'content'=>$content) );
	});



	Route::any('{all}', function($uri)
	{

		$nav = \Sitemap::where('path','=',$uri)->first();
		// check url exists
		if(!$nav) App::abort(404);

		// must redirect ?
		if($redirect = $nav->check_redirect()) return $redirect;

		if($group = $nav->requires_auth)
		{
			if( (!\Auth::check()) OR (\Auth::user()->group < $group) )
			{
				return \View::make('site::errors.denied', array('nav'=>$nav) );
			}
		}

		$content_class = $nav->sitemapable_type;
		
		$content = new stdClass;
		if($content_class)
		{
			$content = $content_class::translate();
			if( $content_class::is_mediable() ) {
				$content = $content->WithMedia();
			}
			$content = $content->where('id','=', $nav->sitemapable_id)->first();
		}
		

		$view = $nav->layout ? 'site::'.$nav->layout : 'retama::no-layout';
		
		return \View::make($view, array('nav'=>$nav, 'content'=>$content, 'body_class' => $nav->layout) );

		//var_dump($nav->sitemapable);
		//die();

		// si el array tiene solo un elemento, buscamos en el modulo content
		// si tiene más, el primer elemento identifica el modulo
		// nos fijamos si el modulo tiene alias
		//$alias = Config::get('routes');
		//print_r($alias);
		// en funcion del alias cargamos el controlador
		//App::abort(404);
	    return 'Esto es cualquiera blanca';
	})->where('all', '.*');


});
