@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

	<header class="form-header">
		<h2>
			Message: {{ $model->subject }}

		</h2>
	</header>

	<div class="form-fields">

        @if($model->name)
        <div class="form-group">
            <label class="control-label">Name</label>
            <div class="form-control-static">{{ $model->name }}</div>
        </div>
        @endif

        @if($model->email)
        <div class="form-group">
            <label class="control-label">Email</label>
            <div class="form-control-static">{{ $model->email }}</div>
        </div>
        @endif

        @if($model->subject)
        <div class="form-group">
            <label class="control-label">Subject</label>
            <div class="form-control-static">{{ $model->subject }}</div>
        </div>
        @endif

        @foreach($model->body as $key => $value)
        <div class="form-group">
            <label class="control-label">{{$key}}</label>
            <div class="form-control-static">{{ $value }}</div>
        </div>
        @endforeach

	</div>

	{{ Form::close() }}





@stop
