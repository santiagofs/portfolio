<html>
	<head>
	</head>

	<body>

        @if($name)
		<p>
			<label>Name:</label> <strong>{{ $name }}</strong>
		</p>
        @endif

        @if($email)
		<p>
			<label>Email:</label> <strong>{{ $email }}</strong>
		</p>
        @endif

        @if($subject)
		<p>
			<label>Subject:</label> <strong>{{ $subject }}</strong>
		</p>
        @endif


        @foreach( $body as $label => $content)
		<label>{{$label}}:</label>
		<p> {{ $content }} </p>
        @endforeach

	</body>
</html>
