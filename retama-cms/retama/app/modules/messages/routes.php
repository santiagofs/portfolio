<?php

Route::group(array('prefix' => 'rt-admin/messages'), function()
{
    Route::controller('formtomail','\Modules\Messages\Controllers\FormToMail');

	Route::get('init-module', function() {


		if(! Auth::user()->is_super) return false;

		$q = "CREATE TABLE `messages` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(250) DEFAULT NULL,
              `email` varchar(250) DEFAULT NULL,
              `subject` varchar(250) DEFAULT NULL,
              `body` text,
              `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
              `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
              `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=unread',
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
		$response = DB::statement($q);
		dd($response);

	});

});

Route::post('messages/formtomail', ['as' => 'formtomail', function(){

	$input = \Input::all();

    if(isset($input['_token']))  unset($input['_token']);

    $view = 'messages::generic-email';
    if(isset($input['view'])) {
        $view = $input['view'];
        unset($input['view']);
    }

	$message = new \Message();

	if(isset($input['email'])) {
		$message->email = $input['email'];
		unset($input['email']);
	} else {
        $message->email = '';
    }

    if(isset($input['subject'])) {
		$message->subject = $input['subject'];
		unset($input['subject']);
	} else {
        $message->subject = 'Form To Mail from ' . \Config::get('site::settings.site.title');
    }

    if(isset($input['name'])) {
		$message->name = $input['name'];
		unset($input['name']);
	} else {
        $message->name = $message->email;
    }

    $message->body = $input;
    $message->save();

    $domain = str_replace('http://', '', str_replace('https://', '', Request::root()));

	$email = \Config::get('site::settings.site.main_email');
	if(!$email) \Response::json(array('success'=>true, 'message'=>$status), 200);

    $domain = str_replace('http://', '', str_replace('https://', '', Request::root()));

    $data = [
        'name' => $message->name,
        'email' => $message->email,
        'subject' => $message->subject,
        'body' => $message->body
    ];

    //return View::make($view,['message'=>$message] );
	$status = Mail::send($view, $data, function($m)  use ($message, $email, $domain)
	{
		$m->from('no-replay@'.$domain, 'Mailer '. $domain);
	    $m->to($email)->subject( ($message->subject ? $message->subject : 'Form To Mail') . ' - ' . $domain);
	});

	return \Response::json(array('success'=>true, 'message'=>$status), 200);

}]);
