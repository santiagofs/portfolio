<?php
namespace Modules\Messages;

class ServiceProvider extends \Modules\ServiceProvider {

    public function register()
    {

        parent::register('messages');
    }

    public function boot()
    {
        parent::boot('messages');
    }

}