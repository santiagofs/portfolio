<?php
namespace Modules\Messages\Models;

class Message extends \BaseModel {

	use \Modules\Retama\Models\LogableTrait;

	protected  $table = 'messages';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

	public static function unread() {
		$rs = static::where('is_read',0)->orderBy('created_at','DESC')->get();
		return $rs;
	}

	public function read() {
		$this->is_read = true;
		parent::save();
	}

	public function setBodyAttribute($value) {
		$this->attributes['body'] =  json_encode($value);
	}

	public function getBodyAttribute() {
		$value = $this->attributes['body'];
		return $value ? json_decode($value) : [];
	}

	public function save(array $options = array())
    {
	    parent::save($options);

		$this->log('New', 'subject', 'Email', true);
	}
}
