<?php
namespace Modules\Messages\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class FormToMail extends \Modules\Retama\RetamaController {
	protected $model_name = '\Modules\Messages\Models\Message';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'subject'		=> 'Subject',
				'name'			=> 'Name',
				'email'			=> 'Email',
				'created_at'	=> 'Date'
			),
			'subject'
		);


		$list->add_filter('text','subject', 'Subject');
		$list->add_filter('text','name', 'Name');
		$list->add_filter('text','email', 'Email');

		$list->creatable = false;

		\Breadcrumbs::add('Messages');
		\Breadcrumbs::add('Form To Mail');
		$this->render('retama::lists.list', array(
			'section_title' => 'Form To Mail',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id = NULL)
	{
		$model->read();


		\Breadcrumbs::add('Messages');
		\Breadcrumbs::add('Form To Mail');
		$this->render('messages::message-view', array(
			'section_title' => 'Form To Mail',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}
