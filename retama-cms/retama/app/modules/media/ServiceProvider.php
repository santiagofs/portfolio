<?php
namespace Modules\Media;

class ServiceProvider extends \Modules\ServiceProvider {

    public function register()
    {
        parent::register('media');
    }

    public function boot()
    {
        parent::boot('media');
    }

}