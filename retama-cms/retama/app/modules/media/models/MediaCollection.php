<?php
namespace Modules\Media\Models;

use \Illuminate\Support\Collection as Collection;

class MediaCollection {

	protected $types = array(

	);
	protected $slugs = array();


	public $all = array();

	public $thumb = null;

	public function __construct($items = array())
	{
		$this->types = array(
			'image' => new Collection,
			'text' => new Collection,
			'video' => new Collection,
			'embed' => new Collection,
			'audio' => new Collection
		);
		$this->items = $items;

		foreach($items as $item)
		{
			$this->index($item);
		}


		if(isset($this->slugs['thumb']))
		{
			$this->thumb = $this->slugs['thumb'][0];
		}
		else
		{
			foreach($items as $item){
				if( ($item->type == 'image') OR ($item->type == 'embed') )
				{
					$this->thumb = $item;
					break;
				}
			}
		}
		//var_dump($this->types['image']); die();

	}

	public function index($item)
	{

		$item->url = $item->url();
		$item->path = $item->path();
		$item->filename = $item->filename();

		$this->all[] = $item;

		if(!isset($this->slugs[$item->slug])) $this->slugs[$item->slug] = new Collection;
		$this->slugs[$item->slug]->push($item);

		if(!isset($this->types[$item->type])) $this->types[$item->type] = new Collection;
		$this->types[$item->type]->push($item);

	}

	public function typed($type = null)
	{
		if($type)
		{
			return isset($this->types[$type]) ? $this->types[$type] : new Collection;
		}
		return $this->types;
	}

	public function sluged($slug = null)
	{
		if($slug)
		{
			return isset($this->slugs[$slug]) ? $this->slugs[$slug] : new Collection;
		}

		return $this->slugs;
	}

	public function one($slug) {

		return isset($this->slugs[$slug]) ? $this->slugs[$slug][0] : new Media;
	}

	public function thumb()
	{
		return $this->thumb ?: new Media;
	}

}