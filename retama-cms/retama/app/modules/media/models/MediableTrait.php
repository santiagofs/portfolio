<?php
namespace Modules\Media\Models;

trait MediableTrait {

	public $media_assorted = null;

	public function scopeWithMedia($query)
	{
		if(in_array('Modules\International\Models\TranslatableTrait',class_uses($this)))
		{
			if(static::$language) {
				$query = $query->with('media.translations');
			}
		}
		else
		{
			$query = $query->with('media');
		}


		return $query;
	}

	public function scopeSluged($query, $slug)
	{
		return $query->where('mediable.slug','=',$slug);
	}

	public function scopeTyped($query, $type)
	{
		return $query->where('mediable.type','=',$type);
	}

    public function media()
    {
        $relation = $this->morphToMany('Media', 'mediable')->select('media.*','mediables.slug','mediables.sort')->orderBy('sort', 'asc');

        /*
$results = $relation->getResults();
		$this->media_assorted = new \Modules\Media\Models\MediaCollection($results);
*/

        return $relation;
    }

    public function save_gallery_from_post($slug='gallery')
    {
	    $media_arr = \Input::get($slug);

		if( !is_array($media_arr) || !count($media_arr)) return;

		$gallery_arr = [];
		$jsons = $media_arr['jsondata'];
		$ids = $media_arr['id'];
		$titles = $media_arr['title'];
		$descriptions = $media_arr['description'];

		$to_delete = [];

		for($i = 0; $i < count($ids); $i++)
		{
			$id = $ids[$i];
			$json = json_decode($jsons[$i]);

			if( ($json->status == 'delete') && $id)
			{
				$to_delete[] = $id;
				continue;
			}

			if( !isset($gallery_arr[$i]) ) $gallery_arr[$i] = ( !$id ? new \Media : \Media::find($id));

			$gallery_arr[$i]->original_name = $json->type == 'embed' ? '' : $json->name;
			$gallery_arr[$i]->type = $json->type;
			$gallery_arr[$i]->source = $json->type == 'embed' ?  $json->source : '';
			$gallery_arr[$i]->key = $json->type == 'embed' ?  $json->key : '';
			$gallery_arr[$i]->embed_url = $json->type == 'embed' ?  $json->embed_url : '';
			$gallery_arr[$i]->embed_thumb = $json->type == 'embed' ?  $json->embed_thumb : '';
			$gallery_arr[$i]->title = $titles[$i];
			$gallery_arr[$i]->description = $descriptions[$i];
			$gallery_arr[$i]->save();
			if($json->status == 'new') {
				$this->media()->attach($gallery_arr[$i], array('slug'=>$slug,'sort'=>$i));
			} else {
				\DB::update('UPDATE mediables SET sort = ? WHERE mediable_type = ? and mediable_id = ? and media_id = ?', array($i, get_class($this), $this->id, $gallery_arr[$i]->id));

			}

		}

		if(count($to_delete)) {
			$this->media()->detach($to_delete);
			foreach($to_delete as $id)
			{
				\Media::find($id)->delete();
			}
		}
    }

    public function save_file_from_post($slug, $input_name = null)
    {

		$input_name || $input_name = $slug;
		$title = \Input::get($input_name.'_title');
		$description = \Input::get($input_name.'_description');
		$media = $this->media_assorted()->one($slug);

		$new = !(bool)$media->id;
		$filename = $filename = \Input::get($input_name);

		if($new && (!$filename)) return;


		$media = $new ? new \Media : \Media::find($media->id);

		if($filename)
		{
			$title || $title = $filename;
			$media->from_filename($filename, $title, $description);

			if($new)
			{
				$this->media()->attach($media, array('slug'=>$slug,'sort'=>0));
			}
		}


		if($title) $media->title = $title;
		$media->description = $description;
		$media->save();

		$status = \Input::get($input_name.'_status');
		//var_dump($status); die();
		if($status == 'delete') $media->delete();
    }

    protected $assorted = null;
    public function media_assorted()
    {
	    if(!$this->assorted) $this->assorted = new \Modules\Media\Models\MediaCollection($this->media);
	    return $this->assorted;
    }


}
