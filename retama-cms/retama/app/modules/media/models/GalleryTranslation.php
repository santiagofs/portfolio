<?php
namespace Modules\Media\Models;

class GalleryTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['title'];
	protected $fillable = ['title'];

	protected $table = 'galleries_translation';
}