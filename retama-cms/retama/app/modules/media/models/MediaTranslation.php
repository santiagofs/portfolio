<?php
namespace Modules\Media\Models;

class MediaTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['title','description'];
	protected $fillable = ['title','description'];

	protected $table = 'media_translation';
}