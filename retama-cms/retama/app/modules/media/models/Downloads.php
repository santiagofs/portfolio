<?php
namespace Modules\Media\Models;

class Downloads extends \BaseModel {

	use \Modules\Media\Models\MediableTrait;
	use \Modules\International\Models\TranslatableTrait;

	protected  $table = 'downloads';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}


	public function save(array $options = array())
    {
	    parent::save($options);

		$this->save_file_from_post('main_image');
		$this->save_file_from_post('download');
    }



}
