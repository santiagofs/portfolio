<?php
namespace Modules\Media\Models;

class Mediable extends \BaseModel {

	protected  $table = 'mediables';

	protected $hidden = array();
	protected $guarded = ['id'];

}