<?php
namespace Modules\Media\Models;

class Media extends \BaseModel {

	use \Modules\International\Models\TranslatableTrait;

	protected  $table = 'media';

	protected $hidden = array();
	protected $guarded = ['id', 'path'];
	protected $fillable = ['title','description','type','mime','original_name','extension','size','width','height','credits','url','key','source'];

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

	public function mediable()
    {
        return $this->morphTo();
    }

    public function save(array $options = array())
    {
	    $is_new = !(bool)$this->id;
	    $not_embed = !(bool)($this->type == 'embed');
	    if($is_new && $not_embed)
	    {
		    $filename = $this->original_name;
		    $temp_path = public_path().'/files/temp/'.$filename;
		    $this->extension = \File::extension($temp_path);

	    }

	    parent::save($options);

		if($is_new && $not_embed)
		{
			\File::move($temp_path, public_path().'/files/media/'.$this->filename());
		}
    }

    public function delete()
    {
		/* Improve: verificar que el archivo no este atachado a ningun mediable antes de borrarlo */
	    \File::delete($this->path());
	    return parent::delete();
    }

    public function from_filename($filename, $title = '', $description = '')
    {
		$this->original_name = $filename;
		$filename = $this->original_name;
	    $temp_path = public_path().'/files/temp/'.$filename;
	    $mimefile = new \Symfony\Component\HttpFoundation\File\File($temp_path);
	    $this->extension = \File::extension($temp_path);
		$this->type = $this->set_type_from_mime( $mimefile->getMimeType() );
		$this->title = $this->original_name;

		parent::save();

		\File::move($temp_path, public_path().'/files/media/'.$this->filename());

    }
	protected function set_type_from_mime($mime)
	{
		$type = 'text';
		if(strpos($mime,'video') !== FALSE) $type = 'video';
		if(strpos($mime,'image') !== FALSE) $type = 'image';
		if(strpos($mime,'audio') !== FALSE) $type = 'audio';

		return $type;
	}



    public static function set_language($language)
	{
		static::$language = $language;
	}


    public function filename()
    {
	    if($this->type == 'embed') {
		    $this->extension = pathinfo($this->embed_thumb, PATHINFO_EXTENSION);
	    }
	    return $this->id . '.' . $this->extension;
    }

	public function url($width=null, $height=null, $crop=null)
	{
		if(!$this->id) return null;

		if($this->type == 'embed') return $this->embed_thumb;
		if( ($this->type == 'image') )
		{
			$params = [];
			if($width) $params['width'] = $width;
			if($height) $params['height'] = $height;
			if($crop) $params['crop'] = $crop;

			$str = http_build_query($params);

			$url = 'media/'.$this->filename() . ($str ? '?'.$str : '' );
		} else {
			$url = 'files/media/'.$this->filename();
		}
		return \URL::to($url,false);
	}

	public function path()
	{

		return $this->id ? public_path().'/files/media/'.$this->filename() : null;
	}

	public function img($width=null, $height=null, $crop=null, $attr= array())
	{
/*
		$params = [];
		if($width) $params['width'] = $width;
		if($height) $params['height'] = $height;
		if($crop) $params['crop'] = $crop;

		if(count($params))
		{
			$params = implode('&', $params);
		}
*/
		//var_dump($this->url($width, $height, $crop));
		$url = $this->url($width, $height, $crop) ;

		return \HTML::image($url, $this->title, $attr);

	}

	public function download()
	{
		return \URL::route('download', $this->id);
	}


	/*
public function newCollection(array $models = array())
	{
		 return new \Modules\Media\Models\MediaCollection($models);
	}
*/

	/* Accessors / Mutators */
	public function getTitleAttribute($value)
	{
		if( !($ret = $value) )
		{
			$ret = $this->getAttribute('original_name');
		}
		return $ret;

	}
	/* relations */


	public function pages()
    {
        return $this->morphedByMany('Page', 'mediable');
    }

    public function news()
    {
        return $this->morphedByMany('News', 'mediable');
    }

    public function profile()
    {
        return $this->morphedByMany('UserProfile', 'mediable');
    }

    public function Gallery()
    {
        return $this->morphedByMany('Gallery', 'mediable');
    }


}
