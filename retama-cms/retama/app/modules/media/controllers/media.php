<?php
namespace Modules\Media\Controllers;

use \Symfony\Component\HttpFoundation\File\UploadedFile as UploadedFile;

class Media extends \BaseController {

	public function postUpload()
	{
		$target_folder = public_path().'/files/temp/';

		$maxsize = UploadedFile::getMaxFilesize();
		$max_filesize_error = sprintf('The file size should be lower than %s%s.',$maxsize / 1024,'mb');

		if((empty($_FILES) && empty($_POST) && isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) == 'post'))
        {
        	return \Response::json(array('status'=>'error', 'message'=>$max_filesize_error), 400);
        }


		try {
			//if (!\Input::hasFile('file')) return;

			$file = \Input::file('media_upload'); // your file upload input field in the form should be named 'file'
			//var_dump(get_class($file)); die();
/*
			$size = $file->getSize();

            if ($size > $file_max) \Response::json($max_filesize_error, 400);
*/

			$filename = $file->getClientOriginalName();

			$success = $file->move($target_folder, $filename);

			$full_path = $target_folder.$filename;
			//dd($full_path);
			chmod($target_folder.$filename, 0777);

			return \Response::json(array('status'=>'success'), 200); // or do a redirect with some message that file was uploaded

		} catch (\Exception $e) {

			return \Response::json(array('status'=>'error', 'message'=>$e->getMessage()), 400);
		}

	}

	public function getFile($filename)
	{
		$path = explode('/', $filename);
		$path = count($path)>1 ? $filename : 'media/'.$filename;

		//dd($path);
		//$img = \Image::make(public_path().'/files/'.$path);

		$img = \Image::cache(function($image) use ($path) {
			$width = \Input::get('width', 1920); // le damos un ancho maximo y forzamos recompresion
			$height = \Input::get('height', null);
			$method = \Input::get('crop', $height ? 'fit' : 'resize');
			$image->make(public_path().'/files/'.$path);

			if($width || $height) {

				$image->$method($width, $height,function ($constraint) {
					$constraint->upsize();
					$constraint->aspectRatio();
				});
			}

		} , 0, true);
		return $img->response();
	}

	public function getDownload($id=null)
	{
		if(!$id) die();

		$media = \Media::find($id);
		if(!$media) die();


		$file= public_path(). "/files/media/".$media->filename();
		$headers = array();
/*
        $headers = array(
              'Content-Type: application/pdf',
            );
*/
        return \Response::download($file, $media->original_name, $headers);

		dd($id);

	}
}