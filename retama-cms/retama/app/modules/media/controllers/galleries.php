<?php
namespace Modules\Media\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Galleries extends \Modules\Retama\RetamaController {
	protected $model_name = '\Gallery';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'title'		=> 'Titulo',
			),
			'title'
		);


		$list->add_filter('text','title', 'Titulo');


		\Breadcrumbs::add('Media');
		\Breadcrumbs::add('Galleries');
		$this->render('retama::lists.list', array(
			'section_title' => 'Galleries',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		$sitemap = $model->sitemap;
		$media = $model->media;
		if(!$model->id)
		{
			$model->sitemap = new \Sitemap;
			$model->sitemap->parent_id = $parent_id ?: \Input::get('parent_id', 1);
		}

		//var_dump($errors); die();
		\Breadcrumbs::add('Media');
		\Breadcrumbs::add('Galleries');
		$this->render('media::gallery-edit', array(
			'section_title' => 'Galleries',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}