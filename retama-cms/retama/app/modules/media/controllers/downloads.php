<?php
namespace Modules\Media\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Downloads extends \Modules\Retama\RetamaController {
	protected $model_name = '\Modules\Media\Models\Downloads';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'title'		=> 'Titulo',
// 				'sitemap.path' => 'Path'
			),
			'title'
		);

		$list->add_filter('text','titulo', 'Titulo');

		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('retama::lists.list', array(
			'section_title' => 'Pages',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{

		$media = $model->media;

		//var_dump($errors); die();
		\Breadcrumbs::add('Media');
		\Breadcrumbs::add('Downloads');
		$this->render('media::download-edit', array(
			'section_title' => 'Downloads',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}