<?php

Route::group(array('prefix' => 'rt-admin/media'), function()
{
	Route::controller('galleries','\Modules\Media\Controllers\Galleries');
	Route::controller('downloads','\Modules\Media\Controllers\Downloads');
   /*
 Route::controller('content/structure','\Modules\Content\Controllers\Structure');
	Route::controller('content/pages','\Modules\Content\Controllers\Pages');
	Route::controller('content/blog','\Modules\Content\Controllers\Blog');
	Route::controller('content/news','\Modules\Content\Controllers\News');
*/
});

Route::group(array('prefix' => 'media'), function()
{

	Route::post('upload','\Modules\Media\Controllers\Media@postUpload');
	Route::get('download/{id?}',array('as' => 'download', 'uses' => '\Modules\Media\Controllers\Media@getDownload'));
    Route::any('{all}', '\Modules\Media\Controllers\Media@getFile')->where('all', '.*');

});
