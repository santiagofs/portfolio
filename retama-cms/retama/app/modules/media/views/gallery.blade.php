<ul media-gallery="{{ $name }}" with-embeds="{{ $with_embeds}}" class="media-gallery" gallery-items='{{ $gallery_items or '[]' }}' title-placeholder="{{ $title_placeholder or 'title' }}"  description-placeholder="{{ $description_placeholder or 'description' }}"></ul>

@if( isset($with_embeds) && $with_embeds)
<?php
	$button = ['second_button'=>'Add Video Embed', 'second_button_attributes'=>['class'=>'btn btn-default', 'trigger-video-embed'=>$name]];
?>
@else
<?php
	$button = [];
?>
@endif


{{ Form::one('uploader', $name, null, null, $button) }}
