@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => URL::full(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">

		<h2>
			@if($model->id)
			Edit Download: {{ $model->title }}
			@else
			New Download
			@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}

		<?php
			$attr = array('class'=>'form-control required');
		?>
		{{ Form::one('text', 'title', 'Title', null, $attr) }}

		{{ Form::one('textarea', 'description', 'Description', null, $attr) }}

		{{ Form::one('file', 'download', 'Download', null, array('file'=>$model->media_assorted()->one('download'))) }}

		{{ Form::one('file', 'main_image', 'Main Image', null, array('file'=>$model->media_assorted()->one('main_image'))) }}



	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


