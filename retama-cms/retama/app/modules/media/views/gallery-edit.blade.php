@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => URL::full(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<ul class="nav nav-pills">
			<li role="presentation" class="active"><a href="#tab-main" data-toggle="tab">Main</a></li>
			<!-- <li role="presentation"><a href="#tab-gallery" data-toggle="tab">Gallery</a></li> -->
			<li role="presentation"><a href="#tab-seo" data-toggle="tab">SEO</a></li>
		</ul>
		<h2>
			@if($model->id)
			Edit Gallery: {{ $model->title }}
			@else
			New Gallery
			@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}
		{{ Form::hidden('sitemap[parent_id]') }}

		<div class="tab-content">
		<div id="tab-main" class="tab-pane active">
		<?php
			$attr = array('class'=>'form-control required');
			if(!$model->id) {
				$attr['slugify-to'] = '#sitemap-slug';
				$attr['copy-to'] = '#sitemap-title';
			}
			$attr['copy-to'] = '#sitemap-name';
		?>
		{{ Form::one('text', 'title', 'Title', null, $attr) }}

		{{ Form::one('select', 'kind', 'Kind', null, ['class'=>'form-control', 'list'=>['image' => 'Image', 'video' => 'Video', 'mixed' => 'Mixed']]) }}
		<?php
			$gallery_items =$model->media_assorted()->sluged('gallery');
			$gallery_items = $gallery_items->toJson(JSON_HEX_APOS | JSON_HEX_QUOT)
		?>

		@include('media::gallery', array('name'=>'gallery', 'with_embeds'=>true, 'gallery_items'=> $gallery_items))

		</div>


<!--
		<div id="tab-gallery" class="tab-pane">

		</div>
-->


		<div id="tab-seo" class="tab-pane">
		@include('content::sitemap-fields')
		</div>
		</div>
	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


