<?php
namespace Modules\International\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Content extends \Modules\Retama\RetamaController {
	protected $model_name = '\Geo';


	protected function get_config($module = null) {
		$config = \Config::get('retama::translatables');
		$site_translatables = \Config::get('site::translatables');
		if($site_translatables) {
			$config = array_merge($config, $site_translatables);
		}

		if($module) $config = $config[$module];

		return $config;
	}
	public function getList()
	{

		// armamos la lista a mano
		$result = array();
		$modules = array();
		$languages = \Language::where('is_active','=',1)->orderBy('is_default','DESC')->get();

		$translatables = $this->get_config();

		foreach($translatables as $key => $ops)
		{

			$original_count =  isset($ops['original_count']) ? $ops['original_count']() : \DB::table($ops['table'])->count();
			$translations =  isset($ops['translation_count']) ? $ops['translation_count']() : $navs_translations = \DB::table($ops['table'].'_translation')
                     ->select(\DB::raw('count(*) as trans_count, language_id'))
                     ->groupBy('language_id')
                     ->lists('trans_count', 'language_id');
			$modules[$key] = [
				'default' => $original_count,
				'translations'=> $translations
			];
		}


		foreach($languages as $language)
		{
			foreach($modules as $key => $values)
			{
				$default = $values['default'];
				$translations = $values['translations'];

				if($language->is_default)
				{
					$result[$key]['lang_'.$language->id] =  $default . ' (100%)';

				} else {
					$percent = isset($translations[$language->id]) ? $translations[$language->id] . ' ('. round($translations[$language->id] * 100/ $default, 2)  .'%)' : '0 (0%)';
					$result[$key]['lang_'.$language->id] =  $percent;

				}

			}

		}

		\Breadcrumbs::add('International');
		\Breadcrumbs::add('Content Translation');
		$this->render('international::retama.language-modules-list', array(
			'section_title' => 'Content Translation',
			'languages' => $languages,
			'result' => $result
		));
	}


	public function getModule($module, $language_id) {

		$config = $this->get_config($module);
		$language = \Language::find($language_id);

		$class_name = $config['class'];


		$records = $class_name::translate($language)
			->orderBy($config['main'], 'ASC');

		if(isset($config['get_module_query']))
		{
			$records = $config['get_module_query']($records);
		}



		$records = $records->paginate(20);

		\Breadcrumbs::add('International');
		\Breadcrumbs::add('Content Translation');
		$this->render('international::retama.language-module-list', array(
			'section_title' => 'Translate Module: ' . $config['caption'] . ' to ' . $language->name,
			'list' => $records,
			'data' => $config,
			'module' => $module,
			'language_id' => $language_id
		));
	}

	public function getItem($module, $id, $language_id)
	{
		$config = $this->get_config($module);
		$language = \Language::find($language_id);
		$class_name = $config['class'];

		$model = $class_name::find($id);
		$translation_class = $model->getTranslationModelName();

		$translation = $translation_class::where('translatable_id',$id)->where('language_id',$language_id)->first();
		$view = isset($config['view']) ? $config['view'] : 'international::retama.modules.edit';

		$config['fields'] = $class_name::fields_config();

		\Breadcrumbs::add('International');
		\Breadcrumbs::add('Content Translation');
		$this->render($view, array(
			'section_title' => 'Translate Module: ' . $config['caption'] . ' to ' . $language->name,
			'original' => $model,
			'translation' => $translation,
			'config' => $config,
			'module' => $module,
			'language_id' => $language_id
		));
	}

	public function postItem($module, $id, $language_id)
	{

		$config = $this->get_config($module);
		$language = \Language::find($language_id);
		$class_name = $config['class'];

		$model = $class_name::find($id);
		$translation_class = $model->getTranslationModelName();

		$translation = $translation_class::where('translatable_id',$id)->where('language_id',$language_id)->first();
		$translation || $translation = new $translation_class;

		$validator = $translation->input_validate();
		if($validator->passes())
		{
			$translation->input_hydrate();
			$translation->translatable_id = $id;
			$translation->language_id = $language_id;

			$translation->save();

			return \Redirect::to('rt-admin/international/content/module/'.$module.'/'.$language_id);
		}
		else
		{
			return \Redirect::to('rt-admin/international/content/item/'.$module.'/'.$id.'/'.$language_id)->withInput()->withErrors($validator);;
		}

	}


}