<?php
namespace Modules\International\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Languages extends \Modules\Retama\RetamaController {
	protected $model_name = '\Language';

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'name'		=> 'Name',
				'display_name'		=> 'Display Name',
			),
			'name'
		);


		$list->add_filter('text','name', 'Name');
		$list->add_filter('text','display_name', 'Display Name');


		\Breadcrumbs::add('International');
		\Breadcrumbs::add('Countries');
		$this->render('retama::lists.list', array(
			'section_title' => 'Countries',
			'list' => $list,
		));
	}


}