<?php
namespace Modules\International\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Ui extends \Modules\Retama\RetamaController {


	public function getList()
	{
		$languages = \Language::where('is_active','=',1)->orderBy('is_default','DESC')->get();
		list($retama, $site) = $this->collect_files(\Language::getDefault()->laravel_prefix);

		\Breadcrumbs::add('International');
		\Breadcrumbs::add('UI Translation');
		$this->render('international::retama.ui-list', array(
			'section_title' => 'UI Translation',
			'languages' => $languages,
			'retama' => $retama,
			'site' => $site
		));
	}

	public function getFile($context,$key, $language_id)
	{

		$language = \Language::find($language_id);
		$prefix = $language->laravel_prefix;
		$original_prefix = \Language::getDefault()->laravel_prefix;

		list($retama, $site) = $this->collect_files($prefix);

		$list = $$context;
		$caption = $key;
		$file = str_replace(' ', '_', $key);

		$original = \Modules\International\Models\UI::get_lang_array($context, $file, $original_prefix);
		$trans = \Modules\International\Models\UI::get_lang_array($context, $file, $prefix);


		\Breadcrumbs::add('International');
		\Breadcrumbs::add('UI Translation');
		$this->render('international::retama.ui-file', array(
			'section_title' => 'UI Translation',
			'language' => $language,
			'original' => $original,
			'trans' => $trans,
			'caption' => $caption
		));
	}

	public function postFile($context, $key, $language_id)
	{
		$fields = \Input::get();;

		$language = \Language::find($language_id);
		$prefix = $language->laravel_prefix;

		list($retama, $site) = $this->collect_files($prefix);

		$list = $$context;
		$file = str_replace(' ', '_', $key);
		$trans = \Modules\International\Models\UI::get_lang_array($context, $file, $prefix);
		$path = $this->get_file_path($context, $prefix, $file);

		foreach($fields as $name => $value)
		{
			if($name == '_token') continue;
			$trans[$name] = $value;
		}


		if(file_exists($path)) unlink($path);
		\File::put($path, '<?php return ' . var_export($trans, true) . ';');
		chmod($path, 0777);

		return \Redirect::to('rt-admin/international/ui');

	}


	protected function get_file_path($context, $prefix, $filename)
	{
		if($context = 'site') return  \Site\ServiceProvider::get_path().'/lang/'.$prefix.'/'.$filename.'.php';

		return '';
	}

	protected function collect_files($prefix)
	{
		//$prefix = \Language::getDefault()->laravel_prefix;

		$retama_path = app_path().'/modules/retama/lang/'.$prefix;
		$site_path = \Site\ServiceProvider::get_path().'/lang/'.$prefix;

		$retama_files = \File::files($retama_path);
		$site_files = \File::files($site_path);

		$retama = [];
		foreach($retama_files as $file)
		{
			$key = str_replace('_',' ', basename($file,'.php'));
			$retama[$key] = $file;
		}

		$site = [];
		foreach($site_files as $file)
		{
			$key = str_replace('_',' ', basename($file,'.php'));
			$site[$key] = $file;
		}

		return [$retama, $site];
	}

}