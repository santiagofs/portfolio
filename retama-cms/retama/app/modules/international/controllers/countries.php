<?php
namespace Modules\International\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Countries extends \Modules\Retama\RetamaController {
	protected $model_name = '\Geo';

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'nicename'		=> 'Name',
			),
			'nicename'
		);

		$list->add_filter('text','nicename', 'Name');

		\Breadcrumbs::add('International');
		\Breadcrumbs::add('Countries');
		$this->render('retama::lists.list', array(
			'section_title' => 'Countries',
			'list' => $list,
		));
	}


}