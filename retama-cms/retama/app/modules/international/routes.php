<?php

Route::group(array('prefix' => 'rt-admin/international'), function($router)
{
	//dd(get_class($router));
    Route::controller('countries','\Modules\International\Controllers\Countries');
	Route::controller('languages','\Modules\International\Controllers\Languages');
	Route::controller('content','\Modules\International\Controllers\Content');
	Route::controller('ui','\Modules\International\Controllers\Ui');
});