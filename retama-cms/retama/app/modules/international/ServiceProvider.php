<?php
namespace Modules\International;

class ServiceProvider extends \Modules\ServiceProvider {

    public function register()
    {
        parent::register('international');
    }

    public function boot()
    {
        parent::boot('international');
    }

}