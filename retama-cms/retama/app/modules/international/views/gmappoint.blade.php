<div class="form-group file gmap-point {{ $required or '' }}">

	{{ Form::label($name.'-gmap-point', $label )}}
	<div class="form-control-holder">
		<div class="name-display" id="{{ $name }}-gmap-point-display">{{ $fulladdress or '' }}</div>
		<input type="hidden" name="{{ $name }}-gmap-point" id="{{ $name }}-gmap-point" value='{{ $json or '' }}' />

		<div class="gmap-point-select-control" style="padding:6px 0px">
		<a class="btn btn-default" id="{{ $name }}-gmap-point-select" gmap-trigger="{{ $name }}"><span><i class="fa fa-map-marker"></i> Set Point</span></a>
		</div>
	@if ($errors->has($name))
		<label id="id-{{$name}}-error" class="error" for="id-{{$name}}-gmap-point">{{ $errors->first($name) }}</label>
	@endif
	</div>

</div>