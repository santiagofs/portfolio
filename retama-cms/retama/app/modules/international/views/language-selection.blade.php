@if(count($arr_languages) > 1)
<ul class="language-selection">
	<li class="current-language "><i class="{{ $current_language->i18n }}"></i> <span class="text">{{ $current_language->{$display_name} }}</span>
	<ul>
		@foreach($arr_languages as $label => $language)
		<li class="">
			<a href="{{ URL::to($language->href,false) }}"><i class="{{ $language->language->i18n }}"></i> <span class="text">{{ $label }}</span></a>
		</li>
		@endforeach
	</ul>
	</li>
</ul>
@else
<?php
	$label = array_keys($arr_languages)[0];
	$language = array_values($arr_languages)[0];
?>
	<span class="current-language">
		<a href="{{ URL::to($language->href,false) }}"><i class="{{ $language->language->i18n }}"></i> <span class="text">{{ $label }}</span></a>
	</span>
@endif