@extends('retama::edit.edit')

@section('form-header')

@stop

<?php
	$view = 'international::retama.ui-file-field';
?>
@section('form')

{{ Form::open(array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			Translate UI: <strong>{{ $caption }}</strong> to {{ $language->display_name }}
		</h2>
	</header>
	<div class="form-fields">

		@foreach($original as $key => $item)
			@include($view, array(
				'name' => $key,
				'label' => ucwords(str_replace('_', ' ', $key)),
				'original' => $original[$key],
				'input' => Form::text($key, isset($trans[$key]) ? $trans[$key] : $original[$key], array('class'=>'form-control required'))
			))

		@endforeach
	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="#" onclick="window.history.go(-1); return false" class="btn btn-default">Cancel</a>

	</footer>

{{ Form::close() }}





@stop


@section('footer-scripts')
@parent

<script type="text/javascript">
	$.validator.messages.required = '{{  \Lang::get('retama::errors.required') }}';
	$.validator.messages.email = '{{  \Lang::get('retama::errors.email') }}';
	$.validator.messages.dateISO = '{{  \Lang::get('retama::errors.dateISO') }}';
	$.validator.messages.number = '{{  \Lang::get('retama::errors.number') }}';
	$.validator.messages.perma = '{{  \Lang::get('retama::errors.perma') }}';
	$.validator.setDefaults({ ignore: ":hidden:not([summernote])" })
	//$.validator.setDefaults({ ignore: ':hidden.ignore' })

	$(function(){

		$('#form-edit').validate({
	        // other rules & options,
	        highlight: function (element, errorClass, validClass) {
	            $(element).parents('.form-group').addClass('error');
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $(element).parents('.form-group').removeClass('error');
	        },
	        invalidHandler: function(form, validator) {
	        	var $error = $(validator.errorList[0].element);
				var $tab = $error.closest('.tab-pane');
				if($tab.length) {
					var id = '#'+$tab.attr('id');
					$('a[href="'+id+'"]').trigger('click');
				}
			}
	    });


	})

</script>
@stop