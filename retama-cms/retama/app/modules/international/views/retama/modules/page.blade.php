@extends('retama::edit.edit')

@section('form-header')

@stop

<?php
	$view = 'international::retama.modules.field';
?>
@section('form')

{{ Form::model($translation, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			Translate Page: {{ $original->title }}
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}

		@include($view, array(
			'name' => 'title',
			'label' => 'Title',
			'original' => $original,
			'input' => Form::text('title', null, array('class'=>'form-control required'))
		))

		@include($view, array(
			'name' => 'preview',
			'label' => 'Preview',
			'original' => $original,
			'input' => Form::textarea('preview', null, array('class'=>'form-control'))
		))

		@include($view, array(
			'name' => 'body',
			'label' => 'Page body',
			'original' => $original,
			'input' => Form::textarea('body', null, array('class'=>'form-control', 'tinymce'=>true, 'id'=>'id-body'))
		))





	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


