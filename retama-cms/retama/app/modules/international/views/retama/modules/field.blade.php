<div class="form-group">
	{{ Form::label($name, $label )}}
	<div class="form-control-holder">
	{{ $input }}
	@if ($errors->has($name))
		<label id="id-{{$name}}-error" class="error" for="id-{{$name}}">{{ $errors->first($name) }}</label>
	@endif
		<div class="original">
			<label>Original:</label>
			<div class="original-content">
			{{ $original->{$name} }}
			</div>
		</div>
	</div>
</div>