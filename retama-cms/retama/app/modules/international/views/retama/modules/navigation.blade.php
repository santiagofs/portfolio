@extends('retama::edit.edit')

@section('form-header')

@stop

<?php
	$view = 'international::retama.modules.field';
?>
@section('form')

{{ Form::model($translation, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			Translate Page: {{ $original->title }}
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}

		@include($view, array(
			'name' => 'name',
			'label' => 'Name',
			'original' => $original,
			'input' => Form::text('name', null, array('class'=>'form-control required'))
		))




	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


@section('footer-scripts')
@parent

<script type="text/javascript">
	$.validator.messages.required = '{{  \Lang::get('retama::errors.required') }}';
	$.validator.messages.email = '{{  \Lang::get('retama::errors.email') }}';
	$.validator.messages.dateISO = '{{  \Lang::get('retama::errors.dateISO') }}';
	$.validator.messages.number = '{{  \Lang::get('retama::errors.number') }}';
	$.validator.messages.perma = '{{  \Lang::get('retama::errors.perma') }}';
	$.validator.setDefaults({ ignore: ":hidden:not([summernote])" })
	//$.validator.setDefaults({ ignore: ':hidden.ignore' })

	$(function(){

		$('#form-edit').validate({
	        // other rules & options,
	        highlight: function (element, errorClass, validClass) {
	            $(element).parents('.form-group').addClass('error');
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $(element).parents('.form-group').removeClass('error');
	        },
	        invalidHandler: function(form, validator) {
	        	var $error = $(validator.errorList[0].element);
				var $tab = $error.closest('.tab-pane');
				if($tab.length) {
					var id = '#'+$tab.attr('id');
					$('a[href="'+id+'"]').trigger('click');
				}
			}
	    });

		$('[summernote]').summernote({
			toolbar: [
				['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
			   /*  ['font', ['strikethrough']], */
			    ['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['insert', ['link', 'picture', 'video', 'table']],
				['misc', ['fullscreen', 'codeview', 'undo', 'redo']]
			],
			height: 240,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			styleWithSpan: false
		});


	})

</script>
@stop