@extends('retama::edit.edit')

@section('form-header')

@stop

<?php
	$view = 'international::retama.modules.field';

?>
@section('form')

{{ Form::model($translation, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			Translate {{ $config['caption'] }}: {{ $original->title }}
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}

		@foreach($config['fields'] as $name => $field)
		<?php
			$css_class= ['form-control'];

			if( isset($field['css_class']) && $field['css_class'] ) $class[] = $field['css_class'];

			$attr = isset($field['attr']) && $field['attr'] ? $field['attr'] : [];

			$attr['class'] = implode(' ', $css_class);
			$type = $field['type'];
			$label = $field['label'];
		?>
		@include($view, array(
			'name' => $name,
			'label' => $label,
			'original' => $original,
			'input' => Form::text($name, null, $attr)
		))

		@endforeach


	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


