@extends('retama::layouts.content')

@section('content-content')

<div class="records-list-holder rounded-box">
	<table cellpadding="0" cellspacing="0" class="records-list">

		<thead>
			<tr>
				<td colspan="3">
					<div class="clear-block">
						{{ $list->links() }}
					</div>
				</td>
			</tr>
			<tr>
				<th>{{ ucfirst($data['main']) }}</th>
				<th> Is translated? </th>
				<th class="actions"></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td>
					<div class="clear-block">
						{{ $list->links() }}
					</div>
				</td>
			</tr>
		</tfoot>

		<tbody>
			@foreach($list as $record)

			<tr>
				<td>
					<a href="{{ URL::to('rt-admin/international/content/item/'.$module.'/'.$record->id.'/'.$language_id, false) }}">
						{{ $record->{$data['main']} }}
					</a>
				</td>

				<td>{{ count($record->translations) ? 'Yes <i class="fa fa-check"></i>'  : 'No <i class="fa fa-times"></i>' }}
				<td class="actions">
					<a href="{{ URL::to('rt-admin/international/content/item/'.$module.'/'.$record->id.'/'.$language_id, false) }}"  class="fa fa-edit"></a>
				</td>

			</tr>
			@endforeach
		</tbody>

	</table>
</div>


@stop