@extends('retama::layouts.content')

@section('content-content')

<div class="records-list-holder rounded-box">
	<table cellpadding="0" cellspacing="0" class="records-list">

		<thead>
			<tr>
				<th>Module</th>
				@foreach($languages as $language)
				<th>{{ $language->name }}</th>
				@endforeach
			</tr>

		</thead>


		<tbody>

			@foreach($site as $name => $path)
			<tr>
				<td>{{ ucwords($name) }}</td>
				@foreach($languages as $language)
				<td>
					<a href="{{ \URL::to('rt-admin/international/ui/file/site/'.$name.'/'.$language->id, false) }}">
					@if($language->is_default)
					Edit
					@else
					Translate
					@endif
					</a>

				</td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
</div>


@stop