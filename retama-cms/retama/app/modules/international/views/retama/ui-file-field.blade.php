<div class="form-group">
	{{ Form::label($name, $label )}}
	<div class="original">

		<div class="original-content">
		{{ $original }}
		</div>
	</div>

	<div class="form-control-holder">
	{{ $input }}
	@if ($errors->has($name))
		<label id="id-{{$name}}-error" class="error" for="id-{{$name}}">{{ $errors->first($name) }}</label>
	@endif

	</div>
</div>