@extends('retama::layouts.content')

@section('content-content')

<div class="records-list-holder rounded-box">
	<table cellpadding="0" cellspacing="0" class="records-list">

		<thead>
			<tr>
				<th> Module</th>
				@foreach($languages as $language)
				<th>{{ $language->name }}</th>
				@endforeach
			</tr>

		</thead>


		<tbody>

			@foreach($result as $key => $module)
			<tr>
				<td>{{ $key }}</td>
				@foreach($languages as $language)
				<td>
					@if($language->is_default)
					{{ $module['lang_'.$language->id] }}
					@else
					<a href="{{ \URL::to('rt-admin/international/content/module/'.$key.'/'.$language->id, false) }}">
					{{ $module['lang_'.$language->id] }}
					</a>
					@endif
				</td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
</div>


@stop