<?php
namespace Modules\International\Models;

class Geo extends \TreeModel {

	protected  $table = 'geo';

	protected $hidden = array();
	protected $guarded = array('id');

	public function children_path()
	{
		return parent::children_path();
	}

	public static function country_list()
	{
		return static::where('level', '=', 1)->orderBy('nicename')->lists('nicename','id');
	}
}
