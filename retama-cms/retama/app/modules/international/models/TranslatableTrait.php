<?php
namespace Modules\International\Models;

trait TranslatableTrait {

	protected static $language = null;

	public function getTranslationModelName()
	{
		return get_class($this).'Translation';
	}
	public function scopeTranslate($query, $language = null)
	{
		//var_dump('scopeTranslate');
		if($language === false)
		{
			$this->language = null;
		} else {
			static::$language = $language ?: \Language::current();
			$query->with('translations');
		}
		return $query;
	}
	protected function must_translate()
	{
		return (bool)( static::$language && (!static::$language->is_default) );
	}

	public function translations()
    {
	    $class = $this->getTranslationModelName();
	    $relation =  $this->hasMany($class, 'translatable_id');

	    if($this->must_translate())
	    {

			$trans_model = new $class;
			$trans_table = $trans_model->getTable();
			$trans_field = $trans_table.'.language_id';
		    $relation->where($trans_field,'=', static::$language->id);
	    }
		return $relation;
    }

    /*
public function translation()
    {

	    $class = $this->getTranslationModelName();
	    $relation =  $this->hasOne($class, 'translatable_id');

		if($this->must_translate() )
		{
			$trans_model_name = $this->getTranslationModelName();
			$trans_model = new $trans_model_name;
			$trans_table = $trans_model->getTable();
			$trans_field = $trans_table.'.language_id';
			$relation = $relation->where($trans_field,'=',$trans_language->id);
		}

		return $relation;
    }
*/

	public function __get($key)
	{
		// hay que traducir ?
		if( !$this->must_translate() ) return parent::getAttribute($key);

		// hay traducciones ?
		if( !isset($this->translations) ) return parent::getAttribute($key);

		// el campo pedido es traducible ?
		$class = $this->getTranslationModelName();
	    $trans_fields = $class::$translatable_fields;
	    if( !in_array($key, $trans_fields) ) return parent::getAttribute($key);

		$translations = parent::getAttribute('translations');

		// existe el registro para el idioma solicitado ?
		if( !count($translations) ) return parent::getAttribute($key);

		// devolvemos la traduccion
		return $translations[0]->$key;
	}
	/*
public function getAttribute($key)
    {
		//var_dump($this->language);
	    if( !$this->must_translate() ) return parent::getAttribute($key);


		if(!isset($this->translations) ) return parent::getAttribute($key);


	    $class = $this->getTranslationModelName();
	    $trans_fields = $class::$translatable_fields;
	    if( !in_array($key, $trans_fields) ) return parent::getAttribute($key);

        return $this->translations[0]->$key;
    }
*/
}