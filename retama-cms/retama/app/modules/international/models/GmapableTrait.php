<?php
namespace Modules\International\Models;

trait GmapableTrait {

    public function gmap()
    {
        return $this->MorphOne('\Modules\International\Models\Gmap', 'gmapable');
    }

    public function save_gmap($data)
    {
	    $gmap = $this->gmap ?: new \Modules\International\Models\Gmap;
	    if( (!isset($data['zoom'])) OR (!$data['zoom']) ) $data['zoom'] = 14;

		$gmap->fill($data);
		$this->gmap()->save($gmap);
    }


    // public function delete()
    // {
	//     $this->gmap()->delete();
    //
	//     return parent::delete();
    // }

}
