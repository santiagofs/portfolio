<?php
namespace Modules\International\Models;

class Gmap extends \BaseModel {

	protected  $table = 'gmap';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

	public function gmapable()
    {
        return $this->morphTo('gmapable');
    }
}
