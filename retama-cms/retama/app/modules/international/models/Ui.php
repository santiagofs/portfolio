<?php
namespace Modules\International\Models;

class UI  {

	public static function get_lang_array($context, $filename, $locale)
	{
		$path = static::get_file_path($context, $filename, $locale);
		if( ! file_exists($path) ) return [];

		return include $path;
	}

	public static function save_lang_file($arr, $context, $filename, $locale)
	{
		$folder_path = static::get_folder_path($context, $locale);
		$path = static::get_file_path($context, $filename, $locale);

		if( !$path ) return;

		if( !file_exists($folder_path) ) \File::makeDirectory($folder_path, 0775, true);

		if(file_exists($path)) unlink($path);
		\File::put($path, '<?php return ' . var_export($arr, true) . ';');
		chmod($path, 0777);
	}

	public static function get_file_path($context, $filename, $locale)
	{
		if($context = 'site') return \Site\ServiceProvider::get_path().'/lang/'.$locale.'/'.$filename.'.php';

		return '';
	}

	public static function get_folder_path($context, $locale)
	{
		if($context = 'site') return \Site\ServiceProvider::get_path().'/lang/'.$locale;
		return '';
	}
}
