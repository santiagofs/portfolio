<?php
namespace Modules\Retama;

use \View as View;
use \Modules\Retama\Retama as Retama;

class RetamaController extends \BaseController {

	protected $layout = 'retama::layouts.retama';


	public function __construct()
	{
		$this->beforeFilter('auth.retama', array('except' => array('getLogin', 'postLogin', 'getForgotPassword')));

		$this->beforeFilter('csrf', array('on' => 'post'));

		//$this->afterFilter('log', array('only' => array('fooAction', 'barAction')));
		//\Breadcrumbs::reset();

	}


	// dashboard
	public function getIndex()
	{

		return $this->getList();
	}

	public function postList()
	{
		$submit = \Input::get('filter-submit');

		if($submit == 'export') return $this->getExport();

		return $this->getList();
	}

	public function getList()
	{
		\Breadcrumbs::add('Dashboard');
		$this->render('retama::dashboard', array(
			'section_title' => 'Dashboard',
		));
	}

	public function getForgotPassword()
	{

		var_dump('saranga'); die();
		return 'YEA!';
	}

	protected function render($view, $data=array())
	{
		$arr = \Session::get('body') OR $arr = array();
		$collect = array();

		foreach($arr as $key => $value)
		{
			if($value) $collect[] = $key;
		}

		\View::share('body_class', implode(' ', $collect));

		$data['breadcrumbs'] = array();


		$content = View::make($view, $data);
		$this->layout->content = $content;
	}


	public function getDelete($ids=null, $redirect = true) {

		$model = new $this->model_name();
		$model::destroy($ids);

		if($redirect) return \Redirect::back()->with('message','Record Deleted');

	}

	public function postDelete()
	{
		$ids = \Input::get('id');
		$this->getDelete($ids, false);
		return \Response::json(array('status' => 'ok'));
	}

	public function getEdit($id=0, $parent_id = null)
	{
		if($id instanceof $this->model_name)
		{
			$model = $id;
		} else {
			$model = new $this->model_name;
			$model = $model->by_id($id);
		}
		$this->onGetEdit($model);

	}
	protected function onGetEdit($model, $parent_id=null){}

	public function postEdit($id=0, $parent_id = null)
	{
		$model = new $this->model_name;
		$model = $model->by_id($id);

		$this->onPostEdit($model);

		$redirect = \Input::get('redirect', \Retama::get_list_url());

		$validator = $model->input_validate();
		if($validator->passes())
		{
			$model->input_hydrate();
			$model->save();

			$current = \Request::url();
			$pos = strpos($current, 'edit');
			if( $pos === false ) return \Redirect::action($action);
			$goto = substr($current, 0, $pos);
			return \Redirect::to($goto);

			///var_dump($redirect); die();
			//return \Redirect::back()->with('message','Record saved');

		}
		else
		{
			//var_dump($validator); die();
			return \Redirect::to(\Retama::edit_url($id,$parent_id))->withInput()->withErrors($validator);;
		}



		//return $this->getEdit($model, $parent_id);
	}

	protected function onPostEdit($model){}

	public function postOrder() {
		$model = new $this->model_name;
		$ids = \Input::get('ids');


		$ndx = 0;
		foreach ($ids as $id) {
			$ndx++;
			$model = $model->by_id($id);
			$model->sort = $ndx;
			$model->save();
		}
		die();
	}

	protected function validate($model)
	{
		$valid = $model->input_validate();
		if($valid) return $model->save();

		return false;
	}


	/*
public function getInitDb()
	{
		if(!\Auth::check()) die();
		if(!\Auth::user()->is_super) die();

		\DB::statement('truncate table sitemap');
		\DB::insert('insert into sitemap (parent_id, tree_left, tree_right, is_homepage, is_root, layout) values (?, ?, ?, ?, ?, ?)', array(0,1,2,0,'Modules\Content\Models\Page',''));

		\DB::statement('truncate table navigation');
		\DB::insert('insert into navigation (parent_id, tree_left, tree_right) values (?, ?, ?)', array(0,1,2));

		\DB::statement('truncate table users');
		\DB::statement('truncate table user_profiles');
		$user = new \User;
		$user->email = 'santiagofs@gmail.com';
		$user->password = 'a';
		$user->first_name = 'Santiago';
		$user->last_name = 'Far Suau';
		$user->is_super = 1;
		$user->group = 100;
		$user->save();

	}
*/


	public function getConfiguration()
	{

		$base = \Config::get('retama::modules');
		$dotted = array_dot($base);

		$site = \Config::get('site::modules');
		if(!$site) $site = [];

		foreach($dotted as $key => $value)
		{
			$value = array_get($site, $key);
			if($value !== null) array_set($base, $key, $value);
		}
		$site = $base;

		\Breadcrumbs::add('Retama');
		\Breadcrumbs::add('Configuration');
		$this->render('retama::edit.configuration', array(
			'section_title' => 'Configuration',
			'site' => $site
		));

	}

	public function postConfiguration()
	{
		$input = \Input::get();

		//dd($input);
		$base = \Config::get('retama::modules');
		$site = $base;

		//$site = $this->setConfigurationModule($site, $base, $input);
		$site = $this->setConfigurationModule($base, $input);

		$path = \Site\ServiceProvider::get_path('config/modules.php');
		//$path = app_path().'/../../site/config/modules.php';
		if(file_exists($path)) unlink($path);
		\File::put($path, '<?php return ' . var_export($site, true) . ';');
		chmod($path, 0777);
		//dd($input);
		//dd($site);

		return \Redirect::to('rt-admin/configuration');
	}

	protected function setConfigurationModule($base, $input)
	{
		$enabled = $input['enabled'];
		$label = $input['label'];
		$url = $input['url'];
		$field_label = $input['field_label'];
		$field_enabled = $input['field_enabled'];

		foreach($enabled as $key => $value)
		{
			array_set($base, $key.'.enabled', $value);
		}
		foreach($label as $key => $value)
		{
			array_set($base, $key.'.label', $value);
		}
		foreach($url as $key => $value)
		{
			array_set($base, $key.'.url', $value);
		}

		foreach($field_label as $key => $value)
		{
			array_set($base, $key, $value);
		}

		foreach($field_enabled as $key => $value)
		{
			array_set($base, $key, $value);
		}

		return $base;
	}

	public function getSettings()
	{
		$base = \Config::get('retama::settings');

		$site = \Config::get('site::settings');
		if(!$site) $site = [];
		$dotted = array_dot($site);

		foreach($dotted as $key => $value)
		{
			if(strpos($key, 'api')===0)
			{
				$arr = explode('.', $key);
				$field = array_pop($arr);
				$arr[] = 'fields';
				$arr[] = $field;
				$arr[] = 'value';
				$base_key = implode('.', $arr);
			} else {
				$base_key = $key.'.value';
			}

			$value = array_get($site, $key);

			if($value !== null) array_set($base, $base_key, $value);
		}
		$site = $base;

		\Breadcrumbs::add('Retama');
		\Breadcrumbs::add('Settings');
		$this->render('retama::edit.settings', array(
			'section_title' => 'Settings',
			'settings' => $site
		));
	}

	public function postSettings()
	{
		$input = \Input::get();
		$site = \Config::get('site::settings');
		if(!$site) $site = [];

		unset($input['_token']);
		foreach($input as $key => $value)
		{
			$dotted = str_replace('_', '.', $key);
			array_set($site, $dotted, $value);

		}

		$path = \Site\ServiceProvider::get_path('config/settings.php');

		if(file_exists($path)) unlink($path);
		\File::put($path, '<?php return ' . var_export($site, true) . ';');
		chmod($path, 0777);
		die();
		return \Redirect::to('rt-admin/settings');
	}

	public function getHelp()
	{
		$path = app_path() .'/modules/retama/config/help.html';

		if(file_exists($path)) {
			$help = file_get_contents($path);
		} else {
			$help = '';
		}

		\Breadcrumbs::add('Retama');
		\Breadcrumbs::add('Help');
		$this->render('retama::help', array(
			'section_title' => 'Help',
			'help' => $help
		));
	}

	public function getEditHelp()
	{

		$path = app_path() .'/modules/retama/config/help.html';


		if(file_exists($path)) {
			$help = file_get_contents($path);
		} else {
			$help = '';
		}

		\Breadcrumbs::add('Retama');
		\Breadcrumbs::add('Help');
		$this->render('retama::edit.help', array(
			'section_title' => 'Help',
			'help_text' => $help
		));
	}

	public function postEditHelp()
	{
		$help = \Input::get('help_text');


		$path = app_path() .'/modules/retama/config/help.html';
		//var_dump($path); die();
		if(file_exists($path)) unlink($path);

		\File::put($path, $help);

		chmod($path, 0777);


		return \Redirect::to('rt-admin/help');
	}
}
