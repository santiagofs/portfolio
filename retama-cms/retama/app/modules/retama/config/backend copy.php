<?php

/*
	module group =>
		label
		enabled ?
		url
		modules [] si modules esta vacio es link directo
			label
			enabled
			url
			fields ['dbname']
				enabled
				label



*/
return array(
	'modules' => [
		'content' => [
			'label'	=> 'Content',
			'icon'	=> 'fa fa-copy',
			'enabled' => true,
			'url'	=> 'content',
			'modules' => [
				'structure' => [
					'label' 	=> 'Site Structure',
					'icon'	=> 'fa fa-sitemap',
					'enabled' 	=> true,
					'url' 		=> 'content/structure',
				],
				'navigation' => [
					'label' 	=> 'Navigation / Menus',
					'icon'	=> 'fa fa-bars',
					'enabled' 	=> true,
					'url' 		=> 'content/navigation',
				],
				'page' => [
					'label' 	=> 'Pages',
					'icon'	=> 'fa fa-file',
					'enabled' 	=> true,
					'url' 		=> 'content/pages',
					'fields'	=> [
						'title'		=> ['label'=>'Title', 'enabled' => true],
						'preview' 	=> ['label'=>'Preview', 'enabled' => false],
						'body'		=> ['label'=>'Body', 'enabled' => true],
						'extra'		=> ['label'=>'Extra', 'enabled' => false],
					],
				],
				'news' => [
					'label' 	=> 'News',
					'icon'	=> 'fa fa-bullhorn',
					'enabled' 	=> true,
					'url' 		=> 'content/news',
					'fields'	=> [],
				],
				'blog' => [
					'label' 	=> 'Blog',
					'icon'	=> 'fa fa-pencil',
					'enabled' 	=> true,
					'url' 		=> 'content/blog',
					'fields'	=> [],
				],
				'widgets' => [
					'label' 	=> 'Widgets',
					'icon'	=> 'fa fa-bell',
					'enabled' 	=> true,
					'url' 		=> 'content/widgets',
					'fields'	=> [],
				],
			]
		],
		'media' => [
			'label'	=> 'Media',
			'icon'	=> 'fa fa-picture-o',
			'enabled' => true,
			'url'	=> 'media',
			'modules' => [
				'galleries' => [
					'label' 	=> 'Galleries',
					'icon'	=> 'fa fa-file-image-o',
					'enabled' 	=> true,
					'url' 		=> 'media/galleries',
				],
			]
		],
		'international' => [
			'label'	=> 'International',
			'icon'	=> 'fa fa-globe',
			'enabled' => true,
			'url'	=> 'international',
			'modules' => [
				'countries' => [
					'label' 	=> 'Countries',
					'icon'	=> 'fa fa-flag',
					'enabled' 	=> true,
					'url' 		=> 'international/countries',
				],
				'languages' => [
					'label' 	=> 'Languages',
					'icon'	=> 'fa fa-language',
					'enabled' 	=> true,
					'url' 		=> 'international/languages',
				],
				'content' => [
					'label' 	=> 'Content translation',
					'icon'	=> 'fa fa-retweet',
					'enabled' 	=> true,
					'url' 		=> 'international/content',
				],
				'ui' => [
					'label' 	=> 'UI translation',
					'icon'	=> 'fa fa-cube',
					'enabled' 	=> true,
					'url' 		=> 'international/ui',
				],
			]
		],
		'settings' => [
			'label'	=> 'Settings',
			'icon'	=> 'fa fa-cogs',
			'enabled' => true,
			'url'	=> 'settings',
			/*
	'modules' => [
				'social' => [
					'label' 	=> 'Social Networks',
					'icon'	=> 'fa fa-flag',
					'enabled' 	=> true,
					'url' 		=> 'settings/socialnetworks',
				],
				'ga' => [
					'label' 	=> 'Google Analytics',
					'icon'	=> 'fa fa-language',
					'enabled' 	=> true,
					'url' 		=> 'settings/ga',
				],
			]
	*/
		],
		'people' => [
			'label'	=> 'People',
			'icon'	=> 'fa fa-users',
			'enabled' => true,
			'url'	=> 'people',
			'modules' => [
				'administrators' => [
					'label' 	=> 'Administrators',
					'icon'	=> 'fa fa-users',
					'enabled' 	=> true,
					'url' 		=> 'people/administrators',
				],
				'users' => [
					'label' 	=> 'Users',
					'icon'	=> 'fa fa-user',
					'enabled' 	=> true,
					'url' 		=> 'people/users',
				],
				'suscribers' => [
					'label' 	=> 'Newsletter suscribers',
					'icon'	=> 'fa fa-envelope',
					'enabled' 	=> true,
					'url' 		=> 'people/newsletter/suscribers',
				],
			]
		],
	],
	'summernote' => [
		'normal' => [
			['style', ['style' => true, 'bold' => true, 'italic' => true, 'underline' => true, 'strikethrough' => true, 'clear' => true]],
		    ['fontsize', ['fontsize' => true]],
		    ['color', ['color' => true]],
		    ['para', ['ul' => true, 'ol' => true, 'paragraph' => true]],
		    ['insert', ['link' => true, 'picture' => true, 'video' => true, 'table' => true]],
			['misc', ['fullscreen' => true, 'codeview' => true, 'undo' => true, 'redo' => true]]
		],
		'small' => [

		]

	]

);
