<?php
	return [
		'pages' => [
			'table' => 'pages',
			'caption' => 'pages',
			'main' => 'title',
			'class' => '\Modules\Content\Models\Page',
			'view' =>	'international::retama.modules.page',
		],
		'news' => [
			'table' => 'news',
			'caption' => 'News',
			'main' => 'title',
			'class' => '\Modules\Content\Models\News',
			'view' =>	'international::retama.modules.news',
		],
		'navigation' => [
			'table' => 'navigation',
			'original_count' => function() {
				return  \DB::table('navigation')->where('parent_id','>',1)->count();
			},
			'caption' => 'Navigation',
			'main' => 'name',
			'class' => '\Modules\Content\Models\Navigation',
			'get_module_query' => function($q) {
				return $q->where('parent_id','>',1);
			},
			'view' =>	'international::retama.modules.navigation',
		]
	];
?>