<?php

return [
	'site' => [
		'title' => ['label' => 'Site Title', 'value' => 'My Title', 'class'=>'required', 'enabled' => true],
		'main_email' => ['label' => 'Notifications Email', 'value' => '', 'class'=>'email', 'enabled' => true],
	],
	'social' => [
		'facebook' => ['label' => 'Facebook Account', 'value' => 'fake', 'icon' => 'fa fa-facebook', 'enabled' => true],
		'facebook_fanbox' => ['label' => 'Facebook Fanbox', 'value' => '', 'icon' => 'fa fa-facebook', 'enabled' => true, 'large'=>true],
		'twitter' => ['label' => 'Twitter Account', 'value' => 'fake', 'icon' => 'fa fa-twitter', 'enabled' => true],
		'twitter_widget' => ['label' => 'Twitter Widget', 'value' => '', 'icon' => 'fa fa-facebook', 'enabled' => true, 'large'=>true],
		'google_plus' => ['label' => 'Google Plus Account', 'value' => 'fake', 'icon' => 'fa fa-google-plus', 'enabled' => true],
		'linkedin' => ['label' => 'Linkedin', 'value' => 'fake', 'icon' => 'fa fa-linkedin', 'enabled' => true],
		'instagram' => ['label' => 'Instagram', 'value' => 'fake', 'icon' => 'fa fa-instagram', 'enabled' => true],
		'youtube' => ['label' => 'Youtube', 'value' => 'fake', 'icon' => 'fa fa-instagram', 'enabled' => true],
	],
	'api' => [
		'ga' => [
			'enabled' => true,
			'label' => 'Google Analitycs',
			'fields' => ['code' => ['label' => 'GA Code', 'value' => '', 'icon' => 'fa fa-google']],
		],
		'recapcha' => [
			'enabled' => true,
			'label' => 'Recapcha',
			'fields' => [
				'public' => ['label' => 'Public', 'value' => '', 'icon' => ''],
				'secret' => ['label' => 'Secret', 'value' => '', 'icon' => ''],
			],
		]
	]

];