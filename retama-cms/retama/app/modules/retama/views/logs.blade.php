
	<h3>Recent activity</h3>
	<ul>
	@foreach($logs as $log)
	<?php
		$anonymous = ($log->user_id == 0);

		if(!$anonymous) {
			if(!$log->user):
				//var_dump($log);
				continue;
			endif;
		}

		if(!$log->logable):
			continue;
		endif;
	?>
	<li>
		@if(!$anonymous)
		{{ $log->user->fullname() }} {{ $log::action_to_verb($log->action)}} the {{ $log->niceclass }} "{{ $log->logable->{$log->captionfield} }}" on {{ $log->created_at->formatLocalized(\Lang::get('retama::dateformats.short-date')) }}
		@else
		{{$log->action}} {{$log->niceclass}} "{{ $log->logable->{$log->captionfield} }}" on {{ $log->created_at->formatLocalized(\Lang::get('retama::dateformats.short-date')) }}
		@endif

	</li>
	@endforeach
	</ul>
