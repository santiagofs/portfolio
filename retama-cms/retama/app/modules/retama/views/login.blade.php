@extends('retama::layouts.retama')

@section('body-id', 'login')

<?php
	$logo = false;
	$logo_path = public_path().'/html/retama/img/logo.';

	$test = $logo_path.'jpg';
	if(file_exists($test)) $logo = '/html/retama/img/logo.jpg';

	$test = $logo_path.'png';
	if(file_exists($test)) $logo = '/html/retama/img/logo.png';

	$test = $logo_path.'svg';
	if(file_exists($test)) $logo = '/html/retama/img/logo.svg';



	if($logo) {
		$logo = '<div style="background:url('.$logo.') no-repeat center center; background-size: auto 30px; width: 300px; height:30px; 	margin: auto"></div>';
	} else {
		$logo = \Config::get('site::settings.site.title');
	}
?>

@section('site-content')

	<div class="cell">
		<div class="form-holder">
			<h2 class="logo">{{ $logo }}</h2>
			<div class="background">
				<h2><strong>Welcome</strong>, please login</h2>
@include('components::login_form', array('url'=> 'rt-admin/login', 'forgot_password_url' => 'rt-admin/forgot-password'))
			</div>
		</div>
	</div>

@stop