<?php
	$modules = \Config::get('site::modules');
	$modules || $modules = [];

	if($site_modules = \Config::get('site::site_modules'))
	{

		$modules = array_merge_recursive($site_modules, $modules);
	}


/*
	$dotted = array_dot($base);

	$site = \Config::get('site::modules');
	if(!$site) $site = [];

	foreach($dotted as $key => $value)
	{
		$value = array_get($site, $key);
		if($value !== null) array_set($base, $key, $value);
	}
	$site = $base;
*/


?>
<aside id="site-sidebar">
	<header>
		<a href="#" class="fa fa-bars" toggle-class="sidebar-collapsed" toggle-target="body"></a>
		<h3>Menu</h3>
	</header>
	<div class="content">
		<ul>
			<li>
				<a href="{{ URL::to('rt-admin') }}">
					<i class="fa fa-home"></i>
					<span>Dashboard</span>
				</a>
			</li>

			@foreach($modules as $module)

				@if($module['enabled'] || (\Auth::user()->is_super) )
					@include('retama::partials.sidebar-module', ['module' => $module])
				@endif

			@endforeach

		</ul>

		@if(\Auth::user()->is_super)
		<hr />
		<ul>
			<li>
				<a href="{{ URL::to('rt-admin/configuration') }}">
					<i class="fa fa-cog"></i>
					<span>Configuration</span>
				</a>
			</li>
		</ul>
		@endif
	</div>
</aside>