<!-- Modal -->
<div class="modal fade" id="retama-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" modal-title>Modal title</h4>
			</div>
			<div class="modal-body" modal-body></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" modal-button-dismiss >Cancel</button>
				<button type="button" class="btn btn-primary" modal-button-ok >Ok</button>
			</div>
		</div>
	</div>
</div>