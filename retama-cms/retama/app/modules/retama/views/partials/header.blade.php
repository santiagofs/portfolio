<?php
	$logo = false;
	$logo_path = public_path().'/html/retama/img/logo.';

	$test = $logo_path.'jpg';
	if(file_exists($test)) $logo = '/html/retama/img/logo.jpg';

	$test = $logo_path.'png';
	if(file_exists($test)) $logo = '/html/retama/img/logo.png';

	$test = $logo_path.'svg';
	if(file_exists($test)) $logo = '/html/retama/img/logo.svg';



	if($logo) {
		$logo = '<div style="background:url('.$logo.') no-repeat; background-size: auto 30px; width: 300px; height:30px; margin-top: 15px"></div>';
	} else {
		$logo = \Config::get('site::settings.site.title');
	}
?>
<header id="site-header">
	<div class="logo-holder">
		<a class="logo" href="/rt-admin">{{ $logo }}</a>

		<a href="#" class="fa fa-bars toggle-sidebar"></a>
	</div>

	@if(Auth::check())

	<div class="header-logged">

<!--
		<form class="search" action="">
			<div class="input-group">
				<input type="text" placeholder="Search..." id="q" name="q" class="form-control">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>

		<div class="inline-sep"></div>
-->

		<ul class="notify">
			<!--
<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle round-icon" href="#">
					<i class="fa fa-tasks"></i>
					<span class="badge">3</span>
				</a>

				<div class="dropdown-menu">
					<div class="notification-title">
						<span class="pull-right label label-default">3</span>
						Tasks
					</div>

					<div class="content">
						<ul>
							<li>
								<p class="clearfix mb-xs">
									<span class="message pull-left">Generating Sales Report</span>
									<span class="message pull-right text-dark">60%</span>
								</p>
								<div class="progress progress-xs light">
									<div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar"></div>
								</div>
							</li>

							<li>
								<p class="clearfix mb-xs">
									<span class="message pull-left">Importing Contacts</span>
									<span class="message pull-right text-dark">98%</span>
								</p>
								<div class="progress progress-xs light">
									<div style="width: 98%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="98" role="progressbar" class="progress-bar"></div>
								</div>
							</li>

							<li>
								<p class="clearfix mb-xs">
									<span class="message pull-left">Uploading something big</span>
									<span class="message pull-right text-dark">33%</span>
								</p>
								<div class="progress progress-xs light mb-xs">
									<div style="width: 33%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="33" role="progressbar" class="progress-bar"></div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</li>
-->
			<li>
				<?php
					$messages = \Message::unread();
					$message_count = count($messages);
				?>
				<a data-toggle="dropdown" class="dropdown-toggle round-icon" href="#">
					<i class="fa fa-envelope"></i>
					<span class="badge">{{ $message_count }}</span>
				</a>

				<div class="dropdown-menu">
					<header>
						<span class="pull-right badge">{{ $message_count }}</span>
						Messages
					</header>

					<div class="content">



						<ul>
							@if(!$message_count)
							<li>No messages</li>
							@else

							<?php $i=0; ?>
							@foreach($messages as $message)
							<?php
								if($i > 2) break;
							?>
							<li>
								<a class="clearfix" href="{{ URL::to('rt-admin/messages/formtomail/edit/'.$message->id) }}">
									<span class="title"><strong>{{ $message->subject }}</strong></span>
									<span class="from">{{ $message->name }}</span>
									<span class="date">{{ $message->created_at->formatLocalized(\Lang::get('retama::dateformats.short-date')) }}</span>
								</a>
							</li>
							@endforeach
							@endif

						</ul>

						<hr>

						<div class="text-right">
							<a class="view-more" href="{{ URL::to('rt-admin/messages/formtomail', false) }}">View All</a>
						</div>

					</div>
				</div>
			</li>
			<li>
				<a data-toggle="dropdown" class="dropdown-toggle round-icon" href="#">
					<i class="fa fa-bell"></i>
					<span class="badge">0</span>
				</a>

				<div class="dropdown-menu notification-menu">
					<header>
						<span class="pull-right badge">0</span>
						Alerts
					</header>

					<div class="content">

						<ul>
							<li>No alerts</li>
							<!--<li>
								<a class="clearfix" href="#">
									<div class="image">
										<i class="fa fa-thumbs-down bg-danger"></i>
									</div>
									<span class="title">Server is Down!</span>
									<span class="message">Just now</span>
								</a>
							</li>
							<li>
								<a class="clearfix" href="#">
									<div class="image">
										<i class="fa fa-lock bg-warning"></i>
									</div>
									<span class="title">User Locked</span>
									<span class="message">15 minutes ago</span>
								</a>
							</li>
							<li>
								<a class="clearfix" href="#">
									<div class="image">
										<i class="fa fa-signal bg-success"></i>
									</div>
									<span class="title">Connection Restaured</span>
									<span class="message">10/10/2014</span>
								</a>
							</li>-->
						</ul>

						<hr>

						<div class="text-right">
							<a class="view-more" href="#">View All</a>
						</div>

					</div>
				</div>
			</li>


			<li>
				<a class="round-icon" href="{{ URL::to('rt-admin/help', false) }}">
					<i class="fa fa-question"></i>
				</a>
			</li>

		</ul>

		<div class="inline-sep"></div>

		<div class="user-info" id="header-user-info">
			<a data-toggle="dropdown" href="#">
				<figure class="picture">
					<img lock-picture="{{ \Auth::user()->gravatar(35) }}" alt="{{{ \Auth::user()->fullname() }}}" src="{{ \Auth::user()->gravatar(35) }}">
				</figure>
				<div lock-email="{{{ \Auth::user()->email }}}" lock-name="{{{ \Auth::user()->fullname() }}}" class="name-role">
					<div class="name">{{{ \Auth::user()->fullname() }}}</div>
					<div class="role">Administrator</div>
				</div>

				<i class="fa fa-angle-down"></i>
			</a>

			<div class="dropdown-menu">
				<hr />
				<ul class="list-unstyled">

					<li>
						<a href="{{ URL::to('rt-admin/people/administrators/profile') }}" tabindex="-1" role="menuitem"><i class="fa fa-user"></i> My Profile</a>
					</li>
					<!--
<li>
						<a data-lock-screen="true" href="#" tabindex="-1" role="menuitem"><i class="fa fa-lock"></i> Lock Screen</a>
					</li>
-->
					<li>
						<a href="{{ URL::to('rt-admin/logout') }}" tabindex="-1" role="menuitem"><i class="fa fa-power-off"></i> Logout</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	@endif

</header>
