<?php
	$has_children = isset($module['modules']);
	$url = 'rt-admin/'. $module['url'];
	$label = $module['label'];
	$icon = $module['icon'];
	$is_request = \Request::is($url.'*');
	//$icon = '';
	if($has_children){

		$class = 'with-children' . ($is_request ? ' expanded' : '');
		$href = '#';
	} else {
		$class = $is_request ? 'active' : '';
		$href = URL::to($url, false);
	}

	$is_super = \Auth::user()->is_super;
?>

			<li class="{{ $class }}">
				<a href="{{ $href }}">
					<i class="{{ $icon }}"></i>
					<span>{{ $label }}</span>
				</a>
				@if($has_children)
				<ul>
					@foreach($module['modules'] as $module)

					@if($module['enabled'] || $is_super )
					@include('retama::partials.sidebar-module', ['module' => $module])
					@endif

					@endforeach
				</ul>
				@endif
