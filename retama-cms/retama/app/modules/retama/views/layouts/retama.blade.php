@extends('layouts.html5')

@section('head-scripts')
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'> -->
	<link rel="stylesheet" href="{{ URL::to('/html/retama/retama.css', false)}}" />
	<link rel="stylesheet" href="{{ URL::to('/html/retama/dragula.min.css', false)}}" />
@stop

@section('seo')
	<title>{{ \Config::get('site::settings.site.title') }}</title>
	<meta name="description" content="">
    <meta name="keywords" content="">

		<!-- favicons -->
	<link rel="shortcut icon" href="{{ URL::to('html/site/img/favicon.ico') }}">
	<link rel="apple-touch-icon" href="{{ URL::to('html/site/img/favicon.png') }}">
	<!--
<link rel="apple-touch-icon" sizes="72x72"  href="{{ URL::to('html/site/img/favicon-72.png') }}">
	<link rel="apple-touch-icon" sizes="114x114"  href="{{ URL::to('html/site/img/favicon-114.png') }}">
	<link rel="apple-touch-icon" sizes="144x144"  href="{{ URL::to('html/site/img/favicon-144.png') }}">
-->

@stop

@section('site-layout')

<div id="site-wrapper">

@include('retama::partials.header')



	<div id="site-canvas">

@if(Auth::check() && \Auth::user()->group >= 100)
	@include('retama::partials.sidebar')
@endif

		<div id="site-content">
@yield('site-content')
		</div>

	</div>

</div>

@include('retama::partials.modal')

@stop


@section('footer-scripts')
	<script>
		var messages = {
			confirm: 'Confirm',
			confirm_action: 'Confirm action',
			confirm_deletion: 'This action can\'t be undone. <br/>Are you sure you wish to continue?',
			create_structure_title: 'Choose the item you want to create',
			create: 'Create'
		}
	</script>
	<script type="text/javascript" src="{{ URL::to('/html/retama/lib'.$suffix.'.js',false) }}"></script>
	<script type="text/javascript" src="{{ URL::to('/html/retama/dragula.min.js',false) }}"></script>
	<script type="text/javascript" src="{{ URL::to('/html/retama/main.js',false) }}"></script>
	<script type="text/javascript" src="/html/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
		    selector: "[tinymce]",
		    content_css : "/html/retama/tiny.css",
		    menubar : false,
		    plugins: 'code fullscreen link image paste',
		    toolbar: "undo redo | cut copy paste | formatselect | alignleft aligncenter alignright alignjustify | bold italic removeformat | link image | bullist numlist | code",
		    paste_data_images: true

		 });

		
	</script>

	<!-- newdocument bold italic underline strikethrough  styleselect formatselect fontselect fontsizeselect cut copy paste bullist numlist outdent indent blockquote undo redo removeformat subscript superscript -->

@stop
