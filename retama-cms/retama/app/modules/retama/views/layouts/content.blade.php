@section('site-content')

	<header class="content-header">
		<h2>{{$section_title or 'Section Title'}}</h2>

		<div class="pull-right">
			<ul class="breadcrumbs">
				<li><a href="{{URL::to('retama')}}"><i  class="fa fa-home"></i></a></li>
				{{ \Breadcrumbs::render() }}
			</ul>
		</div>

	</header>
<?php
	$message = \Session::get('message');
?>
@if($message)
<div class="alert alert-success alert-dismissible" role="alert">
	 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
{{ $message }}</div>
@endif

	<section class="content-content">
@yield('content-content')
	</section>

@stop