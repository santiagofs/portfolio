@extends('retama::layouts.content')



@section('content-content')

<div class="form-edit-holder">

	<header class="form-header">
		<h2>
			Help
		</h2>
	</header>
	<div class="form-fields">
	{{ $help }}
	</div>
</div>

@stop