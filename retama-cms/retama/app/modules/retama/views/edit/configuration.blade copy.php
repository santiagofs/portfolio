@extends('retama::layouts.content')

@section('content-content')
{{ \Form::open() }}
<div class="configuration-control form-edit-holder">
	<header class="form-header">
		<ul class="nav nav-pills">
			<li role="presentation" class="active"><a href="#tab-modules" data-toggle="tab">Modules</a></li>
			<li role="presentation"><a href="#tab-summernote" data-toggle="tab">Summernote</a></li>
		</ul>
		<h2>Site configuration</h2>
	</header>
	<div class="form-fields">


		<div class="tab-content">
			<div id="tab-modules" class="tab-pane active">
				<div class="panel-group" id="modules-accordion" role="tablist" aria-multiselectable="true">
<?php
	$modules = $site['modules'];
?>
@foreach($modules as $key => $item)
	@include('retama::edit.configuration-module', ['accordion'=>'modules-accordion'])
@endforeach
				</div>
			</div>

<?php
	$summernote = $site['summernote'];
?>
			<div id="tab-summernote" class="tab-pane">
			@foreach($summernote as $key => $groups)
				<fieldset>
					<legend>{{ $key }}</legend>
					<div>
					@foreach($groups as $group )
					<?php
						$title = $group[0];
						$buttons = $group[1];
					?>

					@endforeach
					</div>
				</fieldset>
			@endforeach
			</div>

		</div>




	</div>
	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
	</footer>
</div>
{{ \Form::close() }}

@stop
