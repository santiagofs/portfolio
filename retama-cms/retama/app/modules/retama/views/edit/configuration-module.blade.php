<?php
	$scored = str_replace('.', '-', $key);
?>
<div class="module">
	<h3><a href="#" class="collapsed"> {{ $key }} <i class="fa fa-angle-up"></i></a></h3>
	<div class="module-control">
	<div class="form-group">
		{{ Form::label('label['.$key.']', 'Module label' )}}
		<div class="form-control-holder">
			<?php
				// dd($item);
			?>
			{{ \Form::text('label['.$key.']', $item['label'], ['class' => 'form-control']) }}
		</div>
	</div>


	<?php
		$checked = $item['enabled'] ? 'checked' : '';
	?>
	<div class="form-group">
		<div class="form-control-holder">
		{{ \Form::hidden('enabled['.$key.']',$item['enabled'], ['id'=>'id-'.$scored])  }}
		<label > <a toggle-checkbox="#id-{{ $scored }}" class="checkbox {{$checked}}">Enabled</a></label>
		</div>
	</div>


	<div class="form-group">
		{{ Form::label('url['.$key.']', 'Module URL' )}}
		<div class="form-control-holder">
			<?php
				// dd($item);
			?>
			{{ \Form::text('url['.$key.']', $item['url'], ['class' => 'form-control']) }}
		</div>
	</div>

	@if(isset($item['fields']))

		<div class="fields" style="padding-left: 100px;">
			<h3>Fields</h3>
		@foreach($item['fields'] as $field_name => $field_values )

			<div class="form-group">
				<?php
					$input_name = $key . '.fields.'.$field_name.'.';
				?>
				{{ Form::label('label['.$input_name.'label]', 'Field label for ' . $field_name )}}
				<div class="form-control-holder">
					{{ \Form::text('field_label['.$input_name.'label]', $field_values['label'], ['class' => 'form-control']) }}
				</div>
				<?php
					$checked = $field_values['enabled'] ? 'checked' : '';
				?>
				<div class="form-group">
					<div class="form-control-holder">
					{{ \Form::hidden('field_enabled['.$input_name.'enabled]',$field_values['enabled'], ['id'=>'id-'.$scored.'-'.$field_name])  }}
					<label> <a toggle-checkbox="#id-{{ $scored }}-{{ $field_name }}" class="checkbox {{$checked}}">Enabled</a></label>
					</div>
				</div>
			</div>
		@endforeach
		</div>
	@endif

	@if(isset($item['modules']))
	<div class="submodules" style="padding-left: 100px;">
	@foreach($item['modules'] as $module_name => $module)
		@include('retama::edit.configuration-module', ['key'=>$key.'.modules.'.$module_name, 'item'=> $module])
	@endforeach
	</div>
	@endif
	</div>
</div>