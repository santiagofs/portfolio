@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ \Form::open() }}

	<header class="form-header">
		<ul class="nav nav-pills">
			<li role="presentation" class="active"><a href="#tab-site" data-toggle="tab">Site</a></li>
			<li role="presentation"><a href="#tab-social" data-toggle="tab">Social Networks Accounts</a></li>
			<li role="presentation"><a href="#tab-api" data-toggle="tab">Plugins API Keys</a></li>
		</ul>
		<h2>
			Edit Settings:
		</h2>
	</header>
	<div class="form-fields">


		<div class="tab-content">
			<div id="tab-site" class="tab-pane active">
				<fieldset>
					<legend>Site</legend>
				<?php
					$site = array_get($settings, 'site');
				?>
				@foreach($site as $key => $setting)
				<?php
					$class = (isset($setting['class']) ? $setting['class'] : '') . ' form-control';
					$input_name = 'site['.$key.']';
				?>
					{{ Form::one('text', $input_name, $setting['label'],  $setting['value'], ['class'=>$class]) }}
				@endforeach
				</fieldset>
			</div>


			<div id="tab-social" class="tab-pane">
				<fieldset>
					<legend>Social</legend>
				<?php
					$social = array_get($settings, 'social');
				?>
				@foreach($social as $key => $setting)
				<?php
					$class = (isset($setting['class']) ? $setting['class'] : '') . ' form-control';
					$input_name = 'social['.$key.']';
				?>
					@if(isset($setting['large']))
					{{ Form::one('textarea', $input_name, $setting['label'],  $setting['value'], ['class'=>$class]) }}
					@else
					{{ Form::one('text', $input_name, $setting['label'],  $setting['value'], ['class'=>$class]) }}
					@endif

				@endforeach
				</fieldset>
			</div>


			<div id="tab-api" class="tab-pane">
				<?php
					$api = array_get($settings, 'api');
				?>
				@foreach($api as $group_key => $group)
				<fieldset>
					<legend>{{ $group['label'] }}</legend>

					@foreach($group['fields'] as $key => $setting)
					<?php
						$class = (isset($setting['class']) ? $setting['class'] : '') . ' form-control';
						$input_name = 'api['.$group_key.']['.$key.']';
					?>
					{{ Form::one('text', $input_name, $setting['label'],  $setting['value'], ['class'=>$class]) }}
					@endforeach
				</fieldset>
				@endforeach
			</div>
		</div>
	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


