@extends('retama::layouts.content')

@section('content-content')

<div class="configuration-control form-edit-holder">
	<header class="form-header">
		<h2>Edit Help</h2>
	</header>
	{{ \Form::open() }}

		<div class="form-fields">
			{{ Form::one('tinymce', 'help_text', 'Help Text', $help_text, array('class'=>'form-control')) }}
		</div>

		<footer class="form-footer">
			<button type="submit" class="btn btn-primary">Save</button>
		</footer>

	{{ \Form::close() }}

</div>

@stop
