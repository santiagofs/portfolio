@extends('retama::layouts.content')

@section('content-content')

<div class="form-edit-holder">
@yield('form')
</div>

@stop

@section('footer-scripts')
@parent

<script type="text/javascript">
	$.validator.messages.required = '{{  \Lang::get('retama::errors.required') }}';
	$.validator.messages.email = '{{  \Lang::get('retama::errors.email') }}';
	$.validator.messages.dateISO = '{{  \Lang::get('retama::errors.dateISO') }}';
	$.validator.messages.number = '{{  \Lang::get('retama::errors.number') }}';
	$.validator.messages.perma = '{{  \Lang::get('retama::errors.perma') }}';
	$.validator.setDefaults({ ignore: ":hidden:not([summernote])" })
	//$.validator.setDefaults({ ignore: ':hidden.ignore' })

	$(function(){

		$('#form-edit').validate({
	        // other rules & options,
	        highlight: function (element, errorClass, validClass) {
	            $(element).parents('.form-group').addClass('error');
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $(element).parents('.form-group').removeClass('error');
	        },
	        invalidHandler: function(form, validator) {
	        	var $error = $(validator.errorList[0].element);
				var $tab = $error.closest('.tab-pane');
				if($tab.length) {
					var id = '#'+$tab.attr('id');
					$('a[href="'+id+'"]').trigger('click');
				}
			}
	    });

		$('[summernote]').summernote({

			<?php
				$toolbar = \Config::get('site::summernote.normal');
				if($toolbar) {
					$toolbar = json_encode($toolbar);
				}
			?>
			@if($toolbar)
				toolbar:
				{{ $toolbar }},
			@else
				toolbar: [
				['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
			   /*  ['font', ['strikethrough']], */
			    ['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['insert', ['link', 'picture', 'video', 'table']],
				['misc', ['fullscreen', 'codeview', 'undo', 'redo']]
				],
			@endif
			height: 240,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			styleWithSpan: false,
			onpaste: function (e) {
		        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

		        e.preventDefault();

		        document.execCommand('insertText', false, bufferText);
		    }
		});

		$('[datetimepicker]').each(function(i,e){

			var $this = $(e);
			var format = $(e).attr('format');
			var mirror = $(e).attr('mirror');
			$this.datetimepicker({
				format: format,
				minView: $this.attr('datetimepicker'),
				autoclose: true,
				linkField: mirror,
				linkFormat: "yyyy-mm-dd hh:ii:ss"
			});

		})



	})

</script>
@stop