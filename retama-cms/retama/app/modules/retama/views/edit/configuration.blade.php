@extends('retama::layouts.content')

@section('content-content')

<div class="configuration-control form-edit-holder">
	<header class="form-header">
		<h2>Site configuration</h2>
	</header>
	<div class="form-fields">
{{ \Form::open() }}
@foreach($site as $key => $item)
	@include('retama::edit.configuration-module')
@endforeach
	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
	</footer>

{{ \Form::close() }}

</div>

@stop
