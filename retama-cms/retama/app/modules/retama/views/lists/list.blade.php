@extends('retama::layouts.content')

@section('content-content')

@include('lists::filters')

<?php
    isset($table_view) || $table_view = 'lists::table';
?>
@include($table_view)

@stop
