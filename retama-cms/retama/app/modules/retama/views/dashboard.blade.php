@extends('retama::layouts.content')



@section('content-content')

<div class="form-edit-holder">

	<header class="form-header">
		<h2>
			Dashboard
		</h2>
	</header>
	<div class="form-fields">
	<?php
		$logs = \Modules\Retama\Models\Log::with('user')->with('logable')->orderBy('created_at', 'desc')->take(20)->get();
	?>
	@include('retama::logs', ['logs'=> $logs])
	</div>
</div>

@stop