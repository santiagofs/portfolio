<?php

View::share('suffix', App::isLocal() ? '' : '.min');

Route::group(array('prefix' => 'rt-admin'), function()
{

	if( $retama_routes = \Site\ServiceProvider::get_file('routes_retama.php')) \File::requireOnce($retama_routes);


	Route::get('login', function() {

		if(\Auth::check()) \Redirect::to('retama');

		$message = \Session::get('message');
		$username = \Session::get('username');
		$rememberme = \Session::get('rememberme');

		return \View::make('retama::login')
			->withMessage($message)
			->withUsername($username)
			->withRememberme($rememberme);
	});

    Route::post('login', function(){

		$username = \Input::get('username');
		$password = \Input::get('password');
		$rememberme = \Input::get('rememberme');

		if(\Auth::attempt(array('email' => $username, 'password' => $password), $rememberme))
		{
			return \Redirect::intended('rt-admin');
		}
		else
		{
			return \Redirect::back()
				->with('message','Wrong username or password')
				->with('username', $username)
				->with('rememberme', $rememberme);
		}

    });

	Route::get('logout', function(){
		\Auth::logout();
		return \Redirect::to('rt-admin/login');
	});

	Route::get('init-db', function(){

		die();
		if(!\Auth::check()) die();
		if(!\Auth::user()->is_super) die();


		\DB::statement('truncate table sitemap');
		\DB::insert('insert into sitemap (parent_id, tree_left, tree_right, is_homepage, is_root, layout) values (?, ?, ?, ?, ?, ?)', array(0,1,2,0,'Modules\Content\Models\Page',''));

		\DB::statement('truncate pages');

		\DB::statement('truncate table navigation');
		\DB::insert('insert into navigation (parent_id, tree_left, tree_right) values (?, ?, ?)', array(0,1,2));

		\DB::statement('truncate table users');
		\DB::statement('truncate table user_profiles');
		$user = new \User;
		$user->email = 'santiagofs@gmail.com';
		$user->password = 'a';
		$user->first_name = 'Santiago';
		$user->last_name = 'Far Suau';
		$user->is_super = 1;
		$user->group = 100;
		$user->save();
		dd('listo');
	});


	Route::get('configuration', 'Modules\Retama\RetamaController@getConfiguration');
	Route::post('configuration', 'Modules\Retama\RetamaController@postConfiguration');

	Route::get('settings', 'Modules\Retama\RetamaController@getSettings');
	Route::post('settings', 'Modules\Retama\RetamaController@postSettings');

	Route::get('help', 'Modules\Retama\RetamaController@getHelp');
	Route::get('edit-help', 'Modules\Retama\RetamaController@getEditHelp');
	Route::post('edit-help', 'Modules\Retama\RetamaController@postEditHelp');

	Route::get('/', 'Modules\Retama\RetamaController@getIndex');

});
