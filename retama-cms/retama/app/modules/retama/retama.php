<?php
namespace Modules\Retama;

class Retama {

	public static function edit_url($id=0)
	{
		$controller = \Str::parseCallback(\Route::currentRouteAction(), null)[0];
		return \URL::action($controller.'@getEdit', [$id]);
	}

	public static function list_url()
	{
		$controller = \Str::parseCallback(\Route::currentRouteAction(), null)[0];
		return \URL::action($controller.'@getList');
	}

	public static function delete_url($id)
	{
		$controller = \Str::parseCallback(\Route::currentRouteAction(), null)[0];
		return \URL::action($controller.'@postDelete', [$id]);
	}

	public static function save_list_url()
	{
		$full_url = \Request::fullUrl();
		\Session::put('list_url', $full_url);
	}

	public static function get_list_url()
	{
		return \Session::get('list_url');
	}

	public static function controller_name ($includeNamespace = false)
	{
		if(!$action = \Route::currentRouteAction()) return null;

		$controller = head(\Str::parseCallback($action, null));

		// Separate out nested controller resources.
		$controller = str_replace('_', ' ', snake_case($controller));
		// Either separate out the namespaces or remove them.
		$controller = $includeNamespace ? str_replace('\\', '', $controller) : substr(strrchr($controller, '\\'), 1);
		// Remove 'controller' from the controller name.
		return trim($controller);

	}

	public static function file_prefix()
	{
		//dd(\App::environment());
		return \App::environment() == 'local' ? '' : '.min';
	}


	public static function last_query()
	{
		$queries = \DB::getQueryLog();
		$last_query = end($queries);
		dd($last_query);
	}

	public static function queries()
	{
		$queries = \DB::getQueryLog();
		dd($queries);
	}

}