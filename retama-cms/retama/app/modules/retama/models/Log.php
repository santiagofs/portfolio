<?php
namespace Modules\Retama\Models;

class Log extends \BaseModel {

	protected  $table = 'logs';

	public function logable()
    {
        return $this->morphTo();
    }

    public function user()
    {
	    return $this->belongsTo('User');
    }

    public static function action_to_verb($action)
    {
	    switch($action)
	    {
		    case 'create':
		    	return 'created';
		    	break;
		    case 'edit':
		    	return 'edited';
		    	break;
		    case 'delete':
		    	return 'deleted';
		    	break;
			default:
				return $action;
	    }
    }
}
