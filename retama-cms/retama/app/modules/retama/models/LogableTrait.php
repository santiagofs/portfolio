<?php
namespace Modules\Retama\Models;

trait LogableTrait {

	public function logs()
    {
        return $this->morphMany('\Modules\Retama\Models\Log', 'logable');
    }

    public function log($action, $captionfield, $niceclass, $anonymous = false)
    {
	    $log = new \Modules\Retama\Models\Log;
	    $log->action = $action;
	    $log->captionfield = $captionfield;
	    $log->niceclass = $niceclass;
	    $log->user_id = $anonymous ? 0 : \Auth::user()->id;

	    $this->logs()->save($log);
    }

}
