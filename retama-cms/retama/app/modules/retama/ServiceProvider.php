<?php
namespace Modules\Retama;

class ServiceProvider extends \Modules\ServiceProvider {

    public function register()
    {
        parent::register('retama');
    }

    public function boot()
    {
        parent::boot('retama');
    }

}