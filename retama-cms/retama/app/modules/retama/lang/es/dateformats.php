<?php

return array(

	'short-date'	=> '%d/%m/%Y',
	'long-date'		=> '%A %d de %B de %Y',
	'middle-date'		=> '%d de %B de %Y',
	'day-month'		=> '%d de %B',
);
