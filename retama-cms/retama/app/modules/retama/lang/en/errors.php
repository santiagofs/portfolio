<?php

return array(
	'required' => 'This field is required',
	'email' => 'Please, provide a valid email address',
);