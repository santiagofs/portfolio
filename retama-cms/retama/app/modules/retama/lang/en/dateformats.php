<?php

return array(

	'short-date'	=> '%m-%d-%Y',
	'long-date'		=> '%A %d, %B %Y',
	'middle-date'	=> '%d %B %Y',
	'day-month'		=> '%B %d',
);
