<?php
namespace Modules\Users;

class ServiceProvider extends \Modules\ServiceProvider {

    public function register()
    {
        parent::register('users');
    }

    public function boot()
    {
        parent::boot('users');
    }

}