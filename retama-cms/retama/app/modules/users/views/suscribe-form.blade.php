<div class="suscribe-form" success-message="{{ $success or 'Great! Please, check your email to confirm sign up.' }}" id="{{ $name }}-suscribe">

	<form name="suscribe-form" id="suscribe-form" method="post" action="{{ \URL::to('newsletter-suscribe', false) }}"  >

		<div class="form-group">
			<input class="form-control required email{{ $class or '' }}" type="text" id="suscribe-email" name="suscribe-email" placeholder="{{ $placeholder or 'Enter your email' }}">

			<button type="submit" class="btn" id="{{ $name }}-suscribe-submit">{{ $submit or 'Send' }}</button>
		</div>
	</form>
	<div class="suscribe-feedbak" id="{{ $name }}-suscribe-feedback"></div>
</div>