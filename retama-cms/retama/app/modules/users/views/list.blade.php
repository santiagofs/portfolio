<?php
	$filters = true;
?>
@extends('retama::lists.list')

@section('filters')
<div class="filter-holder">
{{ Form::open(array('url' => \Retama::list_url(), 'method' => 'post', 'id'=>'form-filter', 'class'=>'form-inline')) }}



	<button type="submit" class="btn btn-primary">Filter</button>
	<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Reset</a>

{{ Form::close() }}
</div>


@stop