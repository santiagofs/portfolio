@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
		@if($model->id)
		Edit User: {{ $model->fullname() }}
		@else
		New User
		@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}


		{{ Form::one('text', 'first_name', 'First Name', null, array('class'=>'form-control required')) }}

		{{ Form::one('text', 'last_name', 'Last Name', null, array('class'=>'form-control required')) }}

		{{ Form::one('email', 'email', 'Email', null, array('class'=>'form-control required email')) }}

		{{-- Form::one('tinymce',  'last_name', 'Last Name', null, array('class'=>'form-control required')) --}}
		<!--
		@if(Auth::user()->is_super)
		{{ Form::one('checkbox', 'is_super', 'Super User', null, array('class'=>'')) }}
		@endif
		-->
		@if( (Auth::user()->group == 100) OR (Auth::user()->id == $model->id))
		{{ Form::one('password', 'password', 'Password', null, array('class'=>'form-control')) }}
		{{ Form::one('password', 'password_confirm', 'Confirm Password', null, array('class'=>'form-control', 'equalTo'=>'#id-password')) }}
		@endif

	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


