<?php

Route::group(array('prefix' => 'rt-admin'), function()
{

	Route::get('people', function(){
		return Redirect::to('retama/people/administrators');
	});

    Route::controller('people/administrators','\Modules\Users\Controllers\Administrators');
	Route::controller('people/users','\Modules\Users\Controllers\Users');
	Route::get('people/administrators/profile', 'Modules\Users\Controllers\Administrators@getProfile');
	Route::post('people/administrators/profile', 'Modules\Users\Controllers\Administrators@postProfile');
	Route::controller('people/newsletter/suscribers','\Modules\Users\Controllers\NewsletterSuscribers');
	//Route::controller('users/users','\Modules\Users\Controllers\Users');


	Route::get('people/newsletter-suscribers/init-module', function() {


		if(! Auth::user()->is_super) return false;

		$q = "CREATE TABLE `newsletter_suscribers` (
			  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			  `email` varchar(255) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
		$response = DB::statement($q);
		dd($response);

	});
});

Route::post('user/email-exists', function(){

	$email = \Input::get('email');
	$id = \Input::get('id', null);
	$user = \User::where('email','=',$email);
	if($id) $user->where('id','!=', $id);
	$flag = !(bool)$user->count();

	if(\Request::ajax())
	{
		return \Response::json($flag);
	} else {
		return $flag;
	}


});

Route::post('newsletter-suscribe', function(){

	$email = \Input::get('suscribe-email');

	$suscriber = new \Modules\Users\Models\NewsletterSuscriber;
	$suscriber->email = $email;
	$suscriber->save();

	if(\Request::ajax())
	{
		return \Response::json('test');
	} else {
		return 'test';
	}


});