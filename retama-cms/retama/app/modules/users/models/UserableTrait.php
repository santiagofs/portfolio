<?php
namespace Modules\Users\Models;

trait UserableTrait {

    public function user()
    {
        return $this->belongsTo('User');
    }
}
