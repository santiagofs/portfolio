<?php
namespace Modules\Users\Models;

class UserProfile extends \BaseModel {

	use \Modules\Users\Models\UserableTrait;
	use \Modules\Media\Models\MediableTrait;

	protected  $table = 'user_profiles';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

    public function avatar($width=200, $heigh=null)
    {
	    $hash = md5( strtolower( trim( $this->user->email ) ) );
	    $this->media();
	    $avatar = $this->media_assorted()->one('avatar');

	    $avatar_url =  $avatar->id ? $avatar->url($width,$heigh): "http://www.gravatar.com/avatar/".$hash.'?s='.$width.'&d=mm';
	    return $avatar_url;
    }


	public function update_avatar($filename)
	{
		$media = $this->media;
		if($media->count())
		{
			$media = $media[0];
			$media->from_filename($filename);
		} else {
			$media = new \Media();
			$media->from_filename($filename);
			$this->media()->attach($media, array('slug'=>'avatar'));
		}

		return $media;
	}

	public function geo_born()
	{
		return $this->belongsTo('Geo','born_geo_id','id');
	}
	public function geo_residence()
	{
		return $this->belongsTo('Geo','residence_geo_id');
	}
}
