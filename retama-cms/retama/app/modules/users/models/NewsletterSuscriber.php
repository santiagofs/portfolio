<?php
namespace Modules\Users\Models;

class NewsletterSuscriber extends \BaseModel {
	protected  $table = 'newsletter_suscribers';

	public $timestamps = false;

	public static function suscribe_form($name, $texts = [])
	{
		// texts: success, placeholder, submit
		//var_dump($texts); die();
		$data = [
			'name'=>$name,
			'placeholder' => isset($texts['placeholder']) ? $texts['placeholder'] : '',
			'submit' => isset($texts['submit']) ? $texts['submit'] : '',
			'success' => isset($texts['success']) ? $texts['success'] : '',
		];

		return \View::make('users::suscribe-form', $data);
	}
}