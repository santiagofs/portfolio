<?php
namespace Modules\Users\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \BaseModel implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected  $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $guarded = array('id');

	public static $rules = array(
		'email' => 'email|unique:users',
	);

	public function get_rules()
	{
		$rules = array(
			'email' => 'email|unique:users,email,'.$this->id,
		);
		return $rules;
	}

	public function fullname() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function gravatar($size=100) {
		$hash = md5( strtolower( trim( $this->email ) ) );
		return 	"http://www.gravatar.com/avatar/".$hash.'?s='.$size;
	}

	public function setPasswordAttribute($value)
	{
		if(!$value) return;
		$this->attributes['password'] = \Hash::make($value);
	}

	public function getIsSuperAttribute($value)
    {
        return $value ? $value : 0;
    }

	public function save(array $options = array())
    {
	    parent::save($options);

	    $profile_arr = \Input::get('profile');

	    if(is_array($profile_arr) AND count($profile_arr))
	    {
		    $profile = $this->profile ?: new \UserProfile;
			$profile->fill($profile_arr);

			$this->profile()->save($profile);
		}

	}

	/* relations */
	public function news()
    {
        return $this->hasMany('News');
    }

    /* relations */
	public function profile()
    {
        return $this->hasOne('UserProfile');
    }


	/* services */
	public static function index ($per_page, $conditions = array())
	{
		//$per_page = 3;
		$model = static::with('profile');

		foreach($conditions as $commmand => $condition)
		{
			if(!is_array($condition)) $condition = [$condition];

			foreach($condition as $item)
			{
				$model->$commmand($item);
			}
		}

		$records = $model->paginate($per_page);
/*
		$queries = \DB::getQueryLog();
		$last_query = end($queries);
		dd($queries);
*/
		return $records;
	}
}
