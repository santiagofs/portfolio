<?php
namespace Modules\Users\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Users extends Retama {

	public function getList()
	{

		$list = parent::getList();
		$list->wheres(function($query){
			$query->where('group','<',100);
		});


		\Breadcrumbs::add('People');
		\Breadcrumbs::add('Users');
		$this->render('users::list', array(
			'section_title' => 'People',
			'list' => $list,
		));
	}

}