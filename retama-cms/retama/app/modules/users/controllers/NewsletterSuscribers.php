<?php
namespace Modules\Users\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class NewsletterSuscribers extends \Modules\Retama\RetamaController {
	protected $model_name = '\Modules\Users\Models\NewsletterSuscriber';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'email'		=> 'Email',
			),
			'email'
		);
		$list->creatable = false;
		$list->deletable = false;
		$list->exportable = true;


		$list->add_filter('text','email', 'Email');

		\Breadcrumbs::add('Users');
		\Breadcrumbs::add('Newsletter');
		$this->render('retama::lists.list', array(
			'section_title' => 'Newsletter Suscribers',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		$sitemap = $model->sitemap;
		$media = $model->media;
		if(!$model->id)
		{
			$model->sitemap = new \Sitemap;
			$model->sitemap->parent_id = $parent_id ?: \Input::get('parent_id', 1);
		}

		//var_dump($errors); die();
		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('content::page-edit', array(
			'section_title' => 'Pages',
			'model' => $model,
		));
	}

	public function getExport()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'email'		=> 'Email',
			),
			'email'
		);

		$list->add_filter('text','email', 'Email');
		$list->export();



	}


}