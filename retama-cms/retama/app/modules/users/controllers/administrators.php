<?php
namespace Modules\Users\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;
//use \eFS\Forms\Forms as Forms;

class Administrators extends Retama {

	public function getList()
	{

		$list = parent::getList();
		$list->wheres(function($query){
			$query->where('group','>=',100);
		});




		\Breadcrumbs::add('People');
		\Breadcrumbs::add('Administrators');
		$this->render('users::list', array(
			'section_title' => 'Administrators',
			'list' => $list,
		));
	}



}