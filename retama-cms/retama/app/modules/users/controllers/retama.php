<?php
namespace Modules\Users\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Retama extends \Modules\Retama\RetamaController {

	protected $model_name = '\User';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'last_name'		=> 'Apellido',
				'first_name'	=> 'Nombre',
				'email'			=> 'Email',
			),
			'last_name'
		);
		$list->wheres(function($query){
			$query->where('is_super','=',0);
		});

		$list->add_filter('text','first_name', 'First Name');
		$list->add_filter('text','last_name', 'Last Name');
		$list->add_filter('text','email', 'Email');

		return $list;
	}

	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{
		//var_dump($errors); die();
		\Breadcrumbs::add('People');
		\Breadcrumbs::add('Administrators');
		//\Breadcrumbs::add('Sarasa');
		$this->render('users::edit', array(
			'section_title' => 'Users',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{
		$model->group = \Retama::controller_name() == 'administrators' ? 100 : 1;
	}

	public function getProfile()
	{
		parent::getEdit(\Auth::user()->id);
	}

	public function postProfile()
	{
		parent::postEdit(\Auth::user()->id);
	}


}