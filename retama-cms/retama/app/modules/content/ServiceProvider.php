<?php
namespace Modules\Content;

class ServiceProvider extends \Modules\ServiceProvider {

    public function register()
    {

        parent::register('content');
    }

    public function boot()
    {
        parent::boot('content');
    }

}