<?php

Route::group(array('prefix' => 'rt-admin/content'), function()
{
    Route::controller('structure','\Modules\Content\Controllers\Structure');
	Route::controller('pages','\Modules\Content\Controllers\Pages');
	Route::controller('blog','\Modules\Content\Controllers\Blog');
	Route::controller('news','\Modules\Content\Controllers\News');
	Route::controller('news-categories','\Modules\Content\Controllers\NewsCategories');
	Route::controller('navigation','\Modules\Content\Controllers\Navigation');
	Route::controller('widgets','\Modules\Content\Controllers\Widgets');
});