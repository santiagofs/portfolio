<?php
namespace Modules\Content\Models;

class Seo {

	public $title = '';
	public $description = '';
	public $keywords = '';

	public static function forge(\Sitemap $item = null)
	{
		return new static($item);
	}

	public function __construct(\Sitemap $item = null)
	{
		if(!$item)
			$item = \Sitemap::get_homepage();


		if( (!$item->is_homepage) && !($item->seotitle || $item->seodescription || $item->seokeywords))
		{
			$item = \Sitemap::get_homepage();
		}

		$this->title = $item->seotitle;
		$this->description = $item->seodescription;
		$this->keywords = $item->seokeywords;
	}

	public function get_title($site_title = '', $separator = ' :: ', $mode = 'append')
	{
		$site_title || $site_title = \Config::get('site::settings.site.title', '');
		if( $mode == 'ignore' ) return $this->title;

		if( $mode == 'append' ) return ($this->title ? $this->title . $separator: '') . $site_title;

		if( $mode == 'prepend' ) return $site_title . ($this->title ? $separator . $this->title : '');

		if( $mode == 'custom' ) return $site_title;
	}
}
