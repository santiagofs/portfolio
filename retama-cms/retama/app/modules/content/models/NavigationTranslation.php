<?php
namespace Modules\Content\Models;

class NavigationTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['name'];
	protected $fillable = ['name'];

	protected $table = 'navigation_translation';

}