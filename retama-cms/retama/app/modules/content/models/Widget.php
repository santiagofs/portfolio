<?php
namespace Modules\Content\Models;

class Widget extends \BaseModel {

	use \Modules\Media\Models\MediableTrait;

	protected  $table = 'widgets';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}
/*
	public static function getConfigAttribute($value) {
		return json_decode($value);
	}
*/


	public function save(array $options = array())
    {

	    parent::save($options);
	    $type = \Input::get('type');
	    switch($type) {
		    case 'slideshow':
			case 'gallery':
		    	$this->save_gallery_from_post('gallery');
		    	break;

		     case 'banner':
		    	$this->save_file_from_post('banner');
		    	break;

		    case 'form':

				$jsondata = \Input::get('jsondata');

				$jsondata || $jsondata = '{"inputs":[]}';
				$json = json_decode($jsondata);

				$data = new \stdClass();
				$data->inputs =[];

				foreach($json->inputs as $input)
				{
					$value = \Input::get('form-'.$input->name);
					$input->value = $value;
					$data->inputs[] = $input;
				}

				$this->data = json_encode($data);
				parent::save();

		    	break;

		    case 'slider':
		    	$slides = \Input::get('slide');
				$data = [];

				if($slides) {
					is_array($slides) || $slides = [$slides];
					foreach($slides as $json_slide) {
				    	$slide = json_decode($json_slide);

						if($slide->file_status == 'new') {
							$media = new \Modules\Media\Models\Media;
							$media->from_filename($slide->file);
							$media->save();

							$this->media()->attach($media, array('slug'=>'slide','sort'=>0));
							$slide->file = $media->id;
						}

						$slide->file_status = 'old';
						$data[] = $slide;
			    	}
				}


		    	$this->data = json_encode($data);
				parent::save();
		    	break;

		    case 'single-slide':
		    	$slide = \Input::get('slide');


				$slide = json_decode($slide);

				if($slide->file_status == 'new') {
					$media = new \Modules\Media\Models\Media;
					$media->from_filename($slide->file);
					$media->save();

					$this->media()->attach($media, array('slug'=>'slide','sort'=>0));
					$slide->file = $media->id;
				}

				$slide->file_status = 'old';

				$this->data = json_encode($slide);
				parent::save();
		    	break;

		    case 'content-selection':
		    	$items = \Input::get('content-item');
				$data = [];
				if($items) {
					foreach($items as $item) {
						if($item) $data[] = $item;
					}
				}
				$this->data = json_encode($data);
				parent::save();
		    	break;
	    }
    }


	public static function get_slides($slug, $count = null)
	{
		$widget = static::where('slug', $slug)->with('media')->first();
		$gallery = $widget->media_assorted()->sluged('gallery');

		$slides = [];
		$i = 0;
		foreach($gallery as $media)
		{
			$i++;
			if( ($count != null) && ($i>$count) ) break;

			$is_embed = (strpos($media->description,'vimeo') !== false) || (strpos($media->description,'youtube') !== false);

			$slide = (object)[
				'link' => $media->description,
				'caption' => $media->title,
				'image' => $media->url(),
				'thumb' => $media->url(300),
				'type' => $is_embed ? 'embed' : 'image',
				'fancybox' => $is_embed ? $slug : ''
			];
			$slides[] = $slide;
		}

		return $slides;
	}

	public static function get_media($slug) {
		$widget = static::where('slug', $slug)->with('media')->first();
		$gallery = $widget->media_assorted()->sluged('gallery');

		return $gallery;
	}

	public static function get($slug) {
		$widget = static::where('slug', $slug)->with('media')->first();
		$type = str_replace('-', '_', $widget->type);
		$method = 'parse_'.$type;

		return static::$method($widget);

	}

	private static function parse_slider($widget) {
		$data = json_decode($widget->data);
		$imgs = [];
		foreach($widget->media as $media) {
			$imgs[$media->id] = $media;
		}

		foreach($data as &$item) {
			$img = isset($imgs[$item->file]) ? $imgs[$item->file] : new Media;
			$item->img = $img;
		}

		return $data;
	}
	private static function parse_single_slide($widget) {
		$item = json_decode($widget->data);
		$imgs = [];
		foreach($widget->media as $media) {
			$imgs[$media->id] = $media;
		}
		$img = isset($imgs[$item->file]) ? $imgs[$item->file] : new Media;
		$item->img = $img;

		return $item;
	}

	private static function parse_content_selection($widget) {

		$data = json_decode($widget->data);

		if(!is_array($data) || !count($data)) return array();

		$config = json_decode($widget->config);
		$model = $config->model;
		if(!class_exists($model)) return array();


		$query = $model::query();
		if(in_array( 'Modules\International\Models\TranslatableTrait', class_uses($model) )) $query->translate();
		//var_dump( class_uses($model)); die();
		$query->with('media');
		$query->whereIn('id', $data)
			->orderByRaw("FIELD(id,".implode(',', $data).")");
		$records = $query->get();
		return $records;
	}
}
