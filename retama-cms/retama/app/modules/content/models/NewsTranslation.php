<?php
namespace Modules\Content\Models;

class NewsTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['title','subtitle','preview','body','extra'];
	protected $fillable = ['title','subtitle','preview','body','extra'];

	protected $table = 'news_translation';
}