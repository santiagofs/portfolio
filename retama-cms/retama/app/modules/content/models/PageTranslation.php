<?php
namespace Modules\Content\Models;

class PageTranslation extends \BaseModel {

	public $timestamps = false;
	public static $translatable_fields = ['title','preview','body','extra'];
	protected $fillable = ['title','preview','body','extra'];

	protected $table = 'pages_translation';

	public function save(array $options = array())
    {
	    $this->extra = '';
	    return parent::save($options);

    }
}