<?php
namespace Modules\Content\Models;

use Carbon\Carbon;

class News extends \BaseModel {

	use \Modules\Media\Models\MediableTrait;
	use \Modules\Content\Models\SitemapableTrait;
	use \Modules\Users\Models\UserableTrait;
	use \Modules\International\Models\TranslatableTrait;
	use \Modules\Retama\Models\LogableTrait;

	protected  $table = 'news';

	protected $hidden = array();
	protected $guarded = array('id');

	public static $rules = array();
	public function get_rules()
	{
		return static::$rules;
	}

    public function save(array $options = array())
    {
	    $action = $this->id ? 'edit' : 'create';

    	if(!$this->publish_date) $this->publish_date = new Carbon;

    	if(!$this->id) $this->user_id = \Auth::user()->id;

	    parent::save($options);

		$this->save_sitemap('news-item');

		$this->save_gallery_from_post('gallery');
		
		$this->save_file_from_post('pdf');

		$this->log($action, 'title', 'News');
    }

	public function getDates()
	{
		$arr = parent::getDates();
		array_push($arr, 'publish_date');
		return  $arr;
	}

/* services */
	public static function index ($per_page, $conditions = array(), $translate = true)
	{
		//$per_page = 3;
		$news = static::with('sitemap');
		if($translate) $news->translate();

		$news->with('media')
			->orderBy('publish_date','DESC')
			->where('publish','=',1);

		foreach($conditions as $commmand => $condition)
		{
			if(!is_array($condition)) $condition = [$condition];

			foreach($condition as $item)
			{
				$news->$commmand($item);
			}
		}

		$records = $news->paginate($per_page);
/*
		$queries = \DB::getQueryLog();
		$last_query = end($queries);
		dd($queries);
*/
		return $records;
	}

	function publish_date_format($value)
	{
		return $this->publish_date->formatLocalized(\Lang::get('retama::dateformats.short-date'));
	}
}
