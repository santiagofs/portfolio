<?php
namespace Modules\Content\Models;

class Navigation extends \TreeModel {

	use \Modules\International\Models\TranslatableTrait;

	protected  $table = 'navigation';

	protected $hidden = array();
	protected $guarded = array('id');

	public function children_path()
	{
		return parent::children_path();
	}

	public function sitemap()
    {
        return $this->belongsTo('Sitemap');
    }


	public static function get_homepage()
	{
		$homepage = $item = static::where('is_homepage','=',1)->first();
		return $homepage;
	}


	public $childs = array();
	public function branch($childs = null)
	{
		if(!$childs)
		{
			$childs = static::with('sitemap')->where('tree_left', '>', $this->tree_left)
			->where('tree_right','<', $this->tree_right)
			->orderBy('tree_left');

			if(isset(static::$language))
			{
				$childs = $childs->with('translations');
			}

			$childs = $childs->get();
		}

		foreach($childs as $key => $child)
		{
			if($child->parent_id == $this->id)
			{
				$this->childs[] = $child;
				$childs->forget($key);
			}
		}

		if(count($childs))
		{
			foreach($this->childs as $child)
			{
				$child->branch($childs);
			}
		}

		return $this;
	}


    public static function get_menu($slug, $menu_view = 'content::menu', $node_view = 'content::menu-node', $anchor = null)
    {
		$menu = \Navigation::translate()->with('sitemap')->where('slug','=',$slug)->where('parent_id','=',1)->first();
		if(!$menu) return null;

		$menu->branch();


		return \View::make($menu_view, get_defined_vars());
    }

	public static function parse_item($item) {
		if(!$item->sitemap) {
			$is_home_link = false;
			$prefix = '';
			$is_current = false;
			$url = $item->url;
		} else {
			if(isset($parent)) {
				$is_home_link = false;
				$path_translated = \URL::translate($item->sitemap->path);

				$home_translated = \URL::translate('/');
				$prefix = \Language::url_prefix();

				$is_current = ( \Request::is($path_translated .'*') || ($is_home_link && \Request::is($home_translated))) ? 'active' : '';
				$url = $is_home_link ? \URL::to('/') : \URL::to($parent->sitemap->path).'#'.$item->sitemap->slug;

			} else {
				$is_home_link = $item->sitemap->is_homepage;
				$path_translated = \URL::translate($item->sitemap->path);

				$home_translated = \URL::translate('/');
				$prefix = \Language::url_prefix();

				$is_current = ( \Request::is($path_translated .'*') || ($is_home_link && \Request::is($home_translated))) ? 'active' : '';
				$url = $is_home_link ? \URL::to('/') : \URL::to($item->sitemap->path);

			}

		}

		$ret = array(
			'is_home_link' => $is_home_link,
			'prefix' => $prefix,
			'is_current' => $is_current,
			'url' => $url,
		);

		return $ret;
	}

}
