<?php
namespace Modules\Content\Models;

class Sitemap extends \TreeModel {

	protected  $table = 'sitemap';

	protected $hidden = array();
	protected $guarded = array('id');

	public function children_path()
	{
		return parent::children_path();
	}

	public function sitemapable()
    {
        return $this->morphTo('sitemapable');
    }

    public function navigation()
    {
        return $this->hasOne('Navigation');
    }


	public static function get_homepage()
	{
		$homepage = $item = static::where('is_homepage','=',1)->first();
		return $homepage;
	}

	public static function destroy($ids = null)
    {

		$ids = is_array($ids) ? $ids : func_get_args();

		foreach($ids as $id)
		{
			$node = static::find($id);
			if($sitemapable  = $node->sitemapable_type) $sitemapable::destroy($node->sitemapable_id);
			\Navigation::where('sitemap_id', $id)->delete();
		}
		parent::destroy($ids);
    }



	public function check_redirect()
	{

		// is permanent redirect... no? exit

		if( !($redirect = $this->is_redirect) ) return null;

		// is a category with child redirect ?
		if($redirect == -1)
		{
			// redirect to first child path or home
			if( !$this->has_children() ) \Redirect::away($first_child->path);

			$first_child = $this->children[0];
			return \Redirect::away($first_child->path);
		}

		// is a redirect to other sitemap item?
		if(is_numeric($redirect))
		{
			// TODO
		}

		// finally use the string to redirect
		return \Redirect::away($redirect);
	}
}
