<?php
namespace Modules\Content\Models;

trait BlogableTrait {

    public function blog()
    {
        return $this->MorphOne('Blog', 'blogable');
    }

    public function delete()
    {
	    $this->blog()->delete();

	    return parent::delete();
    }

    public function getBlogableTitle()
    {
	    return 'title'; // override if needed;
    }

    public function getBlogRoot()
    {
	    if(!$this->blog) {
		    $blog = new \Blog;
		    $blog->parent_id = 1;
			$this->blog()->save($blog);
	    }

	    return $this->blog;
    }

}
