<?php
namespace Modules\Content\Models;

trait SitemapableTrait {

    public function sitemap()
    {
        return $this->MorphOne('Sitemap', 'sitemapable');
    }

    public function delete()
    {
	    $this->sitemap()->delete();

	    return parent::delete();
    }

    public function get_module_root()
    {
    	$class =  get_class($this);
	    $sitemap = \Sitemap::where('is_root', '=',$class)->first();
	    if(!$sitemap) throw new \Exception('No module root for '. $class);
		return $sitemap;
    }

	public function save_sitemap($layout, $title_field = 'title')
	{
		$sitemap_attr = \Input::get('sitemap');

	    if(is_array($sitemap_attr) AND count($sitemap_attr))
	    {

		    $sitemap = $this->sitemap ?: new \Sitemap;

			$parent_id = (isset($sitemap_attr['parent_id']) && $sitemap_attr['parent_id']) ?  $sitemap_attr['parent_id']: $this->get_module_root()->id;
			//var_dump($sitemap->id);
		    $sitemap->layout OR $sitemap->layout = $layout;

			$sitemap->fill($sitemap_attr);

			if($title_field) {
				$sitemap->name = $this->{$title_field};
				if(!$sitemap->seotitle) $sitemap->seotitle = $this->{$title_field};
			}
			$sitemap->slug = \Str::slug($sitemap_attr['slug']);

			$sitemap->check_parent($parent_id);
			$sitemap->parent_id = $parent_id;

			$this->sitemap()->save($sitemap);
	    }
	}
}
