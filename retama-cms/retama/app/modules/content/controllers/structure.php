<?php
namespace Modules\Content\Controllers;

class Structure extends \Modules\Retama\RetamaController {

	protected $model_name = '\Sitemap';

	// dashboard
	public function getIndex()
	{

		//$Structure = \Structure::with('children_path')->where('parent_id','=',0)->get();
		$structure = \Sitemap::where('parent_id','=',0)->get();
		$root = count($structure) ? $structure[0] : new \Sitemap;


		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Structure');
		$this->render('content::structure', array(
			'section_title' => 'Site Structure',
			'root' => $root,
		));
	}

	public function getList()
	{
		return 'Im the content list';
	}

	public function getStructure()
	{

	}

	protected function onGetEdit($model, $parent_id=null)
	{
		$sitemap = $model->sitemap;
		$media = $model->media;

		if( ! $model->parent_id)
		{
			$parent_id || $parent_id = \Input::get('parent_id') ?: 1;
			$model->parent_id = $parent_id;
		}

		//var_dump($errors); die();
		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('content::structure-edit', array(
			'section_title' => 'Pages',
			'model' => $model,
		));
	}

	public function postMoveNode()
	{
		$node_id = \Input::get('node_id');
		$parent_id = \Input::get('parent_id');
		$previous_id = \Input::get('previous_id');

		$node = \Sitemap::find($node_id);
		$node->move($parent_id, $previous_id);

		return 'sarasa';
	}

}