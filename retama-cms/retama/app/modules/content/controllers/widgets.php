<?php
namespace Modules\Content\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Widgets extends \Modules\Retama\RetamaController {
	protected $model_name = '\Widget';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'name'		=> 'Title',
			),
			'name'
		);
		$list->creatable = false;
		$list->deletable = false;


		$list->add_filter('text','name', 'Title');

		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Widgets');
		$this->render('retama::lists.list', array(
			'section_title' => 'Widgets',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		$sitemap = $model->sitemap;
		$media = $model->media;
		if(!$model->id)
		{
			$model->sitemap = new \Sitemap;
			$model->sitemap->parent_id = $parent_id ?: \Input::get('parent_id', 1);
		}

		//var_dump($errors); die();
		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Widgets');
		$this->render('content::widget-edit', array(
			'section_title' => 'Widgets',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}