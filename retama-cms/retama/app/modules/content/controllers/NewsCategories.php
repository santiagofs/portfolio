<?php
namespace Modules\Content\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class NewsCategories extends \Modules\Retama\RetamaController {
	protected $model_name = '\Sitemap';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'name'		=> 'Nombre',
			),
			'name'
		);

		$list->add_filter('text','title', 'Titulo');
		$list->wheres(function($query){
			$query->where('is_root','Modules\Content\Models\News');
		});

		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('News');
		$this->render('retama::lists.list', array(
			'section_title' => 'News Categories',
			'list' => $list,
		));
	}


	protected function onGetEdit($model, $parent_id=null)
	{

		/*
$sitemap = $model->sitemap;
		$media = $model->media;
*/

		if( ! $model->parent_id)
		{
			$parent_id || $parent_id = \Input::get('parent_id') ?: 1;
			$model->parent_id = $parent_id;
		}

		//var_dump($errors); die();
		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('News');
		$this->render('content::structure-edit', array(
			'section_title' => 'News Categories',
			'model' => $model,
		));
	}

}