<?php
namespace Modules\Content\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class News extends \Modules\Retama\RetamaController {
	protected $model_name = '\News';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'title'		=> 'Title',
				'publish_date' => 'Date',
			),
			'title'
		);


		$list->add_filter('text','title', 'Title');


		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('retama::lists.list', array(
			'section_title' => 'Pages',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		$sitemap = $model->sitemap;
		$media = $model->media;


		//var_dump($model->media_assorted->sluged('gallery')->toJson());die();
		//var_dump(get_class($media)); die();
		//var_dump($model->media_assorted);
		//var_dump($model->media_assorted->typed('image'));

		//var_dump($media);
		//var_dump(get_class($model->media())); die();
/*
		$media = new \Media();
		$media->title = 'test_media';
		$media->description = 'test description';
		$model->media()->save($media, array('slug'=>'main', 'sort'=>1));
		die();
*/

		/*
if(!$seo) $seo = new \Seo;
		$seo->title = 'test title';
		$model->seo()->save($seo);
*/
/*
		$nav = $model->navigation;
		if(!$nav) $nav = new \Structure;
		$nav->parent_id = 1;
		$nav->slug = 'test-slug';
		//$nav->save();
		$model->navigation()->save($nav);
*/


		//var_dump($errors); die();
		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('content::news-edit', array(
			'section_title' => 'Pages',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}