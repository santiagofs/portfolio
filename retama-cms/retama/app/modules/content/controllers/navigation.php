<?php
namespace Modules\Content\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Navigation extends \Modules\Retama\RetamaController {

	protected $model_name = '\Navigation';

	public function getIndex()
	{
		$model_name = $this->model_name;
		//$Structure = \Structure::with('children_path')->where('parent_id','=',0)->get();
		$menus = $model_name::where('parent_id','=',0)->get();
		$root = count($menus) ? $menus[0] : new $model_name;


		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Navigation / Menus');
		$this->render('content::navigation-list', array(
			'section_title' => 'Navigation / Menus',
			'root' => $root,
		));
	}

	public function getList()
	{
		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'name'		=> 'Titulo',
			),
			'name'
		);

		$list->wheres(function($query){
			$query->where('parent_id','=',1);
		});


		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Navigation / Menus');
		$this->render('retama::lists.list', array(
			'section_title' => 'Navigation / Menus',
			'list' => $list,
		));
	}

	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('content::navigation-edit', array(
			'section_title' => 'Pages',
			'model' => $model,
		));
	}

	public function postMoveNode()
	{
		$node_id = \Input::get('node_id');
		$parent_id = \Input::get('parent_id');
		$previous_id = \Input::get('previous_id');

		$node = \Navigation::find($node_id);
		$node->move($parent_id, $previous_id);

		return 'sarasa';
	}


}
