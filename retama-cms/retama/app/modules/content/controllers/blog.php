<?php
namespace Modules\Content\Controllers;

use \View as View;
use \eFS\Lists\Lists as Lists;

class Blog extends \Modules\Retama\RetamaController {
	protected $model_name = '\Blog';

	// dashboard

	public function getList()
	{

		$list = new Lists($this->model_name);
		//$list->sortable = true;

		$list->fields(
			array(
				'title'		=> 'Titulo',
			),
			'title'
		);
		$list->creatable = true;
		$list->deletable = true;


		$list->add_filter('text','titulo', 'Titulo');
		/*
$list->add_filter('text','last_name', 'Last Name');
		$list->add_filter('text','email', 'Email');
*/

/*
		$list->wheres(function($query){
			$query->where('group','<',100);
		});
*/


		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('retama::lists.list', array(
			'section_title' => 'Pages',
			'list' => $list,
		));
	}


	// $id = primary key o instancia del model ya cargado
	protected function onGetEdit($model, $parent_id=null)
	{


		$sitemap = $model->sitemap;
		$media = $model->media;
		if(!$model->id)
		{
			$model->sitemap = new \Sitemap;
			$model->sitemap->parent_id = $parent_id ?: \Input::get('parent_id', 1);
		}

		//var_dump($errors); die();
		\Breadcrumbs::add('Content');
		\Breadcrumbs::add('Pages');
		$this->render('content::page-edit', array(
			'section_title' => 'Pages',
			'model' => $model,
		));
	}

	protected function onPostEdit($model)
	{

	}

}