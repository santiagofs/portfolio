@extends('retama::layouts.content')

@section('content-content')

<div id="structure-list-holder">
<ul id="structure-list" draggable-tree update-url="{{ URL::to('rt-admin/content/structure/move-node') }}">
@foreach($root->children as $item)
@include('content::structure-node', array('item'=>$item))
@endforeach

</ul>
<a href="#" create-structure class="btn btn-primary">Create Page</a>
</div>


<div style="display:none" id="structure-create-holder">
	<ul >
		<li><a href="{{ URL::to('rt-admin/content/structure/edit/0', false) }}">Sitemap URL</a></li>
		<li><a href="{{ URL::to('rt-admin/content/pages/edit/0', false) }}">Page</a></li>
		<li><a href="#">Blog</a></li>
		<li><a href="#">News</a></li>
	</ul>
</div>

<style>
	.onover > .text{
		background: #FFF;
	}
	#placeholder.onover {
		background: #FFF;
	}
	.node-item {
		cursor: pointer;
	}
	#structure-list li {
	}
</style>
@stop

