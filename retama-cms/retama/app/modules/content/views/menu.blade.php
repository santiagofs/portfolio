<nav class="menu {{ $slug }}">
	<ul>
		@foreach($menu->childs as $item)
		@include($node_view, array('item'=>$item))
		@endforeach
	</ul>
</nav>