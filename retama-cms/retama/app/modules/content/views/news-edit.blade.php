@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<ul class="nav nav-pills">
			<li role="presentation" class="active"><a href="#tab-main" data-toggle="tab">Main</a></li>
			<li role="presentation"><a href="#tab-gallery" data-toggle="tab">Gallery</a></li>
			<li role="presentation"><a href="#tab-seo" data-toggle="tab">SEO</a></li>
		</ul>
		<h2>
			@if($model->id)
			Edit News: {{ $model->title }}
			@else
			New News
			@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}
		<div class="tab-content">
		<div id="tab-main" class="tab-pane active">
		<?php
			$attr = array('class'=>'form-control required');
			if(!$model->id) {
				$attr['slugify-to'] = '#sitemap-slug';
				$attr['copy-to'] = '#sitemap-title';
			}
			$attr['copy-to'] = '#sitemap-name';
			$publish_date = $model->publish_date ?: new Carbon;

			$categories = \Sitemap::where('is_root', get_class($model))->lists('Name','id');

		?>

		{{ Form::hidden('publish_date', $publish_date->format('Y-m-d'), array('id'=>'id-display_date')) }}

		{{ Form::one('text', 'display_date_select', 'Publish Date', $publish_date->format('d-m-Y'), array('class'=>'form-control required datepicker', 'datetimepicker'=>2, 'format'=>'dd-mm-yyyy','mirror'=>'id-display_date')) }}

		{{ Form::one('select', 'sitemap[parent_id]', 'Category', null, ['list'=>$categories, 'class'=>'form-control']) }}

		{{ Form::one('text', 'title', 'Title', null, $attr) }}

		{{ Form::one('text', 'subtitle', 'Subtitle', null, array('class'=>'form-control')) }}

		{{ Form::one('textarea', 'preview', 'Preview', null, array('class'=>'form-control')) }}

		{{ Form::one('tinymce', 'body', 'Page body', null, array('class'=>'form-control')) }}
		
		{{ Form::one('file', 'pdf', 'PDF', null, array('file'=>$model->media_assorted()->one('pdf'))) }}

		{{-- Form::one('tinymce', 'extra', 'Extra', null, array('class'=>'form-control')) --}}

		{{-- Form::one('tinymce',  'last_name', 'Last Name', null, array('class'=>'form-control required')) --}}
		</div>


		<div id="tab-gallery" class="tab-pane">
			@include('media::gallery', array('name'=>'gallery', 'with_embeds'=>false, 'gallery_items'=>$model->media_assorted()->sluged('gallery')->toJson(JSON_HEX_APOS | JSON_HEX_QUOT)))
		</div>


		<div id="tab-seo" class="tab-pane">
		@include('content::sitemap-fields')
		</div>
		</div>
	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


