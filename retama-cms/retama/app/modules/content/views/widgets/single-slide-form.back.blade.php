	{{ Form::hidden('slide[status]', 'new') }}

	{{ Form::one('file', 'slide_image', 'Image', null) }}

	{{ Form::one('text', 'slide[text]', 'Text', null, ['class'=>'form-control']) }}

	{{ Form::one('text', 'slide[link]', 'Link', null, ['class'=>'form-control']) }}

	{{ Form::one('text', 'slide[video]', 'Video Url', null, ['class'=>'form-control']) }}
