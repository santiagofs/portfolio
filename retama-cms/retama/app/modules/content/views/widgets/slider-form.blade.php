@if($model->type == 'slider')
				<fieldset class="slides-widget">
					<legend>Slides</legend>

					<div id="slide-sample" class="slide draggable" draggable="true">
						<input type="hidden" name="slide[]" value="" />
						<div class="commands">
							<a href="#" remove-slide><span class="fa fa-ban"></span>

						</div>

						<div class="fa fa-grip-large" data-action="item-move"></div>
						<a class="thumb" edit-slide></a>
						<div class="texts">
							<div class="holder"><label>Text</label><div class="value text"></div></div>
							<div class="holder"><label>Button Text</label><div class="value button"></div></div>
							<div class="holder"><label>Link</label><div class="value link"></div></div>
						</div>
					</div>

					<div id="slides-list" class="slides-list" droppable="true">
			<?php
				$slides = json_decode($model->data);
				if(is_array($slides)):
					foreach($slides as $slide):
					$media = \Media::find($slide->file);
					$url = $media->url(100,100);
					$slide->img_url = $url;
			?>
					<div class="slide draggable" draggable="true">
						<input type="hidden" name="slide[]" value="<?php echo(htmlspecialchars(json_encode($slide, JSON_HEX_APOS | JSON_HEX_QUOT))); ?>" />
						<div class="commands">
							<a href="#" remove-slide><span class="fa fa-ban"></span></a>

						</div>
						<div class="fa fa-grip-large" data-action="item-move"></div>
						<a class="thumb" style="background-image: url('{{$url}}')" edit-slide></a>
						<div class="texts">
							<div class="holder"><label>Text</label><div class="value text">{{$slide->text}}</div></div>
							<div class="holder"><label>Button Text</label><div class="value button">{{$slide->button}}</div></div>
							<div class="holder"><label>Link</label><div class="value link">{{$slide->link}}</div></div>
						</div>
					</div>

			<?php
					endforeach;
				endif;
			?>
					</div>
					<div class="actions">
						<a href class="btn btn-default" add-slide>Add Slide</a>
					</div>



				</fieldset>

			@endif