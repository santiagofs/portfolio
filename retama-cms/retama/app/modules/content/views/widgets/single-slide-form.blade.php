			@if($model->type == 'single-slide')
			<?php
				$slide = json_decode($model->data);
				if($slide) {
					$media = \Media::find($slide->file);
					$url = $media->url(100,100);
					$slide->img_url = $url;
				}

				//var_dump($slide); die();
			?>
				<fieldset>
					<legend>Slide</legend>
					<div class="slide" draggable="true">
						<input type="hidden" name="slide" value="<?php echo(htmlspecialchars(json_encode($slide, JSON_HEX_APOS | JSON_HEX_QUOT))); ?>" />
						<div class="commands">
							<a href="#" remove-slide><span class="fa fa-ban"></span></a>

						</div>
						<div class="fa fa-grip-large" data-action="slide-move"></div>
						<a class="thumb" style="background-image: url('{{$url or ''}}')" edit-slide></a>
						<div class="texts">
							<div class="holder"><label>Text</label><div class="value text">{{$slide->text or ''}}</div></div>
							<div class="holder"><label>Button Text</label><div class="value button">{{$slide->button or ''}}</div></div>
							<div class="holder"><label>Link</label><div class="value link">{{$slide->link or ''}}</div></div>
						</div>
					</div>
				</fieldset>
			@endif
