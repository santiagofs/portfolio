<?php
	$data = $model->data;
	if($data) $data = json_decode($data);
	is_array($data) || $data = [];
?>
<fieldset>
	<legend>Slides</legend>
	<ul id="slide-lists">
	@foreach($data as $slide)
		<li>slide </li>
	@endforeach
	</ul>

	@include('content::widgets.single-slide-form')

	<div class="form-group">
		<div class="form-control-holder">
			<button type="button" id="add-slide" class="btn btn-secondary">Add Slide</button>
		</div>
	</div>

</fieldset>

@section('footer-scripts')
@parent

<script type="text/javascript">

	var slide_template = [
		'<li draggable="true">',
			'<input type="hidden" json="true" name="jsondata[]" value="">',
			'<span class="text"></span>',
			'<a href="#"  remove-slide="true" class="fa fa-times"></a>',
		'</li>'
		].join('');


	$('#add-slide').on('click', function(e) {
		e.preventDefault();
		var $img = $('#slide_image_file');
		if(!$img.val()) {
			alert('Image is required');
			return;
		}

		var obj = {
			image: $img.val(),
			status: $('[name="slide[status]"]').val(),
			text: $('[name="slide[text]"]').val(),
			link: $('[name="slide[link]"]').val(),
			video: $('[name="slide[video]"]').val(),
		}
		var json = JSON.stringify(obj);
		var $li = $(slide_template);
		$('[name="jsondata[]"]', $li).val(json);
		$('.text', $li).text($img.val());
		$('#slide-lists').append($li);

		$img.val('');
		$('#slide_image-display').text('');
		$('[name="slide[status]"]').val('');
		$('[name="slide[text]"]').val('');
		$('[name="slide[link]"]').val('');
		$('[name="slide[video]"]').val('');

	});

	this.$placeholder = $('<li id="placeholder" dropable="true"	 style="height:20px; border: 1px solid #CCC; width:200px;"></li>');


	$(document).on('dragstart', '[draggable]', function(e) {

		var $this = $(this);
		that.$current_drag = $(e.originalEvent.target);
		var node_id = $this.attr('node-id');
		e.originalEvent.dataTransfer.setData('node-id',node_id);
		e.originalEvent.dataTransfer.effectAllowed = 'all';
	})



	$(document).on('drop', '[dropable]', function(e){
		e.preventDefault();
		e.stopPropagation();

		var $target = $(e.originalEvent.target);
		var $this = $(this);
		var $parent = null;

		if(that.$current_drag.has($this).length) return false;

		if($this.attr('id') == 'placeholder') {
			$parent = $this.closest('[node-id]');
			that.$current_drag.insertAfter($target);
		} else {
			if($this.attr('node-id'))
			{
				$parent = $this;
				$('> ul', $this).prepend(that.$current_drag);
			} else {
				$parent = null;
			}
		}

		if($parent)
		{
			that.$placeholder.detach();
			var $previous = that.$current_drag.prev();
			var previous_id = $previous.length ? $previous.attr('node-id') : undefined;
			var parent_id = $parent.attr('node-id') ? $parent.attr('node-id') : 1;
			var node_id = that.$current_drag.attr('node-id');
			that.update_position(node_id, parent_id, previous_id);
		}

	})

	$(document).on('dragenter','[dropable]', function(e) {
		 e.preventDefault();
		 e.stopPropagation();
		 var $this = $(this);
		 var $target = $(e.originalEvent.target);
		 if(that.$current_drag.has($this).length || (that.$current_drag.get(0) === $this.get(0))) return;

		 that.$placeholder.removeClass('onover');
		 $this.addClass('onover');

		 if($this.attr('node-id') != that.$current_drag.attr('node-id'))
		 {
		 	that.$placeholder.insertAfter($this);
		 }
	})
	$(document).on('dragleave','[dropable]', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var $this = $(this);
		$this.removeClass('onover');
	})

	$(document).on('dragend','[dropable]', function(e) {
		  e.preventDefault();
		  e.stopPropagation();
		  that.$placeholder.detach();
		  that.$placeholder.removeClass('onover');
	})

	$(document).on('dragover','[dropable]',function(e){
		e.preventDefault();
		e.stopPropagation();
	})


</script>
@stop
