			@if($model->type == 'content-selection')
			<?php
				$config = json_decode($model->config);
				$content_model = $config->model;
				if(!class_exists($content_model)) {

			?>
			<div> Model <?php echo($content_model); ?>  not found</div>
			<?php
				} elseif(!$config->display) {
			?>
			<div> Display field missing</div>
			<?php

				} else {
					$query = $content_model::query();
					$query->orderBy($config->display, 'ASC');
					$records = $query->lists($config->display, 'id');
					$selected = json_decode($model->data);
					$selected || $selected = "[]";
					$selected = htmlspecialchars(json_encode($selected, JSON_HEX_APOS | JSON_HEX_QUOT));
			?>
			<fieldset>
				<legend>Items</legend>



					<div id="content-item-sample" class="content-selection draggable" draggable="true">
						<input type="hidden" name="content-item[]" value="" />
						<div class="commands">
							<a href="#" remove-content-item><span class="fa fa-ban"></span></a>
						</div>

						<div class="fa fa-grip" data-action="item-move"></div>

						<div class="text">
							<div class="holder"><label>Text</label><div class="value text"></div></div>
						</div>
					</div>


					<div class="form-group">
						<label class="form-control-label" style="width:100%; text-align: left; float: none">Seleccionados <?php echo(@$config->limit ? '(máximo: ' .$config->limit .')': ''); ?>:</label>
							<div class="form-control-holder" style="margin:0">
							<div id="content-selection-list" class="content-selection-list" limit="{{$config->limit or ''}}" droppable="true"  items="<?php echo($selected); ?>">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="content-selection-select clearfix">
							<a class="pull-right btn btn-default" href="#" id="add-content-item">Agregar</a>
							<div class="select-holder" style="margin-right: 90px">
								{{ Form::select('content-item-select', $records, null,array('id'=>'content-item-select', 'class'=>'form-control')) }}
							</div>

						</div>
				</div>

			</fieldset>
			<?php
				}
			?>
			@endif
