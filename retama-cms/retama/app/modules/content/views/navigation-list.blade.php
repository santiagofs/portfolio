@extends('retama::layouts.content')

@section('content-content')

<div id="structure-list-holder">
<ul id="structure-list" draggable-tree update-url="{{ URL::to('rt-admin/content/navigation/move-node') }}">
@foreach($root->children as $item)
@include('content::navigation-list-node', array('item'=>$item))
@endforeach
</ul>
<a href="{{ URL::to('rt-admin/content/navigation/edit/0', false, array('parent_id'=>1)) }}" class="btn btn-primary">Create Menu</a>
</div>


<style>
	.onover > .text{
		background: #FFF;
	}
	#placeholder.onover {
		background: #FFF;
	}
	.node-item {
		cursor: pointer;
	}
	#structure-list li {
	}
</style>
@stop