<?php
	$draggable = $item->parent_id != 1 ? 'draggable="true"' : '';
?>
<li {{ $draggable }} dropable="true" class="node-item" node-id="{{ $item->id }}" node-left="{{ $item->tree_left }}" node-right="{{ $item->tree_right }}">
	<div>{{ $item->name }} <span class="actions">
		<a href="{{  URL::to('rt-admin/content/navigation/edit/'.$item->id, false) }}" class="fa fa-edit"></a>
		<a href="{{ URL::to('rt-admin/content/navigation/delete/'.$item->id, false) }}" delete-item node-id="{{$item->id}}" class="fa fa-times"></a>
		<a href="{{ url('rt-admin/content/navigation/edit/0?parent_id='.$item->id) }}" node-id="{{ $item->id }}" class="fa fa-plus"></a></span> </div>

	<ul>
@if($item->has_children())

	@foreach($item->children as $child)
		@include('content::navigation-list-node', array('item'=>$child))
	@endforeach

@endif
	</ul>
</li>

