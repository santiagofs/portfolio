<?php
	if(!$item->sitemap) {
		$is_home_link = false;
		$prefix = '';
		$is_current = false;
		$url = $item->url;
	} else {
		$is_home_link = $item->sitemap->is_homepage;
		$path_translated = \URL::translate($item->sitemap->path);

		$home_translated = \URL::translate('/');
		$prefix = \Language::url_prefix();

		$is_current = ( Request::is($path_translated .'*') || ($is_home_link && Request::is($home_translated))) ? 'active' : '';
		$url = $is_home_link ? URL::to('/') : URL::to($item->sitemap->path);
	}





	/* var_dump($item->tree_left); var_dump($item->tree_right); */
?>
<li class="{{ $item->has_children() ? 'with-children' : '' }} {{ $is_current }}">
	<a href="{{ $url }}" target="{{ $item->target }}"><span class="menu-text">{{ $item->name }}</span></a>
@if($item->has_children())
	<ul>
	<?php
		$menu_view = $node_view ?: 'content::menu-node';
	?>
	@foreach($item->childs as $child)
		@include( $menu_view , array('item'=>$child))
	@endforeach
	</ul>
@endif
</li>
