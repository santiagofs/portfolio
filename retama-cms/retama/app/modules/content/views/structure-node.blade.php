<li draggable="true" dropable="true" class="node-item" node-id="{{ $item->id }}" node-left="{{ $item->tree_left }}" node-right="{{ $item->tree_right }}">

		<div   style="display:inline-block" class="text"> {{ $item->name }} </div>
		<span class="actions">
			<a href="{{  URL::to('rt-admin/content/structure/edit/'.$item->id, false) }}" class="fa fa-edit"></a>
			<a href="{{ URL::to('rt-admin/content/structure/delete/'.$item->id, false) }}" delete-item node-id="{{$item->id}}" class="fa fa-times"></a>
			<a href="#" create-structure node-id="{{ $item->id }}" class="fa fa-plus"></a>
		</span>


		<ul >
			@if($item->has_children())

				@foreach($item->children as $child)
					@include('content::structure-node', array('item'=>$child))
				@endforeach

			@endif
		</ul>
</li>