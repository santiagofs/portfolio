@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => Request::url(), 'method' => 'post', 'id'=>'form-edit')) }}
<?php
	$parent_id = $model->parent_id ?: \Input::get('parent_id',1);
?>
	<header class="form-header">
		<h2>
			@if($model->id)
				@if($parent_id == 1)
				Edit Menu: {{ $model->name }}
				@else
				Edit Menu Item: {{ $model->name }}
				@endif
			@else
				@if($parent_id == 1)
				New Menu: {{ $model->name }}
				@else
				New Menu Item: {{ $model->name }}
				@endif
			@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}
		{{ Form::hidden('parent_id', $parent_id) }}

		<?php
			$attr = array('class'=>'form-control required');
			if(!$model->id) {
				$attr['slugify-to'] = '#navigation-slug';
			}
		?>
		{{ Form::one('text', 'name', 'Menu name', null, $attr) }}

		{{ Form::one('text', 'slug', 'Slug', null, array('class'=>'form-control', 'id'=>'navigation-slug')) }}

		<?php
			$list = \Sitemap::where('parent_id','>',0)->orderBy('tree_left')->lists('name','id');
			$list = [0 => 'Use URL'] + $list;
		?>

		{{ Form::one('select', 'sitemap_id', 'Sitemap object', null, array('class'=>'form-control','list'=>$list)) }}

		{{ Form::one('text', 'url', 'Url', null, array('class'=>'form-control')) }}

		<?php
			$list = [
				'_self'	=>	'Same page',
				'_blank'	=>	'New page / tab',
				'_parent'	=>	'Parent Frame',
				'_top'	=>	'Top Document',
			];
		?>
		{{ Form::one('select', 'target', 'Target', null, array('class'=>'form-control', 'list'=>$list)) }}

	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ URL::to('rt-admin/content/navigation', false) }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


@section('footer-scripts')
@parent
<script>
	$('#id-sitemap_id').on('blur', function(e){
			console.log('test');
		if($('#id-name').val() == '') {

			$('#id-name').val($('#id-sitemap_id option:selected').text());
			$('#id-name').focus();
			$('#id-navigation-slug').focus();
		}

	});
</script>
@stop