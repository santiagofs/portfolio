@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => URL::full(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">

		<h2>
			@if($model->id)
			Edit Site node: {{ $model->name }}
			@else
			New Site node
			@endif
		</h2>
	</header>
	<div class="form-fields">
		<legend>Main</legend>
		{{ Form::hidden('redirect', \Request::url()) }}
		{{ Form::hidden('id') }}
		{{ Form::hidden('parent_id', $model->parent_id ?: 1) }}

		{{ Form::one('text', 'name', 'Name', null, array('class'=>'form-control')) }}
		{{ Form::one('text', 'slug', 'Url', null, array('class'=>'form-control')) }}
		{{ Form::one('text', 'layout', 'Layout', null, array('class'=>'form-control')) }}
		{{ Form::one('text', 'is_redirect', 'Is Redirect', null, array('class'=>'form-control')) }}
		{{ Form::one('text', 'is_root', 'Is Root / Category', null, array('class'=>'form-control')) }}

		<legend>SEO</legend>
		{{ Form::one('text', 'seotitle', 'Title', null, array('class'=>'form-control')) }}
		{{ Form::one('text', 'seokeywords', 'Keywords', null, array('class'=>'form-control')) }}
		{{ Form::one('textarea', 'seodescription', 'Description', null, array('class'=>'form-control', 'id'=>'sitemap-slug')) }}
	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Cancel</a>

	</footer>

	{{ Form::close() }}





@stop


