@extends('retama::edit.edit')

@section('form-header')

@stop


@section('form')

{{ Form::model($model, array('url' => URL::full(), 'method' => 'post', 'id'=>'form-edit')) }}

	<header class="form-header">
		<h2>
			@if($model->id)
			Edit Widget: {{ $model->name }}
			@else
			New Widget
			@endif
		</h2>
	</header>
	<div class="form-fields">

		{{ Form::hidden('id') }}
		{{ Form::hidden('data') }}
		<?php
			//var_dump(json_encode(['model'=>'site::punto', 'limit'=>3, 'order'=>'title']));
		?>

		@if(\Auth::user()->is_super)
		{{ Form::one('select', 'type', 'Type', null, ['class'=>'form-control required', 'list'=>['single-slide'=>'Single slide', 'slideshow'=>'Slideshow', 'slider'=>'Slider', 'banner'=>'Banner', 'form' => 'Form', 'content-selection'=>'Content selection', 'gallery'=>'Gallery']]) }}
		{{ Form::one('text', 'slug', 'Slug', null, ['class'=>'form-control required']) }}
		{{ Form::one('textarea', 'config', 'Config', null,  ['class'=>'form-control'])}}
		@else
		{{ Form::hidden('type') }}
		{{ Form::hidden('slug') }}
		{{ Form::hidden('config') }}
		@endif

		{{ Form::one('text', 'name', 'Name', null, ['class'=>'form-control required']) }}

		@if($model->id)

			@if($model->type == 'slideshow')
				<fieldset>
					<legend>Slides</legend>
					<!-- <p style="font-size:1.3rem; color: #AAA">Use the title field for a slide caption and the description both for a link or a video url</p> -->
					@include('media::gallery', [
						'name'=>'gallery',
						'with_embeds'=>false,
						'gallery_items'=>$model->media_assorted()->sluged('gallery')->toJson(JSON_HEX_APOS | JSON_HEX_QUOT),
						'title_placeholder' => 'slide caption',
						'description_placeholder' => 'link or video url',
					])
				</fieldset>
			@endif

			@include('content::widgets/slider-form')

			@include('content::widgets/single-slide-form')

			@include('content::widgets/content-selection-form')

			@if($model->type == 'gallery')
				<fieldset>
					<legend>Pictures</legend>
					<!-- <p style="font-size:1.3rem; color: #AAA">Use the title field for a slide caption and the description both for a link or a video url</p> -->
					@include('media::gallery', [
						'name'=>'gallery',
						'with_embeds'=>false,
						'gallery_items'=>$model->media_assorted()->sluged('gallery')->toJson(JSON_HEX_APOS | JSON_HEX_QUOT),
						'title_placeholder' => 'title',
						'description_placeholder' => 'description',
					])
				</fieldset>
			@endif


			@if($model->type == 'banner')
				<fieldset>
					<legend>Banner</legend>
					<?php
						$banner = $model->media_assorted()->one('banner');
					?>
					{{ Form::one('file', 'banner', 'Image', null, array('file'=>$banner)) }}
					{{ Form::one('text', 'banner_title', 'Link', $banner->title, ['class'=>'form-control required']) }}
					{{ Form::one('text', 'banner_description', 'Optional Text', $banner->description, ['class'=>'form-control']) }}
				</fieldset>
			@endif


			@if($model->type == 'form')
			<?php
				$json = '';
				if($model->data) {
					$json = json_decode($model->data);
				}
			?>
				@if($json)
					<input type="hidden" name="jsondata" value='{{ $model->data }}' />
					@foreach($json->inputs as $input)
					{{ Form::one($input->type, 'form-'.$input->name, $input->label, $input->value, ['class'=>'form-control']) }}

					@endforeach

				@endif

			@endif

		@endif


	</div>

	<footer class="form-footer">
		<button type="submit" class="btn btn-primary">Save</button>
		<a href="javascript:window.history.go(-1)" class="btn btn-default">Cancel</a>

	</footer>

{{ Form::close() }}

<!-- Modal -->
<div class="modal fade" id="slider-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" modal-title>Add Slide</h4>
			</div>
			<div class="modal-body " modal-body>
			<div class="form-edit-holder" style="box-shadow: none">
			<div class="form-fields">
			<form id="form-slide">
			{{ Form::one('file', 'slide_form_image', 'Image', null, ['class'=>'required']) }}
			{{ Form::one('textarea', 'slide_form_text', 'Text', null, ['class'=>'form-control required']) }}
			{{ Form::one('text', 'slide_form_button', 'Button Text', null, ['class'=>'form-control']) }}
			{{ Form::one('text', 'slide_form_link', 'Button Link', null, ['class'=>'form-control']) }}
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" modal-button-dismiss >Cancel</button>
				<button type="button" class="btn btn-primary" modal-button-ok >Ok</button>
			</div>
			</div>
			</div>
		</div>
	</div>
</div>


@stop

@section('footer-scripts')
@parent

<script type="text/javascript">

$(function(){

	//var $list = $('#slides-list');
	var $list = $('[droppable="true"]');

	if($list.length) {

		$('[remove-slide]').on('click', function(e) {
			e.preventDefault();
			$(this).parents('.slide').remove();
		});

		var current_target = false;
		var $current_drag = undefined;
		var $placeholder = $('<div draggable="true" id="slider-placeholder"></div>');


		$(document).on('mousedown', '[draggable]', function(e) {
			current_target = e.originalEvent.target;
		})

		$(document).on('dragstart', '[draggable]', function(e) {

			var $this = $(this);
			var handle = $('[data-action="item-move"]', $this)[0];

			if(handle.contains(current_target))
			{
				console.log('dragg!!')
				$current_drag = $this;
				//e.originalEvent.dataTransfer.setData('data','test');
				e.originalEvent.dataTransfer.setData('text/plain', 'handle');
			} else {
				e.preventDefault();
			}


		})

		$($list).on('drop', '#slider-placeholder', function(e){
			e.preventDefault();
			e.stopPropagation();
			var $target = $(e.originalEvent.target);
			if($target.attr('id') == 'slider-placeholder')
			{
				$current_drag.insertAfter($placeholder);
			}

		})
		$(document).on('drop', function(e){
			e.preventDefault();
			e.stopPropagation();
		})

		$($list).on('dragenter','#slider-placeholder', function(e) {
			$(this).addClass('hover');
		});
		$($list).on('dragleave','#slider-placeholder', function(e) {
			$(this).removeClass('hover');
		});


		$($list).on('dragenter','div.draggable', function(e) {
			e.preventDefault();
			e.stopPropagation();
			var $this = $(this);


			if($current_drag[0] === this) return;

			//if(e.originalEvent.target === $current_drag[0]) return;
			//console.log(e.originalEvent.target);

			var $target = $(e.originalEvent.target)
			//console.log($target);
			var cx = e.originalEvent.clientY
			var ox = $this.offset().top;
			var d = (cx - ox) - ($target.height() / 2);

			if(d < 0) {
				$placeholder.insertBefore($this);
			} else {
				$placeholder.insertAfter($this);
			}
		})
		$(document).on('dragleave',$list, function(e) {
			e.preventDefault();
			e.stopPropagation();

			//console.log('leave');
		})

		$(document).on('dragend',$list, function(e) {
			  e.preventDefault();
			  e.stopPropagation();
			  $placeholder.detach();
		})

		$(document).on('dragover',$list,function(e){
			e.preventDefault();
			e.stopPropagation();
		})

	}


	var $content_selection_list = $('#content-selection-list');
	if($content_selection_list.length) {
		var content_selection_list_limit = $content_selection_list.attr('limit');
		var $content_selection_select = $('#content-item-select');
		var $content_item_sample = $('#content-item-sample');


		function remove_content_item(text, id) {
			var $option = $('<option />');
			$option.attr('value', id);
			$option.text(text);
			console.log($option);
			$content_selection_select.append($option);

		}
		function add_content_item(text, id) {
			var $item = $content_item_sample.clone();
			$item.attr('id','');
			var $input = $('input', $item);
			$input.val(id);
			var $text = $('.text', $item);
			$text.text(text);
			$content_selection_list.append($item)

			$('option[value="'+id+'"]', $content_selection_select).remove();
		}

		$('#add-content-item').on('click', function(e) {
			e.preventDefault();
			var total_items = $content_selection_list.children().length;


			if(content_selection_list_limit && total_items >= content_selection_list_limit) {
				 return false;
			}

			var $option = $('option:selected', $content_selection_select);
			add_content_item($option.text(), $content_selection_select.val());

		});
		$(document).on('click', '[remove-content-item]', function(e) {
			e.preventDefault();
			var $item = $(this).parents('.content-selection');
			var $input = $('input', $item);
			var $text = $('.text', $item);
			remove_content_item($text.text(), $input.val());
			$item.remove();
		});

		var selected = $content_selection_list.attr('items');
		selected = JSON.parse(selected);
		for(var i=0; i < selected.length; i++) {
			var id = selected[i];
			var $option = $('option[value="'+id+'"]', $content_selection_select);
			if($option.length) {
				add_content_item($option.text(),id);
			}
		}

	}
})
</script>
@stop
