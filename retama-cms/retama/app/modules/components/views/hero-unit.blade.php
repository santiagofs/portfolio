<div class="hero-unit {{ $class or '' }}" style="background-image:url({{ $image or '' }})">

	<div class="width-control">

		<div class="content-control">

			@if(isset($hero_text))
			<div class="text-control">
				<h2>{{$hero_text}}</h2>
			</div>
			@endif

			@if(isset($cta))
			<?php
				if(isset($cta['class'])) {
					$cta['class'] .= ' cta-btn';
				} else {
					$cta['class'] = 'cta-btn';
				}
				$url = '#'; $content = '';
				if(isset($cta['url']))
				{
					$url = $cta['url'];
					unset($cta['url']);
				}
				if(isset($cta['content']))
				{
					$content = '<span class="cta-content">'.$cta['content'].'</span>';
					unset($cta['content']);
				}
				//if(isset())
				//$fancybox = isset($item->fancybox) ? 'data-fancybox="'.$item->fancybox.'" rel="'.$item->caption.'"' : '';

				$atts = HTML::attributes($cta);

			?>
			<div class="cta-control">
				<a href="{{ $url }}" {{ $atts }}>{{ $content }}</a>
			</div>
			@endif

		</div>

	</div>
</div>