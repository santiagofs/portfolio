<?php
if (  ! (($media instanceof Illuminate\Support\Collection) || is_array($media)) )
{
	$media = [$media];
}
?>
@if(count($media))
<?php
	isset($width) || $width = null;
	isset($height) || $height = null;
	isset($crop) || $crop = null;
	isset($attr) || $attr = [];
	isset($fancybox_name) || $fancybox_name = 'fancybox';
	if(isset($attr['class'])) {
		$attr['class'] .= ' mosaico-image';
	} else {
		$attr['class'] = ' mosaico-image';
	}
?>
<div class="mosaico gridable">
	@foreach($media as $item)
	<?php
		$url = $item->type == 'embed' ? $item->embed_url : $item->url();
	?>
	<a href="{{ $url }}" data-fancybox="{{ $fancybox_name }}" title="{{ $item->title }}" description="{{ $item->description }}" class="mosaico-item gridable-item">
		{{ $item->img($width, $height, $crop, $attr) }}
		<div class="mosaico-title">{{ $item->title }}</div>
	</a>
	@endforeach
</div>
@endif
