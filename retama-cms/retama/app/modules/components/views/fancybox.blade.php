<?php
if (  ! (($media instanceof Illuminate\Support\Collection) || is_array($media)) )
{
	$media = [$media];
}
$arr = [];
if(count($media)) {

	foreach($media as $item):
		if($item->type == 'image' || $item->type == 'embed'):
			$arr[] = $item;
		endif;
	endforeach;

}

?>
@if(count($arr))
<?php


	$first = array_shift($arr);
	isset($width) || $width = null;
	isset($height) || $height = null;
	isset($crop) || $crop = null;
	isset($attr) || $attr = [];
	isset($fancybox_name) || $fancybox_name = 'fancybox';
?>
<div class="fancybox-container">

	<a href="{{ $first->url() }}" data-fancybox="{{ $fancybox_name }}" title="{{ $first->title }}" description="{{ $first->description }}" rel="{{$fancybox_name}}">
		{{ $first->img($width, $height, $crop, $attr) }}
	</a>
	@foreach($arr as $item)
		<a href="{{ $item->url() }}" data-fancybox="{{ $fancybox_name }}" title="{{ $item->title }}" description="{{ $item->description }}"  rel="{{$fancybox_name}}"></a>
	@endforeach
</div>
@endif
