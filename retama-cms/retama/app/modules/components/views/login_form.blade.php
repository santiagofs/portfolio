<fieldset class="login-form">
@if(isset($legend))
	<legend>{{$legend}}</legend>
@endif

{{ Form::open(array('url' => $url)) }}
	<p class="form-group">
		{{ Form::text('username', isset($username) ? $username : '' , array('id'=>'username', 'class'=>'form-control', 'placeholder'=>'Username')) }}
	</p>

	<p class="form-group">
		{{ Form::password('password', array('id'=>'password', 'class'=>'form-control', 'placeholder'=>'Password')) }}
	</p>

@if(isset($message))
	<p class="form-group has-error">
		<label class="control-label">{{{ $message }}}</label>
	</p>
@endif

	<p class="form-actions">
		<label for="rememberme">{{ Form::checkbox('rememberme', 1, isset($rememberme) ? $rememberme : false, array('id'=>'rememberme')) }} Remember me</label>
		{{ Form::submit('Login', array('id'=>'form-submit', 'class'=> (isset($btn_class) ? $btn_class : 'btn btn-primary') .' pull-right')) }}

	</p>

@if(isset($forgot_password_url))
	<div class="forgot-password">
		<hr />
		<a href="{{ URL::to($forgot_password_url) }}" class="pull-right">Forgot password?</a>
	</div>
@endif

{{ Form::close() }}
</fieldset>