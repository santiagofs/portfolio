<div class="thumb-slider {{ $mode or 'no-thumbs' }}">

	<div class="thumb-slider-viewport">

		<ul class="bxslider">
		@foreach($items as $item)
<?php
		$fancybox = isset($item->fancybox) ? 'data-fancybox="'.$item->fancybox.'" rel="'.$item->caption.'"' : '';
?>
			<li>
			@if($item->link)
				<a href="{{ $item->link }}" {{ $fancybox }}>
			@endif
			<div class="image-control {{ $item->type or 'image' }}" style="background-image: url({{ $item->image }})" caption="{{ $item->caption }}">
				<img src="{{ $item->image }}" title="{{ $item->caption }}" />
			</div>
			@if($item->type == 'embed')
			<i class="fa fa-play video-icon"></i>
			@endif


			@if($item->link)
		  		</a>
			@endif
			</li>

		@endforeach
		</ul>
	</div>

	<ul class="thumbs-slider-pager" >
		<?php
			$i = 0;
		?>
		@foreach($items as $item)
		<li>
			<a data-slide-index="{{ $i }}">
				<div class="image-control" style="background-image: url({{ $item->thumb }})">
				</div>
			</a>
			@if($item->type == 'embed')
			<i class="fa fa-play video-icon"></i>
			@endif
		</li>
		<?php
			$i++;
		?>
		@endforeach
	</ul>


</div>