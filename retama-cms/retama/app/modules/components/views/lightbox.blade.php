<?php
if (  ! (($media instanceof Illuminate\Support\Collection) || is_array($media)) )
{
	$media = [$media];
}
?>
@if(count($media))
<?php
	$first = ($media instanceof Illuminate\Support\Collection) ? $media->shift() : array_shift($media);
	isset($width) || $width = null;
	isset($height) || $height = null;
	isset($crop) || $crop = null;
	isset($attr) || $attr = [];
	isset($lightbox_name) || $lightbox_name = 'lightbox';
?>
<div class="lightbox-container">

	<a href="{{ $first->url() }}" data-lightbox="{{ $lightbox_name }}" title="{{ $first->title }}" description="{{ $first->description }}">
		{{ $first->img($width, $height, $crop, $attr) }}
	</a>
	@foreach($media as $item)
		<a href="{{ $item->url() }}" data-lightbox="{{ $lightbox_name }}" title="{{ $item->title }}" description="{{ $item->description }}"></a>
	@endforeach
</div>
@endif
