<?php
isset($slides) || $slides = [];
isset($show_thumbs) || $show_thumbs = false;
isset($show_arrows) || $show_arrows = false;
?>
<section class="cd-hero hero-unit full">
	<ul class="cd-hero-slider">
	<?php
		$ndx = 0;
	?>
	@foreach($slides as $slide)
	<?php
		$ndx++;
	?>

		<li class="slide-{{$ndx}} <?php echo($ndx==1?'selected':''); ?>" style="background-image:url({{$slide->img->url()}})">
			@if($slide->link && !$slide->button)
			<a href="{{$slide->link}}" data-fancybox="cd-hero">
			@endif

			<div class="width-control">

				<div class="content-control">

					<div class="text-control">
					@if($slide->text)
					<h2>{{ nl2br($slide->text) }}</h2>
					@endif

					@if(isset($slide->description))
					<p><!-- description here --></p>
					@endif
					</div>
					<div class="cta-control">
					@if($slide->button)
					<a href="{{$slide->link}}" data-fancybox="cd-hero" class=" cta-btn"><span class="cta-content">{{$slide->button}}</span></a>
					@endif
					</div>

				</div>
			</div> <!-- .cd-full-width -->

			@if($slide->link && !$slide->button)
			</a>
			@endif
		</li>
	@endforeach


	</ul> <!-- .cd-hero-slider -->
@if($show_arrows && count($slides) > 1 )
<div class="cd-slider-arrows">
	<a href="#" class="cd-slider-arrow previous"><span class="fa fa-angle-left">
	<a href="#" class="cd-slider-arrow next"><span class="fa fa-angle-right">
</div>
@endif

 @if($show_thumbs)
	<div class="cd-slider-nav">
		<nav>
			<span class="cd-marker item-1"></span>

			<ul>
				<li class="selected"><a href="#0">Intro</a></li>
				<li><a href="#0">Tech 1</a></li>

			</ul>
		</nav>
	</div> <!-- .cd-slider-nav -->
@endif
</section> <!-- .cd-hero -->