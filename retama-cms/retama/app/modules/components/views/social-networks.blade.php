<div class="social-networks {{ $class or '' }}">
@foreach($networks as $network)
	@if(isset($links[$network]))
	<a href="{{ $links[$network] }}" target="_blank" class="social-link fa fa-{{$network}}"></a>
	@endif
@endforeach
</div>