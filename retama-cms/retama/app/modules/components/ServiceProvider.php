<?php
namespace Modules\Components;

class ServiceProvider extends \Modules\ServiceProvider {

    public function register()
    {
        parent::register('components');
    }

    public function boot()
    {
        parent::boot('components');
    }

}