<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CronJob extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cronjob:run';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Cron job tasks.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}


	public function fire()
	{
		$hour = date('H');
        $minute = date('i');

        $today = date('Y-m-d 00:00:00');

        $logs = \Modules\Retama\Models\Log::with('user')
        	->with('logable')
        	->orderBy('created_at', 'desc')
			->where('created_at', '>', $today)
        	->get();

		if(!count($logs)) {
			$this->info('No logs for today.');
			$this->info('Completed.');
			return false;
		}


		//$email = \Config::get('site::settings.site.main_email');
		//if(!$email) return \Response::json(array('success'=>false, 'message'=>'Error inesperado'), 400);
		$email = 'santiagofs@gmail.com';

		$status = Mail::send('retama::logs',  array('logs'=>$logs), function($m) use ($email, $logs)
		{
			$m->from('denisse.sciamarella@limsi.fr', 'Red Raices');
		    $m->to($email, 'Admin')->subject('Reporte diario');
		});



        $this->info('Completed.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
