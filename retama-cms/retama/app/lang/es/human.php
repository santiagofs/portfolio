<?php

return [
	'day' => 'día|días',
	'week' => 'semana|semanas',
	'month' => 'mes|meses',
	'year' => 'año|años',
	'hour' => 'hora|horas',
	'minute' => 'minuto|minutos',
	'second' => 'segundo|segundos',
	'from_now' => 'dentro de :txt',
	'ago' => 'hace :txt',
	'after' => ':txt despues',
	'before' => ':txt antes',
];