<?php
namespace eFS;

class Json {

	protected $data = null;

	public static function from_array($arr)
	{
		return new static(json_encode($arr));
	}

	public function __construct($data = null)
	{
		if($data) $this->data = (array) json_decode($data); // usamos array para poder usar dot notation
	}


	public function __get($key)
	{
		return array_get($this->data, $key);
	}

	public function __set($key, $value) {
		array_set($this->data, $key, $value);
	}

	public function __toString()
	{
		return json_encode($this->data);
	}
}