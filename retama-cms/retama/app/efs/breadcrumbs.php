<?php
namespace eFS;

class Breadcrumbs {

	protected static $breadcrumbs = array();

	public static function reset()
	{
		static::$breadcrumbs = array();
	}

	public static function add($label, $url = null)
	{
		static::$breadcrumbs[] = (object)['label'=>$label, 'url'=>$url];
	}

	public static function get()
	{
		return static::$breadcrumbs;
	}

	public static function render($crumb = '<li><a href="{{url}}">{{label}}</a></li>', $last = '<li>{{label}}</li>')
	{
		$ret = '';
		for($i=0; $i < count(static::$breadcrumbs); $i++)
		{
			$breadcrumb = static::$breadcrumbs[$i];
			$str = $i == (count(static::$breadcrumbs)-1) ? $last : $crumb;
			$ret.= str_replace('{{url}}', $breadcrumb->url, str_replace('{{label}}', $breadcrumb->label, $str));
		}

		return $ret;
	}
}