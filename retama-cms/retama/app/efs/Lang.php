<?php
namespace eFS;

class Lang extends \Illuminate\Support\Facades\Lang {

	public static function get($key, array $replace = array(), $locale = null)
	{


		list($namespace, $group, $item) = parent::parseKey($key);

		if($namespace == 'site')
		{
			$arr = \Modules\International\Models\UI::get_lang_array($namespace, $group, \Language::getDefault()->laravel_prefix);
			if( $item && (!array_key_exists($item, $arr)) ) {
				$arr[$item] = ucfirst(str_replace('_', ' ', $item));

				\Modules\International\Models\UI::save_lang_file($arr, $namespace, $group, \Language::getDefault()->laravel_prefix);
			}
		}

		return str_replace("'", "’", parent::get($key, $replace, $locale));


	}
}