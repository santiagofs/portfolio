<?php
namespace eFS;

class Human {


 	public static function diff(\Carbon\Carbon $date, \Carbon\Carbon $other = null)
    {
         $isNow = $other === null;

        if ($isNow) {
            $other = $date::now($date->tz);
        }

        $isFuture = $date->gt($other);

        $delta = $other->diffInSeconds($date);

        // a little weeks per month, 365 days per year... good enough!!
        $divs = array(
            'second' => $date::SECONDS_PER_MINUTE,
            'minute' => $date::MINUTES_PER_HOUR,
            'hour' => $date::HOURS_PER_DAY,
            'day' => $date::DAYS_PER_WEEK,
            'week' => 30 / $date::DAYS_PER_WEEK,
            'month' => $date::MONTHS_PER_YEAR
        );

        $unit = 'year';

        foreach ($divs as $divUnit => $divValue) {
            if ($delta < $divValue) {
                $unit = $divUnit;
                break;
            }

            $delta = $delta / $divValue;
        }

        $delta = (int) $delta;

        if ($delta == 0) {
            $delta = 1;
        }

        $txt = $delta . ' ' . \Lang::choice('human.'.$unit, $delta);
        //$txt .= $delta == 1 ? '' : 's';

        if ($isNow) {
            if ($isFuture) {
                return \Lang::get('human.from_now', ['txt'=>$txt]);
            }

            return \Lang::get('human.ago', ['txt'=>$txt]);
        }

        if ($isFuture) {
            return \Lang::get('human.after', ['txt'=>$txt]);
        }

        return \Lang::get('human.before', ['txt'=>$txt]);
    }

}