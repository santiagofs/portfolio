<?php
namespace eFS\Forms;

use Illuminate\Support\Facades\Facade as IlluminateFacade;

class FormBuilderFacade extends IlluminateFacade {

    protected static function getFacadeAccessor() { return 'formbuilder'; }

}