<?php
	$filename = isset($file) ? $file->original_name : '';
	$id = isset($file) ? $file->id : '';
	
?>
<div class="form-group file {{ $required }}">

	{{ Form::label($name, $label )}}
	<div class="form-control-holder">
		<div class="input-group" style="max-width:300px;">
		<div class="name-display form-control" id="{{ $name }}_display">{{ $filename }}</div><a href="#" class="input-group-addon remove-addon" remove-file="{{$name}}"><span class="fa fa-times"></span></a>
		</div>
		<input type="hidden" name="{{ $name }}" id="{{ $name }}_file" value="" />
		<input type="hidden" name="{{ $name }}_status" id="{{ $name }}_status" value="old" />
	@if ($errors->has($name))
		<label id="id-{{$name}}-error" class="error" for="id-{{$name}}">{{ $errors->first($name) }}</label>
	@endif
	{{ Form::one('uploader', $name, null, null, $uploader_atts) }}
	</div>

</div>

<script type="text/javascript">
	
	
	
	function {{ $name }}_uploader_callback(file)
	{
		var $display = $('#{{ $name }}_display');
		var $input = $('#{{ $name }}_file');
		
		var data = {status:'new', url:'/media/temp/'+file.name, id:0, name: file.name}
		var farray = file.name.split('.');
		var ext = farray[farray.length-1].toLowerCase();

		switch(ext)
		{
			case 'gif':
			case 'png':
			case 'jpg':
			case 'jpeg':
				data.type = 'image';
				break;
			case 'flv':
			case 'f4v':
			case 'mp4':
				data.type = 'video';
				break;
			case 'swf':
				data.type = 'flash';
				break;
			default:
				data.type = 'document';
		}

		$input.val(file.name);
		$display.html(file.name);
		
		
		$('[remove-file="{{$name}}"]').on('click', function(e) {
			e.preventDefault();
			$input.val('');
			$display.html('');
		});
	}
	
	



</script>