<div class="uploader" id="id-{{ $name }}-uploader" uploader-name="{{ $name }}" uploader-url="{{ \URL::to('/media/upload', false)}}" second-button="{{ $config['second_button_id'] }}" extensions="{{ $config['extensions'] }}">
	<ul id="{{ $name }}_uploader_queue" class="upload-queue"></ul>

	{{$input}}

	<a class="btn btn-default" id="{{ $name }}_uploader_select"><span><i class="fa fa-upload"></i> {{ $config['button_text'] }}</span></a>

	{{ $config['second_button'] or '' }}


</div>