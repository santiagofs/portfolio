<?php
	$checked = $value ? 'checked' : '';
?>
<div class="form-group">
	<div class="form-control-holder">
	{{ $input }}
	<label > <a toggle-checkbox="#{{$attributes['id']}}" class="checkbox {{$checked}}">{{$label}}</a></label>

	@if ($errors->has($name)) <label id="id-{{$name}}-error" class="error" for="id-{{$name}}">{{ $errors->first($name) }}</label> @endif
	</div>
</div>