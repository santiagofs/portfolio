<div class="form-group">
	{{ Form::label($name, $label )}}
	<div class="form-control-holder">

		<ul class="multiselect-checkbox">
		@foreach($all_values as $key => $display)
		<?php
			$checked = in_array($key, $selected) ? 'checked="checked"' : '';
		?>
			<li>
				<label><input type="checkbox" name="{{ $name }}[]" value="{{ $key }}" {{ $checked }}>{{ $display }}</label>
			</li>
		@endforeach

		</ul>

	</div>
</div>