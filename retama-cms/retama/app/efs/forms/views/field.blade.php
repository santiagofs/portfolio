<div class="form-group {{ $required }}">
	{{ Form::label($name, $label )}}
	@if(isset($help))
		<p class="help" style="margin-bottom: 5px">{{ $help }}</p>
	@endif
	<div class="form-control-holder">
	{{ $input }}
	@if ($errors->has($name))
		<label id="id-{{$name}}-error" class="error" for="id-{{$name}}">{{ $errors->first($name) }}</label>
	@endif


	</div>
</div>