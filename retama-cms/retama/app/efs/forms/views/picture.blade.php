<input type="hidden" name="<?php echo($name); ?>" id="<?php echo($name); ?>_input" value="<?php echo($media->path()); ?>" />
<div class="picture-holder" id="<?php echo($name); ?>_picture_holder">
<?php
	if($media->id)
	{
?>
		<a href="<?php echo($media->original()); ?>" class="clearbox"><?php echo($media->get_img(190,143,'4x3')); ?></a>
<?php
	}
?>
</div>

<?php echo($uploader); ?>

<script type="text/javascript">


	function <?php echo($name) ?>_uploader_callback(file)
	{
		var $img_holder = $('#<?php echo($name); ?>_picture_holder');
		$img_holder.html('');
		$img_holder.addClass('loading');

		data = {status:'new', path:'files/temp/'+file.name, id:0, filename: file.name}
		var farray = file.name.split('.');
		var ext = farray[farray.length-1].toLowerCase();

		switch(ext)
		{
			case 'gif':
			case 'png':
			case 'jpg':
			case 'jpeg':
				data.type = 'image';
				break;
			default:
				alert('Invalid file type');
				return false;
		}

		var $img = $('<img class="thumb" />');
		$img.on('load',function()
		{
			setTimeout(function(){
				$('#<?php echo($name); ?>_picture_holder').removeClass('loading');
				$('#<?php echo($name); ?>_picture_holder').html($img);
				/*
$img.width(160);
				$img.height(120);
*/
			}, 2000)

		})
		$img.attr('src', data.url+'?width=160&height=120');

		$('#<?php echo($name); ?>_input').val(data.path);

	}

	function <?php echo($name) ?>_uploader_second_callback(e)
	{
		e.preventDefault();
		$('#<?php echo($name); ?>_input').val('');
		$('#<?php echo($name); ?>_picture_holder').html('');
	}

	<?php echo($name); ?>_uploader.on_files_selected = function()
	{
		$('#<?php echo($name); ?>_input').val('');
		$('#<?php echo($name); ?>_picture_holder').html('');
		$('#<?php echo($name); ?>_picture_holder').addClass('loading');
	}

	<?php echo($name); ?>_onSelect =  function(e){
		$('#<?php echo($name); ?>_input').val('');
		$('#<?php echo($name); ?>_picture_holder').html('');
		$('#<?php echo($name); ?>_picture_holder').addClass('loading');
	};
	<?php echo($name); ?>_onUploadError = function(error, message)
	{
		$('#<?php echo($name); ?>_input').val('');
		$('#<?php echo($name); ?>_picture_holder').html('');
		$('#<?php echo($name); ?>_picture_holder').removeClass('loading');
		alert(message);
	}


</script>