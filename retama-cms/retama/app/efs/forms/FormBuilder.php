<?php
namespace eFS\Forms;
use  \eFS\Forms\InputBuilder as InputBuilder;

class FormBuilder extends \Illuminate\Html\FormBuilder{

	protected static $input_envelop = '<div class="form-group">{label}{input}{errors}</div>';

	public function render($field = null)
	{
		$this->inputs[$inputname] = InputBuilder::forge($classname,$inputname,$value,$label);
		return $this->inputs[$inputname];
	}


	public function test()
	{
		echo \View::make('forms::test'); die();
	}


	public function one ($input_class, $name, $label = null, $value = null, $attributes=array(), $envelope = null)
	{
		if(!array_key_exists('id', $attributes)) $attributes['id'] = 'id-'.$name;
		
		$required = false;
		if(isset($attributes['class']))
		{
			$required = (strpos($attributes['class'], 'required') !== false) ? 'required' : '';
		}
		if(isset($attributes['help']))
		{
			$help = $attributes['help'];
			unset($attributes['help']);
		}

		$view = 'forms::field';
		switch($input_class)
		{
			case 'summernote':
				$attributes['summernote'] = true;
				$input = \Form::textarea($name, $value, $attributes);
				break;
			case 'tinymce':
				$attributes['tinymce'] = true;
				$input = \Form::textarea($name, $value, $attributes);
				break;
			case 'checkbox':
				$view = 'forms::checkbox';
				$input = \Form::hidden($name, $value, $attributes);
				break;

			case 'password':
				$input = \Form::password($name, $attributes);
				break;

			case 'mutiselect':
				$input = '';
				$all_values = $attributes['list'];
				$selected =  $attributes['selected_list'];
				unset($attributes['list']);
				unset($attributes['selected_list']);
				if(isset($attributes['view']))
				{
					$view = $attributes['view'];
					unset($attributes['view']);

				} else {
					$view = 'forms::multiselect-checkbox';
				}

				$selected || $selected = [];
				$all_values || $all_values = [];

				break;

			case 'uploader':
				$view = 'forms::uploader';
				$attributes['id'] = $name.'_uploader';
				$config = array(
					'button_text' => 'Browse files',
					'second_button' => null,
					'second_button_id' => null
				);

				isset($attributes['multiple']) OR $attributes['multiple'] = true;
				if($attributes['multiple'] === false) unset($attributes['multiple']);

				isset($attributes['accept']) OR $attributes['accept'] = '*';
				
				if(isset($attributes['button_text']))
				{
					$config['button_text'] = $attributes['button_text'];
					unset($attributes['button_text']);
				}

				if(isset($attributes['second_button']))
				{
					if(isset($attributes['second_button_attributes']))
					{
						$attrs = $attributes['second_button_attributes'];
						unset($attributes['second_button_attributes']);
					} else {
						$attrs = [];
					}

					$attrs['id'] = $name.'_second_button';
					$config['second_button_id'] = $attrs['id'];

					$config['second_button'] = \HTML::link('#', $attributes['second_button'], $attrs);

					unset($attributes['second_button']);
				}
				if(isset($attributes['extensions']))
				{
					$config['extensions'] = $attributes['extensions'];
					unset($attributes['extensions']);
				} else {
					$config['extensions'] = '';
				}

				$attributes['style'] = 'display:none';

				$input = \Form::file($name.'_uploader', $attributes);
				break;

			case 'select':
			
				$list = $attributes['list'];
				unset($attributes['list']);
				$input = \Form::select($name, $list, $value, $attributes);

				break;

			case 'file':
				
				if(isset($attributes['file']))
				{
					$file = $attributes['file'];
					unset($attributes['file']);
				}
				
				if(isset($attributes['uploader'])) {
					$uploader_atts = $attributes['uploader'];
					unset($attributes['uploader']);
				} else {
					$uploader_atts = [];
				}
				
				$view = 'forms::file';
				$input = '';

				break;



			default:
				$input = \Form::$input_class($name, $value, $attributes);
		}

		$envelope || $envelope = $view;

		$value = \Form::getValueAttribute($name, $value);
		echo \View::make($envelope, get_defined_vars());
	}



	public function getModel()
	{
		return $this->model;
	}

	public function date($name, $value = null, $options = array())
	{
		return $this->input('date', $name, $value, $options);
	}
}