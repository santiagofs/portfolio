<?php

namespace eFS\Forms\Inputs;

class Text extends \eFS\Forms\InputBuilder {

	function get_input() {

		isset($this->attributes['autocomplete']) OR $this->attributes['autocomplete'] = 'off';
		return \Html::input($this->name, $this->value, $this->attributes);
	}

}
