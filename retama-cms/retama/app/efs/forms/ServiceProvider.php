<?php
namespace eFS\Forms;
// this is a comment

class ServiceProvider extends \eFS\ServiceProvider {

    public function register()
    {
        parent::register('formbuilder');

        $this->app->bindShared('formbuilder', function($app)
        {
            $form = new FormBuilder($app['html'], $app['url'], $app['session.store']->getToken());

            return $form->setSessionStore($app['session.store']);
        });
    }

    public function boot()
    {
        parent::boot('forms');
    }

	public function provides()
	{
		 return array('formbuilder');
	}
}