<?php
namespace eFS\Forms;

class InputBuilder {

protected $envelope = '<div class="group">
		<label>{label}{get_description}</label>
		{get_input}
		<small></small>
	</div>';

	protected $no_label_envelope ='<div class="group"><span class="simil-input">{get_input}</span></div>';

	protected $errors = NULL;
	protected $description = '';
	protected $rules = '';
	protected $attributes = array();
	protected $config = array();

	public $template = NULL;

	protected static $meta_loaded = false;

	public static function forge($classname, $inputname, $value='', $label='')
	{
		$classname = '\\eFS\\Forms\\Inputs\\'.$classname;

		$input = new $classname();
		$input->name = $inputname;
		$input->label = $label ? $label : $inputname;
		$input->attributes['id'] = 'npt_'.$inputname;

		if(is_object($value)) {

			if( (is_subclass_of($value,'Base_model')) AND (property_exists($value->row_data(),$inputname)) )
			{
				$value  = $value->{$inputname};
			}
			//$value = !is_null($value->{$inputname}) ? $value->{$inputname} : $value;
		}

		(is_array($value) AND isset($value[$inputname])) AND $value = $value[$inputname];
		$input->value = $value;

		$input->get_meta();
		return $input;
	}

	function envelope($envelope='')
	{
		$this->envelope = $envelope;
		return $this;
	}

	function get_input()
	{
		return '<div class="display">'.$this->value.'</div><input type="hidden" name="'.$this->name.'" value="'.$this->value.'" />';
	}

	public function get_description()
	{
		return $this->description ? '<a class="tooltip-help" title="'.$this->description.'"></a>' : '';
	}

	function get_meta()
	{
		return '';
	}

	function get_css()
	{
		return '';
	}

	function get_js()
	{
		return '';
	}

	function __toString()
	{

		switch(get_class($this))
		{
			case 'Forms\Inputs\Checkbox':
				$envelope = $this->no_label_envelope;
				break;
			default:
				$envelope = $this->envelope;
		}

		$envelope = $this->envelope;
		$html = \Envelope::parse($envelope,$this);
		return $html;
	}

	public function rules($rules)
	{
		$this->rules = $rules;

		(! isset($this->attributes['class'])) AND $this->attributes['class'] = '';

		$class = $this->attributes['class'] . ' ' . str_replace('|',' ', $rules);
		$this->attributes['class'] = trim($class);

		$this->rules = $rules;
	}

	public function config($mixed, $value = null)
	{
		if(is_array($mixed))
		{
			foreach($mixed as $key => $value)
			{
				$this->config($key,$value);
			}
			return $this;
		}
		$this->config[$mixed] = $value;

		return $this; // chainable
	}

	public function attribute($mixed,$value)
	{
		if(is_array($mixed))
		{
			foreach($mixed as $key => $value)
			{
				return $this->attribute($key,$value);
			}
		}

		if($mixed == 'class') {
			isset($this->attributes['class']) ? $this->attributes['class'] .= ' ' . $value : $this->attributes['class'] = $value;
		}
		else
		{
			$this->attributes[$mixed] = $value;
		}

		return $this; // chainable
	}

}