<?php
namespace eFS;

class Tab
{
	protected static $tabs = array();
	public $name = '';
	public $view = null;
	public $data = array();

	public static function reset()
	{
		static::$tabs = array();
	}

	public function __construct($name, $view, $data = array())
	{
		$this->name = $name;
		$this->view = $view;
		$this->data = $data;
		$this->id = 'tab-'.(count(static::$tabs)+1);
		static::$tabs[$name] = $this;
		return static::$tabs[$name];
	}

	public static function render_tabs($tab_envelope, $container_envelope = null)
	{
		$html = '';
		if(count(static::$tabs )<=1 ) return $html;

		foreach(static::$tabs as $tab)
		{
			$html .= \Envelope::parse($tab_envelope, $tab);
		}
		$container_envelope AND $html = \Envelope::parse($container_envelope, array('tabs'=>$html));
		echo($html);
	}

	public static function render_contents($content_envelope='<div id="{id}" class="tab-content">{tab}</div>')
	{
		$html = '';
		foreach(static::$tabs as $tab)
		{
			//$partial = \ci::load()->view($tab->view,$tab->data,true);
			$partial = \View::make($tab->view, $tab->data);
			$html .= \Envelope::parse($content_envelope, array('tab'=>$partial,'id'=> $tab->id));
		}
		echo($html);
	}

	public static function render($content_envelope, $tab_envelope, $container_envelope = null)
	{
		static::render_tabs($tab_envelope, $container_envelope);
		static::render_contents($content_envelope);
	}


}