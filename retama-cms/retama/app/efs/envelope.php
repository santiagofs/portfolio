<?php
namespace eFS;

class Envelope {

	public static function parse($envelope, $object = null)
	{
		$vars = static::extract($envelope);
		$properties = $vars[1];
		$replacements = $vars[0];
		is_array($object) AND $object = (object) $object;

		$ret = $envelope;

		foreach($properties as $key => $property)
		{
			$value = '';

			if(method_exists($object,$property)) {
				$value = $object->$property();
			}
			elseif(property_exists($object,$property)) {
				$test = $object->$property;
				if(is_array($test)) {
					foreach($test as $item)
					{
						$value .= $item;
					}
				}
				else
				{
					$value = $test;
				}
			}
			else
			{
				$value = @$object->{$property};
			}
			$ret = str_replace($replacements[$key], $value, $ret);
		}
		return $ret;
	}

	public static function extract($envelope)
	{
		$pattern = "/{([^}]*)}/";
		$matches = array();
		preg_match_all($pattern, $envelope, $matches);
		return $matches;
	}
}
