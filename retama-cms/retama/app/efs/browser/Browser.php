<?php
/* si se actualiza la clase mobileDetect, recordar sacar el _ del nombre, tanto en la clase como en el archivo */
namespace eFS\Browser;

class Browser extends \hisorange\BrowserDetect\Facade\Parser {
/*
    protected static function getFacadeAccessor() { return '\eFS\Browser\MobileDetect'; }


	public static function is_desktop()
	{
		$detect = new \eFS\Browser\MobileDetect;
		return  !(boolean)($detect->is_mobile() || $detect->is_tablet());
	}
*/

	public static function device()
	{
		if( static::isDesktop()) return 'device-desktop';

		if( static::isMobile()) return 'device-mobile';

		if( static::isTablet()) return 'device-tablet';

		if( static::isBot()) return 'device-bot';
	}
}