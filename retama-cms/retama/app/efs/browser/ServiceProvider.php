<?php
namespace eFS\Browser;

class ServiceProvider extends \eFS\ServiceProvider {

    public function register()
    {
        parent::register('browser');
    }

    public function boot()
    {
        parent::boot('browser');
    }

}