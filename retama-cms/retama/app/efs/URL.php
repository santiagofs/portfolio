<?php
namespace eFS;

class URL extends \Illuminate\Support\Facades\URL {

	public static function to($url, $translate = true, $parameters=array(), $secure=false)
	{
		if(!$translate) return parent::to($url, $parameters, $secure);

		$prefix = \Language::url_prefix();
		if($prefix) $prefix.='/';
		return parent::to($prefix.$url, $parameters, $secure);
	}

	public static function translate($url, $language = null)
	{
		if($language)
		{
			$prefix = $language; // agregar codigo si $language es \Language model
		}
		else
		{
			$prefix = \Language::url_prefix();
		}

		if($prefix) {
			if($url == '/')
			{
				$url = $prefix;
			} else {
				$url = $prefix.'/'.$url;
			}

		}
		return $url;
	}
}