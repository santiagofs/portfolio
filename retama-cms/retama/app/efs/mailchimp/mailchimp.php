<?php
namespace eFS\Mailchimp;

class Mailchimp {

	// grab an API Key from http://admin.mailchimp.com/account/api/

	// grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
	// Click the "settings" link for the list - the Unique Id is at the bottom of that page.

	public static function suscribe($email, $list_id, $apikey)
	{
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $email)) {
			return  "Please provide a valid email address";
		}

		$api = new \eFS\Mailchimp\MCAPI($apikey);


		return $api->listSubscribe($list_id, $email, '') === true ? true : $api->errorMessage;
	}

	public static function suscribe_form($name, $list_id, $texts = [])
	{
		// texts: success, placeholder, submit
		//var_dump($texts); die();
		$data = [
			'name'=>$name,
			'list_id' => $list_id,
			'placeholder' => isset($texts['placeholder']) ? $texts['placeholder'] : '',
			'submit' => isset($texts['submit']) ? $texts['submit'] : '',
			'success' => isset($texts['success']) ? $texts['success'] : '',
		];

		return \View::make('mailchimp::mailchimp-form', $data);
	}
}