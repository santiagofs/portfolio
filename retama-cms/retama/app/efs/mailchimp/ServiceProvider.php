<?php
namespace eFS\Mailchimp;

class ServiceProvider extends \eFS\ServiceProvider {

    public function register()
    {
        parent::register('mailchimp');
    }

    public function boot()
    {
        parent::boot('mailchimp');
    }

}