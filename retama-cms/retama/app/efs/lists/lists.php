<?php
namespace eFS\Lists;

use \View as View;

class Lists {

	public $model_class_name = '';
	public $model = null;

	public $args = array();
	public $base_url = '';

	public $field_names = array();
	public $field_labels = array();
	public $main_fields = array('');

	protected $sort_field = null;
	protected $page = 1;
	public $per_page = 20;

	protected static $instance = null;

	public $sortable = false; // replace with the sort url ?
	public $selectable = true;
	public $editable = true;
	public $deletable = true;
	public $creatable = true;

	protected $filter_form = null;
	protected $wheres = array();
	protected $joins = array();
	protected $relations = array();
	protected $filters = array();

	protected $records = null;


	public function __construct($model_class_name)
	{
		$this->model_class_name = $model_class_name;
		$this->args = \Input::all();

		if(isset($this->args['sort']))
		{
			$this->sort_field = $this->args['sort'];
			unset($this->args['sort']);
		}

		if(isset($this->args['page']))
		{
			$this->page = $this->args['page'];
			unset($this->args['page']);
		}

		if(isset($this->args['per_page']))
		{
			$this->page = $this->args['per_page'];
			unset($this->args['per_page']);
		}

		$this->model = new $model_class_name;
	}

	public function __call($method, $arguments)
	{
		// if method called belongs to the db class...
		try
		{
			$ret =  call_user_func_array( array($this->model, $method), $arguments);
			return ( (is_object($ret) AND get_class($ret) == get_class($this->model)) ? $this : $ret);
		}
		catch(\Exception $e)
		{
			throw $e;
		}
	}

	public function __get($property)
	{
		try
		{
			$ret =  $this->model->$property;
		}
		catch(\Exception $e)
		{
			throw $e;
		}
	}


	public function fields($fields, $main = null)
	{
		// reset the field_name array
		$this->field_names = array();

		foreach($fields as $field_name => $field_label)
		{
			$this->field_names[] = $field_name;
			$this->field_labels[$field_name] = $field_label;
		}
		if($main)
		{
			$this->main_fields = is_array($main) ? $main : array($main);
		}
		else
		{
			$this->main_fields = array();
			$this->main_fields[] = $this->field_names[0];
		}

		$this->default_sort($this->main_fields[0]);

		return $this;
	}

	public function add_filter($input, $field, $label, $mode = 'like', $alias = null)
	{

		$filter = (object)['input'=>$input, 'field'=>$field, 'label'=>$label, 'mode'=>$mode, 'alias'=>($alias ?: $field)];
		$this->filters[$field] = $filter;
		return $filter;
	}
	public function has_filters()
	{
		return (count($this->filters) > 0);
	}

	public function get_filters($filter = null)
	{
		if($filter) return $this->filters($filter);

		return $this->filters;
	}

	public function wheres(\Closure $function)
	{
		$this->wheres[] = $function;
	}

	public function joins($table, $from, $to)
	{
		$this->joins[] = (object)['table'=>$table, 'from'=> $from, 'to'=> $to];
	}
	public function relations($relation)
	{
		$this->relations[] = $relation;
	}

	public function default_sort($sort_field='sort')
	{
		isset($this->sort_field) || $this->sort_field = $sort_field;
	}
	public function set_sort($sort_field='sort') {
		$this->sort_field = $sort_field;
	}

	protected $select = null;
	public function select($select)
	{
		$this->select = $select;
		return $this;
	}

	protected $relateds = [];
	public function related($relateds)
	{
		if(is_array($relateds)) {
			foreach($relateds as $related)
			{
				$this->related($related);
			}
		}

		$this->relateds[] = $relateds;
		return $this;
	}


	protected function do_forge($force_per_page = null)
	{
		//($per_page = $force_per_page) OR ($per_page = \Session::get('records_per_page',20));
		$per_page = $force_per_page ? $force_per_page : $this->per_page;

		//$model = $model::where('1 = 1');
		$classname = $this->model_class_name;
		list($sortfield, $sortmode) = $this->get_sort();


		if(strpos($sortfield, '.') !== false) {
			$arr = explode('.', $sortfield);
			$related = $arr[0];
			$model = $classname::with([ $related => function ($query) use($arr, $sortmode) {
		    	$query->orderBy($arr[1], $sortmode);
		    }]);
		} else {
			$model = $classname::orderBy($sortfield, $sortmode == '-' ? 'DESC' : 'ASC');
		}

/*
		$test = $model->get();
		$queries = \DB::getQueryLog();
		$last_query = end($queries);
		var_dump($queries);
		die();
*/
		//$model = $classname::where('1','=','1');

		foreach($this->wheres as $where)
		{
			$model->where($where);
			//echo('where!'); die();
			//$model->where($where);
		}

		foreach($this->relateds as $related)
		{
			$model->with($related);
		}

		foreach($this->joins as $join)
		{
			$model->join($join->table, $join->from, '=', $join->to);
		}

		if($this->select)
		{
			$model->select($this->select);
		}

		foreach(\Input::all() as $key => $value)
		{
			if(isset($this->filters[$key]) AND $value !== '')
			{
				$filter = $this->filters[$key];
				if($filter->mode == 'like') $value = '%'.$value.'%';

				if(strpos($filter->alias, '.') !== false) {

					$arr = explode('.', $filter->alias);
					$alias = $arr[1]; $mode = $filter->mode;

					//$model->join($arr[0])
					/*
$model->with(array($arr[0]=> function($query) use ($alias, $mode, $value) {
						return $query->where($alias, $mode, $value);
					}));
*/
				} else {
					$model->where($filter->alias, $filter->mode, $value);
				}

			}
		}

		return $model;

	}
	public function forge($force_per_page = null)
	{
		$model = $this->do_forge($force_per_page);
		//($per_page = $force_per_page) OR ($per_page = \Session::get('records_per_page',20));
		$per_page = $force_per_page ? $force_per_page : $this->per_page;

		//$this->records = $model->get();

		$this->records = $model->paginate($per_page);


		$queries = \DB::getQueryLog();
		$last_query = end($queries);
		//var_dump($queries);
		//dd($queries);



		\Retama::save_list_url();

		return $this;
	}

	public function export()
	{
		$model = $this->do_forge(1000000);
		// $this->forge(1000000);
		$records = $model->get();
		//var_dump($records);die();

		\Excel::create('Suscribers', function($excel) use($records) {

		    $excel->sheet('Suscribers', function($sheet) use($records) {

		        $sheet->fromArray($records);

		    });
		})->download('xlsx');
	}



	public function records()
	{
		if(!$this->records)
		{
			$this->forge();
		}

		return $this->records;
	}

	public function render_actions()
	{
		return \View::make('retama::lists.actions',array('instance', $this));
	}

	public function column_count()
	{
		$count = count($this->field_names);
		if($this->sortable) $count++;
		if($this->editable || $this->deletable) $count++;
		$count++; // checkbox;

		return $count;
	}

	public function get_sort()
	{
		if($this->sort_field[0] == '-')
		{
			$mode = '-';
			$fieldname = substr($this->sort_field, 1);
		}
		else
		{
			$mode = '+';
			$fieldname = $this->sort_field;
		}

		return array($fieldname, $mode);
	}

	public function get_sort_url($fieldname)
	{
		$query_string = '';
		foreach($this->args as $key => $value)
		{
			$query_string .= $key.'='.$value;
		}

		$query_string .= 'sort='. $fieldname;

		return \URL::current().'?'.$query_string;
	}

	public function get_edit_url($id=0)
	{
		$controller = \Str::parseCallback(\Route::currentRouteAction(), null)[0];

		return \URL::action($controller.'@getEdit', [$id]);
	}

	public function get_delete_url($id = 0)
	{
		$controller = \Str::parseCallback(\Route::currentRouteAction(), null)[0];

		return \URL::action($controller.'@postDelete', [$id]);
	}

	public function get_order_url()
	{
		$controller = \Str::parseCallback(\Route::currentRouteAction(), null)[0];
		return \URL::action($controller.'@postOrder');
	}


	public function headers($envelope = '<th class="{class}">{header}</th>')
	{
		$ret = '';
		foreach($this->field_names as $name)
		{
			$ret .= \Envelope::parse($envelope, array('class'=>$name,'header'=>$this->build_header($name))	).PHP_EOL;
		}

		return $ret;
	}

	protected function build_header ($fieldname)
	{

		$sort_up = $this->get_sort_url($fieldname);
		$sort_down =  $this->get_sort_url('-'.$fieldname);

		list($sortname, $sortmode) = $this->get_sort();

		if($fieldname ==  $sortname)
		{
			$icon_class =  $sortmode == '+' ? 'sort-up' : 'sort-down';
			$url =  $sortmode == '+' ? $sort_down : $sort_up;
		}
		else
		{
			$icon_class = '';
			$url = $sort_up;
		}

		$label = \Lang::get($this->field_labels[$fieldname]);
		$header = \HTML::link($url,$label,array('class'=>'sort ' .$icon_class));
		//$header.= '<i class="'.$icon_class.'"></i>';

		return $header;
	}

	public function field_value($fieldname, $record)
	{
		$value = $record->get_value($fieldname);
		$value = (in_array($fieldname, $this->main_fields) AND $this->editable) ? \Html::link($this->get_edit_url($record->id), $value) : $value;

		return $value;
	}

}
