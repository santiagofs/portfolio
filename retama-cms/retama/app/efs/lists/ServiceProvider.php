<?php
namespace eFS\Lists;

class ServiceProvider extends \eFS\ServiceProvider {

    public function register()
    {
        parent::register('lists');
    }

    public function boot()
    {
        parent::boot('lists');
    }

}