<div class="records-list-holder rounded-box">
	<table cellpadding="0" cellspacing="0" class="records-list" droppable="{{ $list->sortable ? 'true' : 'false' }}">
		<thead>
			<tr>
				<td colspan="{{ $list->column_count() }}">
					<div class="clear-block">
						@include('lists::pagination')

						@include('lists::actions')
					</div>
				</td>
			</tr>
			<tr>
				@if($list->sortable)
				<th class="sort-grip"></th>
				@endif
				@if($list->selectable)
				<th class="select-item" style="cursor: pointer;"><input type="checkbox" name="check_all" class="check-all"></th>
				@endif
				{{ $list->headers() }}
				<th class="actions"></th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<td colspan="{{ $list->column_count() }}">
					<div class="clear-block">
						@include('lists::pagination')

						@include('lists::actions')
					</div>
				</td>
			</tr>
		</tfoot>

		<tbody class="" >
			@foreach($list->records() as $record)

			<tr draggable="{{  $list->sortable  ? 'true' : 'false' }}" class="record {{ $list->sortable ? 'draggable' : '' }}" record-id="{{ $record->id }}">
				@if($list->sortable)
				<td class="sort-grip" data-action="item-move"><a class="fa fa-arrows-v"></a></td>
				@endif
				@if($list->selectable)
				<td class="select-item"><input type="checkbox" class="check-item" name="id[]" value="{{ $record->id }}" /></td>
				@endif
				@foreach($list->field_names as $name)
				<td class="{{ $name }}">{{ $list->field_value($name, $record) }}</td>
				@endforeach
				<td class="actions">
					@if($list->creatable)
					<a href="{{ $list->get_edit_url($record->id) }}" class="fa fa-edit" title="{{ \Lang::get('retama::retama.create') }}"></a>
					@endif

					@if($list->deletable)
					<a href="{{ $list->get_delete_url($record->id) }}" class="fa fa-times" delete-item title="{{ \Lang::get('retama::retama.delete_item') }}"></a>
					@endif
				</td>
			</tr>
			@endforeach

			@if($list->sortable)
			<tr id="list-placeholder">
				@if($list->sortable)
				<td class="sort-grip" data-action="item-move"></td>
				@endif
				@if($list->selectable)
				<td class="select-item"></td>
				@endif
				@foreach($list->field_names as $name)
				<td></td>
				@endforeach
				<td class="actions"></td>
			</tr>
			@endif
		</tbody>
	</table>
</div>


@section('footer-scripts')
@parent
<style>
#list-placeholder {
	height:30px;
	width: 100%;
	background: #FFFFFF;
}
#list-placeholder.hover {
	background: #CCC;
}
</style>
<script>

$(function(){

   //var $list = $('#slides-list');
   var $list = $('[droppable="true"]');

   if($list.length) {

	   $('[remove-slide]').on('click', function(e) {
		   e.preventDefault();
		   $(this).parents('.slide').remove();
	   });

	   var current_target = false;
	   var $current_drag = undefined;
	   var $placeholder = $('#list-placeholder');
	   $placeholder.detach();

	   $(document).on('mousedown', '[draggable]', function(e) {
		   current_target = e.originalEvent.target;
	   })

	   $(document).on('dragstart', '[draggable]', function(e) {

		   var $this = $(this);
		   var handle = $('[data-action="item-move"]', $this)[0];

		   if(handle.contains(current_target))
		   {
			   console.log('dragg!!')
			   $current_drag = $this;
			   //e.originalEvent.dataTransfer.setData('data','test');
			   e.originalEvent.dataTransfer.setData('text/plain', 'handle');
		   } else {
			   e.preventDefault();
		   }


	   })
	   console.log($list);
	   $($list).on('drop', '#list-placeholder', function(e){
		   e.preventDefault();
		   e.stopPropagation();
		// 	console.log('test');
		// 	console.log(this);
		//    var $target = $(e.originalEvent.target);
		//    console.log($target.attr('id'))
		    $current_drag.insertAfter($placeholder);

			// collect the ids
			var ids = [];
			$('tr[record-id]').each(function(i,e) {
				var $this = $(this);
				var id = $this.attr('record-id');
				ids.push(id);
			})
			var post = {
				type: "POST",
				url: '{{ $list->get_order_url() }}',
				data: {'ids':ids},
				// success: function()
				// {
				// 	top.location.reload();
				// },
				/* dataType: dataType, */
				beforeSend: function(request) {
			        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
			    }
			}
			$.ajax(post);

		//    if($target.attr('id') == 'list-placeholder')
		//    {
		   //
		//    }

	   })
	   $(document).on('drop',  function(e){
		    console.log('test2');
			var $target = $(e.originalEvent.target);
			console.log($target)
		   e.preventDefault();
		   e.stopPropagation();
	   })

	   $($list).on('dragenter','#list-placeholder', function(e) {
		   $(this).addClass('hover');
	   });
	   $($list).on('dragleave','#list-placeholder', function(e) {
		   $(this).removeClass('hover');
	   });


	   $($list).on('dragenter','tr.draggable', function(e) {
		   e.preventDefault();
		   e.stopPropagation();
		   var $this = $(this);


		   if($current_drag[0] === this) return;

		   //if(e.originalEvent.target === $current_drag[0]) return;
		   //console.log(e.originalEvent.target);

		   var $target = $(e.originalEvent.target)
		   //console.log($target);
		   var cx = e.originalEvent.clientY
		   var ox = $this.offset().top;
		   var d = (cx - ox) - ($target.height() / 2);

		   if(d < 0) {
			   $placeholder.insertBefore($this);
		   } else {
			   $placeholder.insertAfter($this);
		   }
	   })
	   $(document).on('dragleave',$list, function(e) {
		   e.preventDefault();
		   e.stopPropagation();

		   //console.log('leave');
	   })

	   $(document).on('dragend',$list, function(e) {
			 e.preventDefault();
			 e.stopPropagation();
			 $placeholder.detach();
	   })

	   $(document).on('dragover',$list,function(e){
		   e.preventDefault();
		   e.stopPropagation();
	   })

   }
});
</script>
@stop
