<div class="bulk-actions">
	@if($list->creatable)
	<a href="{{ $list->get_edit_url() }}" class="btn btn-primary"><i class="fa fa-edit"></i> {{ \Lang::get('retama::retama.create') }}</a>
	@endif

	@if($list->deletable)
	<a href="{{ $list->get_delete_url() }}" class="btn btn-default" delete-selected><i class="fa fa-times"></i> {{ \Lang::get('retama::retama.delete_selected') }}</a>
	@endif
</div>