<div class="filter-holder">
{{ Form::open(array('url' => \Retama::list_url(), 'method' => 'post', 'id'=>'form-filter', 'class'=>'form-inline')) }}

@foreach($list->get_filters() as $fieldname => $filter)

{{ \Form::one('text', $fieldname, $filter->label, \Input::get($fieldname),  array('class'=>'form-control', 'placeholder'=>$filter->label) ) }}

@endforeach
	<button type="submit" name="filter-submit" value="filter" class="btn btn-primary">Filter</button>
	<a href="{{ \Retama::get_list_url() }}" class="btn btn-default">Reset</a>

	@if($list->exportable)
	<button type="submit" name="filter-submit" value="export" class="btn btn-secondary">Export</button>
	@endif

{{ Form::close() }}
</div>
