<?php

class InitDB extends BaseController
{

	public function create($databasename)
	{
		//\DB::getConnection()->statement('CREATE DATABASE :schema CHARACTER SET utf8 COLLATE utf8_general_ci;', array('schema' => $databasename));

		$this->blog();
	}

	public function table($table)
	{
		$this->$table();
	}

	private function blog()
	{
		\DB::statement("DROP TABLE IF EXISTS `blog`;");
		\DB::statement("CREATE TABLE `blog` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`indent` int(11) DEFAULT NULL,`parent_id` int(11) NOT NULL DEFAULT '0',`tree_left` int(11) NOT NULL DEFAULT '0',`tree_right` int(11) NOT NULL DEFAULT '0',`updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',`created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',`title` varchar(255) DEFAULT NULL,`post` text,`user_id` int(11) DEFAULT '0',`user_alias` varchar(50) DEFAULT '',`user_ip` varchar(20) DEFAULT NULL,`allow_comments` tinyint(4) DEFAULT '1',`blogable_id` int(11) DEFAULT NULL,`blogable_type` varchar(255) DEFAULT NULL,PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
		//\DB::statement("LOCK TABLES `blog` WRITE;");

		\DB::statement("INSERT INTO `blog` (`id`, `indent`, `parent_id`, `tree_left`, `tree_right`, `updated_at`, `created_at`, `title`, `post`, `user_id`, `user_alias`, `user_ip`, `allow_comments`, `blogable_id`, `blogable_type`) VALUES(16,0,0,1,2,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL,0,'',NULL,1,NULL,NULL);");

		//\DB::statement("UNLOCK TABLES;");

	}
}

?>