@extends('layouts.html5')

@section('site-layout')

<div id="site-wrapper">

	<div id="site-canvas">

		<div class="off-canvas off-canvas-{{{$off_canvas_mode or 'left'}}}">
@yield('off-canvas')
		</div>
		<div id="site-content">
@yield('site-content')
		</div>
	</div>

</div>
@stop
