<!DOCTYPE html>
<html lang="en" class="{{ \Browser::device() }}">
<head>
	<meta name="csrf-token" content="<?= csrf_token() ?>">
@section('seo')

	<title>Here goes the site title</title>
	<meta name="description" content="">
    <meta name="keywords" content="">

@show

	@section('head-scripts')
	@show

	<!-- Fav Icon -->
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
	<link rel="apple-touch-icon" href="{{ asset('favicon.png') }}">
	<link rel="apple-touch-icon" sizes="128x128" href="{{ asset('favicon-128.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon-144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon-152.png') }}">

</head>

<body id="@yield('body-id')" class="{{ $body_class or '' }} {{ $layout or '' }}">

	@section('site-layout')
	@show


	@section('footer-scripts')
	@show

</body>
</html>