@extends('base.html5')

@section('site-layout')

<div id="site-viewport">
	<!-- offcanvas menu -->
	<aside id="site-off-canvas" class="off-canvas {{{$off_canvas_mode or 'left'}}}">
		@yield('site-off-canvas')
	</aside>
	<div id="off-canvas-overlay"></div>
	<!-- end offcanvas menu -->


	<!-- site header -->
	<header id="site-header">
		@yield('site-header')
	</header>
	<!-- end site header -->

	<!-- site body -->
	<div id="site-body">
		@yield('site-body')
	</div>
	<!-- end site content -->


	<footer id="site-footer">
		@yield('site-footer')
	</footer>

</div>

@stop
