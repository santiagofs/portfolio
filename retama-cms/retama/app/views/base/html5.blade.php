<!DOCTYPE html>
<html lang="en" class="{{ \Browser::device() }}">
<head>
	<meta name="csrf-token" content="<?= csrf_token() ?>">

	@section('common-heads')
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	@show

	@yield('seo')

	@yield('head-scripts')

	<?php
		$now = '?v='.time();
	?>
	<!-- Fav Icon -->
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}{{$now}}">
	<link rel="apple-touch-icon" href="{{ asset('favicon.png') }}">
	<link rel="apple-touch-icon" sizes="128x128" href="{{ asset('favicon-128.png') }}{{$now}}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon-144.png') }}{{$now}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon-152.png') }}{{$now}}">

</head>

<body id="{{ $body_id or '' }}" class="{{ $body_class or '' }}">

	@yield('fb-root')

	@yield('site-layout')

	@yield('fixed-elements')

	@yield('footer-scripts')

</body>
</html>