<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model 
{
	public $table = 'timelines';
	
	protected $fillable = [
	    'lead_id', 'user_id', 'timelineable_id', 'timelineable_type', 'timeline_type', 'timeline_value'
	];
	
	public function timelineable() {
		return $this->morphTo();
	}
	
}
