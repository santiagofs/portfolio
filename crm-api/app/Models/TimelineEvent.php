<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimelineEvent extends Model 
{
	public $table = 'timeline_events';
	
	protected $fillable = [
	    'user_id', 'lead_id', 'lead_status_id', 'notes'
	];
	
	protected $appends = ['headline'];
	
	public static function defaultSort() {
	    return '-created_at';
    }
    
    public static function postRules() {
	    return [
		    'item.lead_id' => 'required',
		    'item.user_id' => 'required',
	        'item.lead_status_id' => 'required',
	    ];
    }
    

	
	public function timeline()
	{
		return $this->morphMany('App\Models\Timeline', 'timelineable');
	}
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}
	
	public function lead()
	{
		return $this->belongsTo('App\Models\Lead');
	}
	
	
	public function status() {
		return $this->belongsTo('App\Models\LeadStatus', 'lead_status_id', 'id');
	}
	
	
	public function getHeadlineAttribute() {
		return $this->status ? "Lead status changed to: " . $this->status->name : '';
	}
}
