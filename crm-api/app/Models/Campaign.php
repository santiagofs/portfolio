<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
	public $table = 'campaign';

	protected $fillable = [
        'name', 'start_date', 'end_date', 'active',
    ];


    public function leads()
    {
	    return $this->hasMany('App\Models\Lead');
    }

    public static function postRules() {
	    return [
	        'item.name' => 'sometimes|required',
	        'item.start_date' => 'sometimes|date',
	        'item.end_date' => 'sometimes|date',
	    ];
    }

	public static function customDelete($ids=[]) {
		// check no leads assigned to this campaings
		if(count(Lead::whereIn('campaign_id', $ids)->get())) {
			return response()->json(['message'=>'This campaign has associated leads and cannot be deleted'], 400);
		}
		$resp = static::whereIn('id',$ids)->delete();
		return response()->json(['message'=>'ok', 'rsp' => $resp], 200);
	}
}
