<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Hashing\BcryptHasher as Hash;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public static $current = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token', 'role_id', 'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token_expiration', 'created_at', 'updated_at'
    ];

    /* RELATIONS */
    public function role()
    {
        return $this->belongsTo('App\Models\UserRole');
    }

    public function notifications()
	{
		return $this->belongsToMany('App\Models\Notification', 'users_notifications', 'notification_id', 'user_id');
	}

	public function pendingNotifications()
	{
		return $this->belongsToMany('App\Models\Notification', 'users_notifications', 'notification_id', 'user_id')->wherePivot('seen', 0);
	}

	public function messages()
	{
		return $this->hasMany('App\Models\Chat', 'to_id');
	}

    public function sales() {
		$relation =  $this->hasMany('App\Models\Timeline')->where('timelineable_type', 'App\Models\Sale')->orderBy('created_at', 'DESC');

		return $relation;
	}




    public function setPasswordAttribute($value) {
	    if(!$value) return;
		$this->attributes['password'] = (new Hash)->make($value);
    }

    public static function auth($email, $password) {

	    $user = static::where('email', $email)->first();
	    if(!$user) return false;


	    if( !(new Hash)->check($password, $user->password) ) return false;

	    $user->api_token = str_random(60);
	    $user->api_token_expiration = static::token_expiration();
	    $user->save();

	    return $user;

    }

    public function keep_alive() {
	    $this->api_token_expiration = static::token_expiration();
	    $this->save();
    }

    protected static function token_expiration ()
    {
	    return date("Y-m-d H:i:s", strtotime('+1 hours'));
    }

    public function revalidate() {
	    $this->api_token_expiration = static::token_expiration();
	    $this->save();
    }

    public static function check($token) {
	    $mytime = \Carbon\Carbon::now();
	    $user = static::where('api_token', $token)
	    	->where('api_token_expiration', '>', $mytime->toDateTimeString())->first();
	    if($user) $user->revalidate();
	    static::$current = $user;
	    return $user;
    }


    public static function unauthenticated() {
		return response()->json(['message'=>'This resource needs authentication'], 401);
	}

	public static function unauthorized() {
		return response()->json(['message'=>'You are not allowed on this resource'], 403);
	}

    public static function customDelete($ids) {
        $id = static::$current->id;
        if(in_array($id, $ids)) {
            return response()->json(['message'=>'You cannot delete the current user'], 400);
        }

        if(count(Sale::whereIn('user_id', $ids)->get())) {
			return response()->json(['message'=>'User/s have associated sales and cannot be deleted'], 400);
		}

		ScheduleCall::whereIn('lead_id', $ids)->delete();
		TimelineEvent::whereIn('lead_id', $ids)->delete();
		$resp = static::whereIn('id',$ids)->delete();

		return response()->json(['message'=>'ok'], 200);
	}
}
