<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	public $table = 'notes';

	protected $fillable = [
	    'lead_id', 'user_id', 'subject', 'body'
	];
	protected $appends = array('headline');

	public static function defaultSort() {
	    return '-created_at';
    }

	public static function postRules() {
	    return [
		    'item.lead_id' => 'required',
		    'item.user_id' => 'required',
		    //'item.subject' => 'required',
	    ];
    }


	public function timeline()
	{
		return $this->morphMany('App\Models\Timeline', 'timelineable');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function lead()
	{
		return $this->belongsTo('App\Models\Lead');
	}

	public function getHeadlineAttribute() {
		return $this->subject;
	}
}
