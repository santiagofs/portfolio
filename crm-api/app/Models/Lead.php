<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
	public $table = 'leads';

	protected $fillable = [
	    'name', 'email', 'phone', 'source_id', 'campaign_id', 'user_id', 'type_id', 'status_id', 'notes', 'timezone', 'total_shares', 'avg_price'
	];


	public static function defaultSort() {
	    return 'name';
    }

    public static function postRules() {
	    return [
		    'item.name' => 'required',
		    'item.phone' => 'required',
	        //'item.email' => 'required',
	    ];
    }

	public function source()
	{
		return $this->belongsTo('App\Models\Source');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

	// public function agent() {
	// 	return $this->belongsTo('App\Models\User');
	// }

	public function campaign()
	{
		return $this->belongsTo('App\Models\Campaign');
	}

	public function type()
	{
		return $this->belongsTo('App\Models\LeadType');
	}

	public function status()
	{
		return $this->belongsTo('App\Models\LeadStatus');
	}

	public function timeline() {
		return $this->hasMany('App\Models\Timeline')->orderBy('created_at', 'DESC');;
	}

	public function lastEvent() {
		$relation =  $this->hasMany('App\Models\Timeline')->orderBy('created_at', 'DESC');
		return $relation;

	}
	public function sales() {
		$relation =  $this->hasMany('App\Models\Timeline')->where('timelineable_type', 'App\Models\Sale')->orderBy('created_at', 'DESC');

		return $relation;
	}

	public function save(array $options = array()) {
		if(!$this->id) {
			$this->status_id = 1;
		}
		parent::save($options);
	}

	public static function import($leads, $campaigns, $agents, $source_id) {
		$campaign_ndx = 0;
		$agent_ndx = 0;

		foreach($leads as $lead) {
			$test = static::where('phone', $lead['phone'])->orWhere('email', $lead['email'])->first();
			if($test) continue;

			$lead['campaign_id'] = count($campaigns) ? $campaigns[$campaign_ndx] : 0;
			$lead['user_id'] = count($agents) ? $agents[$agent_ndx] : 0;
			$lead['source_id'] = $source_id ? $source_id : 0;
			$lead['status_id'] = 6;

			$model = new static;
			$model->fill($lead);
			$model->save();
			$model->status_id = 6; // hack espantoso!!!!
			$model->save();
			$campaign_ndx = ($campaign_ndx < (count($campaigns)-1)) ? $campaign_ndx+1 : 0;
			$agent_ndx = ($agent_ndx < (count($agents)-1)) ? $agent_ndx+1 : 0;
		}
	}

	public static function reassign($campaigns, $agents, $ids = []) {
		$q = static::whereIn('campaign_id', $campaigns)
				->whereNotIn('status_id', [3,4]);
		if(count($ids)) $q->whereIn('id', $ids);
		$leads = $q->get();

		$agent_ndx = 0;

		foreach($leads as $lead) {
			//$lead->campaign_id = $campaigns[$campaign_ndx];
			$lead->user_id = $agents[$agent_ndx];
			$lead->save();

			//$campaign_ndx = ($campaign_ndx < (count($campaigns)-1)) ? $campaign_ndx+1 : 0;
			$agent_ndx = ($agent_ndx < (count($agents)-1)) ? $agent_ndx+1 : 0;
		}

		return count($leads);

	}
	public static function massEdit($ids, $agent_id, $campaign_id, $status_id) {
		\DB::enableQueryLog();
		if(!count($ids)) return;
		$update = [];
		if($agent_id) $update['user_id'] = $agent_id;
		if($campaign_id) $update['campaign_id'] = $campaign_id;
		if($status_id) $update['status_id'] = $status_id;

		\DB::table('leads')->whereIn('id', $ids)->update($update);
	}

	public static function customDelete($ids) {
		// check no leads assigned to this campaings
		if(count(Sale::whereIn('lead_id', $ids)->get())) {
			return response()->json(['message'=>'Lead/s have associated sales and cannot be deleted'], 400);
		}

		ScheduleCall::whereIn('lead_id', $ids)->delete();
		TimelineEvent::whereIn('lead_id', $ids)->delete();
		$resp = static::whereIn('id',$ids)->delete();
		return response()->json(['message'=>'ok', 'rsp' => $resp], 200);

		return response()->json(['message'=>'not implemented yet'], 400);
	}
}
