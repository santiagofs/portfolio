<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ScheduleCall extends Model
{


	public $table = 'schedule_calls';

	protected $fillable = [
	    'user_id', 'lead_id', 'call_date', 'notes'
	];
	protected $appends = array('headline');

	public static function defaultSort() {
	    return 'call_date';
    }

    public static function postRules() {
	    return [
		    'item.lead_id' => 'required',
		    'item.call_date' => 'required|date',
	    ];
    }


	public function timeline()
	{
		return $this->morphMany('App\Models\Timeline', 'timelineable');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function lead()
	{
		return $this->belongsTo('App\Models\Lead');
	}

	public function getHeadlineAttribute() {

		if(User::$current && User::$current->timezone !== null) {
			$diff = User::$current->timezone;
		} else {
			$diff = '-3';
		}
		$diff = '-5';
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->call_date, 'UTC')->addHours($diff)->format('m-d-Y h:i:s A');
		return "Schedule call to : " . $date;
	}
}
