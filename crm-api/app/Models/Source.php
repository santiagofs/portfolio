<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
	public $table = 'sources';

	protected $fillable = [
	    'source'
	];

	public static function defaultSort() {
		return 'source';
	}

	public static function postRules() {
		return [
			'item.source' => 'required',
		];
	}

	public static function customDelete($ids) {
		return response()->json(['message'=>'not implemented yet'], 400);
	}
}
