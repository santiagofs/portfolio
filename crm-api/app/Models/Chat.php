<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model 
{
	public $table = 'chats';
	
	protected $fillable = [
	    'from_id', 'to_id', 'body', 'seen'
	];
	
	public function from()
	{
		return $this->belongsTo('App\Models\User');
	}
	
	public function to()
	{
		return $this->belongsTo('App\Models\User');
	}
	
}
