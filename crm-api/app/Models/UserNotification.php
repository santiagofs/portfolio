<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model 
{
	public $table = 'users_notifications';
	
	protected $fillable = [
	    'seen'
	];
	
	public function notification()
	{
		return $this->belongsTo('App\Models\Notification');
	}
	
	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

}
