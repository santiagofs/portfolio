<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadType extends Model 
{
	public $table = 'lead_types';
	
	protected $fillable = [
	    'name'
	];
	
	public function leads()
	{
		return $this->hasMany('App\Models\Lead');
	}
	
	public static function postRules() {
		return [
		    'item.name' => 'required',
		];
	}
	
}
