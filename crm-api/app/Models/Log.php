<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $table = 'logs';

	protected $fillable = [
	    'user_id', 'url', 'data'
	];
}
