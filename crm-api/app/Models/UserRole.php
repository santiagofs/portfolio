<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model 
{
	public $table = 'user_roles';
	
	protected $fillable = [
	    'role'
	];
	
	
	public function users()
    {
        return $this->hasMany('App\Models\User');
    }
    
    public static function defaultSort() {
	    return 'role';
    }
    
    public static function postRules() {
	    return [
	        'item.role' => 'required',
	    ];
    }
}
