<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	public $table = 'notifications';

	protected $fillable = [
	    'subject', 'body'
	];

	public static function defaultSort() {
	    return '-created_at';
    }

	public static function postRules() {
	    return [
		    'item.subject' => 'required',
	    ];
    }

	public static function customDelete($ids) {
		return response()->json(['message'=>'not implemented yet'], 400);
	}

/*
    public static function defaultSort() {
	    return 'updated_at';
    }
*/

/*
	public function users()
	{
		return $this->belongsToMany('App\Models\User', 'users_notifications', 'user_id', 'notification_id');
	}
*/

	public function status() {
		return $this->hasOne('App\Models\UserNotification');
	}

}
