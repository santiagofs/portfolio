<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lead;

class Sale extends Model
{
	public $table = 'sales';

	protected $fillable = [
	    'user_id', 'lead_id', 'stock', 'amount', 'price', 'notes', 'campaign_id'
	];
	protected $appends = array('headline');



	public static function defaultSort() {
	    return '-created_at';
    }

    public static function postRules() {
	    return [
		    'item.lead_id' => 'required',
		    'item.amount' => 'required',
	        'item.price' => 'required',
	    ];
    }


	public function timeline()
	{
		return $this->morphMany('App\Models\Timeline', 'timelineable');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function lead()
	{
		return $this->belongsTo('App\Models\Lead');
	}
	public function campaign()
	{
		return $this->belongsTo('App\Models\Campaign');
	}

	public function getHeadlineAttribute() {
		//var_dump($this->campaign);
		return "Sale: " . ($this->campaign ? $this->campaign->name : 'N/A') . ' - ' . $this->amount . ' x $' . $this->price . ' = $'. $this->total;
	}

	public function save(array $options = []) {
		$this->total = $this->amount * $this->price;
		parent::save($options);

		// update lead sales
		$totals = Sale::where('lead_id', $this->lead_id)
			->select(\DB::raw('SUM(amount) AS total_shares, SUM(total) AS total_price'))
			->first();

		// var_dump($totals->total_shares);
		// var_dump($totals->total_price);

		$lead = Lead::find($this->lead_id);
		$lead->total_shares = $totals->total_shares;
		$lead->avg_price = $totals->total_price/$totals->total_shares;
		$lead->save();
	}

	public static function customDelete($ids=[]) {
		//var_dump($ids);
		foreach($ids as $id) {
			$sale = Sale::find($id);
			if(!$sale) continue;

			$lead_id = $sale->lead_id;
			$sale->delete();

			$totals = Sale::where('lead_id', $sale->lead_id)
				->select(\DB::raw('SUM(amount) AS total_shares, SUM(total) AS total_price'))
				->first();

			$lead = Lead::find($sale->lead_id);
			$lead->total_shares = $totals->total_shares;
			$lead->avg_price = $totals->total_price/$totals->total_shares;
			$lead->save();

		}
		return response()->json(['message'=>'ok', 'rsp' => ''], 200);
	}
}
