<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User as User;

class Authenticate
{
    public function handle($request, Closure $next)
    {

	    $token = $request->header('authorization');
		$token = $request->input('api_token', $token);


		$user = User::check($token);

		if(!$user) return User::unauthenticated();
		$request->user = $user;

		$request->attributes->add(['user' => $user]);


        $pathinfo =  $request->getPathInfo();
        $method = $request->getMethod();

        if(strpos($pathinfo, 'pending') === false) {
            $log = new \App\Models\Log;
            $log->user_id = $user->id;
            $log->url = $method . ' : ' . $pathinfo;
            $log->data = json_encode($request->input());
            $log->save();
        }


        return $next($request);
    }
}
