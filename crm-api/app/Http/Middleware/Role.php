<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User as User;

class Role
{
    public function handle($request, Closure $next)
    {
	    
	    $token = app('request')->header('authorization');
		
		$token = app('request')->input('api_token', $token);
		if(!$token) return User::unauthenticated();;
		
		$user = User::check($token);

        return $next($request, $user);
    }
}
