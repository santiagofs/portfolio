<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User as User;

class GetListMiddleware
{
    public function handle($request, Closure $next, $class)
    {
	    //set_time_limit(0);
		//ini_set('max_execution_time', 10000);
	    //\DB::enableQueryLog();
	    $fullClass = '\\App\\Models\\'. $class;
	    $name = strtolower($class).'s';
	    
	   
	    $q = $fullClass::query();
			
		_apply_filters($q);
		_apply_relation($q);
		_apply_sort($q, method_exists($fullClass, 'defaultSort') ? $fullClass::defaultSort() : 'name');
		$count = $q->count();
		
		
		list($per_page, $page) = _apply_paging($q);
		
		
		
		$pages = ceil($count/$per_page);
		$next = min($page+1, $pages);
		$last = $pages;
		$previous = max($page-1, 1);
		$first = 1;
		
		$list = $q->get();
		//dd(\DB::getQueryLog());

		return response()->json(['items'=>$list, 'pagination'=>['rows'=>$count, 'pages'=>$pages, 'page'=>$page, 'first'=>$first, 'previous'=>$previous, 'next'=>$next, 'last'=>$last]], 200);	 
    }
}
