<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User as User;

class GetOneMiddleware
{
    public function handle($request, Closure $next, $class)
    {
		$validator = \Validator::make($request->all(), [
			'id' => 'required',
        ]);
        if($validator->fails()) return response()->json(['errors'=>$validator->errors()->all()], 400);
        
        
        $fullClass = '\\App\\Models\\'. $class;
		$name = strtolower($class);
		$id = $request->input('id');
		
		$q = $fullClass::query();
		_apply_relation($q);
		$item = $q->where('id', $id)->first();
			
		if(!$item) return response()->json(['errors'=> $name.' not found'], 400);

		return response()->json(['item'=>$item], 200);
		 
    }
}
