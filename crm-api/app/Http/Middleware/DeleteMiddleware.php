<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User as User;

class DeleteMiddleware
{
    public function handle($request, Closure $next, $class, $role = null)
    {

		if($role) {
			$roles = explode(':', $role);
			if(  !(validate_role($request->user, $roles)) ) return User::unauthorized();
		}



	    $validator = \Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if($validator->fails()) {
	        return response()->json(['errors'=>$validator->errors()->all()], 400);
        }

		$ids = $request->input('id');
		is_array($ids) || $ids = [$ids];

		if($class) {
			$class = '\\App\\Models\\'. $class;

            if(method_exists($class, 'customDelete')) {
                return $class::customDelete($ids);
            } else {
                $resp = $class::whereIn('id',$ids)->delete();
    			return response()->json(['message'=>'ok'], 200);
            }
		} else {
			$request->attributes->add(['ids' => $ids]);
			return $next($request);
		}




    }
}
