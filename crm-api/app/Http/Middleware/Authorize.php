<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User as User;

class Authorize
{
    public function handle($request, Closure $next, $role)
    {
	    
	    if($role) {
			 if(  !(validate_role($request->user, $role)) ) return User::unauthorized();
		}
		
        return $next($request);
    }
}
