<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User as User;

class EditMiddleware
{
    public function handle($request, Closure $next, $class, $role=null)
    {
	    if($role) {
			$roles = explode(':', $role);

			if(  !(validate_role($request->user, $roles)) ) return User::unauthorized();
		}

		$fullClass = '\\App\\Models\\'. $class;
		$rules = $fullClass::postRules();

		$validator = \Validator::make($request->all(), $rules);
		if($validator->fails()) return response()->json(['errors'=>$validator->errors()->all()], 400);

		$name = strtolower($class);

		$item = $request->input('item');
		$id = isset($item['id']) ? $item['id'] : null;
		$model = $fullClass::findOrNew($id);
		$model->fill($item);
		$model->save();

        if($class == 'Lead') {
            $model->campaign;
            $model->status;
            $model->user;
            $model->source;
        }

		if(method_exists($model, 'timeline') AND ($id == null) AND ($class != 'Lead')) {
			$event = new \App\Models\Timeline;
			$event->lead_id = $model->lead_id;
			$event->user_id = $model->user_id;
			$model->timeline()->save($event);

			$lead = \App\Models\Lead::find($model->lead_id);

			switch($class) {
				case 'Sale':
					$lead->type_id = 2; // Buyer
					$lead->status_id = 7; // Buyer
					$lead->save();
					break;

				case 'ScheduleCall':
					$lead->status_id = 2; // callback
					$lead->save();
					break;

				case 'TimelineEvent':
					$lead->status_id = $model->lead_status_id;
					if($model->lead_status_id == 3 OR $model->lead_status_id == 4) { //DNC Not Interested or DNC Wrong or Invalid Number
						$lead->type_id = 3; // Not Buyer;
					}
					$lead->save();
					break;
			}
		}

		return response()->json(['item'=>$model], 200);

    }
}
