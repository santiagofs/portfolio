<?php

use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User as User;
use App\Models\Lead as Lead;
use App\Models\UserRole as Role;
use App\Models\Campaign as Campaign;
use App\Models\LeadType as LeadType;
use App\Models\LeadStatus as LeadStatus;
use App\Models\UserRole as UserRole;
use App\Models\Source as Source;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


function validate_request() {

	$token = app('request')->header('authorization');

	$token = app('request')->input('api_token', $token);
	if(!$token) return null;

	$user = App\Models\User::check($token);
	return $user;
}

function validate_role($user, $roles) {
	is_array($roles) || $roles = [$roles];

	foreach($roles as $role) {
		if($user->role_id == $role) return true;
	}
	return false;
}

function _apply_filters($q) {
	$request = Request::capture();


	if( $filters = $request->input('filters') ) {
		is_array($filters) || $filters = json_decode($filters, true);
		//var_dump($filters);

		foreach($filters as $filter) {
			if(isset($filter['skip'])) continue;
			$field = $filter['field'];
			$value = $filter['value'];
			$op = isset($filter['op']) ? $filter['op'] : '=';
			$mode = isset($filter['mode']) ? $filter['mode'] : 'where';
			if($op === 'like') $value = '%'.$value.'%';
			$q = $q->$mode($field, $op, $value);
		}
	}
}

function _apply_sort($q, $default = null) {
	$request = Request::capture();


	$mode = 'ASC';
	$sort = $request->input('sort') ? $request->input('sort') : $default;

	if(starts_with($sort, '+') || starts_with($sort, '-')) {
		$mode = (substr($sort, 0, 1)) == '+' ? 'ASC' : 'DESC';
		$field = substr($sort, 1);
	} else {

		$field = $sort;
	}

	$q->orderBy($field, $mode);
}

function _apply_relation($q) {
	$request = Request::capture();
	if( $relations = $request->input('relations') ) {
		if(!is_array($relations)) $relations = explode(',', $relations);

		foreach($relations as $r) {
			if($r == 'lastEvent.timelineable') continue;
			$q->with($r);
		}
	}
}

function _apply_paging($q) {
	$request = Request::capture();
	$per_page = $request->input('per_page', 20);
	$page = $request->input('page', 1);
	$q->offset(($page-1)*$per_page)
		->take($per_page);

	return [$per_page, $page];
}


function get_sort_mode($request, $default) {


	$field = $default;
	$mode = 'ASC';
	if( $sort = $request->input('sort') ) {
		if(starts_with($sort, '+') || starts_with($sort, '-')) {
			$mode = (substr($sort, 0, 1)) == '+' ? 'ASC' : 'DESC';
			$field = substr($sort, 1);
		} else {

			$field = $sort;
		}
	}

	return [$field, $mode];
}


$app->get('/', function () use ($app) {
    return $app->version();
});



$app->get('/test', function () use ($app) {

	die();
	$user = User::find(1);
	//var_dump($user);
	$user->password = 'nopdws';
	$user->save();

	die();

	if( !($user = validate_request()) ) return response()->json(['message'=>'Invalid credentials'], 401);

	$campaign = App\Models\Campaign::find(1);
	var_dump($campaign);
	die();

	var_dump($user);
});

$app->get('/auto', function () use ($app) {

	//var_dump(get_class_methods('Auth'));
	$res = Auth::validate(['api_token' => 'Rkupmer1WiAaLqIThRa22j1ScHDimDegUfR6uLR0ySwb2iT1ZtgD24zNz0ak']);
	var_dump($res);
});


$app->options('{all:.*}',function(Request $request) {
	return '';
});

$app->group(['prefix' => 'api'], function () use ($app) {





	$app->post('login', function(Request $request) use ($app) {

		$user = User::auth($request->input('email'), $request->input('password'));

		if(!$user) return response()->json(['message' => 'Invalid username or password'], 401);

		return response()->json(['user' => $user]);
	});

	$app->get('user/check', function(Request $request) use ($app ) {
		$token = app('request')->header('authorization');
		$token = app('request')->input('api_token', $token);
		if(!$token) return response()->json(['message'=>'Invalid credentials'], 401);

		$user = App\Models\User::check($token);
		return response()->json(['status' => $user ? true : false]);

	});

	$app->group(['middleware' => 'auth'], function ($a) use ($app ) {

	    $app->get('/bar', function (Request $request)    {
		    var_dump($request->user);
	        var_dump('test');
	    });

	    $app->get('user/profile', function () {
	        // Uses Auth Middleware
	    });

	    /* ROLES */
/*
		$app->get('roles', function(Request $request) use ($app) {

			$roles = Role::where('id', '>', $request->user->role_id)->get();
			return response()->json(['roles'=>$roles], 200);
		});
*/



		/* USERS */
		$app->post('users', function(Request $request) use ($app) {

			if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();

			$q = User::query();
			$q->where('role_id', '>=', $request->user->role_id);

			_apply_filters($q);
			_apply_relation($q);
			_apply_sort($q, 'name');

			$users = $q->get();
			return response()->json(['users'=>$users], 200);

		});

		$app->get('user', function(Request $request) use ($app) {

			if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();

			$errors = $this->validate($request, [
		        'id' => 'required'
		    ]);
			if($errors) return response()->json(['errors'=>$errors], 400);


			$id = $request->input('id');

			$q = User::query();
			$q->where('role_id', '>=', $request->user->role_id);

			_apply_relation($q);

			$user = $q->where('id', $id)->first();

			if(!$user) return response()->json(['errors'=>'User not found'], 400);

			return response()->json(['user'=>$user], 200);

		});


		$app->post('user', function(Request $request) use ($app) {

			if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();

			$user = $request->input('user');
			if(!$user) $user = $request->input('item');

			if(!$user['id']){
				$errors = $this->validate($request, [
			        'user.name' => 'sometimes|required',
			        'user.email' => 'sometimes|required|email|unique:users,email'.($user['id'] ? ', '.$user['id'] : ''),
			    ]);
				if($errors) return response()->json(['errors'=>$errors], 400);
			}


			$id = isset($user['id']) ? $user['id'] : null;
			$model = User::findOrNew($id);
			$model->fill($user);
			$model->save();

			return response()->json(['user'=>$model], 200);
		});


		$app->delete('users', ['middleware'=> 'delete:User,1:2', function(Request $request) use ($app) {
			$resp = User::whereIn('id', $request->attributes->get('ids'))->delete();
			return response()->json(['message'=>'ok'], 200);
		}]);



		/* ROlES */
		$app->post('user-roles',  function(Request $request) use ($app) {

			$q = Role::query();

			_apply_filters($q);
			$q->where('id', '>=', $request->user->role_id);
			_apply_relation($q);
			_apply_sort($q, 'id');

			$items = $q->get();
			return response()->json(['items'=>$items], 200);

		});

		$app->get('user-role',  ['middleware' => 'one:UserRole',  function(){

		}]);

		$app->post('user-role', ['middleware' => 'edit:UserRole,1:2',  function(){}]); // edit disabled

		// $app->delete('user-roles', ['middleware'=> 'delete:UserRole,1:2',  function(){}]); // delete disabled



		/* CAMPAIGNS */
		$app->post('campaigns',  ['middleware' => 'list:Campaign', function(Request $request) use ($app) {

		}]);

		$app->get('campaign',  ['middleware' => 'one:Campaign', function(Request $request) use ($app) {

		}]);

		$app->post('campaign', ['middleware' => 'edit:Campaign,1:2', function(Request $request) use ($app) {

		}]);

		$app->delete('campaigns', ['middleware'=> 'delete:Campaign,1:2', function(Request $request) use ($app) {

		}]);




		$app->group(['prefix' => 'leads'], function () use ($app) {


			/* Leads */

			$app->get('forAdmin', function(Request $request) use($app) {
				set_time_limit(0);
				ini_set('max_execution_time', 10000);

				$for_buyers = $request->input('buyers', false);
				//DB::connection()->enableQueryLog();

				$q = DB::table('leads')
					->select('leads.*', 'campaign.name as campaign', 'users.name as agent', 'sources.source as source', 'lead_statuses.name as status','timelines.id as timeline_id', DB::raw('COALESCE(timelines.created_at, leads.updated_at) as modified'))
					->leftJoin('lead_statuses', 'lead_statuses.id', '=', 'leads.status_id')
					->leftJoin('users', 'users.id', '=', 'leads.user_id')
					->leftJoin('campaign', 'campaign.id', '=', 'leads.campaign_id')
					->leftJoin('sources', 'sources.id', '=', 'leads.source_id')
					->leftJoin(DB::raw('(timelines  INNER JOIN  (select max(t1.id) as id, t1.lead_id from timelines t1 group by t1.lead_id) temp ON temp.id = timelines.id)'), 'leads.id', '=', 'timelines.lead_id');


				_apply_filters($q);
				if( $filters = $request->input('filters') ) {
					is_array($filters) || $filters = json_decode($filters, true);

					foreach($filters as $filter) {
						$field = $filter['field'];
						$value = $filter['value'];
						if($field == 'text') {
							$q->where('leads.name', 'like', '%'.$value.'%')
								->orWhere('leads.phone', 'like', '%'.$value.'%');
						}
					}
				}


				$count = $q->count();

				_apply_sort($q, 'name');
				$q->orderBy('leads.name');


				list($per_page, $page) = _apply_paging($q);

				$pages = ceil($count/$per_page);
				$next = min($page+1, $pages);
				$last = $pages;
				$previous = max($page-1, 1);
				$first = 1;
				//$q->with('lastEvent.timelineable');
				$list = $q->get();

				$queries    = DB::getQueryLog();
				$last_query = end($queries);


				if(count($list)) {

					$ids = [];
					foreach($list as $item) {
						if(!$item->timeline_id) continue;
						$ids[] = $item->timeline_id;
					}
					$timelines = \App\Models\Timeline::with('timelineable')->whereIn('id', $ids)->get();;

					$tls_assorted = [];
					foreach($timelines as $timeline) {
						$tls_assorted[$timeline->lead_id] = $timeline;
					}


					foreach($list as $key => $item) {
						$item->headline = isset($tls_assorted[$item->id]) ? (isset($tls_assorted[$item->id]->timelineable) ? $tls_assorted[$item->id]->timelineable->headline : null) : null;
						$item->last_notes = isset($tls_assorted[$item->id]) ? (isset($tls_assorted[$item->id]->timelineable) ? $tls_assorted[$item->id]->timelineable->notes : null) : null;

					}
				}

				return response()->json(['items'=>$list, 'pagination'=>['rows'=>$count, 'pages'=>$pages, 'page'=>intval($page), 'first'=>$first, 'previous'=>$previous, 'next'=>$next, 'last'=>$last]], 200);

			});

			$app->get('leads', function(Request $request) use($app) {

				set_time_limit(0);
				ini_set('max_execution_time', 10000);

			    $q = \App\Models\Lead::query();

				_apply_filters($q);
				_apply_relation($q);
				_apply_sort($q, 'name');
				$count = $q->count();

				list($per_page, $page) = _apply_paging($q);

				$pages = ceil($count/$per_page);
				$next = min($page+1, $pages);
				$last = $pages;
				$previous = max($page-1, 1);
				$first = 1;
				//$q->with('lastEvent.timelineable');
				$list = $q->get();

				//relations:user,status,lastEvent.timelineable,campaign
				$relations = $request->input('relations');
				is_array($relations) || $relations = explode(',', $relations);
				if(in_array('lastEvent.timelineable', $relations) && count($list)) {
					$ids = array();
					foreach($list as $row) {
						$ids[] = $row->id;
					}
					$ids = implode(',', $ids);
					$tls = DB::select('select max(t1.id) as id, t1.lead_id from timelines t1 inner join (select lead_id, max(created_at) as MaxDate from timelines group by lead_id) t2 on t1.lead_id = t2.lead_id and t1.created_at = t2.MaxDate WHERE t1.lead_id IN('.$ids. ') group by t1.lead_id');

					$ids = [];
					foreach($tls as $tl) {
						$ids[] = $tl->id;
					}

					$timelines = \App\Models\Timeline::with('timelineable')->whereIn('id', $ids)->get();;

					$tls_assorted = [];
					foreach($timelines as $timeline) {
						$tls_assorted[$timeline->lead_id] = $timeline;
					}

					foreach($list as $key => $item) {
						$item->last_event = isset($tls_assorted[$item->id]) ? $tls_assorted[$item->id] : null;

					}
				}


				//dd(\DB::getQueryLog());

				return response()->json(['items'=>$list, 'pagination'=>['rows'=>$count, 'pages'=>$pages, 'page'=>$page, 'first'=>$first, 'previous'=>$previous, 'next'=>$next, 'last'=>$last]], 200);
			});

			$app->post('leads',  ['middleware' => 'list:Lead', function(){}]);

			$app->get('lead',  ['middleware' => 'one:Lead',  function(){}]);

			$app->post('lead', ['middleware' => 'edit:Lead,1:2:3:4',  function(){}]);

			$app->delete('lead', ['middleware'=> 'delete:Lead,1:2',  function(){}]);

			$app->post('import', function(Request $request) use ($app) {

				if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();

				$agents = $request->input('agents');
				$campaigns = $request->input('campaigns');
				$leads = $request->input('leads');
				//$source = $request->input('source');
				$source_id = $request->input('source');

				is_array($agents) || $agents = [$agents];
				is_array($campaigns) || $campaigns = [$campaigns];
				is_array($leads) || $leads = [$leads];

				//if(!count($agents)) return response()->json(['message'=>'No agents assigned'], 400);
				//if(!count($campaigns)) return response()->json(['message'=>'No campaigns assigned'], 400);
				if(!count($leads)) return response()->json(['message'=>'No leads assigned'], 400);

				$status = Lead::import($leads, $campaigns, $agents, $source_id);

				return response()->json(['message'=>'import ok'], 200);

			});

			$app->post('reassign', function(Request $request) use ($app) {
				if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();

				$agents = $request->input('agents');
				$campaigns = $request->input('campaigns');

				if(!count($agents)) return response()->json(['message'=>'No agents assigned'], 400);
				if(!count($campaigns)) return response()->json(['message'=>'No campaigns assigned'], 400);

				$lead_count = Lead::reassign($campaigns, $agents);

				return response()->json(['message'=> $lead_count.' leads reassigned'], 200);

			});

			$app->post('mass-edit', function(Request $request) use ($app) {
				if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();

				$ids = $request->input('ids');
				$agent_id = $request->input('agent_id');
				$campaign_id = $request->input('campaign_id');
				$status_id = $request->input('status_id');

				Lead::massEdit($ids, $agent_id, $campaign_id, $status_id);


				$q = DB::table('leads')
					->select('leads.*', 'campaign.name as campaign', 'users.name as agent', 'sources.source as source', 'lead_statuses.name as status','timelines.id as timeline_id', DB::raw('COALESCE(timelines.created_at, leads.updated_at) as modified'))
					->leftJoin('lead_statuses', 'lead_statuses.id', '=', 'leads.status_id')
					->leftJoin('users', 'users.id', '=', 'leads.user_id')
					->leftJoin('campaign', 'campaign.id', '=', 'leads.campaign_id')
					->leftJoin('sources', 'sources.id', '=', 'leads.source_id')
					->leftJoin(DB::raw('(timelines  INNER JOIN  (select max(t1.id) as id, t1.lead_id from timelines t1 group by t1.lead_id) temp ON temp.id = timelines.id)'), 'leads.id', '=', 'timelines.lead_id')
					->whereIn('leads.id', $ids);



				$list = $q->get();



				if(count($list)) {

					$ids = [];
					foreach($list as $item) {
						if(!$item->timeline_id) continue;
						$ids[] = $item->timeline_id;
					}
					$timelines = \App\Models\Timeline::with('timelineable')->whereIn('id', $ids)->get();;

					$tls_assorted = [];
					foreach($timelines as $timeline) {
						$tls_assorted[$timeline->lead_id] = $timeline;
					}


					foreach($list as $key => $item) {
						$item->headline = isset($tls_assorted[$item->id]) ? $tls_assorted[$item->id]->timelineable->headline : null;
						$item->last_notes = isset($tls_assorted[$item->id]) ? $tls_assorted[$item->id]->timelineable->notes : null;

					}
				}



				return response()->json(['message'=> 'ok', 'items'=>$list], 200);
			});

			$app->get('/', function(Request $request) use ($app) {


				return response()->json(['message'=>'list ok'], 200);
			});

			$app->get('auxs', function(Request $request) use($app) {
				$sources = Source::select('id', 'source')->orderBy('source')->get();
				$campaigns = Campaign::select('id', 'name', 'active')->orderBy('name')->get();
				$agents = User::select('id', 'name', 'active')->where('role_id', '>', 2)->orderBy('name')->get();

				return response()->json(['sources'=>$sources, 'campaigns'=>$campaigns, 'agents'=>$agents], 200);
			});

			/* TYPES */
			$app->post('types',  ['middleware' => 'list:LeadType', function(){}]);

			$app->get('type',  ['middleware' => 'one:LeadType',  function(){}]);

			$app->post('type', ['middleware' => 'edit:LeadType,1:2',  function(){}]);

			$app->delete('types', ['middleware'=> 'delete:LeadType,1:2',  function(){}]);

			/* STATUSES */
			$app->post('statuses',  ['middleware' => 'list:LeadStatus', function(){}]);

			$app->get('status',  ['middleware' => 'one:LeadStatus',  function(){}]);

			$app->post('status', ['middleware' => 'edit:LeadStatus,1:2',  function(){}]);

			$app->delete('statuses', ['middleware'=> 'delete:LeadStatus,1:2',  function(){}]);

		});


		/* NOTES */
		$app->post('notes',  ['middleware' => 'list:Note', function(){}]);

		$app->get('note',  ['middleware' => 'one:Note',  function(){}]);

		$app->post('note', ['middleware' => 'edit:Note',  function(){}]);

		$app->delete('notes', ['middleware'=> 'delete:Note',  function(){}]);


		/* SOURCES */
		$app->post('sources',  ['middleware' => 'list:Source', function(){}]);

		$app->get('source',  ['middleware' => 'one:Source',  function(){}]);

		$app->post('source', ['middleware' => 'edit:Source',  function(){}]);

		$app->delete('sources', ['middleware'=> 'delete:Source',  function(){}]);


		/* NOTIFICATIONS */
		$app->post('notifications',  function(Request $request) use ($app) {

			$user_id = $request->input('user_id');
			$user_id || $user_id = $request->user->id;

		    $q = \App\Models\Notification::query()
					->with(['status'=>function($q) use($user_id) {
						$q->where('user_id', $user_id);
						//$q->select('id', 'read');
					}]);

			_apply_filters($q);
			_apply_relation($q);
			_apply_sort($q, \App\Models\Notification::defaultSort());
			$count = $q->count();

			list($per_page, $page) = _apply_paging($q);



			$pages = ceil($count/$per_page);
			$next = min($page+1, $pages);
			$last = $pages;
			$previous = max($page-1, 1);
			$first = 1;

			$list = $q->get();
			return response()->json(['items'=>$list, 'pagination'=>['rows'=>$count, 'pages'=>$pages, 'page'=>$page, 'first'=>$first, 'previous'=>$previous, 'next'=>$next, 'last'=>$last]], 200);

		});

		$app->get('notification',  ['middleware' => 'one:Notification',  function(){}]);

		$app->post('notification', ['middleware' => 'edit:Notification',  function(){}]);

		$app->delete('notifications', ['middleware'=> 'delete:Notification',  function(){}]);

		$app->get('notifications/unread',  function(Request $request) use ($app) {

			$user_id = $request->input('user_id');
			$user_id || $user_id = $request->user->id;

			$notifications = \App\Models\Notification::whereHas('status', function($q) use($user_id) {
									$q->where('user_id', $user_id);

								},'<', 1)
								->orWhereHas('status', function($q)  use($user_id) {

									$q->where('user_id', $user_id);
									$q->where(function($q2) {
										$q2->whereNull('read')->orWhere('read', 0);
									});

								})
								->get();



			return response()->json(['notifications'=> $notifications], 200);

		});
		$app->post('notification/read', function(Request $request) use ($app) {

			$user_id = $request->input('user_id');
			$user_id || $user_id = $request->user->id;
			$id = $request->input('id');
			$read = $request->input('read');
			$read || $read = 1;



			if(!$id) return response()->json(['message'=>'No notification id'], 400);

			$user_notification = \App\Models\UserNotification::where('user_id', $user_id)
					->where('notification_id', $id)->first();

			$user_notification || $user_notification = new \App\Models\UserNotification;
			$user_notification->notification_id = $id;
			$user_notification->user_id = $user_id;
			$user_notification->read = $read;
			$user_notification->save();

			return response()->json(['message'=> ($read ? 'Read' : 'Unread')], 200);
		});





		/* SALES */
		$app->post('sales',  ['middleware' => 'list:Sale', function(){}]);
		$app->get('sales', function(Request $request) use ($app) {

			$q = DB::table('sales')
				->select('sales.*', 'leads.name as lead', 'users.name as agent', 'campaign.name as campaign')
				->leftJoin('leads', 'sales.lead_id', '=', 'leads.id')
				->leftJoin('users', 'sales.user_id', '=', 'users.id')
				->leftJoin('campaign', 'sales.campaign_id', '=', 'campaign.id');

			_apply_filters($q);
			if( $filters = $request->input('filters') ) {
				is_array($filters) || $filters = json_decode($filters, true);

				foreach($filters as $filter) {
					$field = $filter['field'];
					$value = $filter['value'];
					if($field == 'text') {
						$q->where('leads.name', 'like', '%'.$value.'%');
							//->orWhere('leads.phone', 'like', '%'.$value.'%');
					}
				}
			}

			$count = $q->count();

			list($per_page, $page) = _apply_paging($q);

			$pages = ceil($count/$per_page);
			$next = min($page+1, $pages);
			$last = $pages;
			$previous = max($page-1, 1);
			$first = 1;

			_apply_sort($q, 'sales.created_at');
			$q->orderBy('sales.created_at', 'DESC');

			$list = $q->get();


			return response()->json(['items'=>$list, 'pagination'=>['rows'=>$count, 'pages'=>$pages, 'page'=>intval($page), 'first'=>$first, 'previous'=>$previous, 'next'=>$next, 'last'=>$last]], 200);
		});
		$app->get('sale',  ['middleware' => 'one:Sale',  function(){}]);

		$app->post('sale', function(Request $request) use ($app) {

			//if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();

			$rules = \App\Models\Sale::postRules();
			$validator = \Validator::make($request->all(), $rules);
			if($validator->fails()) return response()->json(['errors'=>$validator->errors()->all()], 400);

			$item = $request->input('item');
			$id = isset($item['id']) ? $item['id'] : null;
			$model = \App\Models\Sale::findOrNew($id);
			$model->fill($item);
			$model->save();

			if(!$id) {
				$event = new \App\Models\Timeline;
				$event->lead_id = $model->lead_id;
				$event->user_id = $model->user_id;
				$model->timeline()->save($event);

				$lead = \App\Models\Lead::find($model->lead_id);
				$lead->type_id = 2; // Buyer
				$lead->status_id = 7; // Buyer
				$lead->save();
			}


			$model->campaign;
			$model->user;
			$model->lead;

			return response()->json(['item'=>$model], 200);

		});

		$app->delete('sales', ['middleware'=> 'delete:Sale',  function(){}]);


		/* SCHEDULES */
		$app->post('schedules',  ['middleware' => 'list:ScheduleCall', function(){}]);

		$app->get('schedule',  ['middleware' => 'one:ScheduleCall',  function(){}]);

		$app->post('schedule', ['middleware' => 'edit:ScheduleCall',  function(){}]);

		$app->delete('schedules', ['middleware'=> 'delete:ScheduleCall',  function(){}]);


		/* EVENTS */
		$app->post('timeline-events',  ['middleware' => 'list:TimelineEvent', function(){}]);

		$app->get('timeline-event',  ['middleware' => 'one:TimelineEvent',  function(){}]);

		$app->post('timeline-event', ['middleware' => 'edit:TimelineEvent',  function(){}]);

		$app->delete('timeline-events', ['middleware'=> 'delete:TimelineEvent',  function(){}]);


		/* TIMELINES */
		$app->post('timelines', function(Request $request) use ($app) {

			$user_id = $request->input('user_id');
			$user_id || $user_id = $request->user->id;

			if($user_id != $request->user->id) {
				if( !(validate_role($request->user, [1,2])) ) return User::unauthorized();
			}

			$query = \App\Models\Lead::where('user_id', $user_id);

			$query->whereNotIn('status_id', [3,4]);

			$query->with(['campaign' => function($q){
					$q->select('id', 'name');
				}])
				->with(['status' => function($q){
					$q->select('id', 'name');
				}])
				->with(['type' => function($q){
					$q->select('id', 'name');
				}])
				->with(['user' => function($q){
					$q->select('id', 'name');
				}])
				->with(['source' => function($q){
					$q->select('id', 'source');
				}]);

			_apply_filters($query);

			$items = $query->get();

			// Last events for items
			$ids = array();
			foreach($items as $row) {
				$ids[] = $row->id;
			}


			if(!count($ids)) {
				return response()->json(['items'=>[]], 200);
			}
			$ids = implode(',', $ids);
			$tls = DB::select('select max(t1.id) as id, t1.lead_id from timelines t1 inner join (select lead_id, max(created_at) as MaxDate from timelines group by lead_id) t2 on t1.lead_id = t2.lead_id and t1.created_at = t2.MaxDate WHERE t1.lead_id IN('.$ids. ') group by t1.lead_id');

			$ids = [];
			foreach($tls as $tl) {
				$ids[] = $tl->id;
			}

			$timelines = \App\Models\Timeline::with('timelineable')->whereIn('id', $ids)->get();;

			$tls_assorted = [];
			foreach($timelines as $timeline) {
				$tls_assorted[$timeline->lead_id] = $timeline;
			}

			foreach($items as $key => $item) {
				$item->last_event = isset($tls_assorted[$item->id]) ? $tls_assorted[$item->id] : null;

			}

			//$query->orderByRaw("field(status_id, 1, 5, 2)")->get();



			return response()->json(['items'=>$items], 200);
		});

		$app->post('schedules/pending', function(Request $request) use ($app) {


		    $q = App\Models\ScheduleCall::query();

			_apply_filters($q);
			_apply_relation($q);
			$count = $q->count();

			list($per_page, $page) = _apply_paging($q);



			$pages = ceil($count/$per_page);
			$next = min($page+1, $pages);
			$last = $pages;
			$previous = max($page-1, 1);
			$first = 1;

			$list = $q->get();
			return response()->json(['items'=>$list, 'pagination'=>['rows'=>$count, 'pages'=>$pages, 'page'=>$page, 'first'=>$first, 'previous'=>$previous, 'next'=>$next, 'last'=>$last]], 200);
		});



		$app->get('timeline',  ['middleware' => 'one:Timeline',  function(){}]);

/*
		$app->post('timeline', function(Request $request) use ($app) {

			$sale = $request->input('sale');
			$call = $request->input('call');
			$status = $request->input('status');
			$note = $request->input('note');
			$lead_id = $request->input('lead_id');
			$user_id = $request->input('user_id');

			return response()->json(['message'=>'ok'], 200);
		});

		$app->delete('timelines', ['middleware'=> 'delete:Timeline',  function(){}]);
*/


		/* CHATS */
		$app->post('chats',  ['middleware' => 'list:Chat', function(){}]);

		$app->get('chat',  ['middleware' => 'one:Chat',  function(){}]);

		$app->post('chat', ['middleware' => 'edit:Chat',  function(){}]);

		$app->delete('chats', ['middleware'=> 'delete:Chat',  function(){}]);


		/* AUXILIARY REFERENCES */
		$app->get('aux',  function(Request $request) use ($app) {
			$leadTypes = LeadType::get(['id', 'name']);
			$userRoles = UserRole::where('id', '>=', $request->user()->id)->get(['id', 'role']);
			$leadStatus = LeadStatus::get(['id', 'name']);

			return response()->json(['leadTypes'=>$leadTypes, 'userRoles'=>$userRoles, 'leadStatus' => $leadStatus], 200);

		});



		/* REPORTS */

		$app->get('reports/all-sales', function(Request $request) use($app){

			$q = DB::table('sales')
				->leftJoin('users', 'sales.user_id', '=', 'users.id')
				->leftJoin('leads', 'sales.lead_id', '=', 'leads.id');

			if($select = $request->input('select')) {
				$q->select(explode(',', $select));
			} else {
				$q->select('sales.*', 'users.name as user', 'leads.name as lead');
			}



			_apply_filters($q);
			_apply_sort($q, '-sales.created_at');
			_apply_paging($q);

			$rows = $q->get();

			return response()->json(['sales'=>$rows], 200);
		});

		$app->get('reports/agent-totals', function(Request $request) use($app){
			//DB::enableQueryLog();
			$q = DB::table('sales')
				->selectRaw('TRUNCATE(SUM(total), 2) as gt, user_id, users.name as user')
				->leftJoin('users', 'sales.user_id', '=', 'users.id')
				->groupBy('user_id')
				->orderBy('gt', 'ASC');

			if($start = $request->input('start') AND $end = $request->input('end')) {
				$q->whereRaw("sales.created_at BETWEEN '$start' AND '$end'");
			} else {
				$q->whereRaw('sales.created_at BETWEEN CURDATE()-INTERVAL 1 WEEK AND CURDATE()');
			}

			_apply_filters($q);
			_apply_sort($q, '+gt');
			_apply_paging($q);

			$rows = $q->get();

			return response()->json(['sales'=>$rows], 200);

/*
			SELECT user_id, SUM(total) as gt
	FROM sales
 	WHERE created_at BETWEEN CURDATE()-INTERVAL 1 WEEK AND CURDATE()
 	GROUP BY user_id
 	ORDER BY GT DESC LIMIT 3
*/
		});


		$app->get('reports/leads', function(Request $request) use($app){
			$group_by = $request->input('group_by');
			$only_active = $request->input('only_active') === 'true' ? true : false;

			$group_by || $group_by = 'user_id'; // user_id, campaign_id or source

			if($group_by == 'source') $group_by = 'source_id';

			$related = null;
			$rs = null;
			$unmatched = [
				'user_id' => '[ unasigned ]',
				'campaign_id' => '[ no campaign ]',
				'source_id' => '[ no source ]',
			];
			switch ($group_by) {
				case 'user_id':
					$q = DB::table('users')->select('id', 'name');
					if($only_active) $q->where('active', 1);
					$rs = $q->get();

					$field = 'name';
					break;

				case 'campaign_id':
					$q = DB::table('campaign')->select('id', 'name');
					if($only_active) $q->where('active', 1);
					$rs = $q->get();

					$field = 'name';
					break;

				case 'source_id':
					$q = DB::table('sources')->select('id', 'source');
					//if($only_active) $q->where('active', 1);
					$rs = $q->get();

					$field = 'source';
					break;

				default:
					$rs = null;
					break;
			}
			if($rs) {
				$related = [];
				foreach($rs as $row) {
					$related[$row->id] = $row->$field;
				}
			}

			$positions = [
				'6' => 2, //'New',
				'1' => 4, // 'Attempting Contact',
				'5' => 6, //'Pitched',
				'2'	=> 8, //'Schedule Callback',
				'3' => 10, //'DNC Not Interested',
				'4' => 12, //'DNC Wrong or Invalid Number',
			];


			$rs = DB::table('leads')
				->selectRaw("$group_by, COUNT(id) as total")
				->groupBy($group_by)
				->get();

			$ret = [];

			foreach($rs as $item) {

				if($only_active) {
					if($related AND !isset($related[$item->$group_by])) continue;
				}

				$ret[$item->$group_by] = [
					($related AND @$related[$item->$group_by]) ? $related[$item->$group_by] : $unmatched[$group_by], // 0
					$item->total // 1
				];
				foreach($positions as $id => $position) {
					$ret[$item->$group_by][$position] = 0;
					$ret[$item->$group_by][$position+1] = 0;
				}
			}

			$current_ids = array_keys($ret);

			$q = DB::table('leads')
				->selectRaw("lead_statuses.name as status, leads.status_id, $group_by, COUNT(leads.id) as partial")
				->leftJoin('lead_statuses', 'leads.status_id', '=', 'lead_statuses.id')
				->groupBy('status_id',$group_by);
			if(count($current_ids)) {
				$q->whereIn($group_by, $current_ids);
			}
			$rs = $q->get();

			foreach($rs as $item) {
				if(!isset($positions[$item->status_id])) continue;
				$total_position = $positions[$item->status_id];
				$percent_position = $total_position + 1;
				$total = $ret[$item->$group_by][1];
				$percent = $item->partial * 100 / $total;
				$ret[$item->$group_by][$total_position] = $item->partial;
				$ret[$item->$group_by][$percent_position] = number_format($percent,2);

				// init sales as 0;
				$ret[$item->$group_by][14] = '-';
				$ret[$item->$group_by][15] = '-';
			}


			$q = DB::table('leads')
				->selectRaw("$group_by, COUNT(id) as sales")
				->where('type_id', 2)
				->groupBy($group_by);
			if(count($current_ids)) {
				$q->whereIn($group_by, $current_ids);
			}
			$rs = $q->get();

			$total_position = 14;
			$percent_position = 15;
			foreach($rs as $item) {
				//var_dump($item);
				$total = $ret[$item->$group_by][1];
				$percent = $item->sales * 100 / $total;
				$ret[$item->$group_by][$total_position] = $item->sales;
				$ret[$item->$group_by][$percent_position] = number_format($percent,2);
			}
			$ret = array_values($ret);
			return response()->json(['sales'=>$ret], 200);
		});

	});













});




$app->get('gendata', function(){


/*
	for($i=1; $i<=4; $i++) {
		$campaign = new \App\Models\Campaign;
		$campaign->name = 'Campaign #'  . $i;
		$campaign->active = true;
		$start_year = 2014 + ($i-1);
		var_dump($start_year);
		$end_year = 2014 + $i+1;
		var_dump($end_year);
		$campaign->start_date = $start_year . '/01/01 00:00:00';
		$campaign->end_date = $end_year . '/12/01 00:00:00';
		$campaign->save();
	}
*/

	$campaign_ids = [1,2,3,4]; $campaign_ndx = 0;
	$agent_ids = [4, 6, 7, 8, 12]; $agent_ndx = 0;
	$sources = [1,2,3]; $source_ndx = 0;
	$tzs = [-1, -2, -3, -4, -5, -6, -7]; $tz_ndx = 0;
// asignacion de campañas, agentes, timezone y sources

/*
	$leads = \App\Models\Lead::get();
	foreach($leads as $lead) {
		$campaign_ndx = $campaign_ndx < (count($campaign_ids)-1) ? $campaign_ndx+1 : 0;
		$agent_ndx = $agent_ndx < (count($agent_ids)-1) ? $agent_ndx+1 : 0;
		$source_ndx = $source_ndx < (count($sources)-1) ? $source_ndx+1 : 0;
		$tz_ndx = $tz_ndx < (count($tzs)-1) ? $tz_ndx+1 : 0;

		$lead->campaign_id = $campaign_ids[$campaign_ndx];
		$lead->user_id = $agent_ids[$agent_ndx];
		$lead->source = 'Source #' . $sources[$source_ndx];
		$lead->timezone = $tzs[$tz_ndx];

		$is_buyer =  rand(0,9) < 5;
		if($is_buyer) {
			$lead->status_id = 5;
			$lead->type_id = 2;
		}
		$lead->save();
	}
*/

// generacion de ventas

/*
	$maxdays = 365*3;
	$now = '2017-03-18';
	set_time_limit(0);
	ini_set('max_execution_time', 10000);
	for($i = 0; $i < $maxdays; $i++) {

		$date = new DateTime($now);
		$interval = 'P'.$i.'D';

		$date->sub(new DateInterval($interval)); // P1D means a period of 1 day
		$sale_date = $date->format('Y-m-d H:i:s');
		$num_sales = rand(20,100);
		for($h = 0; $h < $num_sales; $h++) {
			$lead = \App\Models\Lead::where('type_id', 2)->inRandomOrder()->first();
			$sale = new \App\Models\Sale;
			$sale->user_id = $agent_ids[rand(0,4)];
			$sale->lead_id = $lead->id;
			$sale->created_at = $sale_date;
			$sale->updated_at = $sale_date;
			$sale->stock = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 3);
			$amount = rand(50, 500);
			$price = rand(7, 53)/10;
			$sale->amount = $amount;
			$sale->price = $price;
			$sale->total = $amount * $price;
			$sale->save();

			$timeline = new \App\Models\Timeline;
			$timeline->lead_id = $sale->lead_id;
			$timeline->user_id = $sale->user_id;
			$timeline->created_at = $sale_date;
			$timeline->updated_at = $sale_date;
			$sale->timeline()->save($timeline);


		}
	}
*/

/*
	$lead = \App\Models\Lead::inRandomOrder()->first();
	var_dump($lead->id);
*/

});
