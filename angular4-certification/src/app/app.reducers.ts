import { ActionReducerMap } from '@ngrx/store';

import * as fromProducts from './store/products/products.reducers';
import * as fromUsers from './store/users/users.reducers';
import * as fromShared from './store/shared/shared.reducers';

import * as fromCart from './public/store/cart.reducers';


export interface AppState {
  products: fromProducts.State;
  users: fromUsers.State;
  cart: fromCart.State;
  shared: fromShared.State;
}

export const reducers: ActionReducerMap<any> = {
  products: fromProducts.ProductsReducers,
  users: fromUsers.UsersReducers,
  cart: fromCart.CartReducers,
  shared: fromShared.SharedReducers
};
