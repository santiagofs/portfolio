import { Product } from '../../store/products/product.model';

export class CartItem {
    constructor(
        public product: Product,
        public qty: number,
        ) {}
}