import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromProducts from '../../../store/products/products.reducers';
import * as fromApp from '../../../app.reducers'

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  productsState: Observable<fromProducts.State>;

  constructor(private router: Router,
      private route: ActivatedRoute,
      private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.productsState = this.store.select('products');
    this.store.select('products').subscribe(data => console.log(data))
  }

}