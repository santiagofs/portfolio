import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRouting }   from './public.routing';
import { PublicComponent }   from './public.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { ProductDetailComponent } from './products/product-detail/product-detail.component';
import { ProfileComponent } from './user/profile/profile.component';
import { OrdersComponent } from './user/orders/orders.component';
import { CartComponent } from './order/cart/cart.component';
import { ReviewComponent } from './order/review/review.component';
import { BillingComponent } from './order/billing/billing.component';
import { PaymentComponent } from './order/payment/payment.component';
import { SuccessComponent } from './order/success/success.component';
import { ErrorComponent } from './order/error/error.component';
import { UserComponent } from './user/user.component';
import { OrderComponent } from './order/order.component';
import { ProductListItemComponent } from './products/product-list/product-list-item/product-list-item.component';
import { AddToCartComponent } from './products/add-to-cart/add-to-cart.component';

@NgModule({
  imports: [ 
    CommonModule,
    PublicRouting 
  ],
  declarations: [
    PublicComponent,
    ProductListComponent,
    ProductDetailComponent,
    ProfileComponent,
    OrdersComponent,
    CartComponent,
    ReviewComponent,
    BillingComponent,
    PaymentComponent,
    SuccessComponent,
    ErrorComponent,
    UserComponent,
    OrderComponent,
    ProductListItemComponent,
    AddToCartComponent
  ]
})
export class PublicModule { }
