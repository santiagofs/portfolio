import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as UsersActions from '../../../store/users/users.actions';
import * as fromUsers from '../../../store/users/users.reducers';
import * as fromApp from '../../../app.reducers';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  usersState: Observable<fromUsers.State>;

  constructor(private router: Router,
      private route: ActivatedRoute,
      private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.usersState = this.store.select('users');
  }

  onRemove(id) {
    this.store.dispatch(new UsersActions.UsersDelete(id));
  }
}
