import { AdminGuard } from './../store/shared/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';

import { OrdersReviewComponent } from './orders/orders-review/orders-review.component';
import { OrdersListComponent } from './orders/orders-list/orders-list.component';
import { OrdersComponent } from './orders/orders.component';
import { ProductsEditComponent } from './products/products-edit/products-edit.component';
import { ProductsListComponent } from './products/products-list/products-list.component';
import { ProductsComponent } from './products/products.component';
import { UsersEditComponent } from './users/users-edit/users-edit.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { UsersComponent } from './users/users.component';


const routes: Routes = [
  { path: 'users', component: UsersComponent, children: [
      { path: '', redirectTo: 'list'},
      { path: 'list', component: UsersListComponent },
      { path: 'edit/:id', component: UsersEditComponent },
    ],
    canActivate: [AdminGuard]
  },
  { path: 'products', component: ProductsComponent , children: [
    { path: '', redirectTo: 'list'},
    { path: 'list', component: ProductsListComponent },
    { path: 'edit/:id', component: ProductsEditComponent },
  ]},
  { path: 'orders', component: OrdersComponent , children: [
    { path: '', redirectTo: 'list'},
    { path: 'list', component: OrdersListComponent },
    { path: 'review/:id', component: OrdersReviewComponent },
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRouting {}
