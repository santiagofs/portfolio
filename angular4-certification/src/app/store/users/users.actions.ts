import { Action } from '@ngrx/store';

import { User } from './user.model';

export const USERS_FETCH = 'USERS_FETCH';
export const USERS_SET = 'USERS_SET';

export const USERS_SAVE = 'USERS_SAVE';
export const USERS_ADD = 'USERS_ADD';
export const USERS_UPDATE = 'USERS_UPDATE';

export const USERS_DELETE = 'PRODUCS_DELETE';
export const USERS_ONDELETE = 'PRODUCS_ONDELETE';

export class UsersFetch implements Action {
  readonly type = USERS_FETCH;
}

export class UsersSet implements Action {
  readonly type = USERS_SET;
  constructor(public payload: User[]) {}
}

export class UsersSave implements Action {
  readonly type = USERS_SAVE;
  constructor(public payload: User) {}
}

export class UsersAdd implements Action {
  readonly type = USERS_ADD;
  constructor(public payload: User) {}
}

export class UsersUpdate implements Action {
  readonly type = USERS_UPDATE;
  constructor(public payload: User) {}
}

export class UsersDelete implements Action {
  readonly type = USERS_DELETE;
  constructor(public payload: number) {}
}
export class UsersOnDelete implements Action {
  readonly type = USERS_ONDELETE;
  constructor(public payload: number) {}
}

export type UsersActions = UsersFetch |
    UsersSet |
    UsersSave |
    UsersAdd |
    UsersUpdate |
    UsersDelete |
    UsersOnDelete;
