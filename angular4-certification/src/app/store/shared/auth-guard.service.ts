import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromApp from '../../app.reducers';
import * as fromShared from './shared.reducers';

@Injectable()
export class UserGuard implements CanActivate {

  constructor(private store: Store<fromApp.AppState>, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select('shared')
      .take(1)
      .map((sharedState: fromShared.State) => {
        if ((sharedState.loggedUser !== null)) {
          return true;
        } else {
          if (sharedState.loggedUser !== null) {
            this.router.navigate(['/']);
          } else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
          }
          return false;
        }
      });
  }
}

@Injectable()
export class ManagerGuard implements CanActivate {

  constructor(private store: Store<fromApp.AppState>, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select('shared')
      .take(1)
      .map((sharedState: fromShared.State) => {
        if ((sharedState.loggedUser !== null) && (sharedState.loggedUser.role_id >= 10)) {
          return true;
        } else {
          if (sharedState.loggedUser !== null) {
            this.router.navigate(['/']);
          } else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
          }
          return false;
        }
      });
  }
}

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private store: Store<fromApp.AppState>, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select('shared')
      .take(1)
      .map((sharedState: fromShared.State) => {
        if ((sharedState.loggedUser !== null) && (sharedState.loggedUser.role_id >= 100)) {
          return true;
        } else {
          if (sharedState.loggedUser !== null) {
            this.router.navigate(['/']);
          } else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
          }
          return false;
        }
      });
  }
}
