import { Action } from '@ngrx/store';

import { Product } from './product.model';

export const PRODUCTS_FETCH = 'PRODUCTS_FETCH';
export const PRODUCTS_SET = 'PRODUCTS_SET';

export const PRODUCTS_SAVE = 'PRODUCTS_SAVE';
export const PRODUCTS_ADD = 'PRODUCTS_ADD';
export const PRODUCTS_UPDATE = 'PRODUCTS_UPDATE';

export const PRODUCTS_DELETE = 'PRODUCS_DELETE';
export const PRODUCTS_ONDELETE = 'PRODUCTS_ONDELETE';

export class ProductsFetch implements Action {
  readonly type = PRODUCTS_FETCH;
}

export class ProductsSet implements Action {
  readonly type = PRODUCTS_SET;
  constructor(public payload: Product[]) {}
}

export class ProductsSave implements Action {
  readonly type = PRODUCTS_SAVE;
  constructor(public payload: Product) {}
}

export class ProductsAdd implements Action {
  readonly type = PRODUCTS_ADD;
  constructor(public payload: Product) {}
}

export class ProductsUpdate implements Action {
readonly type = PRODUCTS_UPDATE;
  constructor(public payload: Product) {}
}

export class ProductsDelete implements Action {
  readonly type = PRODUCTS_DELETE;
  constructor(public payload: number) {}
}
export class ProductsOnDelete implements Action {
  readonly type = PRODUCTS_ONDELETE;
  constructor(public payload: number) {}
}

export type ProductsActions = ProductsFetch |
    ProductsSet |
    ProductsAdd |
    ProductsUpdate |
    ProductsDelete |
    ProductsOnDelete;