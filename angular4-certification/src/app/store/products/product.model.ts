export class Product {
    public id = 0;
    public name: string = null;
    public description: string = null;
    // public img_url: string = null,
    public price = 0;
    // public createdAt

    constructor(data?: any) {
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.description = data.description;
            this.price = data.price;
        }
    }

    get img_url():string {
        return `https://unsplash.it/600/450?image=${this.id}`;
    }
    set img_url(value:string) {

    }
  }