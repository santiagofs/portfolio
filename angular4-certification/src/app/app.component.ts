import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as SharedActions from './store/shared/shared.actions';
import * as fromShared from './store/shared/shared.reducers';
import * as ProductsActions from './store/products/products.actions';

import * as fromCart from './public/store/cart.reducers';
import { CartItem } from './public/store/cart-item.model';

import * as fromApp from './app.reducers';

import { User } from './store/users/user.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  loggedUser: User;
  cart: CartItem[];

  constructor(
    private store: Store<fromApp.AppState>,
    private router: Router
  ) {}

  ngOnInit() {
    this.store.dispatch(new ProductsActions.ProductsFetch())

    this.store.select('shared').subscribe((sharedState:fromShared.State) => {
      this.loggedUser = sharedState.loggedUser;
    });

    this.store.select('cart').subscribe((cartState:fromCart.State) => {
      this.cart = cartState.cart;
    });

  }
  
  onLogout() {
    this.router.navigate(['/']);
    this.store.dispatch(new SharedActions.SharedLogout());
  }
}