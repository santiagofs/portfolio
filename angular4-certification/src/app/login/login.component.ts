import { Actions } from '@ngrx/effects';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromApp from '../app.reducers'
import * as fromShared from '../store/shared/shared.reducers';
import * as SharedActions from '../store/shared/shared.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loginError: string = null;
  returnUrl: string;
  
  constructor(
    private store: Store<fromApp.AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private actions$: Actions
    ) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.store.select('shared').subscribe((sharedState:fromShared.State) => {
      this.loginError = sharedState.loginErrorMessage;
    });

    this.actions$
      .ofType(SharedActions.SHARED_LOGIN_SUCCESS)
      .do(() => {
        this.router.navigate([this.returnUrl]);
      }).subscribe();
  }

  onLogin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;

    this.store.dispatch(new SharedActions.SharedLoginRequest({username: email, password: password}));
  }
}
