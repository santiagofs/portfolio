angular.module('app.fields')
    .directive('inputAddress', ['StatesModel', 'CountriesModel', 'X2JS', '$http', function (StatesModel, CountriesModel, X2JS, $http) {
        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'common/directives/fields/input-address.tpl.html',
            transclude:true,
            require: '^form',
            scope: {
                model:'=',
                country:'@',
                required:'@',
				withCountry: '=',
				countriesList: '='
            },
            link: function ($scope, $elem, $attrs, form) {

	            if($scope.withCountry !== true) {
		            $scope.country = 'US';
				}

				$scope.countries = $scope.countriesList ? $scope.countriesList : CountriesModel.get(['USA', 'CAN']);
	            
	            //console.log($scope.countries);
	            $scope.states = [];
	            var getStates = function() {
		            if(!$scope.model.Country) return [];
		            $scope.states = StatesModel.get($scope.model.Country.toLowerCase());
	            };
                
                $scope.$watch(function(){
	            	return $scope.model ? $scope.model.Country : null;
	            }, function(newValue, oldValue){
	                if(oldValue !== newValue) {
		                console.log(newValue);
		                getStates();
		            }
                });
                
                $scope.addressByZip = function() {
	                if(($scope.model.Zip && $scope.model.Zip.length < 5 ) ||
                        ($scope.model.Country && $scope.model.Country !== 'USA') ||
                        // not a number
                        !(!isNaN(parseFloat($scope.model.Zip)) && isFinite($scope.model.Zip)))  {
	                    return false;
                    }
	                
	                $http.get('https://maps.googleapis.com/maps/api/geocode/json', {params: {
						//address: $scope.model.Zip,
						components: 'country:' + $scope.model.Country + '|postal_code:'+$scope.model.Zip,
		                sensor: true
		            }, withCredentials: false}).then(function(rsp){

			            var results = rsp.data.results;
						if(!results.length) return false;
						console.log(results);

			            var comps = results[0].address_components;
			            var components = {};
			            angular.forEach(comps, function(e,i,a){
				            components[e.types[0]] = {
					            long_name: e.long_name,
					            short_name: e.short_name
				            };
			            });
			            $scope.model.City = components.locality.long_name;
						$scope.model.State = components.administrative_area_level_1.short_name;
			        	
		            }, function(err){
			            
		            });


                };
               
                
/*
                var xmlText = "<MyRoot><test>Success</test><test2><item>val1</item><item>val2</item></test2></MyRoot>";
				var jsonObj = X2JS.xml_str2json( xmlText );

                console.log(jsonObj);
                
                jsonObj.MyRoot._ID = 1;
                
                var xml = X2JS.json2xml_str(jsonObj);
                console.log(xml);
*/
                
            }
        };
    }]);
