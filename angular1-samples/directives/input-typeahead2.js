angular.module('app.fields')
    .directive('inputTypeahead2', [function () {
        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'common/directives/fields/input-typeahead2.tpl.html',
            transclude: true,
            require: '?ngModel',
            scope: {
                source: '=',
                display: '=',
                onSelectItem: '=',
                format: '=',
                filter: '=',
                multiple: '@',
                placeholder: '@',
                tagDisplay: '@',
                trackby: '@',
            },
            link: function ($scope, $elem, $attrs, ngModel) {
                $scope.list = [];
                $scope.selected = null;
                $scope.search = '';
                $scope.searching = false;

                if (!$scope.multiple) {
                    $scope.multiple = false;
                }
                if ($scope.multiple) {
                    $scope.item = [];
                } else {
                    $scope.item = undefined;
                }

                if (!$scope.tagDisplay) {
                    $scope.tagDisplay = $scope.display;
                }

                function traverseList(mode, fromList) {
                    var selected = 0;
                    if (fromList) {
                        if (mode === 'up') {
                            selected = $scope.selected - 1;
                        } else {
                            selected = Math.min($scope.list.length - 1, $scope.selected + 1);
                        }
                        if (selected < 0) {
                            selected = null;
                            $('[search-input]', $elem).focus();
                        }
                    } else {
                        $('[results-list]', $elem).focus();
                        if (mode === 'up') {
                            selected = $scope.list.length - 1;
                        } else {
                            selected = 0;
                        }
                    }
                    $scope.selected = selected;
                }

                $scope.getDisplay = function (e) {
                    if (e === undefined) return e;

                    if (angular.isFunction($scope.display)) {
                        return $scope.display(e);
                    } else {
                        return e[$scope.display];
                    }
                };

                $scope.getValue = function (e) {
                    if (angular.isFunction($scope.format)) {
                        return $scope.format(e);
                    } else {
                        return e;
                    }
                };

                $scope.onKeyDown = function (e, fromList) {
                    switch (e.which) {
                        case 13:
                            e.preventDefault();
                            if (fromList) {
                                $scope.selectItem($scope.list[$scope.selected]);
                            }
                            break;

                        case 38:
                            e.preventDefault();
                            traverseList('up', fromList);
                            break;

                        case 40:
                            e.preventDefault();
                            traverseList('down', fromList);
                            break;
                    }
                };


                $scope.onChange = function () {
                    if (!$scope.multiple) {
                        $scope.item = null;
                    }

                    if ($scope.search.length > 1) {
                        $scope.searching = true;
                        $scope.source($scope.search, $scope.item).then(function (items) {

                            if ($scope.filter) {
                                $scope.list = items.filter(function (e, i, a) {
                                    return $scope.getDisplay(e).toLowerCase().indexOf($scope.search.toLowerCase()) !== -1;
                                });
                            } else {
                                $scope.list = items;
                            }

                            if ($scope.multiple && $scope.trackby) {
                                var compare = $scope.item.map(function (e, i, a) {
                                    return e[$scope.trackby];
                                });
                                $scope.list = $scope.list.filter(function (e, i, a) {
                                    return compare.indexOf(e[$scope.trackby]) === -1;
                                });
                            }

                            $scope.searching = false;
                        });

                    } else {
                        $scope.list = [];
                    }
                };

                $scope.selectItem = function (item) {
                    $scope.list = [];

                    $('[search-input]', $elem).focus();
                    if ($scope.multiple) {
                        $scope.item.push(item);
                        $scope.search = '';
                    } else {
                        $scope.search = $scope.getDisplay(item);
                        $scope.item = item;
                    }
                    if ($scope.onSelectItem) {
                        $scope.onSelectItem(item);
                    }
                };

                $scope.removeItem = function (index) {
                    $scope.item.splice(index, 1);
                };

                $scope.onBlur = function(e) {
                    ngModel.$setTouched();
                };

                if (ngModel) {

                    ngModel.$render = function () {
                        $scope.item = ngModel.$viewValue;
                        $scope.search = $scope.multiple ? '' : $scope.getDisplay($scope.item);
                    };

                    $scope.$watch(function () {
                        return ngModel.$viewValue;
                    }, function (oldValue, newValue) {
                        /*
                         $scope.item = ngModel.$viewValue;
                         if($scope.item) {
                         //$scope.search = $scope.multiple ? '' : $scope.getDisplay($scope.item);
                         }
                         */
                        if (newValue !== oldValue) {
                            //console.log('value change');
                            $scope.item = ngModel.$viewValue;
                            if ($scope.item) {
                                $scope.search = $scope.multiple ? '' : $scope.getDisplay($scope.item);
                            }
                        }
                    });

                    $scope.$watch('item', function (item) {
                        ngModel.$setViewValue($scope.getValue(item));

                        // IF REQUIRED
                        if ($attrs.required && $attrs.required !== 'false') {
                            ngModel.$setValidity('required', !!(item && Object.keys(item).length > 0));
                        }

                    }, true);
                }


            }
        };
    }]);
