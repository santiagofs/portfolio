angular.module('app.fields')
    .directive('inputContacts', ['ContactInformationModel',function (ContactInformationModel) {
        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'common/directives/fields/input-contacts.tpl.html',
            transclude:true,
            require: '^form',
            scope: {
                model:'=',
                contacts:'=',
            },
            link: function ($scope, $elem, $attrs, form) {
                $scope.form = form;
                $scope.required = $elem.attr('required') !== undefined;
                
                $scope.contactTypes = $attrs.contactTypes || ContactInformationModel.types();

                $scope.addContact = function() {
                    var sample = ContactInformationModel.empty();
                    $scope.contacts.push(sample);
                };

                $scope.removeContact = function(ndx) {
                    $scope.contacts.splice(ndx, 1);
                };
                
                $scope.$watch(function() {
                    return $scope.contacts;
                }, function(newValue, oldValue) {
                   
                    if($scope.required && $scope.contacts.length === 0) {
		                var sample = ContactInformationModel.empty();
	                    $scope.contacts.push(sample);
	                }
                    
                }, true);
            }
        };
    }]);
