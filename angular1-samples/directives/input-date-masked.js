angular.module('app.fields')
    .directive('inputDateMasked', [function() {

        function setCaretPosition(elem, caretPos) {
            if (elem !== null) {
                if (elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.move('character', caretPos);
                    range.select();
                } else {
                    if (elem.setSelectionRange) {
                        elem.focus();
                        elem.setSelectionRange(caretPos, caretPos);
                    } else
                        elem.focus();
                }
            }
        }

        function replaceValue(elem, position, char) {
            var current = elem.value;
            //console.log('current', current);
            var newval = current.substr(0,position) + char + current.substr(position+1);
            //elem.value = newval;
            return newval;
        }


        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'common/directives/fields/input-date-masked.tpl.html',
            transclude: true,
            //require: '^form',
            scope: {
                label: '@',
                date: '=',
                displayErrors: '=?',
                onChange: '&',
                updateOnChange: '=?'
            },
            link: function($scope, $elem, $attrs, form) {
                $scope.required = $elem.attr('required') !== undefined;

                if($scope.displayErrors === undefined) $scope.displayErrors = true;
                if($scope.updateOnChange === undefined) $scope.updateOnChange = false;

                var npts = {
                    day: $elem.find('.day'),
                    month: $elem.find('.month'),
                    year: $elem.find('.year')
                };

                var nptDay = $elem.find('.day');
                var nptMonth = $elem.find('.month');
                var nptYear = $elem.find('.year');

                var actionKeys = [46, 9, 27, 13, 110];
                var deniedKeys = [32];

                function setDate() {
                    var oldValue = moment($scope.date);
                    var year = $scope.parsed.year,
                        month = $scope.parsed.month,
                        day = $scope.parsed.day;

                    //if(!month || !year || !day) return;
                    var m = moment(year + '-' + month + '-' + day, 'YYYY-MM-DD');

                    //if (year && month && day) {
                        
                        if (m == null || !m.isValid()) {
                            // add form validation here
                            $scope.dateForm.year.$setValidity('fulldate', false);
                            if($scope.updateOnChange) {
                                $scope.date = m.format('YYYY-MM-DD');
                            }
                        } else {
                            $scope.dateForm.year.$setValidity('fulldate', true);
                            $scope.date = m.format('YYYY-MM-DD');
                        }
                        if($scope.onChange !== undefined) {

                            $scope.onChange({newValue: m, oldValue: oldValue});
                        }
                        $scope.$apply();
                    //}
                }

                function handleKeyPress(ev, nptName, npts) {

                    var code = ev.keyCode;
                    var npt = npts[nptName][0];
                    var caret = npt.selectionStart;

                    var next = nptName==='month' ? npts.day[0] : (nptName==='day'? npts['year'][0] : null);
                    var prev = nptName==='year' ? npts.day[0] : (nptName==='day'? npts['month'][0] : null);

                    if(ev.shiftKey) return false; // prevent shift key
                    if(ev.altKey) return false; // prevent alt key

                    if(actionKeys.indexOf(code) !== -1) return;
                    // handle arrows and backspace

                    if((code >= 35 && code <= 39) || code === 8) {
                        if((code === 39) && (caret > 0) && next) {
                            setCaretPosition(next,0);
                            ev.preventDefault();
                            return;
                        }

                        if((code === 37)  && (caret === 0) && prev) {
                            setCaretPosition(prev,2);
                            ev.preventDefault();
                            return false;
                        }
                        if((code === 8)  && (caret === 0) && (npt.selectionEnd === caret) && prev) {
                            setCaretPosition(prev,2);
                            return false;
                        }
                        return;
                    }

                    ev.preventDefault();
                    if(!((code >= 48 && code <= 57) || (code >= 96 && code <= 105)) || (deniedKeys.indexOf(code) !== -1)) {
                        return false;
                    }

                    if(code >= 96 && code <= 105) {
                        code = code - 48; // hack, porque soy como Carlos Calvo!!
                    }

                    var char = String.fromCharCode(code);

                    if(caret > 1 && next) {
                        var nextVal = replaceValue(next, 0, char);
                        $scope.dateForm[next.name].$setViewValue(nextVal);
                        $scope.dateForm[next.name].$render();
                        setCaretPosition(next,1);
                        return false;
                    }
                    if(caret > 3 && next === null) {
                        return false;
                    }

                    var nptVal = replaceValue(npt, caret, char);
                    npt.value = nptVal;
                    $scope.dateForm[nptName].$setViewValue(nptVal);
                    //$scope.dateForm[nptName].$render();
                    //$scope.parsed[nptName] = nptVal;

                    if(nptName === 'year') {

                    }
                    $scope.parsed[nptName] = npt.value;
                    if(next && caret ===1) {
                        setCaretPosition(next,0);
                    } else {
                        setCaretPosition(npt,caret+1);
                    }
                    // $scope.$apply();
                    // setDate();
                }

                nptMonth.bind('keydown', function(ev){
                    handleKeyPress(ev, 'month', npts);
                });

                nptDay.bind('keydown', function(ev){
                    handleKeyPress(ev, 'day', npts);
                });

                nptYear.bind('keydown', function(ev){
                    handleKeyPress(ev, 'year', npts);
                });

                nptYear.bind('blur', function(ev){
                    var val = $scope.parsed.year;
                    if(val && val !== '' && val.toString().length < 4) {
                        $scope.dateForm.year.$setValidity('format', false);
                    } else {
                        $scope.dateForm.year.$setValidity('format', true);
                    }
                    if(val && val !== '' && (val < 1900 || val > 2100)) {
                        $scope.dateForm.year.$setValidity('range', false);
                    } else {

                        $scope.dateForm.year.$setValidity('range', true);
                    }
                    $scope.$apply();
                    setDate();
                });
                nptMonth.bind('blur', function(ev){
                    setDate();
                });
                nptDay.bind('blur', function(ev){
                    setDate();
                });



                function parseDate() {
					var m = moment($scope.date, 'YYYY-MM-DD');
					if (m == null || !m.isValid()) {
	                    //m = moment('1970-01-01', 'YYYY-MM-DD');
	                    $scope.parsed = {
		                    year: '',
		                    month: '',
		                    day: ''
		                };

					} else {
						$scope.parsed = {
							year: m.year(),
							month: m.month() + 1,
							day: m.date()
						};
					}
				}
                parseDate();


                $scope.$watch(function(){
                    return $scope.date;
                }, function(newValue, oldValue){

                    if (newValue !== oldValue) {
                        parseDate();
                        //getRanges();
                    }
                });
            }
        };
    }]);
