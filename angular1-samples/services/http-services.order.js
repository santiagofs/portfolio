/* globals angular */
angular.module('app.http-services')
    .factory('OrderHttpService', ['$http', '$q', '$site-config', '$window', 'OrderModel', function ($http, $q, $config, $window, OrderModel) {

        var wpService = $window.ajaxurl,
            portalService = $config.PORTAL_API,
            peopleService = $config.PEOPLE_API;

        var saveOrder = function (data) {
            var deferred = $q.defer(),
                service = portalService + 'order';

            function success(res) {
                deferred.resolve(res.data);
            }

            function error(res) {
                deferred.reject(res);
            }

            if (!data.Id) {
                $http.post(service, data, {withCredentials: true}).then(success, error);
            } else {
                $http.put(service, data, {withCredentials: true}).then(success, error);
            }

            return deferred.promise;
        };

        var getOrder = function (orderId) {
            var deferred = $q.defer(),
                service = portalService + 'order/' + orderId;

            function success(res) {
                deferred.resolve(res.data);
            }

            function error(res) {
                deferred.reject(res);
            }

            $http.get(service, {withCredentials: true}).then(success, error);

            return deferred.promise;
        };

        var providers = function (account) {
            var deferred = $q.defer(),
                service = portalService + 'order/orderingProviders/list/' + account;

            function success(res) {
                deferred.resolve(res.data);
            }

            function error(res) {
                deferred.reject(res);
            }

            $http.get(service, {withCredentials: true}).then(success, error);


            return deferred.promise;
        };
        
        var addOnQualification = function (patientId) {
            var deferred = $q.defer(),
                service = portalService + 'order/addOn/qualification/' + patientId;

            function success(res) {
                deferred.resolve(res.data);
            }

            function error(res) {
                deferred.reject(res);
            }

            $http.get(service, {withCredentials: true}).then(success, error);


            return deferred.promise;
        };
        
        var isCustomTest = function (testCodes) {
	        var deferred = $q.defer(),
				service = portalService + 'references/tests/validate';
				
			//var dummies = ['906-A2ML1', '906-AAAS', '906-AARS2', '906-ABCA12', '906-ABCA4'];
			// WE ARE ONLY SUPPORTING ONE TEST AT THE TIME RIGHT NOW!!
			// TODO: REVIEW THE CODE ON THE DEL/DUP VARIANT TO HANDLE MULTIPLE CODES AT THE SAME TIME
			if(!Array.isArray(testCodes)) testCodes = [testCodes];
			
			var data = {
				TestCodes: testCodes
			};
			
            function success(res) {
	            // WE ARE ONLY SUPPORTING ONE TEST AT THE TIME RIGHT NOW!!
				// TODO: REVIEW THE CODE ON THE DEL/DUP VARIANT TO HANDLE MULTIPLE CODES AT THE SAME TIME
				try {
					deferred.resolve(!res.data.Data.Results[0].Valid);
				} catch(e) {
					console.log(res);
				}
	           
                //deferred.resolve(dummies.indexOf(testCode) === -1);
            }

            function error(res) {
                deferred.reject(res);
            }
            
            // call to the api
            //api/references/tests/validate
			$http.post(service, data, {withCredentials: true}).then(success, error);
			
            return deferred.promise;
        };


        var requisition = function (orderId) {
            var deferred = $q.defer(),
                service = portalService + 'order/requisition/' + orderId;

            function success(res) {
                deferred.resolve(res.data);
            }

            function error(res) {
                deferred.reject(res);
            }

            $http.get(service, {withCredentials: true, responseType: 'arraybuffer'}).then(success, error);

            return deferred.promise;
        };

        var saveDraft = function (data) {
            data.OrderType = OrderModel.orderType.draft; // set to draft;
            return saveOrder(data);
        };

        var placeOrder = function (data) {
            data.OrderType = OrderModel.orderType.orderNow; // set to Order now
            return saveOrder(data);
        };
        
        var placeConsult = function (data) {
            data.OrderType = OrderModel.orderType.waitForApproval; // set to wait for approval
            return saveOrder(data);
        };
        
        var placeBi = function (data) {
            data.OrderType = OrderModel.orderType.BI; // set to BI
            return saveOrder(data);
        };
        
        
        

        var getDraft = function (orderId) {

        };

        return {
            saveDraft: saveDraft,
            saveOrder: saveOrder,
            placeOrder: placeOrder,
            placeConsult: placeConsult,
            placeBi: placeBi,
            getOrder: getOrder,
            providers: providers,
            requisition: requisition,
            addOnQualification: addOnQualification,
            isCustomTest: isCustomTest,
        };
    }]);
