/* globals angular */
angular.module('app.http-services')
    .factory('PatientService', ['$http', '$q', '$site-config', '$window', 'UserService', function ($http, $q, $config, $window, UserService) {

    var wpService = $window.ajaxurl,
        portalService = $config.PORTAL_API,
        peopleService = $config.PEOPLE_API;

    function query(accountNumbers) {
        var deferred = $q.defer(),
            service = portalService + 'patient/search/summary';

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        var data = {
            AccountNumbers: accountNumbers ? accountNumbers : UserService.getUser().accounts
        };

        $http.post(service, data).then(success, error);
        return deferred.promise;
    }

    function get(searchTerm, pageNumber, pageSize, typeahead, accountNumbers) {
        var deferred = $q.defer(),
            service = portalService + 'patient/search',
            pageNm = pageNumber ? pageNumber : 1,
            pageSz = pageSize ? pageSize : 20;

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        var data = {
            AccountNumbers: accountNumbers ? accountNumbers : UserService.getUser().accounts,
            SearchTerm: searchTerm,
            FirstCharLastNameOnly: !typeahead,
            PageNumber: pageNm,
            PageSize: pageSz
        };

        $http.post(service, data).then(success, error);

        return deferred.promise;
    }

    function getPatient(id) {
        var deferred = $q.defer(),
            service = portalService + 'patient/' + id;

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        $http.get(service).then(success, error);

        return deferred.promise;
    }

    function remove (id) {
        var deferred = $q.defer(),
            service = portalService + 'patient/' + id;

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        $http.delete(service).then(success, error);

        return deferred.promise;
    }

    function getInsurances() {
        var deferred = $q.defer(),
            service = portalService + 'references/insurance/all/';

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        $http.get(service).then(success, error);

        return deferred.promise;
    }

    function createPatient(data) {
        var deferred = $q.defer(),
            service = portalService + 'patient';

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        $http.post(service, data).then(success, error);

        return deferred.promise;
    }

    function savePatient(data) {
        var deferred = $q.defer(),
            service = portalService + 'patient';

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        $http.put(service, data).then(success, error);

        return deferred.promise;
    }

    function getQdxInsurances() {
        var deferred = $q.defer(),
            service = portalService + 'references/insurance/mapping/QdxEligible';

        function success(res) {
            deferred.resolve(res.data);
        }

        function error(res) {
            deferred.reject(res);
        }

        $http.get(service).then(success, error);

        return deferred.promise;
    }

    return {
        query: query,
        get: get,
        getPatient: getPatient,
        remove: remove,
        getInsurances: getInsurances,
        getQdxInsurances: getQdxInsurances,
        create: createPatient,
        save: savePatient
    };
}]);